<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * FormFieldsFixture
 */
class FormFieldsFixture extends TestFixture
{
    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'form_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'name' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'utf8mb4_czech_ci', 'comment' => 'label/popis formulářového pole', 'precision' => null, 'fixed' => null],
        'description' => ['type' => 'text', 'length' => null, 'null' => true, 'default' => null, 'collate' => 'utf8mb4_czech_ci', 'comment' => 'nepovinný delší textový popis formulářového pole', 'precision' => null],
        'is_required' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '1', 'comment' => '', 'precision' => null],
        'form_field_type_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'is_required' => ['type' => 'index', 'columns' => ['is_required'], 'length' => []],
            'form_field_type_id' => ['type' => 'index', 'columns' => ['form_field_type_id'], 'length' => []],
            'form_id' => ['type' => 'index', 'columns' => ['form_id'], 'length' => []],
            'name' => ['type' => 'fulltext', 'columns' => ['name'], 'length' => []],
            'description' => ['type' => 'fulltext', 'columns' => ['description'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'form_fields_ibfk_1' => ['type' => 'foreign', 'columns' => ['form_field_type_id'], 'references' => ['form_field_types', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'form_fields_ibfk_2' => ['type' => 'foreign', 'columns' => ['form_id'], 'references' => ['forms', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8mb4_czech_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd
    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'id' => 1,
                'form_id' => 1,
                'name' => 'Lorem ipsum dolor sit amet',
                'description' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
                'is_required' => 1,
                'form_field_type_id' => 1,
            ],
        ];
        parent::init();
    }
}
