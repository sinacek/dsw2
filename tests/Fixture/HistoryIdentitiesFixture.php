<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * HistoryIdentitiesFixture
 */
class HistoryIdentitiesFixture extends TestFixture
{
    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'name' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'utf8mb4_czech_ci', 'comment' => 'název žadatele, pro evidenci a rozpoznání', 'precision' => null, 'fixed' => null],
        'first_name' => ['type' => 'string', 'length' => 50, 'null' => true, 'default' => null, 'collate' => 'utf8mb4_czech_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'last_name' => ['type' => 'string', 'length' => 50, 'null' => true, 'default' => null, 'collate' => 'utf8mb4_czech_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'date_of_birth' => ['type' => 'date', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'ico' => ['type' => 'string', 'length' => 9, 'null' => true, 'default' => null, 'collate' => 'utf8mb4_czech_ci', 'comment' => 'ičo', 'precision' => null, 'fixed' => null],
        'dic' => ['type' => 'string', 'length' => 13, 'null' => true, 'default' => null, 'collate' => 'utf8mb4_czech_ci', 'comment' => 'dič', 'precision' => null, 'fixed' => null],
        'databox_id' => ['type' => 'string', 'length' => 10, 'null' => true, 'default' => null, 'collate' => 'utf8mb4_czech_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'name' => ['type' => 'fulltext', 'columns' => ['name'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'ico' => ['type' => 'unique', 'columns' => ['ico'], 'length' => []],
            'dic' => ['type' => 'unique', 'columns' => ['dic'], 'length' => []],
            'databox_id' => ['type' => 'unique', 'columns' => ['databox_id'], 'length' => []],
            'fo_unique' => ['type' => 'unique', 'columns' => ['first_name', 'last_name', 'date_of_birth'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8mb4_czech_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd
    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'id' => 1,
                'name' => 'Lorem ipsum dolor sit amet',
                'first_name' => 'Lorem ipsum dolor sit amet',
                'last_name' => 'Lorem ipsum dolor sit amet',
                'date_of_birth' => '2020-04-28',
                'ico' => 'Lorem i',
                'dic' => 'Lorem ipsum',
                'databox_id' => 'Lorem ip',
                'modified' => '2020-04-28 00:27:12',
                'created' => '2020-04-28 00:27:12',
            ],
        ];
        parent::init();
    }
}
