<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * CsuCountriesFixture
 */
class CsuCountriesFixture extends TestFixture
{
    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'number' => ['type' => 'string', 'length' => 38, 'null' => false, 'default' => null, 'collate' => 'utf8mb4_czech_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'short_name' => ['type' => 'string', 'length' => 42, 'null' => false, 'default' => null, 'collate' => 'utf8mb4_czech_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'name' => ['type' => 'string', 'length' => 64, 'null' => false, 'default' => null, 'collate' => 'utf8mb4_czech_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        '_indexes' => [
            'short_name' => ['type' => 'fulltext', 'columns' => ['short_name'], 'length' => []],
            'name' => ['type' => 'fulltext', 'columns' => ['name'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'number' => ['type' => 'unique', 'columns' => ['number'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8mb4_czech_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd
    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'id' => 1,
                'number' => 'Lorem ipsum dolor sit amet',
                'short_name' => 'Lorem ipsum dolor sit amet',
                'name' => 'Lorem ipsum dolor sit amet',
            ],
        ];
        parent::init();
    }
}
