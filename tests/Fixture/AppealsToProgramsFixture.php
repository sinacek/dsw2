<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * AppealsToProgramsFixture
 */
class AppealsToProgramsFixture extends TestFixture
{
    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'appeal_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'program_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'appeal_id' => ['type' => 'index', 'columns' => ['appeal_id'], 'length' => []],
            'program_id' => ['type' => 'index', 'columns' => ['program_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'appeals_to_programs_ibfk_1' => ['type' => 'foreign', 'columns' => ['appeal_id'], 'references' => ['appeals', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'appeals_to_programs_ibfk_2' => ['type' => 'foreign', 'columns' => ['program_id'], 'references' => ['programs', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8mb4_czech_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd
    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'id' => 1,
                'appeal_id' => 1,
                'program_id' => 1,
                'modified' => '2020-04-10 14:40:04',
            ],
        ];
        parent::init();
    }
}
