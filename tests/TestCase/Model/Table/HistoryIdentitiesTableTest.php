<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\HistoryIdentitiesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\HistoryIdentitiesTable Test Case
 */
class HistoryIdentitiesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\HistoryIdentitiesTable
     */
    public $HistoryIdentities;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.HistoryIdentities',
        'app.Databoxes',
        'app.Histories',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('HistoryIdentities') ? [] : ['className' => HistoryIdentitiesTable::class];
        $this->HistoryIdentities = TableRegistry::getTableLocator()->get('HistoryIdentities', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->HistoryIdentities);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
