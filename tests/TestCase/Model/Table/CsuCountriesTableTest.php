<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CsuCountriesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CsuCountriesTable Test Case
 */
class CsuCountriesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\CsuCountriesTable
     */
    public $CsuCountries;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.CsuCountries',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('CsuCountries') ? [] : ['className' => CsuCountriesTable::class];
        $this->CsuCountries = TableRegistry::getTableLocator()->get('CsuCountries', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CsuCountries);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
