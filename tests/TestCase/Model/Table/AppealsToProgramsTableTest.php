<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AppealsToProgramsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AppealsToProgramsTable Test Case
 */
class AppealsToProgramsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\AppealsToProgramsTable
     */
    public $AppealsToPrograms;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.AppealsToPrograms',
        'app.Appeals',
        'app.Programs',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('AppealsToPrograms') ? [] : ['className' => AppealsToProgramsTable::class];
        $this->AppealsToPrograms = TableRegistry::getTableLocator()->get('AppealsToPrograms', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->AppealsToPrograms);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
