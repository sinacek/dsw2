<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RequestStatesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RequestStatesTable Test Case
 */
class RequestStatesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\RequestStatesTable
     */
    public $RequestStates;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.RequestStates',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('RequestStates') ? [] : ['className' => RequestStatesTable::class];
        $this->RequestStates = TableRegistry::getTableLocator()->get('RequestStates', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->RequestStates);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
