<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UsersToOlddswAccountsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UsersToOlddswAccountsTable Test Case
 */
class UsersToOlddswAccountsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\UsersToOlddswAccountsTable
     */
    public $UsersToOlddswAccounts;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.UsersToOlddswAccounts',
        'app.Users',
        'app.Ucty',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('UsersToOlddswAccounts') ? [] : ['className' => UsersToOlddswAccountsTable::class];
        $this->UsersToOlddswAccounts = TableRegistry::getTableLocator()->get('UsersToOlddswAccounts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UsersToOlddswAccounts);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
