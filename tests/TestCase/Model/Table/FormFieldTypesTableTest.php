<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FormFieldTypesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FormFieldTypesTable Test Case
 */
class FormFieldTypesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\FormFieldTypesTable
     */
    public $FormFieldTypes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.FormFieldTypes',
        'app.FormFields',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('FormFieldTypes') ? [] : ['className' => FormFieldTypesTable::class];
        $this->FormFieldTypes = TableRegistry::getTableLocator()->get('FormFieldTypes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->FormFieldTypes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
