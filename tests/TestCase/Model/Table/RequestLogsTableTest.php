<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RequestLogsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RequestLogsTable Test Case
 */
class RequestLogsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\RequestLogsTable
     */
    public $RequestLogs;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.RequestLogs',
        'app.Requests',
        'app.RequestStates',
        'app.Users',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('RequestLogs') ? [] : ['className' => RequestLogsTable::class];
        $this->RequestLogs = TableRegistry::getTableLocator()->get('RequestLogs', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->RequestLogs);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
