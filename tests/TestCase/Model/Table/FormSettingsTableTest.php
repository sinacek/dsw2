<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FormSettingsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FormSettingsTable Test Case
 */
class FormSettingsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\FormSettingsTable
     */
    public $FormSettings;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.FormSettings',
        'app.Forms',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('FormSettings') ? [] : ['className' => FormSettingsTable::class];
        $this->FormSettings = TableRegistry::getTableLocator()->get('FormSettings', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->FormSettings);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
