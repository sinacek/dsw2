<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RequestBudgetsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RequestBudgetsTable Test Case
 */
class RequestBudgetsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\RequestBudgetsTable
     */
    public $RequestBudgets;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.RequestBudgets',
        'app.Requests',
        'app.BudgetItems',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('RequestBudgets') ? [] : ['className' => RequestBudgetsTable::class];
        $this->RequestBudgets = TableRegistry::getTableLocator()->get('RequestBudgets', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->RequestBudgets);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
