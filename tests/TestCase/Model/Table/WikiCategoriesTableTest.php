<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\WikiCategoriesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\WikiCategoriesTable Test Case
 */
class WikiCategoriesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\WikiCategoriesTable
     */
    public $WikiCategories;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.WikiCategories',
        'app.Organizations',
        'app.Wikis',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('WikiCategories') ? [] : ['className' => WikiCategoriesTable::class];
        $this->WikiCategories = TableRegistry::getTableLocator()->get('WikiCategories', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->WikiCategories);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
