<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\WikisTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\WikisTable Test Case
 */
class WikisTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\WikisTable
     */
    public $Wikis;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Wikis',
        'app.WikiCategories',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Wikis') ? [] : ['className' => WikisTable::class];
        $this->Wikis = TableRegistry::getTableLocator()->get('Wikis', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Wikis);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
