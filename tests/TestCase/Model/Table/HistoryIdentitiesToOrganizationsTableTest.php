<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\HistoryIdentitiesToOrganizationsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\HistoryIdentitiesToOrganizationsTable Test Case
 */
class HistoryIdentitiesToOrganizationsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\HistoryIdentitiesToOrganizationsTable
     */
    public $HistoryIdentitiesToOrganizations;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.HistoryIdentitiesToOrganizations',
        'app.HistoryIdentities',
        'app.Organizations',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('HistoryIdentitiesToOrganizations') ? [] : ['className' => HistoryIdentitiesToOrganizationsTable::class];
        $this->HistoryIdentitiesToOrganizations = TableRegistry::getTableLocator()->get('HistoryIdentitiesToOrganizations', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->HistoryIdentitiesToOrganizations);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
