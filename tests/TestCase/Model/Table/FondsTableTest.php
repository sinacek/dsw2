<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FondsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FondsTable Test Case
 */
class FondsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\FondsTable
     */
    public $Fonds;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Fonds',
        'app.Organizations',
        'app.Realms',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Fonds') ? [] : ['className' => FondsTable::class];
        $this->Fonds = TableRegistry::getTableLocator()->get('Fonds', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Fonds);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
