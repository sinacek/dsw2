<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RequestFilledFieldsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RequestFilledFieldsTable Test Case
 */
class RequestFilledFieldsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\RequestFilledFieldsTable
     */
    public $RequestFilledFields;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.RequestFilledFields',
        'app.Requests',
        'app.Forms',
        'app.FormFields',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('RequestFilledFields') ? [] : ['className' => RequestFilledFieldsTable::class];
        $this->RequestFilledFields = TableRegistry::getTableLocator()->get('RequestFilledFields', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->RequestFilledFields);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
