<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CsuMunicipalitiesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CsuMunicipalitiesTable Test Case
 */
class CsuMunicipalitiesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\CsuMunicipalitiesTable
     */
    public $CsuMunicipalities;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.CsuMunicipalities',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('CsuMunicipalities') ? [] : ['className' => CsuMunicipalitiesTable::class];
        $this->CsuMunicipalities = TableRegistry::getTableLocator()->get('CsuMunicipalities', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CsuMunicipalities);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
