<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AppealsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AppealsTable Test Case
 */
class AppealsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\AppealsTable
     */
    public $Appeals;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Appeals',
        'app.Programs',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Appeals') ? [] : ['className' => AppealsTable::class];
        $this->Appeals = TableRegistry::getTableLocator()->get('Appeals', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Appeals);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
