<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CsuMunicipalitiesToCsuMunicipalityPartsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CsuMunicipalitiesToCsuMunicipalityPartsTable Test Case
 */
class CsuMunicipalitiesToCsuMunicipalityPartsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\CsuMunicipalitiesToCsuMunicipalityPartsTable
     */
    public $CsuMunicipalitiesToCsuMunicipalityParts;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.CsuMunicipalitiesToCsuMunicipalityParts',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('CsuMunicipalitiesToCsuMunicipalityParts') ? [] : ['className' => CsuMunicipalitiesToCsuMunicipalityPartsTable::class];
        $this->CsuMunicipalitiesToCsuMunicipalityParts = TableRegistry::getTableLocator()->get('CsuMunicipalitiesToCsuMunicipalityParts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CsuMunicipalitiesToCsuMunicipalityParts);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
