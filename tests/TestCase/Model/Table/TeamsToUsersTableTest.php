<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TeamsToUsersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TeamsToUsersTable Test Case
 */
class TeamsToUsersTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\TeamsToUsersTable
     */
    public $TeamsToUsers;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.TeamsToUsers',
        'app.Teams',
        'app.Users',
        'app.TeamRoles',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('TeamsToUsers') ? [] : ['className' => TeamsToUsersTable::class];
        $this->TeamsToUsers = TableRegistry::getTableLocator()->get('TeamsToUsers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TeamsToUsers);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
