<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CsuLegalFormsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CsuLegalFormsTable Test Case
 */
class CsuLegalFormsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\CsuLegalFormsTable
     */
    public $CsuLegalForms;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.CsuLegalForms',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('CsuLegalForms') ? [] : ['className' => CsuLegalFormsTable::class];
        $this->CsuLegalForms = TableRegistry::getTableLocator()->get('CsuLegalForms', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CsuLegalForms);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
