<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PublicIncomeSourcesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PublicIncomeSourcesTable Test Case
 */
class PublicIncomeSourcesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\PublicIncomeSourcesTable
     */
    public $PublicIncomeSources;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.PublicIncomeSources',
        'app.Organizations',
        'app.PublicIncomeHistories',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('PublicIncomeSources') ? [] : ['className' => PublicIncomeSourcesTable::class];
        $this->PublicIncomeSources = TableRegistry::getTableLocator()->get('PublicIncomeSources', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PublicIncomeSources);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
