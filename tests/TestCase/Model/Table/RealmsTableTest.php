<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RealmsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RealmsTable Test Case
 */
class RealmsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\RealmsTable
     */
    public $Realms;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Realms',
        'app.Fonds',
        'app.Programs',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Realms') ? [] : ['className' => RealmsTable::class];
        $this->Realms = TableRegistry::getTableLocator()->get('Realms', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Realms);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
