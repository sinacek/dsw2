<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FormTypesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FormTypesTable Test Case
 */
class FormTypesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\FormTypesTable
     */
    public $FormTypes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.FormTypes',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('FormTypes') ? [] : ['className' => FormTypesTable::class];
        $this->FormTypes = TableRegistry::getTableLocator()->get('FormTypes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->FormTypes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
