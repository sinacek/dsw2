<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

use Cake\Http\Middleware\CsrfProtectionMiddleware;
use Cake\Routing\Route\DashedRoute;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;

Router::defaultRouteClass(DashedRoute::class);

Router::scope('/', function (RouteBuilder $routes) {
    // CSRF
    $routes->registerMiddleware('csrf', new CsrfProtectionMiddleware([
        'httpOnly' => true,
        'secure' => true,
        'cookieName' => '__Host-dswCSRF',
    ]));

    $routes->applyMiddleware('csrf');

    $routes->connect('/', ['controller' => 'Public', 'action' => 'index']);
    $routes->connect('/favicon.ico', ['controller' => 'Public', 'action' => 'favicon']);
    $routes->connect('/explore/fonds', ['controller' => 'Public', 'action' => 'exploreFonds'], ['_name' => 'public_fonds']);
    $routes->connect('/explore/appeals', ['controller' => 'Public', 'action' => 'exploreAppeals'], ['_name' => 'public_appeals']);
    $routes->connect('/podpora', ['controller' => 'Public', 'action' => 'wiki'], ['_name' => 'public_wiki']);
    $routes->connect('/about', ['controller' => 'Public', 'action' => 'about']);
    $routes->connect('/gdpr', ['controller' => 'Public', 'action' => 'gdpr']);
    $routes->connect('/old-browser', ['controller' => 'Public', 'action' => 'oldBrowser']);
    $routes->connect('/admin/docs', ['controller' => 'Public', 'action' => 'adminDocs'], ['_name' => 'admin_docs']);

    $routes->connect('/login', ['controller' => 'Users', 'action' => 'login'], ['_name' => 'login']);
    $routes->connect('/user/logout', ['controller' => 'Users', 'action' => 'logout'], ['_name' => 'logout']);
    $routes->connect('/register', ['controller' => 'Users', 'action' => 'register'], ['_name' => 'register']);
    $routes->connect('/verify_email/:token/:requestId', ['controller' => 'Users', 'action' => 'verifyEmail'], ['_name' => 'verify_email']);
    $routes->connect('/invite_accept/:token/:requestId', ['controller' => 'Users', 'action' => 'inviteAccept'], ['_name' => 'invite_accept']);
    $routes->connect('/password_recovery', ['controller' => 'Users', 'action' => 'passwordRecovery'], ['_name' => 'password_recovery']);
    $routes->connect('/password_recovery_action/:token/:requestId', ['controller' => 'Users', 'action' => 'passwordRecoveryGo'], ['_name' => 'password_recovery_go']);

    $routes->connect('/user/change_password', ['controller' => 'Users', 'action' => 'changePassword'], ['_name' => 'change_password']);
    $routes->connect('/user/share_my_account', ['controller' => 'Users', 'action' => 'shareMyAccount'], ['_name' => 'share_my_account']);
    $routes->connect('/user/switch_to_allowed/:id', ['controller' => 'Users', 'action' => 'switchToAllowed'], ['pass' => ['id'], '_name' => 'switch_to_user']);
    $routes->connect('/user/update_info/public_income', ['controller' => 'Users', 'action' => 'publicIncomeHistories'], ['_name' => 'update_self_history']);
    $routes->connect('/user/update_info/public_income/:id/delete', ['controller' => 'Users', 'action' => 'incomeHistoryDelete'], ['pass' => ['id']]);
    $routes->connect('/user/update_info', ['controller' => 'Users', 'action' => 'changeInfo'], ['_name' => 'update_self_info']);
    $routes->connect('/user/update_info/ares/:ico', ['controller' => 'Users', 'action' => 'aresFetch', 'type' => 'bas'], ['_name' => 'ares', 'pass' => ['ico', 'type'], 'ico' => '[0-9]+']);
    $routes->connect('/user/update_info/ares/or/:ico', ['controller' => 'Users', 'action' => 'aresFetch', 'type' => 'or'], ['_name' => 'ares_or', 'pass' => ['ico', 'type'], 'ico' => '[0-9]+']);

    $routes->connect('/user/requests/old/:id', ['controller' => 'UserRequests', 'action' => 'dswZadost'], ['pass' => ['id'], 'id' => '[0-9]+']);
    $routes->connect('/user/requests/old/:id/ohlasit', ['controller' => 'UserRequests', 'action' => 'dswOhlasit'], ['pass' => ['id'], 'id' => '[0-9]+']);

    $routes->connect('/user/requests', ['controller' => 'UserRequests', 'action' => 'index'], ['_name' => 'user_requests']);
    $routes->connect('/user/requests/add', ['controller' => 'UserRequests', 'action' => 'addModify']);
    $routes->connect('/user/requests/:id', ['controller' => 'UserRequests', 'action' => 'requestDetail'], ['pass' => ['id'], 'id' => '[0-9]+', '_name' => 'request_detail']);
    $routes->connect('/user/requests/:id/pdf', ['controller' => 'UserRequests', 'action' => 'getPdf'], ['pass' => ['id'], 'id' => '[0-9]+']);
    $routes->connect('/user/requests/:id/submit/databox', ['controller' => 'UserRequests', 'action' => 'requestSubmitDatabox'], ['pass' => ['id'], 'id' => '[0-9]+']);
    $routes->connect('/user/requests/:id/submit/databox/result', ['controller' => 'UserRequests', 'action' => 'requestSubmitDataboxResult'], ['pass' => ['id'], 'id' => '[0-9]+']);
    $routes->connect('/user/requests/:id/delete', ['controller' => 'UserRequests', 'action' => 'requestDelete'], ['pass' => ['id'], 'id' => '[0-9]+']);
    $routes->connect('/user/requests/:id/budget', ['controller' => 'UserRequests', 'action' => 'requestBudget'], ['pass' => ['id'], 'id' => '[0-9]+', '_name' => 'request_budget']);
    $routes->connect('/user/requests/:id/settings', ['controller' => 'UserRequests', 'action' => 'addModify'], ['pass' => ['id'], 'id' => '[0-9]+', '_name' => 'request_settings']);
    $routes->connect('/user/requests/:id/fill_form/:form_id', ['controller' => 'UserRequests', 'action' => 'formFill'], ['pass' => ['id', 'form_id'], 'id' => '[0-9]+', 'form_id' => '[0-9]+']);
    $routes->connect('/user/requests/:id/reset_form/:form_id', ['controller' => 'UserRequests', 'action' => 'formFillReset'], ['pass' => ['id', 'form_id'], 'id' => '[0-9]+', 'form_id' => '[0-9]+']);
    $routes->connect('/user/requests/:id/upload', ['controller' => 'UserRequests', 'action' => 'attachmentAdd'], ['pass' => ['id'], 'id' => '[0-9]+']);
    $routes->connect('/user/requests/:id/attachment_delete/:file_id', ['controller' => 'UserRequests', 'action' => 'attachmentDelete'], ['pass' => ['id', 'file_id'], 'id' => '[0-9]+', 'file_id' => '[0-9]+']);
    $routes->connect('/user/requests/:id/attachment/:file_id/download', ['controller' => 'UserRequests', 'action' => 'fileDownload'], ['pass' => ['id', 'file_id'], 'id' => '[0-9]+', 'file_id' => '[0-9]+']);

    $routes->connect('/user/isds_auth', ['controller' => 'Isds', 'action' => 'authRedirect'], ['_name' => 'isds_auth']);
    $routes->connect('/user/isds_auth/:sessionId', ['controller' => 'Isds', 'action' => 'authCallback'], ['_name' => 'isds_auth_callback', 'pass' => ['sessionId']]);
    $routes->connect('/user/after_isds_auth', ['controller' => 'Isds', 'action' => 'storeVerifiedAttributes']);

    $routes->connect('/admin/organizations', ['controller' => 'Organizations', 'action' => 'index'], ['_name' => 'admin_organizations']);
    $routes->connect('/admin/organizations/create', ['controller' => 'Organizations', 'action' => 'create']);
    $routes->connect('/admin/organizations/edit', ['controller' => 'Organizations', 'action' => 'edit']);
    $routes->connect('/admin/organizations/edit/:id', ['controller' => 'Organizations', 'action' => 'edit'], ['pass' => ['id']]);
    $routes->connect('/admin/organizations/delete/:id', ['controller' => 'Organizations', 'action' => 'delete'], ['pass' => ['id']]);

    $routes->connect('/admin/organizations/:org_id/domain/add', ['controller' => 'Organizations', 'action' => 'domainAdd'], ['pass' => ['org_id']]);
    $routes->connect('/admin/organizations/:org_id/domain/delete/:id', ['controller' => 'Organizations', 'action' => 'domainDelete'], ['pass' => ['org_id', 'id']]);
    $routes->connect('/admin/organizations/:org_id/domain/trigger/:id', ['controller' => 'Organizations', 'action' => 'domainTrigger'], ['pass' => ['org_id', 'id']]);

    $routes->connect('/admin/users', ['controller' => 'UsersManager', 'action' => 'index'], ['_name' => 'admin_users']);
    $routes->connect('/admin/users/invite', ['controller' => 'UsersManager', 'action' => 'invite'], ['_name' => 'admin_users_invite']);
    $routes->connect('/admin/users/:id/edit', ['controller' => 'UsersManager', 'action' => 'edit'], ['pass' => ['id']]);
    $routes->connect('/admin/users/:id/old_link', ['controller' => 'UsersManager', 'action' => 'oldLink'], ['pass' => ['id']]);
    $routes->connect('/admin/users/:id/role_delete/:role_id', ['controller' => 'UsersManager', 'action' => 'roleDelete'], ['pass' => ['id', 'role_id']]);
    $routes->connect('/admin/users/:id/role_add', ['controller' => 'UsersManager', 'action' => 'roleAdd'], ['pass' => ['id']]);
    $routes->connect('/admin/users/:id/login', ['controller' => 'UsersManager', 'action' => 'loginAs'], ['pass' => ['id']]);

    $routes->connect('/admin/teams', ['controller' => 'TeamsManager', 'action' => 'index'], ['_name' => 'admin_teams']);
    $routes->connect('/admin/teams/add', ['controller' => 'TeamsManager', 'action' => 'addModify']);
    $routes->connect('/admin/teams/:id', ['controller' => 'TeamsManager', 'action' => 'addModify'], ['pass' => ['id']]);
    $routes->connect('/admin/teams/:id/delete', ['controller' => 'TeamsManager', 'action' => 'teamDelete'], ['pass' => ['id']]);
    $routes->connect('/admin/teams/:id/copy', ['controller' => 'TeamsManager', 'action' => 'teamCopy'], ['pass' => ['id']]);

    $routes->connect('/csu/countries', ['controller' => 'Csu', 'action' => 'countries'], ['_name' => 'csu_countries']);
    $routes->connect('/csu/countries.json', ['controller' => 'Csu', 'action' => 'countries', 'json'], ['_name' => 'csu_countries_json']);
    $routes->connect('/csu/legal_forms', ['controller' => 'Csu', 'action' => 'legalForms'], ['_name' => 'csu_legal_forms']);
    $routes->connect('/csu/legal_forms.json', ['controller' => 'Csu', 'action' => 'legalForms', 'json'], ['_name' => 'csu_legal_forms_json']);
    $routes->connect('/csu/municipalities', ['controller' => 'Csu', 'action' => 'municipalities'], ['_name' => 'csu_municipalities']);
    $routes->connect('/csu/municipalities.json', ['controller' => 'Csu', 'action' => 'municipalities', 'json'], ['_name' => 'csu_municipalities_json']);
    $routes->connect('/csu/municipality_parts', ['controller' => 'Csu', 'action' => 'municipalityParts'], ['_name' => 'csu_municipality_parts']);
    $routes->connect('/csu/municipality_parts.json', ['controller' => 'Csu', 'action' => 'municipalityParts', 'json'], ['_name' => 'csu_municipality_parts_json']);
    $routes->connect('/csu/municipality_parts/list.json', ['controller' => 'Csu', 'action' => 'municipalityPartsList'], ['_name' => 'csu_municipality_parts_list_json']);

    $routes->connect('/admin/grants/fonds', ['controller' => 'GrantsManager', 'action' => 'indexFonds'], ['_name' => 'admin_fonds']);
    $routes->connect('/admin/grants/fonds/add', ['controller' => 'GrantsManager', 'action' => 'fondAddModify']);
    $routes->connect('/admin/grants/fonds/:id', ['controller' => 'GrantsManager', 'action' => 'fondAddModify'], ['pass' => ['id']]);
    $routes->connect('/admin/grants/fonds/:id/copy', ['controller' => 'GrantsManager', 'action' => 'fondCopy'], ['pass' => ['id']]);
    $routes->connect('/admin/grants/fonds/:id/toggle', ['controller' => 'GrantsManager', 'action' => 'fondToggle'], ['pass' => ['id']]);
    $routes->connect('/admin/grants/fonds/:id/delete', ['controller' => 'GrantsManager', 'action' => 'fondDelete'], ['pass' => ['id']]);

    $routes->connect('/admin/grants/realms', ['controller' => 'GrantsManager', 'action' => 'indexRealms'], ['_name' => 'admin_realms']);
    // adding realm to fond
    $routes->connect('/admin/grants/realms/add/for_fond/:fond_id', ['controller' => 'GrantsManager', 'action' => 'realmAddModify']);
    $routes->connect('/admin/grants/realms/add', ['controller' => 'GrantsManager', 'action' => 'realmAddModify']);
    $routes->connect('/admin/grants/realms/:id', ['controller' => 'GrantsManager', 'action' => 'realmAddModify'], ['pass' => ['id']]);
    $routes->connect('/admin/grants/realms/:id/toggle', ['controller' => 'GrantsManager', 'action' => 'realmToggle'], ['pass' => ['id']]);
    $routes->connect('/admin/grants/realms/:id/delete', ['controller' => 'GrantsManager', 'action' => 'realmDelete'], ['pass' => ['id']]);

    $routes->connect('/admin/grants/programs', ['controller' => 'GrantsManager', 'action' => 'indexPrograms'], ['_name' => 'admin_programs']);
    // adding top-level program to realm
    $routes->connect('/admin/grants/programs/add/for_realm/:realm_id', ['controller' => 'GrantsManager', 'action' => 'programAddModify']);
    // adding child program to parent
    $routes->connect('/admin/grants/programs/add/for_program/:parent_id', ['controller' => 'GrantsManager', 'action' => 'programAddModify']);
    $routes->connect('/admin/grants/programs/add', ['controller' => 'GrantsManager', 'action' => 'programAddModify']);
    $routes->connect('/admin/grants/programs/:id', ['controller' => 'GrantsManager', 'action' => 'programAddModify'], ['pass' => ['id']]);
    $routes->connect('/admin/grants/programs/:id/toggle', ['controller' => 'GrantsManager', 'action' => 'programToggle'], ['pass' => ['id']]);
    $routes->connect('/admin/grants/programs/:id/delete', ['controller' => 'GrantsManager', 'action' => 'programDelete'], ['pass' => ['id']]);

    $routes->connect('/admin/grants/appeals', ['controller' => 'GrantsManager', 'action' => 'indexAppeals'], ['_name' => 'admin_appeals']);
    $routes->connect('/admin/grants/appeals/add', ['controller' => 'GrantsManager', 'action' => 'appealAddModify']);
    $routes->connect('/admin/grants/appeals/:id', ['controller' => 'GrantsManager', 'action' => 'appealAddModify'], ['pass' => ['id']]);
    $routes->connect('/admin/grants/appeals/:id/details', ['controller' => 'GrantsManager', 'action' => 'appealDetailSettings'], ['pass' => ['id']]);
    $routes->connect('/admin/grants/appeals/:id/toggle', ['controller' => 'GrantsManager', 'action' => 'appealToggle'], ['pass' => ['id']]);
    $routes->connect('/admin/grants/appeals/:id/delete', ['controller' => 'GrantsManager', 'action' => 'appealDelete'], ['pass' => ['id']]);

    $routes->connect('/admin/grants/evaluation_criteria', ['controller' => 'EvaluationCriteria', 'action' => 'index'], ['_name' => 'admin_evaluation_criteria']);
    $routes->connect('/admin/grants/evaluation_criteria/add/for/:parent_id', ['controller' => 'EvaluationCriteria', 'action' => 'criteriumAddModify']);
    $routes->connect('/admin/grants/evaluation_criteria/add', ['controller' => 'EvaluationCriteria', 'action' => 'criteriumAddModify']);
    $routes->connect('/admin/grants/evaluation_criteria/:id', ['controller' => 'EvaluationCriteria', 'action' => 'criteriumAddModify'], ['pass' => ['id']]);
    $routes->connect('/admin/grants/evaluation_criteria/:id/delete', ['controller' => 'EvaluationCriteria', 'action' => 'criteriumDelete'], ['pass' => ['id']]);
    $routes->connect('/admin/grants/evaluation_criteria/:id/copy', ['controller' => 'EvaluationCriteria', 'action' => 'criteriumCopy'], ['pass' => ['id']]);

    $routes->connect('/admin/my_teams', ['controller' => 'Teams', 'action' => 'index'], ['_name' => 'my_teams']);
    $routes->connect('/admin/my_teams/request_attachment/:request_id/:file_id', ['controller' => 'Teams', 'action' => 'downloadAttachment'], ['pass' => ['request_id', 'file_id'], 'file_id' => '\d+', 'request_id' => '\d+']);
    $routes->connect('/admin/my_teams/request_detail/:id', ['controller' => 'Teams', 'action' => 'requestDetailReadOnly'], ['_name' => 'my_teams_request_detail', 'pass' => ['id']]);
    $routes->connect('/admin/my_teams/formal_control', ['controller' => 'Teams', 'action' => 'indexFormalControl'], ['_name' => 'team_formal_control_index']);
    $routes->connect('/admin/my_teams/formal_control/manual_submit/:id', ['controller' => 'Teams', 'action' => 'formalControlManualSubmit'], ['_name' => 'formal_control_manual_submit', 'pass' => ['id']]);
    $routes->connect('/admin/my_teams/formal_control/review/:id', ['controller' => 'Teams', 'action' => 'formalControlCheckRequest'], ['_name' => 'formal_control_check_request', 'pass' => ['id']]);
    $routes->connect('/admin/my_teams/formal_control/send_email/:id', ['controller' => 'Teams', 'action' => 'formalControlSendEmail'], ['_name' => 'formal_control_send_notice', 'pass' => ['id']]);
    $routes->connect('/admin/my_teams/comments', ['controller' => 'Teams', 'action' => 'indexComments'], ['_name' => 'team_evaluators_index']);
    $routes->connect('/admin/my_teams/comments/evaluate/:id', ['controller' => 'Teams', 'action' => 'commentsEvaluate'], ['_name' => 'comments_evaluate', 'pass' => ['id']]);
    $routes->connect('/admin/my_teams/proposals', ['controller' => 'Teams', 'action' => 'indexProposals'], ['_name' => 'team_proposals_index']);
    $routes->connect('/admin/my_teams/proposals/:id', ['controller' => 'Teams', 'action' => 'proposalsEdit'], ['_name' => 'proposals_edit', 'pass' => ['id']]);
    $routes->connect('/admin/my_teams/approvals', ['controller' => 'Teams', 'action' => 'indexApprovals'], ['_name' => 'team_approvals_index']);
    $routes->connect('/admin/my_teams/approvals/:id', ['controller' => 'Teams', 'action' => 'approvalsEdit'], ['_name' => 'approvals_edit', 'pass' => ['id']]);
    $routes->connect('/admin/my_teams/managers', ['controller' => 'Teams', 'action' => 'indexManagers'], ['_name' => 'team_managers_index']);
    $routes->connect('/admin/my_teams/managers/:id', ['controller' => 'Teams', 'action' => 'managersEdit'], ['_name' => 'managers_edit', 'pass' => ['id']]);

    $routes->connect('/admin/economics/by_appeal/:appeal_id/by_current_status/:state_id', ['controller' => 'Economics', 'action' => 'index'], ['_name' => 'economics_by_appeal_and_state', 'pass' => ['appeal_id', 'state_id']]);
    $routes->connect('/admin/economics/by_appeal/:appeal_id', ['controller' => 'Economics', 'action' => 'index'], ['_name' => 'economics_by_appeal', 'pass' => ['appeal_id']]);
    $routes->connect('/admin/economics/by_current_status/:state_id', ['controller' => 'Economics', 'action' => 'index'], ['_name' => 'economics_by_request_state', 'pass' => ['state_id']]);
    $routes->connect('/admin/economics', ['controller' => 'Economics', 'action' => 'index'], ['_name' => 'my_economics']);

    $routes->connect('/admin/translations', ['controller' => 'Translations', 'action' => 'index'], ['_name' => 'admin_translations']);
    //$routes->connect('/admin/translations/add', ['controller' => 'Translations', 'action' => 'addModify']);
    $routes->connect('/admin/translations/:id', ['controller' => 'Translations', 'action' => 'addModify'], ['pass' => ['id']]);

    $routes->connect('/admin/forms', ['controller' => 'Forms', 'action' => 'index'], ['_name' => 'admin_forms']);
    $routes->connect('/admin/forms/add', ['controller' => 'Forms', 'action' => 'addModify']);
    $routes->connect('/admin/forms/:id', ['controller' => 'Forms', 'action' => 'addModify'], ['pass' => ['id']]);
    $routes->connect('/admin/forms/:id/copy', ['controller' => 'Forms', 'action' => 'formCopy'], ['pass' => ['id']]);
    $routes->connect('/admin/forms/:id/preview', ['controller' => 'Forms', 'action' => 'formPreview'], ['pass' => ['id']]);
    $routes->connect('/admin/forms/:id/delete', ['controller' => 'Forms', 'action' => 'formDelete'], ['pass' => ['id']]);
    $routes->connect('/admin/forms/:id/detail', ['controller' => 'Forms', 'action' => 'formDetail'], ['pass' => ['id']]);
    $routes->connect('/admin/forms/:id/fields/:field_id', ['controller' => 'Forms', 'action' => 'fieldAddModify'], ['pass' => ['id', 'field_id'], 'field_id' => '\d+']);
    $routes->connect('/admin/forms/:id/fields/add', ['controller' => 'Forms', 'action' => 'fieldAddModify'], ['pass' => ['id', 0]]);
    $routes->connect('/admin/forms/:id/fields/:field_id/delete', ['controller' => 'Forms', 'action' => 'fieldDelete'], ['pass' => ['id', 'field_id']]);

    $routes->connect('/admin/histories', ['controller' => 'History', 'action' => 'index'], ['_name' => 'admin_history_identities']);
    $routes->connect('/admin/histories/search', ['controller' => 'History', 'action' => 'index'], ['_name' => 'admin_history_search']);
    $routes->connect('/admin/histories/identity/upload', ['controller' => 'History', 'action' => 'uploadIdentities']);
    $routes->connect('/admin/histories/identity/:id/edit', ['controller' => 'History', 'action' => 'addModifyIdentity'], ['pass' => ['id']]);
    $routes->connect('/admin/histories/identity/add', ['controller' => 'History', 'action' => 'addModifyIdentity']);
    $routes->connect('/admin/histories/identity/:id', ['controller' => 'History', 'action' => 'detail'], ['pass' => ['id']]);
    $routes->connect('/admin/histories/identity/:identity_id/record/:id', ['controller' => 'History', 'action' => 'addModifyHistory'], ['pass' => ['identity_id', 'id']]);
    $routes->connect('/admin/histories/identity/:identity_id/record/:id/delete', ['controller' => 'History', 'action' => 'deleteHistory'], ['pass' => ['identity_id', 'id']]);
    $routes->connect('/admin/histories/identity/:identity_id/record/add', ['controller' => 'History', 'action' => 'addModifyHistory'], ['pass' => ['identity_id']]);

    // only useful when OldDsw plugin loaded
    $routes->connect('/admin/histories/dsw', ['controller' => 'History', 'action' => 'dsw'], ['_name' => 'admin_history_dsw']);
    $routes->connect('/admin/histories/dsw/zadosti', ['controller' => 'History', 'action' => 'dswZadosti'], ['_name' => 'admin_history_dsw_zadosti']);
    $routes->connect('/admin/histories/dsw/:id', ['controller' => 'History', 'action' => 'dswDetail'], ['pass' => ['id']]);
    $routes->connect('/admin/histories/dsw/:id/hide', ['controller' => 'History', 'action' => 'dswHideAccount'], ['pass' => ['id']]);
    $routes->connect('/admin/histories/dsw/zadost/:id', ['controller' => 'History', 'action' => 'dswZadost'], ['pass' => ['id']]);
    $routes->connect('/admin/histories/dsw/zadost/:id/hide', ['controller' => 'History', 'action' => 'dswHideZadost'], ['pass' => ['id']]);
    $routes->connect('/admin/histories/dsw/download/:id', ['controller' => 'History', 'action' => 'dswDownload'], ['pass' => ['id']]);

    $routes->connect('/admin/wiki', ['controller' => 'WikiManager', 'action' => 'index'], ['_name' => 'admin_wiki']);
    $routes->connect('/admin/wiki/category/:id', ['controller' => 'WikiManager', 'action' => 'categoryAddModify'], ['pass' => ['id']]);
    $routes->connect('/admin/wiki/category/:id/delete', ['controller' => 'WikiManager', 'action' => 'categoryDelete'], ['pass' => ['id']]);
    $routes->connect('/admin/wiki/category/add', ['controller' => 'WikiManager', 'action' => 'categoryAddModify']);

    $routes->connect('/admin/wiki/:id', ['controller' => 'WikiManager', 'action' => 'addModify'], ['pass' => ['id']]);
    $routes->connect('/admin/wiki/:id/delete', ['controller' => 'WikiManager', 'action' => 'delete'], ['pass' => ['id']]);
    $routes->connect('/admin/wiki/add', ['controller' => 'WikiManager', 'action' => 'addModify']);

    $routes->connect('/sitemap_index.xml', ['controller' => 'Public', 'action' => 'sitemap']);
    $routes->connect('/sitemap.xml', ['controller' => 'Public', 'action' => 'sitemap']);
    $routes->redirect('/sitemap.xml.gz', '/sitemap.xml');
    $routes->connect('/atom.xml', ['controller' => 'Public', 'action' => 'rss']);

    $routes->redirect('/prihlaseni', '/login');
    $routes->redirect('/admin', '/login');
    $routes->redirect('/admin/sign/in', '/login');
    $routes->redirect('/login.html', '/login');
    $routes->redirect('/registration', '/register');
    $routes->redirect('/registration/new', '/register');
    $routes->redirect('/registration/obcan', '/register');
    $routes->redirect('/registration/pravnicka-osoba', '/register');
    $routes->redirect('/registration/zapomenute-heslo', '/password_recovery');
    $routes->redirect('/registration/reset-hesla', '/password_recovery');
    $routes->redirect('/www/podpora', '/podpora');

    $routes->redirect('/www/ticket', '/rickroll');
    $routes->redirect('/UploadTest.aspx', '/rickroll');
    $routes->redirect('/Telerik{rest}', '/rickroll');
    $routes->redirect('/=/**', '/rickroll');
    $routes->redirect('/s=/**', '/rickroll');
    $routes->redirect('/log', '/rickroll');
    $routes->redirect('/log/*', '/rickroll');
    $routes->redirect('/app/config/key.pem', '/rickroll');
    $routes->redirect('/app/config/key.pem.', '/rickroll');
    $routes->redirect('/solr/*', '/rickroll');
    $routes->redirect('/api/*', '/rickroll');
    $routes->redirect('/HNAP1', '/rickroll');
    $routes->redirect('/rickroll', 'https://www.youtube.com/watch?v=dQw4w9WgXcQ');
});
