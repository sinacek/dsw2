<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.8
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

/*
 * Configure paths required to find CakePHP + general filepath constants
 */
require __DIR__ . '/paths.php';

/*
 * Bootstrap CakePHP.
 *
 * Does the various bits of setup that CakePHP needs to do.
 * This includes:
 *
 * - Registering the CakePHP autoloader.
 * - Setting the default application paths.
 */
require CORE_PATH . 'config' . DS . 'bootstrap.php';

use ADmad\I18n\I18n\DbMessagesLoader;
use App\ORM\Database\Type\AppDateTimeType;
use Cake\Cache\Cache;
use Cake\Console\ConsoleErrorHandler;
use Cake\Core\Configure;
use Cake\Core\Configure\Engine\PhpConfig;
use Cake\Database\Type;
use Cake\Datasource\ConnectionManager;
use ErrorEmail\Error\ErrorHandler;
use Cake\Http\ServerRequest;
use Cake\I18n\I18n;
use Cake\Log\Log;
use Cake\Utility\Security;
use Detection\MobileDetect;

/*
 * Read configuration file and inject configuration into various
 * CakePHP classes.
 *
 * By default there is only one configuration file. It is often a good
 * idea to create multiple configuration files, and separate the configuration
 * that changes from configuration that does not. This makes deployment simpler.
 */
try {
    Configure::config('default', new PhpConfig());
    Configure::load('app', 'default', false);
} catch (Exception $e) {
    exit($e->getMessage() . "\n");
}

if (file_exists(CONFIG . 'app_local.php')) {
    Configure::load('app_local', 'default');
}

/*
 * When debug = true the metadata cache should only last
 * for a short time.
 */
if (Configure::read('debug')) {
    Configure::write('Cache._cake_model_.duration', '+2 minutes');
    Configure::write('Cache._cake_core_.duration', '+2 minutes');
    // disable router cache during development
    Configure::write('Cache._cake_routes_.duration', '+2 seconds');
}

/*
 * Set the default server timezone. Using UTC makes time calculations / conversions easier.
 * Check http://php.net/manual/en/timezones.php for list of valid timezone strings.
 */
if (!date_default_timezone_set(Configure::read('App.defaultTimezone'))) {
    throw new InvalidArgumentException('Timezone is not valid');
}

/*
 * Configure the mbstring extension to use the correct encoding.
 */
mb_internal_encoding(Configure::read('App.encoding'));

/*
 * Set the default locale. This controls how dates, number and currency is
 * formatted and sets the default language to use for translations.
 */
ini_set('intl.default_locale', Configure::read('App.defaultLocale'));
/*
 * Register application error and exception handlers.
 */
$isCli = PHP_SAPI === 'cli';
if ($isCli) {
    (new ConsoleErrorHandler(Configure::read('Error')))->register();
} else {
    (new ErrorHandler(Configure::read('Error')))->register();
}

/*
 * Include the CLI bootstrap overrides.
 */
if ($isCli) {
    require __DIR__ . '/bootstrap_cli.php';
}

/*
 * Set the full base URL.
 * This URL is used as the base of all absolute links.
 *
 * If you define fullBaseUrl in your config file you can remove this.
 */
if (!Configure::read('App.fullBaseUrl')) {
    $s = null;
    if (env('HTTPS')) {
        $s = 's';
    }

    $httpHost = env('HTTP_HOST');
    if (isset($httpHost)) {
        Configure::write('App.fullBaseUrl', 'http' . $s . '://' . $httpHost);
    }
    unset($httpHost, $s);
}

Cache::setConfig(Configure::consume('Cache'));
ConnectionManager::setConfig(Configure::consume('Datasources'));
Log::setConfig(Configure::consume('Log'));
Security::setSalt(Configure::consume('Security.salt'));

Configure::write('I18n.languages', ['cs']);
I18n::config('default', function ($domain, $locale) {
    return new DbMessagesLoader(
        $domain,
        $locale
    );
});
I18n::config('email', function ($domain, $locale) {
    return new DbMessagesLoader(
        $domain,
        $locale
    );
});
I18n::config('cake', function ($domain, $locale) {
    return new DbMessagesLoader(
        $domain,
        $locale
    );
});
I18n::setLocale('cs');

/*
 * The default crypto extension in 3.0 is OpenSSL.
 * If you are migrating from 2.x uncomment this code to
 * use a more compatible Mcrypt based implementation
 */
//Security::engine(new \Cake\Utility\Crypto\Mcrypt());

/*
 * Setup detectors for mobile and tablet.
 */
ServerRequest::addDetector('mobile', function ($request) {
    $detector = new MobileDetect();

    return $detector->isMobile();
});
ServerRequest::addDetector('tablet', function ($request) {
    $detector = new MobileDetect();

    return $detector->isTablet();
});

/*
 * Enable immutable time objects in the ORM.
 *
 * You can enable default locale format parsing by adding calls
 * to `useLocaleParser()`. This enables the automatic conversion of
 * locale specific date formats. For details see
 * @link https://book.cakephp.org/3.0/en/core-libraries/internationalization-and-localization.html#parsing-localized-datetime-data
 */
Type::build('time')
    ->useImmutable();
Type::build('date')
    ->useImmutable();
Type::set('datetime', new AppDateTimeType());
Type::build('datetime')
    ->useImmutable();
Type::build('timestamp')
    ->useImmutable();

if (!function_exists('random_str')) {
    /**
     * Generate and return a random characters string
     *
     * Useful for generating passwords or hashes.
     *
     * The default string returned is 8 alphanumeric characters string.
     *
     * The type of string returned can be changed with the "type" parameter.
     * Seven types are - by default - available: basic, alpha, alphanum, num, nozero, unique and md5.
     *
     * @param string $type Type of random string.  basic, alpha, alphanum, num, nozero, unique and md5.
     * @param int $length Length of the string to be generated, Default: 8 characters long.
     * @return  string
     */
    function random_str($type = 'alphanum', $length = 8)
    {
        switch ($type) {
            case 'basic':
                return mt_rand();
                break;
            default:
            case 'alpha':
            case 'alphanum':
            case 'num':
            case 'nozero':
                $seedings = [];
                $seedings['alpha'] = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $seedings['alphanum'] = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $seedings['num'] = '0123456789';
                $seedings['nozero'] = '123456789';

                $pool = $seedings[$type];

                $str = '';
                for ($i = 0; $i < $length; $i++) {
                    $str .= substr($pool, mt_rand(0, strlen($pool) - 1), 1);
                }

                return $str;
                break;
            case 'unique':
            case 'md5':
                return md5(uniqid(mt_rand()));
                break;
        }
    }
}

if (!function_exists('isValidBankAccount')) {
    // https://www.kutac.cz/pocitace-a-internety/jak-poznat-spravne-cislo-uctu
    function isValidBankAccount($accNumber)
    {
        $matches = [];
        if (!preg_match('/^(?:([0-9]{1,6})-)?([0-9]{2,10})\/([0-9]{4})$/', $accNumber, $matches)) {
            return false;
        }
        $weights = [6, 3, 7, 9, 10, 5, 8, 4, 2, 1];
        $prefix = str_pad($matches[1], 10, '0', STR_PAD_LEFT);
        $main = str_pad($matches[2], 10, '0', STR_PAD_LEFT);

        // Check prefix
        $checkSum = 0;
        for ($i = 0; $i < strlen($prefix); $i++) {
            $checkSum += $weights[$i] * (int)$prefix[$i];
        }
        if ($checkSum % 11 !== 0) {
            return false;
        }

        // Check main part
        $checkSum = 0;
        for ($i = 0; $i < strlen($main); $i++) {
            $checkSum += $weights[$i] * (int)$main[$i];
        }
        if ($checkSum % 11 !== 0) {
            return false;
        }

        return true;
    }
}

if (!function_exists('convertPHPSizeToBytes')) {
    /**
     * This function transforms the php.ini notation for numbers (like '2M') to an integer (2*1024*1024 in this case)
     *
     * @param string $sSize
     * @return integer The value in bytes
     * @noinspection PhpMissingBreakStatementInspection
     */
    function convertPHPSizeToBytes($sSize)
    {
        //
        $sSuffix = strtoupper(substr($sSize, -1));
        if (!in_array($sSuffix, ['P', 'T', 'G', 'M', 'K'])) {
            return (int)$sSize;
        }
        $iValue = substr($sSize, 0, -1);
        switch ($sSuffix) {
            case 'P':
                $iValue *= 1024;
            // Fallthrough intended
            case 'T':
                $iValue *= 1024;
            // Fallthrough intended
            case 'G':
                $iValue *= 1024;
            // Fallthrough intended
            case 'M':
                $iValue *= 1024;
            // Fallthrough intended
            case 'K':
                $iValue *= 1024;
                break;
        }

        return (int)$iValue;
    }
}

if (!function_exists('getMaximumFileUploadSize')) {
    /**
     * This function returns the maximum files size that can be uploaded
     * in PHP
     * @returns int File size in bytes
     **/
    function getMaximumFileUploadSize()
    {
        return min(convertPHPSizeToBytes(ini_get('post_max_size')), convertPHPSizeToBytes(ini_get('upload_max_filesize')));
    }
}

if (!function_exists("startsWith")) {
    function startsWith($haystack, $needle)
    {
        $length = mb_strlen($needle);

        return (mb_substr($haystack, 0, $length) === $needle);
    }
}

if (!function_exists("endsWith")) {
    function endsWith($haystack, $needle)
    {
        $length = mb_strlen($needle);
        if ($length == 0) {
            return true;
        }

        return (mb_substr($haystack, -$length) === $needle);
    }
}

if (!function_exists('autoUTF')) {
    // from https://phpfashion.com/autoczech-aneb-automaticka-detekce-kodovani
    // convert to UTF-8 by dgx
    function autoUTF($s)
    {
        // detect UTF-8
        if (preg_match('#[\x80-\x{1FF}\x{2000}-\x{3FFF}]#u', $s)) {
            return $s;
        }

        // detect WINDOWS-1250
        if (preg_match('#[\x7F-\x9F\xBC]#', $s)) {
            return iconv('WINDOWS-1250', 'UTF-8', $s);
        }

        // assume ISO-8859-2
        return iconv('ISO-8859-2', 'UTF-8', $s);
    }
}
