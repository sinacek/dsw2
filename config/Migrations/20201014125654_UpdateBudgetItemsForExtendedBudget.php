<?php

use Migrations\AbstractMigration;

class UpdateBudgetItemsForExtendedBudget extends AbstractMigration
{
    public function change()
    {
        $budgetItems = $this->table('budget_items');
        $budgetItems->addColumn('original_amount', 'decimal', [
            'precision' => 18,
            'scale' => 2,
            'default' => 0,
            'null' => false,
            'after' => 'amount'
        ]);
        $budgetItems->addColumn('comment', 'string', [
            'after' => 'description',
            'null' => true,
            'default' => null,
            'limit' => 255
        ]);
        $budgetItems->update();
    }
}
