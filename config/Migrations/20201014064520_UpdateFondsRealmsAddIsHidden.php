<?php

use Migrations\AbstractMigration;

class UpdateFondsRealmsAddIsHidden extends AbstractMigration
{
    public function change()
    {
        $fonds = $this->table('fonds');
        $fonds->addColumn('is_hidden', 'boolean', [
            'default' => false,
            'null' => false,
            'after' => 'is_enabled',
        ]);
        $fonds->update();

        $realms = $this->table('realms');
        $realms->addColumn('is_hidden', 'boolean', [
            'default' => false,
            'null' => false,
            'after' => 'is_enabled',
        ]);
        $realms->update();
    }
}
