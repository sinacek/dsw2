<?php

use Cake\Database\Connection;
use Cake\Database\Driver\Mysql;

return [
    // pokud při práci s aplikací dostáváte nespecifický 500 Server Error, změnit na true a vyřešit problém
    // pro produkční nasazení vždy "false", důležité!
    'debug' => filter_var(env('DEBUG', true), FILTER_VALIDATE_BOOLEAN),

    'Security' => [
        // typicky 64 znaků
        'salt' => env('SECURITY_SALT', '__SALT__'),
    ],

    'Datasources' => [
        'default' => [
            'className' => Connection::class,
            'driver' => Mysql::class,
            'username' => env('DATABASE_USER', null),
            'password' => env('DATABASE_PASSWORD', null),
            'database' => env('DATABASE_DATABASE', null),
            'url' => env('DATABASE_URL', null),
            'host' => env('DATABASE_HOST', 'localhost'),
            'timezone' => env('DATABASE_TZ', 'Europe/Prague'),
        ],
    ]
];
