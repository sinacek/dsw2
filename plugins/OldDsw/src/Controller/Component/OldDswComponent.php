<?php

namespace OldDsw\Controller\Component;

use Cake\Controller\Component;
use OldDsw\Model\Entity\Ucet;
use OldDsw\Model\Entity\Uzivatel;
use OldDsw\Model\Entity\Zadost;
use OldDsw\Model\Table\UctyTable;
use OldDsw\Model\Table\UzivateleTable;
use OldDsw\Model\Table\ZadostiTable;

/**
 * @property UctyTable $Ucty
 * @property ZadostiTable $Zadosti
 * @property UzivateleTable $Uzivatele
 */
class OldDswComponent extends Component
{

    const ZADOSTI_CONTAIN = [
        'Fond',
        'Program',
        'Ucel',
        'Ucet',
        'Ucet.FyzickeOsoby',
        'Ucet.StatutarniOrgan',
        'DefiniceProjektu',
        'DefiniceProjektu.Terminy',
        'RozpoctyProjektu',
        'RozpoctyProjektu.RozpoctyProjektuVlastniZdroje',
        'RozpoctyProjektu.RozpoctyProjektuJineZdroje',
        'RozpoctyProjektu.RozpoctyProjektuVynosy',
        'RozpoctyProjektu.RozpoctyProjektuNaklady',
        'RozpoctyProjektu.RozpoctyProjektuNakladyInvesticni',
        'OhlasovaciFormular',
        'OhlasovaciFormular.Prilohy',
        'PrilohyZadosti',
        'PrilohyFormulareProhlaseniOVerejnePodpore',
    ];

    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->getController()->loadModel('OldDsw.Ucty');
        $this->getController()->loadModel('OldDsw.Zadosti');
        $this->getController()->loadModel('OldDsw.Uzivatele');
    }

    public function __get($name)
    {
        if ($name === 'Ucty') {
            return $this->getController()->Ucty;
        }
        if ($name === 'Zadosti') {
            return $this->getController()->Zadosti;
        }
        if ($name === 'Uzivatele') {
            return $this->getController()->Uzivatele;
        }

        return parent::__get($name);
    }

    public function getAccounts($conditions = [])
    {
        return $this->Ucty->find('all', [
            'conditions' => [
                    'Ucty.is_hidden' => false,
                ] + $conditions,
        ]);
    }

    /**
     * @param string $email
     * @return Ucet[]
     */
    public function getAccountsByUsersEmail(string $email): array
    {
        /** @var Uzivatel[] $uzivatele */
        $uzivatele = $this->Uzivatele->find('all', [
            'conditions' => [
                'Uzivatele.email' => $email,
            ],
        ]);
        $rtn = [];
        foreach ($uzivatele as $uzivatel) {
            $rtn[$uzivatel->ucet_id] = $this->getAccountById($uzivatel->ucet_id);
        }

        return $rtn;
    }

    public function getAccountById(int $id)
    {
        return $this->Ucty->get($id, [
            'contain' => [
                'FyzickeOsoby',
                'StatutarniOrgan',
                'Zadosti',
                'Zadosti.Fond',
                'Zadosti.Program',
                'Zadosti.Ucel',
                'Zadosti.OhlasovaciFormular',
            ],
        ]);
    }

    public function getPriloha(int $id)
    {
        return $this->Zadosti->PrilohyZadosti->get($id);
    }

    public function getZadost(int $id, array $allowed_ucty_ids = [])
    {
        return $this->Zadosti->get($id, [
            'contain' => self::ZADOSTI_CONTAIN,
            'conditions' => empty($allowed_ucty_ids) ? [] : [
                'Zadosti.ucet_id IN' => $allowed_ucty_ids,
            ],
        ]);
    }

    public function getZadosti()
    {
        return $this->Zadosti->find('all', [
            'conditions' => [
                'Zadosti.is_hidden' => false,
                'Ucet.is_hidden' => false,
            ],
            'contain' => [
                'Ucet',
                'Program',
                'Ucel',
                'OhlasovaciFormular',
                'OhlasovaciFormular.Prilohy',
            ],
        ]);
    }

    public function authenticate(string $username, string $plaintextPassword)
    {
        return $this->Uzivatele->find('all', [
            'conditions' => [
                'login' => $username,
                'password' => sha1($plaintextPassword),
            ],
        ])->first();
    }

    /**
     * @param array $stare_ucty_ids
     * @param bool $contain
     * @return Zadost[]
     */
    public function getZadostiByAccountIds(array $stare_ucty_ids, bool $contain = false): array
    {
        if (empty($stare_ucty_ids)) {
            return [];
        }

        return $this->Zadosti->find('all', [
            'conditions' => [
                'Zadosti.ucet_id IN' => $stare_ucty_ids,
            ],
            'contain' => $contain ? self::ZADOSTI_CONTAIN : [],
        ])->toArray();
    }
}
