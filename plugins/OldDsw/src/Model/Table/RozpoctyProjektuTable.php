<?php
namespace OldDsw\Model\Table;

use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsToMany;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use OldDsw\Model\Entity\RozpoctyProjektu;

/**
 * RozpoctyProjektu Model
 *
 * @property NakladyTable&BelongsToMany $Naklady
 * @property VlastniZdrojeTable&BelongsToMany $VlastniZdroje
 * @property VynosyTable&BelongsToMany $Vynosy
 *
 * @method RozpoctyProjektu get($primaryKey, $options = [])
 * @method RozpoctyProjektu newEntity($data = null, array $options = [])
 * @method RozpoctyProjektu[] newEntities(array $data, array $options = [])
 * @method RozpoctyProjektu|false save(EntityInterface $entity, $options = [])
 * @method RozpoctyProjektu saveOrFail(EntityInterface $entity, $options = [])
 * @method RozpoctyProjektu patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method RozpoctyProjektu[] patchEntities($entities, array $data, array $options = [])
 * @method RozpoctyProjektu findOrCreate($search, callable $callback = null, $options = [])
 */
class RozpoctyProjektuTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('rozpocty_projektu');
        $this->setDisplayField('zadost_id');
        $this->setPrimaryKey('zadost_id');

        $this->belongsToMany('Naklady', [
            'foreignKey' => 'rozpocty_projektu_id',
            'targetForeignKey' => 'naklady_id',
            'joinTable' => 'rozpocty_projektu_naklady',
            'className' => 'OldDsw.Naklady',
        ]);
        $this->hasMany('RozpoctyProjektuVlastniZdroje', [
            'foreignKey' => 'zadost_id',
            'bindingKey' => 'zadost_id',
            'joinTable' => 'rozpocty_projektu_vlastni_zdroje',
            'className' => 'OldDsw.RozpoctyProjektuVlastniZdroje',
        ]);
        $this->hasMany('RozpoctyProjektuVynosy', [
            'foreignKey' => 'zadost_id',
            'bindingKey' => 'zadost_id',
            'joinTable' => 'rozpocty_projektu_vlastni_zdroje',
            'className' => 'OldDsw.RozpoctyProjektuVynosy',
        ]);
        $this->hasMany('RozpoctyProjektuJineZdroje', [
            'foreignKey' => 'zadost_id',
            'bindingKey' => 'zadost_id',
            'joinTable' => 'rozpocty_projektu_jine_zdroje',
            'className' => 'OldDsw.RozpoctyProjektuJineZdroje',
        ]);
        $this->hasMany('RozpoctyProjektuNaklady', [
            'foreignKey' => 'zadost_id',
            'bindingKey' => 'zadost_id',
            'joinTable' => 'rozpocty_projektu_naklady',
            'className' => 'OldDsw.RozpoctyProjektuNaklady',
        ]);
        $this->hasMany('RozpoctyProjektuNakladyInvesticni', [
            'foreignKey' => 'zadost_id',
            'bindingKey' => 'zadost_id',
            'joinTable' => 'rozpocty_projektu_naklady_investicni',
            'className' => 'OldDsw.RozpoctyProjektuNakladyInvesticni',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('zadost_id')
            ->allowEmptyString('zadost_id', null, 'create');

        $validator
            ->numeric('vyse_dotace')
            ->notEmptyString('vyse_dotace');

        $validator
            ->numeric('dotace_z_ministerstva_cr')
            ->notEmptyString('dotace_z_ministerstva_cr');

        $validator
            ->numeric('dotace_z_eu')
            ->notEmptyString('dotace_z_eu');

        $validator
            ->numeric('dotace_z_mhmp')
            ->notEmptyString('dotace_z_mhmp');

        $validator
            ->numeric('celkove_vynosy')
            ->notEmptyString('celkove_vynosy');

        $validator
            ->numeric('celkove_naklady')
            ->notEmptyString('celkove_naklady');

        $validator
            ->scalar('dotace_z_ministerstva_cr_projekt')
            ->maxLength('dotace_z_ministerstva_cr_projekt', 255)
            ->requirePresence('dotace_z_ministerstva_cr_projekt', 'create')
            ->notEmptyString('dotace_z_ministerstva_cr_projekt');

        $validator
            ->scalar('dotace_z_eu_projekt')
            ->maxLength('dotace_z_eu_projekt', 255)
            ->requirePresence('dotace_z_eu_projekt', 'create')
            ->notEmptyString('dotace_z_eu_projekt');

        $validator
            ->scalar('dotace_z_mhmp_projekt')
            ->maxLength('dotace_z_mhmp_projekt', 255)
            ->requirePresence('dotace_z_mhmp_projekt', 'create')
            ->notEmptyString('dotace_z_mhmp_projekt');

        return $validator;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'dsw';
    }
}
