<?php
namespace OldDsw\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * VlastniZdroje Model
 *
 * @property \OldDsw\Model\Table\RequestsTable&\Cake\ORM\Association\BelongsTo $Requests
 * @property \OldDsw\Model\Table\RozpoctyProjektuTable&\Cake\ORM\Association\BelongsToMany $RozpoctyProjektu
 *
 * @method \OldDsw\Model\Entity\VlastniZdroje get($primaryKey, $options = [])
 * @method \OldDsw\Model\Entity\VlastniZdroje newEntity($data = null, array $options = [])
 * @method \OldDsw\Model\Entity\VlastniZdroje[] newEntities(array $data, array $options = [])
 * @method \OldDsw\Model\Entity\VlastniZdroje|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \OldDsw\Model\Entity\VlastniZdroje saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \OldDsw\Model\Entity\VlastniZdroje patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \OldDsw\Model\Entity\VlastniZdroje[] patchEntities($entities, array $data, array $options = [])
 * @method \OldDsw\Model\Entity\VlastniZdroje findOrCreate($search, callable $callback = null, $options = [])
 */
class VlastniZdrojeTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('vlastni_zdroje');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Requests', [
            'foreignKey' => 'request_id',
            'joinType' => 'INNER',
            'className' => 'OldDsw.Requests',
        ]);
        $this->belongsToMany('RozpoctyProjektu', [
            'foreignKey' => 'vlastni_zdroje_id',
            'targetForeignKey' => 'rozpocty_projektu_id',
            'joinTable' => 'rozpocty_projektu_vlastni_zdroje',
            'className' => 'OldDsw.RozpoctyProjektu',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('project_name')
            ->maxLength('project_name', 255)
            ->notEmptyString('project_name');

        $validator
            ->numeric('amount')
            ->notEmptyString('amount');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['request_id'], 'Requests'));

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'dsw';
    }
}
