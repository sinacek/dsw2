<?php
namespace OldDsw\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OhlasovaciFormularPrilohy Model
 *
 * @method \OldDsw\Model\Entity\OhlasovaciFormularPriloha get($primaryKey, $options = [])
 * @method \OldDsw\Model\Entity\OhlasovaciFormularPriloha newEntity($data = null, array $options = [])
 * @method \OldDsw\Model\Entity\OhlasovaciFormularPriloha[] newEntities(array $data, array $options = [])
 * @method \OldDsw\Model\Entity\OhlasovaciFormularPriloha|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \OldDsw\Model\Entity\OhlasovaciFormularPriloha saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \OldDsw\Model\Entity\OhlasovaciFormularPriloha patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \OldDsw\Model\Entity\OhlasovaciFormularPriloha[] patchEntities($entities, array $data, array $options = [])
 * @method \OldDsw\Model\Entity\OhlasovaciFormularPriloha findOrCreate($search, callable $callback = null, $options = [])
 */
class OhlasovaciFormularPrilohyTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('ohlasovaci_formular_prilohy');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        $this->setEntityClass('OldDsw.OhlasovaciFormularPriloha');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('filename')
            ->maxLength('filename', 255)
            ->notEmptyFile('filename');

        $validator
            ->scalar('orig_filename')
            ->maxLength('orig_filename', 255)
            ->notEmptyFile('orig_filename');

        $validator
            ->numeric('filesize')
            ->notEmptyFile('filesize');

        $validator
            ->dateTime('added_when')
            ->notEmptyDateTime('added_when');

        $validator
            ->scalar('mime')
            ->maxLength('mime', 255)
            ->requirePresence('mime', 'create')
            ->notEmptyString('mime');

        return $validator;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'dsw';
    }
}
