<?php
namespace OldDsw\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PrilohyFormulareProhlaseniOVerejnePodpore Model
 *
 * @method \OldDsw\Model\Entity\PrilohyFormulareProhlaseniOVerejnePodpore get($primaryKey, $options = [])
 * @method \OldDsw\Model\Entity\PrilohyFormulareProhlaseniOVerejnePodpore newEntity($data = null, array $options = [])
 * @method \OldDsw\Model\Entity\PrilohyFormulareProhlaseniOVerejnePodpore[] newEntities(array $data, array $options = [])
 * @method \OldDsw\Model\Entity\PrilohyFormulareProhlaseniOVerejnePodpore|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \OldDsw\Model\Entity\PrilohyFormulareProhlaseniOVerejnePodpore saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \OldDsw\Model\Entity\PrilohyFormulareProhlaseniOVerejnePodpore patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \OldDsw\Model\Entity\PrilohyFormulareProhlaseniOVerejnePodpore[] patchEntities($entities, array $data, array $options = [])
 * @method \OldDsw\Model\Entity\PrilohyFormulareProhlaseniOVerejnePodpore findOrCreate($search, callable $callback = null, $options = [])
 */
class PrilohyFormulareProhlaseniOVerejnePodporeTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('prilohy_formulare_prohlaseni_o_verejne_podpore');
        $this->setDisplayField('zadost_id');
        $this->setPrimaryKey('zadost_id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('zadost_id')
            ->allowEmptyString('zadost_id', null, 'create');

        $validator
            ->nonNegativeInteger('poskytnuti_verejne_podpory')
            ->notEmptyString('poskytnuti_verejne_podpory');

        $validator
            ->nonNegativeInteger('rezim_podpory')
            ->notEmptyString('rezim_podpory');

        return $validator;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'dsw';
    }
}
