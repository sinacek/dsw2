<?php

namespace OldDsw\Model\Table;

use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Association\HasMany;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use OldDsw\Model\Entity\Zadost;

/**
 * Zadosti Model
 *
 * @property UctyTable&BelongsTo $Ucets
 * @property PrilohyTable&HasMany $PrilohyZadosti
 * @property OhlasovaciFormularTable&HasMany $OhlasovaciFormular
 *
 * @method Zadost get($primaryKey, $options = [])
 * @method Zadost newEntity($data = null, array $options = [])
 * @method Zadost[] newEntities(array $data, array $options = [])
 * @method Zadost|false save(EntityInterface $entity, $options = [])
 * @method Zadost saveOrFail(EntityInterface $entity, $options = [])
 * @method Zadost patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method Zadost[] patchEntities($entities, array $data, array $options = [])
 * @method Zadost findOrCreate($search, callable $callback = null, $options = [])
 */
class ZadostiTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('zadosti');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        $this->setEntityClass('OldDsw.Zadost');

        $this->belongsTo('Ucet', [
            'foreignKey' => 'ucet_id',
            'joinType' => 'INNER',
            'className' => 'OldDsw.Ucty',
        ]);
        $this->belongsTo('Fond', [
            'foreignKey' => 'fond_id',
            'className' => 'OldDsw.Fondy',
        ]);
        $this->belongsTo('Program', [
            'foreignKey' => 'program_id',
            'className' => 'OldDsw.Programy',
        ]);
        $this->belongsTo('Ucel', [
            'foreignKey' => 'ucel_id',
            'className' => 'OldDsw.Ucely',
        ]);
        $this->hasOne('DefiniceProjektu', [
            'foreignKey' => 'zadost_id',
            'className' => 'OldDsw.DefiniceProjektu',
        ]);
        $this->hasOne('RozpoctyProjektu', [
            'foreignKey' => 'zadost_id',
            'className' => 'OldDsw.RozpoctyProjektu',
        ]);
        $this->hasMany('OhlasovaciFormular', [
            'foreignKey' => 'zadost_id',
            'className' => 'OldDsw.OhlasovaciFormular',
        ]);
        $this->hasMany('PrilohyZadosti', [
            'foreignKey' => 'request_id',
            'className' => 'OldDsw.Prilohy',
        ]);
        $this->hasOne('PrilohyFormulareProhlaseniOVerejnePodpore', [
            'foreignKey' => 'zadost_id',
            'className' => 'OldDsw.PrilohyFormulareProhlaseniOVerejnePodpore',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('nazev')
            ->maxLength('nazev', 255)
            ->notEmptyString('nazev');

        $validator
            ->numeric('pozadovana_castka')
            ->notEmptyString('pozadovana_castka');

        $validator
            ->numeric('navrhovana_castka')
            ->notEmptyString('navrhovana_castka');

        $validator
            ->numeric('konecna_castka')
            ->notEmptyString('konecna_castka');

        $validator
            ->scalar('status')
            ->maxLength('status', 45)
            ->notEmptyString('status');

        $validator
            ->scalar('verejny_status')
            ->maxLength('verejny_status', 45)
            ->notEmptyString('verejny_status');

        $validator
            ->nonNegativeInteger('pocet_bodu')
            ->notEmptyString('pocet_bodu');

        $validator
            ->dateTime('pridano')
            ->notEmptyDateTime('pridano');

        $validator
            ->nonNegativeInteger('pridano_kym')
            ->notEmptyString('pridano_kym');

        $validator
            ->dateTime('posledni_uprava_kdy')
            ->allowEmptyDateTime('posledni_uprava_kdy');

        $validator
            ->nonNegativeInteger('posledni_uprava_kym')
            ->allowEmptyString('posledni_uprava_kym');

        $validator
            ->boolean('uzamceno')
            ->notEmptyString('uzamceno');

        $validator
            ->dateTime('uzamknout_kdy')
            ->allowEmptyDateTime('uzamknout_kdy');

        $validator
            ->scalar('poznamka_spravy')
            ->requirePresence('poznamka_spravy', 'create')
            ->notEmptyString('poznamka_spravy');

        $validator
            ->scalar('cislo_jednaci')
            ->maxLength('cislo_jednaci', 45)
            ->allowEmptyString('cislo_jednaci');

        $validator
            ->date('datum_podani')
            ->allowEmptyDate('datum_podani');

        $validator
            ->nonNegativeInteger('dotacni_obdobi')
            ->notEmptyString('dotacni_obdobi');

        $validator
            ->boolean('smazano')
            ->notEmptyString('smazano');

        $validator
            ->dateTime('smazano_kdy')
            ->allowEmptyDateTime('smazano_kdy');

        $validator
            ->integer('smazano_kym')
            ->allowEmptyString('smazano_kym');

        $validator
            ->boolean('odeslano')
            ->notEmptyString('odeslano');

        $validator
            ->dateTime('odeslano_kdy')
            ->allowEmptyDateTime('odeslano_kdy');

        $validator
            ->integer('odeslano_kym')
            ->allowEmptyString('odeslano_kym');

        $validator
            ->scalar('cislo_uctu')
            ->maxLength('cislo_uctu', 30)
            ->allowEmptyString('cislo_uctu');

        $validator
            ->scalar('ratingMessage')
            ->maxLength('ratingMessage', 300)
            ->allowEmptyString('ratingMessage');

        $validator
            ->scalar('short_purpouse')
            ->maxLength('short_purpouse', 300)
            ->allowEmptyString('short_purpouse');

        $validator
            ->scalar('econom_status')
            ->maxLength('econom_status', 300)
            ->allowEmptyString('econom_status');

        $validator
            ->scalar('final_report')
            ->maxLength('final_report', 300)
            ->allowEmptyString('final_report');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['ucet_id'], 'Ucet'));
        $rules->add($rules->existsIn(['fond_id'], 'Fond'));
        $rules->add($rules->existsIn(['program_id'], 'Program'));
        $rules->add($rules->existsIn(['ucel_id'], 'Ucel'));

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'dsw';
    }
}
