<?php

namespace OldDsw\Model\Table;

use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use OldDsw\Model\Entity\Program;

/**
 * Programy Model
 *
 * @property FondsTable&BelongsTo $Fonds
 * @property DotacniFondDataTable&HasMany $DotacniFondData
 * @property GrantAttachmentSettingsTable&HasMany $GrantAttachmentSettings
 * @property GrantRequestsTable&HasMany $GrantRequests
 * @property NastaveniAlokovaneCastkyTable&HasMany $NastaveniAlokovaneCastky
 * @property PrilohyFormulareNastaveniTable&HasMany $PrilohyFormulareNastaveni
 * @property PurposesTable&HasMany $Purposes
 * @property RolesTable&HasMany $Roles
 * @property ZadostiTable&HasMany $Zadosti
 *
 * @method Program get($primaryKey, $options = [])
 * @method Program newEntity($data = null, array $options = [])
 * @method Program[] newEntities(array $data, array $options = [])
 * @method Program|false save(EntityInterface $entity, $options = [])
 * @method Program saveOrFail(EntityInterface $entity, $options = [])
 * @method Program patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method Program[] patchEntities($entities, array $data, array $options = [])
 * @method Program findOrCreate($search, callable $callback = null, $options = [])
 */
class ProgramyTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('programs');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');
        $this->setEntityClass('OldDsw.Program');

        $this->belongsTo('Fonds', [
            'foreignKey' => 'fond_id',
            'joinType' => 'INNER',
            'className' => 'OldDsw.Fonds',
        ]);
        $this->hasMany('DotacniFondData', [
            'foreignKey' => 'program_id',
            'className' => 'OldDsw.DotacniFondData',
        ]);
        $this->hasMany('GrantAttachmentSettings', [
            'foreignKey' => 'program_id',
            'className' => 'OldDsw.GrantAttachmentSettings',
        ]);
        $this->hasMany('GrantRequests', [
            'foreignKey' => 'program_id',
            'className' => 'OldDsw.GrantRequests',
        ]);
        $this->hasMany('NastaveniAlokovaneCastky', [
            'foreignKey' => 'program_id',
            'className' => 'OldDsw.NastaveniAlokovaneCastky',
        ]);
        $this->hasMany('PrilohyFormulareNastaveni', [
            'foreignKey' => 'program_id',
            'className' => 'OldDsw.PrilohyFormulareNastaveni',
        ]);
        $this->hasMany('Purposes', [
            'foreignKey' => 'program_id',
            'className' => 'OldDsw.Purposes',
        ]);
        $this->hasMany('Roles', [
            'foreignKey' => 'program_id',
            'className' => 'OldDsw.Roles',
        ]);
        $this->hasMany('Zadosti', [
            'foreignKey' => 'program_id',
            'className' => 'OldDsw.Zadosti',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->notEmptyString('name');

        $validator
            ->scalar('description')
            ->allowEmptyString('description');

        $validator
            ->boolean('active')
            ->notEmptyString('active');

        $validator
            ->boolean('deleted')
            ->notEmptyString('deleted');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['fond_id'], 'Fonds'));

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'dsw';
    }
}
