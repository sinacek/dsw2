<?php

namespace OldDsw\Model\Table;

use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use OldDsw\Model\Entity\UctyFyzickeOsoby;

/**
 * UctyFyzickeOsoby Model
 *
 * @property UctyTable&BelongsTo $Ucties
 * @property FyzickeOsobyTable&BelongsTo $FyzickeOsobies
 *
 * @method UctyFyzickeOsoby get($primaryKey, $options = [])
 * @method UctyFyzickeOsoby newEntity($data = null, array $options = [])
 * @method UctyFyzickeOsoby[] newEntities(array $data, array $options = [])
 * @method UctyFyzickeOsoby|false save(EntityInterface $entity, $options = [])
 * @method UctyFyzickeOsoby saveOrFail(EntityInterface $entity, $options = [])
 * @method UctyFyzickeOsoby patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method UctyFyzickeOsoby[] patchEntities($entities, array $data, array $options = [])
 * @method UctyFyzickeOsoby findOrCreate($search, callable $callback = null, $options = [])
 */
class UctyFyzickeOsobyTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('ucty_fyzicke_osoby');
        $this->setDisplayField('ucty_id');
        $this->setPrimaryKey(['ucty_id', 'fyzicke_osoby_id']);

        $this->belongsTo('Ucties', [
            'foreignKey' => 'ucty_id',
            'joinType' => 'INNER',
            'className' => 'OldDsw.Ucties',
        ]);
        $this->belongsTo('FyzickeOsobies', [
            'foreignKey' => 'fyzicke_osoby_id',
            'joinType' => 'INNER',
            'className' => 'OldDsw.FyzickeOsobies',
        ]);
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['ucty_id'], 'Ucty'));
        $rules->add($rules->existsIn(['fyzicke_osoby_id'], 'FyzickeOsoby'));

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'dsw';
    }
}
