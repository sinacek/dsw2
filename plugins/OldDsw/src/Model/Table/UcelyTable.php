<?php

namespace OldDsw\Model\Table;

use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use OldDsw\Model\Entity\Ucel;

/**
 * Ucely Model
 *
 * @property ProgramsTable&BelongsTo $Programs
 * @property DotacniFondDataTable&HasMany $DotacniFondData
 * @property GrantAttachmentSettingsTable&HasMany $GrantAttachmentSettings
 * @property GrantRequestsTable&HasMany $GrantRequests
 * @property HodnoceniTable&HasMany $Hodnoceni
 * @property PrilohyFormulareNastaveniTable&HasMany $PrilohyFormulareNastaveni
 * @property QuestionnairesTable&HasMany $Questionnaires
 * @property RatingQuestionnairesTable&HasMany $RatingQuestionnaires
 * @property RozvojDataTable&HasMany $RozvojData
 *
 * @method Ucel get($primaryKey, $options = [])
 * @method Ucel newEntity($data = null, array $options = [])
 * @method Ucel[] newEntities(array $data, array $options = [])
 * @method Ucel|false save(EntityInterface $entity, $options = [])
 * @method Ucel saveOrFail(EntityInterface $entity, $options = [])
 * @method Ucel patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method Ucel[] patchEntities($entities, array $data, array $options = [])
 * @method Ucel findOrCreate($search, callable $callback = null, $options = [])
 */
class UcelyTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('purposes');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');
        $this->setEntityClass('OldDsw.Ucel');

        $this->belongsTo('Programs', [
            'foreignKey' => 'program_id',
            'joinType' => 'INNER',
            'className' => 'OldDsw.Programs',
        ]);
        $this->hasMany('DotacniFondData', [
            'foreignKey' => 'purpose_id',
            'className' => 'OldDsw.DotacniFondData',
        ]);
        $this->hasMany('GrantAttachmentSettings', [
            'foreignKey' => 'purpose_id',
            'className' => 'OldDsw.GrantAttachmentSettings',
        ]);
        $this->hasMany('GrantRequests', [
            'foreignKey' => 'purpose_id',
            'className' => 'OldDsw.GrantRequests',
        ]);
        $this->hasMany('Hodnoceni', [
            'foreignKey' => 'purpose_id',
            'className' => 'OldDsw.Hodnoceni',
        ]);
        $this->hasMany('PrilohyFormulareNastaveni', [
            'foreignKey' => 'purpose_id',
            'className' => 'OldDsw.PrilohyFormulareNastaveni',
        ]);
        $this->hasMany('Questionnaires', [
            'foreignKey' => 'purpose_id',
            'className' => 'OldDsw.Questionnaires',
        ]);
        $this->hasMany('RatingQuestionnaires', [
            'foreignKey' => 'purpose_id',
            'className' => 'OldDsw.RatingQuestionnaires',
        ]);
        $this->hasMany('RozvojData', [
            'foreignKey' => 'purpose_id',
            'className' => 'OldDsw.RozvojData',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->notEmptyString('name');

        $validator
            ->scalar('description')
            ->requirePresence('description', 'create')
            ->notEmptyString('description');

        $validator
            ->boolean('active')
            ->notEmptyString('active');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['program_id'], 'Programs'));

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'dsw';
    }
}
