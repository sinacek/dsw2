<?php
namespace OldDsw\Model\Table;

use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use OldDsw\Model\Entity\StatutarniOrganFyzickeOsoby;

/**
 * StatutarniOrganFyzickeOsoby Model
 *
 * @property AccountsTable&BelongsTo $Accounts
 * @property FyzickeOsobiesTable&BelongsTo $FyzickeOsobies
 *
 * @method StatutarniOrganFyzickeOsoby get($primaryKey, $options = [])
 * @method StatutarniOrganFyzickeOsoby newEntity($data = null, array $options = [])
 * @method StatutarniOrganFyzickeOsoby[] newEntities(array $data, array $options = [])
 * @method StatutarniOrganFyzickeOsoby|false save(EntityInterface $entity, $options = [])
 * @method StatutarniOrganFyzickeOsoby saveOrFail(EntityInterface $entity, $options = [])
 * @method StatutarniOrganFyzickeOsoby patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method StatutarniOrganFyzickeOsoby[] patchEntities($entities, array $data, array $options = [])
 * @method StatutarniOrganFyzickeOsoby findOrCreate($search, callable $callback = null, $options = [])
 */
class StatutarniOrganFyzickeOsobyTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('statutarni_organ_fyzicke_osoby');
        $this->setDisplayField('account_id');
        $this->setPrimaryKey(['account_id', 'fyzicke_osoby_id']);

        $this->belongsTo('Ucet', [
            'foreignKey' => 'account_id',
            'joinType' => 'INNER',
            'className' => 'OldDsw.Ucty',
        ]);
        $this->belongsTo('FyzickaOsoba', [
            'foreignKey' => 'fyzicke_osoby_id',
            'joinType' => 'INNER',
            'className' => 'OldDsw.FyzickeOsoby',
        ]);
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['account_id'], 'Accounts'));
        $rules->add($rules->existsIn(['fyzicke_osoby_id'], 'FyzickeOsobies'));

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'dsw';
    }
}
