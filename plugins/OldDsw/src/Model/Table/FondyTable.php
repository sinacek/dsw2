<?php

namespace OldDsw\Model\Table;

use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use OldDsw\Model\Entity\Fond;

/**
 * Fondy Model
 *
 * @property GrantAttachmentSettingsTable&HasMany $GrantAttachmentSettings
 * @property GrantRequestsTable&HasMany $GrantRequests
 * @property GridsColumnsSettingsTable&HasMany $GridsColumnsSettings
 * @property PrilohyFormulareNastaveniTable&HasMany $PrilohyFormulareNastaveni
 * @property ProgramsTable&HasMany $Programs
 * @property RolesTable&HasMany $Roles
 * @property ZadostiTable&HasMany $Zadosti
 *
 * @method Fond get($primaryKey, $options = [])
 * @method Fond newEntity($data = null, array $options = [])
 * @method Fond[] newEntities(array $data, array $options = [])
 * @method Fond|false save(EntityInterface $entity, $options = [])
 * @method Fond saveOrFail(EntityInterface $entity, $options = [])
 * @method Fond patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method Fond[] patchEntities($entities, array $data, array $options = [])
 * @method Fond findOrCreate($search, callable $callback = null, $options = [])
 */
class FondyTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('fonds');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');
        $this->setEntityClass('OldDsw.Fond');

        $this->hasMany('GrantAttachmentSettings', [
            'foreignKey' => 'fond_id',
            'className' => 'OldDsw.GrantAttachmentSettings',
        ]);
        $this->hasMany('GrantRequests', [
            'foreignKey' => 'fond_id',
            'className' => 'OldDsw.GrantRequests',
        ]);
        $this->hasMany('GridsColumnsSettings', [
            'foreignKey' => 'fond_id',
            'className' => 'OldDsw.GridsColumnsSettings',
        ]);
        $this->hasMany('PrilohyFormulareNastaveni', [
            'foreignKey' => 'fond_id',
            'className' => 'OldDsw.PrilohyFormulareNastaveni',
        ]);
        $this->hasMany('Programs', [
            'foreignKey' => 'fond_id',
            'className' => 'OldDsw.Programs',
        ]);
        $this->hasMany('Roles', [
            'foreignKey' => 'fond_id',
            'className' => 'OldDsw.Roles',
        ]);
        $this->hasMany('Zadosti', [
            'foreignKey' => 'fond_id',
            'className' => 'OldDsw.Zadosti',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->notEmptyString('name');

        $validator
            ->scalar('description')
            ->allowEmptyString('description');

        $validator
            ->boolean('active')
            ->notEmptyString('active');

        $validator
            ->boolean('deleted')
            ->notEmptyString('deleted');

        $validator
            ->scalar('url_identifier')
            ->maxLength('url_identifier', 45)
            ->notEmptyString('url_identifier');

        $validator
            ->scalar('classname')
            ->maxLength('classname', 45)
            ->notEmptyString('classname');

        return $validator;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'dsw';
    }
}
