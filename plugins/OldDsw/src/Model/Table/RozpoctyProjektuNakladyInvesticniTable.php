<?php
namespace OldDsw\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * RozpoctyProjektuNakladyInvesticni Model
 *
 * @property \OldDsw\Model\Table\ZadostsTable&\Cake\ORM\Association\BelongsTo $Zadosts
 *
 * @method \OldDsw\Model\Entity\RozpoctyProjektuNakladyInvesticni get($primaryKey, $options = [])
 * @method \OldDsw\Model\Entity\RozpoctyProjektuNakladyInvesticni newEntity($data = null, array $options = [])
 * @method \OldDsw\Model\Entity\RozpoctyProjektuNakladyInvesticni[] newEntities(array $data, array $options = [])
 * @method \OldDsw\Model\Entity\RozpoctyProjektuNakladyInvesticni|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \OldDsw\Model\Entity\RozpoctyProjektuNakladyInvesticni saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \OldDsw\Model\Entity\RozpoctyProjektuNakladyInvesticni patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \OldDsw\Model\Entity\RozpoctyProjektuNakladyInvesticni[] patchEntities($entities, array $data, array $options = [])
 * @method \OldDsw\Model\Entity\RozpoctyProjektuNakladyInvesticni findOrCreate($search, callable $callback = null, $options = [])
 */
class RozpoctyProjektuNakladyInvesticniTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('rozpocty_projektu_naklady_investicni');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Zadosts', [
            'foreignKey' => 'zadost_id',
            'joinType' => 'INNER',
            'className' => 'OldDsw.Zadosts',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('nazev')
            ->maxLength('nazev', 255)
            ->notEmptyString('nazev');

        $validator
            ->numeric('castka')
            ->notEmptyString('castka');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['zadost_id'], 'Zadosts'));

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'dsw';
    }
}
