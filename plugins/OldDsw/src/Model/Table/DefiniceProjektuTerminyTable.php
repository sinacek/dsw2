<?php
namespace OldDsw\Model\Table;

use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use OldDsw\Model\Entity\DefiniceProjektuTerminy;

/**
 * DefiniceProjektuTerminy Model
 *
 * @property ZadostsTable&BelongsTo $Zadosts
 *
 * @method DefiniceProjektuTerminy get($primaryKey, $options = [])
 * @method DefiniceProjektuTerminy newEntity($data = null, array $options = [])
 * @method DefiniceProjektuTerminy[] newEntities(array $data, array $options = [])
 * @method DefiniceProjektuTerminy|false save(EntityInterface $entity, $options = [])
 * @method DefiniceProjektuTerminy saveOrFail(EntityInterface $entity, $options = [])
 * @method DefiniceProjektuTerminy patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method DefiniceProjektuTerminy[] patchEntities($entities, array $data, array $options = [])
 * @method DefiniceProjektuTerminy findOrCreate($search, callable $callback = null, $options = [])
 */
class DefiniceProjektuTerminyTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('definice_projektu_terminy');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Zadosts', [
            'foreignKey' => 'zadost_id',
            'joinType' => 'INNER',
            'className' => 'OldDsw.Zadosts',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->date('zahajeni')
            ->notEmptyDate('zahajeni');

        $validator
            ->date('ukonceni')
            ->notEmptyDate('ukonceni');

        $validator
            ->scalar('misto_realizace')
            ->maxLength('misto_realizace', 255)
            ->notEmptyString('misto_realizace');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['zadost_id'], 'Zadosts'));

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'dsw';
    }
}
