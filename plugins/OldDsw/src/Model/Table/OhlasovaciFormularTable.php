<?php
namespace OldDsw\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OhlasovaciFormular Model
 *
 * @property \OldDsw\Model\Table\ZadostsTable&\Cake\ORM\Association\BelongsTo $Zadosts
 * @property \OldDsw\Model\Table\PrilohasTable&\Cake\ORM\Association\BelongsTo $Prilohas
 * @property \OldDsw\Model\Table\AddedByUzivatelsTable&\Cake\ORM\Association\BelongsTo $AddedByUzivatels
 * @property \OldDsw\Model\Table\PrilohyTable&\Cake\ORM\Association\BelongsToMany $Prilohy
 *
 * @method \OldDsw\Model\Entity\OhlasovaciFormular get($primaryKey, $options = [])
 * @method \OldDsw\Model\Entity\OhlasovaciFormular newEntity($data = null, array $options = [])
 * @method \OldDsw\Model\Entity\OhlasovaciFormular[] newEntities(array $data, array $options = [])
 * @method \OldDsw\Model\Entity\OhlasovaciFormular|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \OldDsw\Model\Entity\OhlasovaciFormular saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \OldDsw\Model\Entity\OhlasovaciFormular patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \OldDsw\Model\Entity\OhlasovaciFormular[] patchEntities($entities, array $data, array $options = [])
 * @method \OldDsw\Model\Entity\OhlasovaciFormular findOrCreate($search, callable $callback = null, $options = [])
 */
class OhlasovaciFormularTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('ohlasovaci_formular');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Zadosti', [
            'foreignKey' => 'zadost_id',
            'joinType' => 'INNER',
            'className' => 'OldDsw.Zadosti',
        ]);
        $this->belongsTo('Prilohy', [
            'foreignKey' => 'priloha_id',
            'className' => 'OldDsw.Prilohas',
        ]);
        $this->belongsTo('AddedByUzivatel', [
            'foreignKey' => 'added_by_uzivatel_id',
            'joinType' => 'INNER',
            'className' => 'OldDsw.Uzivatele',
        ]);
        $this->hasOne('Prilohy', [
            'foreignKey' => 'id',
            'bindingKey' => 'priloha_id',
            'joinTable' => 'ohlasovaci_formular_prilohy',
            'className' => 'OldDsw.OhlasovaciFormularPrilohy',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->nonNegativeInteger('typ')
            ->notEmptyString('typ');

        $validator
            ->date('datum_zahajeni')
            ->allowEmptyDate('datum_zahajeni');

        $validator
            ->date('datum_ukonceni')
            ->notEmptyDate('datum_ukonceni');

        $validator
            ->scalar('misto_konani')
            ->requirePresence('misto_konani', 'create')
            ->notEmptyString('misto_konani');

        $validator
            ->scalar('garant')
            ->maxLength('garant', 255)
            ->notEmptyString('garant');

        $validator
            ->scalar('garant_telefon')
            ->maxLength('garant_telefon', 255)
            ->notEmptyString('garant_telefon');

        $validator
            ->scalar('garant_email')
            ->maxLength('garant_email', 255)
            ->notEmptyString('garant_email');

        $validator
            ->numeric('cerpana_castka')
            ->notEmptyString('cerpana_castka');

        $validator
            ->scalar('poznamka')
            ->requirePresence('poznamka', 'create')
            ->notEmptyString('poznamka');

        $validator
            ->dateTime('added_when')
            ->notEmptyDateTime('added_when');

        $validator
            ->scalar('pocet_ucastniku')
            ->maxLength('pocet_ucastniku', 255)
            ->requirePresence('pocet_ucastniku', 'create')
            ->notEmptyString('pocet_ucastniku');

        $validator
            ->boolean('celorocni_cinnost')
            ->notEmptyString('celorocni_cinnost');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['zadost_id'], 'Zadosti'));
        $rules->add($rules->existsIn(['priloha_id'], 'Prilohy'));
        $rules->add($rules->existsIn(['added_by_uzivatel_id'], 'AddedByUzivatel'));

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'dsw';
    }
}
