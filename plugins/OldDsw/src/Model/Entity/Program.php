<?php

namespace OldDsw\Model\Entity;

use Cake\ORM\Entity;

/**
 * Programy Entity
 *
 * @property int $id
 * @property int $fond_id
 * @property string $name
 * @property string|null $description
 * @property bool $active
 * @property bool $deleted
 *
 * @property Fond $fond
 * @property DotacniFondData[] $dotacni_fond_data
 * @property GrantAttachmentSetting[] $grant_attachment_settings
 * @property GrantRequest[] $grant_requests
 * @property NastaveniAlokovaneCastky[] $nastaveni_alokovane_castky
 * @property PrilohyFormulareNastaveni[] $prilohy_formulare_nastaveni
 * @property Purpose[] $purposes
 * @property Role[] $roles
 * @property Zadost[] $zadosti
 */
class Program extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'fond_id' => true,
        'name' => true,
        'description' => true,
        'active' => true,
        'deleted' => true,
        'fond' => true,
        'dotacni_fond_data' => true,
        'grant_attachment_settings' => true,
        'grant_requests' => true,
        'nastaveni_alokovane_castky' => true,
        'prilohy_formulare_nastaveni' => true,
        'purposes' => true,
        'roles' => true,
        'zadosti' => true,
    ];
}
