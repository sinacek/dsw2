<?php
namespace OldDsw\Model\Entity;

use Cake\ORM\Entity;

/**
 * RozpoctyProjektuJineZdroje Entity
 *
 * @property int $id
 * @property int $zadost_id
 * @property string $nazev
 * @property float $castka
 * @property string $projekt
 *
 * @property \OldDsw\Model\Entity\Zadost $zadost
 */
class RozpoctyProjektuJineZdroje extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'zadost_id' => true,
        'nazev' => true,
        'castka' => true,
        'projekt' => true,
        'zadost' => true,
    ];
}
