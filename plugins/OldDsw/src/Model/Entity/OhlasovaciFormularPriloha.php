<?php
namespace OldDsw\Model\Entity;

use Cake\ORM\Entity;

/**
 * OhlasovaciFormularPrilohy Entity
 *
 * @property int $id
 * @property string $filename
 * @property string $orig_filename
 * @property float $filesize
 * @property \Cake\I18n\FrozenTime $added_when
 * @property string $mime
 */
class OhlasovaciFormularPriloha extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'filename' => true,
        'orig_filename' => true,
        'filesize' => true,
        'added_when' => true,
        'mime' => true,
    ];
}
