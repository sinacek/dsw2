<?php

namespace OldDsw\Model\Entity;

use Cake\I18n\FrozenDate;
use Cake\I18n\FrozenTime;
use Cake\ORM\Entity;

/**
 * Zadosti Entity
 *
 * @property int $id
 * @property bool $is_hidden
 * @property int $ucet_id
 * @property int $fond_id
 * @property int|null $program_id
 * @property int $ucel_id
 * @property string $nazev
 * @property float $pozadovana_castka
 * @property float $navrhovana_castka
 * @property float $konecna_castka
 * @property string $status
 * @property string $verejny_status
 * @property int $pocet_bodu
 * @property FrozenTime $pridano
 * @property int $pridano_kym
 * @property FrozenTime|null $posledni_uprava_kdy
 * @property int|null $posledni_uprava_kym
 * @property bool $uzamceno
 * @property FrozenTime|null $uzamknout_kdy
 * @property string $poznamka_spravy
 * @property string|null $cislo_jednaci
 * @property FrozenDate|null $datum_podani
 * @property int $dotacni_obdobi
 * @property bool $smazano
 * @property FrozenTime|null $smazano_kdy
 * @property int|null $smazano_kym
 * @property bool $odeslano
 * @property FrozenTime|null $odeslano_kdy
 * @property int|null $odeslano_kym
 * @property string|null $cislo_uctu
 * @property string|null $ratingMessage
 * @property string|null $short_purpouse
 * @property string|null $econom_status
 * @property string|null $final_report
 *
 * @property Ucet $ucet
 * @property Fond $fond
 * @property Program $program
 * @property Ucel $ucel
 * @property DefiniceProjektu $definice_projektu
 * @property RozpoctyProjektu $rozpocty_projektu
 * @property Prilohy[] $prilohy_zadosti
 * @property PrilohyFormulareProhlaseniOVerejnePodpore $prilohy_formulare_prohlaseni_o_verejne_podpore
 */
class Zadost extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'ucet_id' => true,
        'is_hidden' => true,
        'fond_id' => true,
        'program_id' => true,
        'ucel_id' => true,
        'nazev' => true,
        'pozadovana_castka' => true,
        'navrhovana_castka' => true,
        'konecna_castka' => true,
        'status' => true,
        'verejny_status' => true,
        'pocet_bodu' => true,
        'pridano' => true,
        'pridano_kym' => true,
        'posledni_uprava_kdy' => true,
        'posledni_uprava_kym' => true,
        'uzamceno' => true,
        'uzamknout_kdy' => true,
        'poznamka_spravy' => true,
        'cislo_jednaci' => true,
        'datum_podani' => true,
        'dotacni_obdobi' => true,
        'smazano' => true,
        'smazano_kdy' => true,
        'smazano_kym' => true,
        'odeslano' => true,
        'odeslano_kdy' => true,
        'odeslano_kym' => true,
        'cislo_uctu' => true,
        'ratingMessage' => true,
        'short_purpouse' => true,
        'econom_status' => true,
        'final_report' => true,
        'ucet' => true,
        'fond' => true,
        'program' => true,
        'ucel' => true,
    ];
}
