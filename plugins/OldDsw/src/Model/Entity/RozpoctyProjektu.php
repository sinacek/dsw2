<?php
namespace OldDsw\Model\Entity;

use Cake\ORM\Entity;

/**
 * RozpoctyProjektu Entity
 *
 * @property int $zadost_id
 * @property float $vyse_dotace
 * @property float $dotace_z_ministerstva_cr
 * @property float $dotace_z_eu
 * @property float $dotace_z_mhmp
 * @property float $celkove_vynosy
 * @property float $celkove_naklady
 * @property string $dotace_z_ministerstva_cr_projekt
 * @property string $dotace_z_eu_projekt
 * @property string $dotace_z_mhmp_projekt
 *
 * @property RozpoctyProjektuNaklady[] $rozpocty_projektu_naklady
 * @property RozpoctyProjektuNakladyInvesticni[] $rozpocty_projektu_naklady_investicni
 * @property RozpoctyProjektuVlastniZdroje[] $rozpocty_projektu_vlastni_zdroje
 * @property RozpoctyProjektuVynosy[] $rozpocty_projektu_vynosy
 * @property RozpoctyProjektuJineZdroje[] $rozpocty_projektu_jine_zdroje
 */
class RozpoctyProjektu extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'vyse_dotace' => true,
        'dotace_z_ministerstva_cr' => true,
        'dotace_z_eu' => true,
        'dotace_z_mhmp' => true,
        'celkove_vynosy' => true,
        'celkove_naklady' => true,
        'dotace_z_ministerstva_cr_projekt' => true,
        'dotace_z_eu_projekt' => true,
        'dotace_z_mhmp_projekt' => true,
        'naklady' => true,
        'vlastni_zdroje' => true,
        'vynosy' => true,
    ];
}
