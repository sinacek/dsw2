<?php

namespace OldDsw\Model\Entity;

use Cake\I18n\FrozenTime;
use Cake\ORM\Entity;

/**
 * Account Entity
 *
 * @property int $id
 * @property bool $is_hidden
 * @property string $identifier
 * @property string $nazev
 * @property string $pravni_forma
 * @property string|null $ico
 * @property string|null $dic
 * @property string $ulice
 * @property string $obec
 * @property string $psc
 * @property string $nazev_banky
 * @property string $cislo_uctu
 * @property string $kod_banky
 * @property string $typ_uctu
 * @property FrozenTime|null $added_when
 * @property string|null $cast_obce
 * @property string $cislo_popisne
 * @property string|null $cislo_orientacni
 * @property string|null $obec_dorucovaci
 * @property string|null $cast_obce_dorucovaci
 * @property string|null $ulice_dorucovaci
 * @property string|null $cislo_popisne_dorucovaci
 * @property string|null $cislo_orientacni_dorucovaci
 * @property string|null $psc_dorucovaci
 * @property string|null $telefon
 * @property string|null $email
 * @property string|null $poznamka
 * @property float $kod_pravni_formy
 * @property string|null $cislo_jednaci
 * @property string|null $sz
 *
 * @property FyzickaOsoba[] $fyzicke_osoby
 * @property Zadost[] $zadosti
 * @property FyzickaOsoba[] $statutarni_organ
 */
class Ucet extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'identifier' => true,
        'is_hidden' => true,
        'nazev' => true,
        'pravni_forma' => true,
        'ico' => true,
        'dic' => true,
        'ulice' => true,
        'obec' => true,
        'psc' => true,
        'nazev_banky' => true,
        'cislo_uctu' => true,
        'kod_banky' => true,
        'typ_uctu' => true,
        'added_when' => true,
        'cast_obce' => true,
        'cislo_popisne' => true,
        'cislo_orientacni' => true,
        'obec_dorucovaci' => true,
        'cast_obce_dorucovaci' => true,
        'ulice_dorucovaci' => true,
        'cislo_popisne_dorucovaci' => true,
        'cislo_orientacni_dorucovaci' => true,
        'psc_dorucovaci' => true,
        'telefon' => true,
        'email' => true,
        'poznamka' => true,
        'kod_pravni_formy' => true,
        'cislo_jednaci' => true,
        'sz' => true,
        'fyzicke_osoby' => true,
    ];

    public function formatAddress(): string
    {
        return sprintf("%s %s%s, %s, %s", $this->ulice, $this->cislo_popisne, empty($this->cislo_orientacni) ? '' : '/' . $this->cislo_orientacni, $this->obec, $this->psc);
    }

    public function formatDorucovaciAddress(): string
    {
        return sprintf("%s %s%s, %s, %s", $this->ulice_dorucovaci, $this->cislo_popisne_dorucovaci, empty($this->cislo_orientacni_dorucovaci) ? '' : '/' . $this->cislo_orientacni_dorucovaci, $this->obec_dorucovaci, $this->psc_dorucovaci);
    }

    public function formatIcoDic(): string
    {
        $ic = empty($this->ico) ? '' : sprintf("IČO: %s,", $this->ico);
        $dic = empty($this->dic) ? '' : sprintf("DIČ: %s,", $this->dic);
        return sprintf("%s%s", $ic, $dic);
    }

    public function isPO(): bool
    {
        return $this->typ_uctu === 'P';
    }

    public function _getNameWithYear()
    {
        $nazev = $this->nazev;
        if (empty($this->ico) || startsWith($this->pravni_forma, 'Fyz')) {
            $explode = explode(' ', $nazev);
            if (count($explode) > 1) {
                $nazev = $explode[count($explode) - 1] . ', ' . join(' ', $explode);
            }
        }
        return sprintf('%s (%s) (%s)', $nazev, $this->email, $this->added_when ? $this->added_when->format('Y') : '');
    }
}
