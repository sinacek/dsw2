<?php
/**
 * @var $this AppView
 * @var $zadosti Zadost[]
 */

if (!isset($zadosti) || empty($zadosti)) {
    return;
}

use App\View\AppView;
use Cake\I18n\Number;
use Cake\Routing\Router;
use OldDsw\Model\Entity\Zadost;

?>
<table class="table" id="dtable">
    <thead>
    <tr>
        <th><?= __d('olddsw', 'Název') ?></th>
        <th><?= __d('olddsw', 'Program') ?></th>
        <th><?= __d('olddsw', 'Účel') ?></th>
        <th><?= __d('olddsw', 'Dotační Období') ?></th>
        <th><?= __d('olddsw', 'Požadovaná výše dotace') ?></th>
        <th><?= __d('olddsw', 'Navrhovaná výše dotace') ?></th>
        <th><?= __d('olddsw', 'Konečná výše dotace') ?></th>
        <th><?= __d('olddsw', 'Ohlášeno') ?></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($zadosti as $zadost): ?>
        <?php if (empty($zadost) || !isset($zadost->nazev) || empty($zadost->nazev)) {
            continue;
        } ?>
        <tr>
            <td><?= $this->Html->link($zadost->nazev, ['action' => 'dswZadost', $zadost->id]) ?></td>
            <td><?= $zadost->program ? $zadost->program->name : __('N/A') ?></td>
            <td><?= $zadost->ucel ? $zadost->ucel->name : __('N/A') ?></td>
            <td><?= $zadost->dotacni_obdobi ?></td>
            <td><?= Number::currency($zadost->pozadovana_castka, 'CZK') ?></td>
            <td><?= Number::currency($zadost->navrhovana_castka, 'CZK') ?></td>
            <td><?= Number::currency($zadost->konecna_castka, 'CZK') ?></td>
            <td class="text-center">
                <span class="d-none"><?= empty($zadost->ohlasovaci_formular) ? '1' : '0' ?></span>
                <?php if (empty($zadost->ohlasovaci_formular) && Router::routeExists(['action' => 'dswOhlasit', $zadost->id])): ?>
                    <?= $this->Html->link(__('Ohlásit akci/činnost'), ['action' => 'dswOhlasit', $zadost->id], ['class' => 'text-success']) ?>
                <?php else: ?>
                    <button type="button" data-toggle="modal" data-target="#modal-<?= $zadost->id ?>"
                            class="btn pr-2 <?= empty($zadost->ohlasovaci_formular) ? 'btn-danger' : 'btn-success' ?>">
                        &nbsp;
                    </button>
                    <?php if (!empty($zadost->ohlasovaci_formular)): ?>
                        <div class="modal fade" id="modal-<?= $zadost->id ?>" tabindex="-1" role="dialog"
                             aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <?php if (Router::routeExists(['action' => 'dswOhlasit', $zadost->id])) {
                                        echo $this->Html->link(__('Ohlásit akci/činnost'), ['action' => 'dswOhlasit', $zadost->id], ['class' => 'btn btn-success']);
                                        echo '<hr/>';
                                    }
                                    ?>
                                    <?php foreach ($zadost->ohlasovaci_formular as $ohlaseni) {
                                        echo $this->element('OldDsw.ohlaseni', ['ohlaseni' => $ohlaseni, 'card' => true]);
                                    } ?>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endif; ?>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
