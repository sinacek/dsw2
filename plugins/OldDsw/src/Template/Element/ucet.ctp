<?php
/**
 * @var $this AppView
 * @var $ucet Ucet
 */

if (!isset($ucet) || empty($ucet)) {
    return;
}

use App\View\AppView;
use OldDsw\Model\Entity\Ucet;

?>
<div class="row">
    <div class="col-md-6">
        <table class="table">
            <tr>
                <td>
                    <?= __d('olddsw', 'Název') ?>
                </td>
                <td>
                    <?= $ucet->nazev ?>
                </td>
            </tr>
            <tr>
                <td>
                    <?= __d('olddsw', 'Právní Forma') ?>
                </td>
                <td>
                    <?= $ucet->pravni_forma ?>
                </td>
            </tr>
            <tr>
                <td>
                    <?= __d('olddsw', 'Adresa') ?>
                </td>
                <td>
                    <?= $ucet->formatAddress() ?>
                </td>
            </tr>
            <?php if (!$ucet->isPO()): ?>
                <tr>
                    <td>
                        <?= __d('olddsw', 'Adresa doručovací') ?>
                    </td>
                    <td>
                        <?= $ucet->formatDorucovaciAddress() ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?= __d('olddsw', 'Datum Narození') ?>
                    </td>
                    <td>
                        <?= empty($ucet->fyzicke_osoby) ? '' : $ucet->fyzicke_osoby[0]->datum_narozeni ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?= __d('olddsw', 'Číslo OP') ?>
                    </td>
                    <td>
                        <?= empty($ucet->fyzicke_osoby) ? '' : $ucet->fyzicke_osoby[0]->cislo_OP ?>
                    </td>
                </tr>
            <?php endif; ?>
            <tr>
                <td>
                    <?= __d('olddsw', 'Telefon') ?>
                </td>
                <td>
                    <?= $ucet->telefon ?>
                </td>
            </tr>
            <tr>
                <td>
                    <?= __d('olddsw', 'Email') ?>
                </td>
                <td>
                    <?= $ucet->email ?>
                </td>
            </tr>
            <tr>
                <td>
                    <?= __d('olddsw', 'IČO') ?>
                </td>
                <td>
                    <?= $ucet->dic ?>
                </td>
            </tr>
            <tr>
                <td>
                    <?= __d('olddsw', 'DIČ') ?>
                </td>
                <td>
                    <?= $ucet->ico ?>
                </td>
            </tr>
        </table>
        <?php if ($ucet->isPO() && !empty($ucet->statutarni_organ)): ?>
            <strong>Statutární orgán, osoby zastupující právnickou osobu</strong>
            <table class="table">
                <tr>
                    <td>Jméno a Příjmení</td>
                    <td><?= $ucet->statutarni_organ[0]->formatFullName() ?></td>
                </tr>
                <tr>
                    <td>Právní důvod zastoupení</td>
                    <td><?= $ucet->statutarni_organ[0]->funkce ?></td>
                </tr>
                <tr>
                    <td>Telefon</td>
                    <td><?= $ucet->statutarni_organ[0]->telefon ?></td>
                </tr>
                <tr>
                    <td>E-mail</td>
                    <td><?= $ucet->statutarni_organ[0]->email ?></td>
                </tr>
            </table>
        <?php endif; ?>
    </div>
    <div class="col-md-6">
        <table class="table">
            <tr>
                <td>
                    <?= __d('olddsw', 'Bankovní spojení') ?>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <?= __d('olddsw', 'Název Banky') ?>
                </td>
                <td>
                    <?= $ucet->nazev_banky ?>
                </td>
            </tr>
            <tr>
                <td>
                    <?= __d('olddsw', 'Číslo účtu / Kód banky') ?>
                </td>
                <td>
                    <?= $ucet->cislo_uctu . '/' . $ucet->kod_banky ?>
                </td>
            </tr>
        </table>
    </div>
</div>