<?php
/**
 * @var $this AppView
 * @var $ohlaseni OhlasovaciFormular
 */

use App\View\AppView;
use Cake\I18n\Number;
use OldDsw\Model\Entity\OhlasovaciFormular;

$typy_ohlaseni = [
    0 => 'akce',
    1 => 'dílo',
    2 => 'celoroční činnost'
];
$card = (isset($card) && is_bool($card)) ? $card : false;

?>

<?php if ($card): ?>
    <div class="card">
    <div class="card-header">
<?php endif; ?>
    <strong>Ohlášení (odesláno <?= $ohlaseni->added_when ? $ohlaseni->added_when->nice() : '' ?>)</strong>
<?php if ($card): ?>
    </div>
    <div class="card-body">
    <table class="table">
        <tr>
            <td><strong>Zahájení</strong></td>
            <td><?= $ohlaseni->datum_zahajeni ? $ohlaseni->datum_zahajeni->nice() : '' ?></td>
        </tr>
        <tr>
            <td><strong>Ukončení</strong></td>
            <td><?= $ohlaseni->datum_ukonceni ? $ohlaseni->datum_ukonceni->nice() : '' ?></td>
        </tr>
        <tr>
            <td><strong>Typ ohlášení</strong></td>
            <td><?= $typy_ohlaseni[$ohlaseni->typ] ?></td>
        </tr>
        <tr>
            <td><strong>Místo konání</strong></td>
            <td><?= $ohlaseni->misto_konani ?></td>
        </tr>
        <tr>
            <td><strong>Garant</strong></td>
            <td><?= sprintf("%s (%s %s)", $ohlaseni->garant, $ohlaseni->garant_telefon, $ohlaseni->garant_email) ?></td>
        </tr>
        <tr>
            <td><strong>Poznámka</strong></td>
            <td><?= $ohlaseni->poznamka ?></td>
        </tr>
        <tr>
            <td><strong>Předpokládaný počet účastníků</strong></td>
            <td><?= $ohlaseni->pocet_ucastniku ?></td>
        </tr>
        <tr>
            <td><strong>Čerpaná částka</strong></td>
            <td><?= Number::currency($ohlaseni->cerpana_castka, 'CZK') ?></td>
        </tr>
        <tr>
            <td><strong>Příloha</strong></td>
            <td><?= empty($ohlaseni->prilohy) ? 'Bez přílohy' : $ohlaseni->prilohy->orig_filename ?></td>
        </tr>
    </table>
<?php endif; ?>
<?php if ($card): ?>
    </div>
    </div>
<?php endif; ?>