<?php
/**
 * @var $this AppView
 * @var $accounts Ucet[]
 */

use App\View\AppView;
use OldDsw\Model\Entity\Ucet;

$this->assign('title', __d('olddsw', 'Archiv žadatelů staré verze DSW'));
echo $this->element('simple_datatable');
?>
<h1><?= $this->fetch('title') ?></h1>
<table id="dtable" class="table">
    <thead>
    <tr>
        <th><?= __d('olddsw', 'Název / Jméno Příjmení') ?></th>
        <th><?= __d('olddsw', 'Adresa') ?></th>
        <th><?= __d('olddsw', 'IČO') ?></th>
        <th><?= __d('olddsw', 'Telefon') ?></th>
        <th><?= __d('olddsw', 'Právní Forma') ?></th>
        <th><?= __d('olddsw', 'Vytvořeno') ?></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($accounts as $account): ?>
        <tr>
            <td><?= $this->Html->link($account->name_with_year, ['action' => 'dswDetail', $account->id]) ?></td>
            <td><?= $account->formatAddress() ?></td>
            <td><?= $account->ico ?></td>
            <td><?= $account->telefon ?></td>
            <td><?= $account->pravni_forma ?></td>
            <td><?= $account->added_when ? $account->added_when->toDateString() : __('N/A') ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
