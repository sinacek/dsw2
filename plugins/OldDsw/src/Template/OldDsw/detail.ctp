<?php
/**
 * @var $this AppView
 * @var $account Ucet
 */

use App\View\AppView;
use Cake\Routing\Router;
use OldDsw\Model\Entity\Ucet;

$this->assign('title', $account->nazev);
?>
    <h1><?= $this->fetch('title') ?></h1>
<?php
$route = ['action' => 'dswHideAccount', $account->id];
if (Router::routeExists($route) && !$account->is_hidden) {
    echo $this->Html->link(__('Skrýt tento účet'), $route, ['class' => 'btn btn-warning']);
}
?>
<?= $this->element('OldDsw.ucet', ['ucet' => $account]) ?>
    <hr/>
    <h2><?= __d('olddsw', 'Archiv žádostí') ?></h2>
<?= $this->element('OldDsw.zadosti', ['zadosti' => $account->zadosti]) ?>