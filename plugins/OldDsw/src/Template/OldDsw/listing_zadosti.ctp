<?php
/**
 * @var $this AppView
 * @var $zadosti Zadost[]
 */

use App\View\AppView;
use Cake\I18n\Number;
use OldDsw\Model\Entity\Zadost;

$this->assign('title', __d('olddsw', 'Archiv žádostí staré verze DSW'));
echo $this->element('simple_datatable');
?>
    <h1><?= $this->fetch('title') ?></h1>

<?= $this->element('OldDsw.zadosti', compact('zadosti'));