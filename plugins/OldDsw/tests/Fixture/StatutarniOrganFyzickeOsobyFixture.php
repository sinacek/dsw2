<?php
namespace OldDsw\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * StatutarniOrganFyzickeOsobyFixture
 */
class StatutarniOrganFyzickeOsobyFixture extends TestFixture
{
    /**
     * Table name
     *
     * @var string
     */
    public $table = 'statutarni_organ_fyzicke_osoby';
    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'account_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => '0', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'fyzicke_osoby_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => '0', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['account_id', 'fyzicke_osoby_id'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd
    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'account_id' => 1,
                'fyzicke_osoby_id' => 1,
            ],
        ];
        parent::init();
    }
}
