<?php
namespace OldDsw\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * RozpoctyProjektuFixture
 */
class RozpoctyProjektuFixture extends TestFixture
{
    /**
     * Table name
     *
     * @var string
     */
    public $table = 'rozpocty_projektu';
    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'zadost_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'vyse_dotace' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => false, 'default' => '0', 'comment' => ''],
        'dotace_z_ministerstva_cr' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => false, 'default' => '0', 'comment' => ''],
        'dotace_z_eu' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => false, 'default' => '0', 'comment' => ''],
        'dotace_z_mhmp' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => false, 'default' => '0', 'comment' => ''],
        'celkove_vynosy' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => false, 'default' => '0', 'comment' => ''],
        'celkove_naklady' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => false, 'default' => '0', 'comment' => ''],
        'dotace_z_ministerstva_cr_projekt' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'dotace_z_eu_projekt' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'dotace_z_mhmp_projekt' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['zadost_id'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd
    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'zadost_id' => 1,
                'vyse_dotace' => 1,
                'dotace_z_ministerstva_cr' => 1,
                'dotace_z_eu' => 1,
                'dotace_z_mhmp' => 1,
                'celkove_vynosy' => 1,
                'celkove_naklady' => 1,
                'dotace_z_ministerstva_cr_projekt' => 'Lorem ipsum dolor sit amet',
                'dotace_z_eu_projekt' => 'Lorem ipsum dolor sit amet',
                'dotace_z_mhmp_projekt' => 'Lorem ipsum dolor sit amet',
            ],
        ];
        parent::init();
    }
}
