<?php
namespace OldDsw\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * PrilohyFormulareProhlaseniOVerejnePodporeFixture
 */
class PrilohyFormulareProhlaseniOVerejnePodporeFixture extends TestFixture
{
    /**
     * Table name
     *
     * @var string
     */
    public $table = 'prilohy_formulare_prohlaseni_o_verejne_podpore';
    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'zadost_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => '0', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'poskytnuti_verejne_podpory' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => '0', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'rezim_podpory' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => '0', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['zadost_id'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd
    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'zadost_id' => 1,
                'poskytnuti_verejne_podpory' => 1,
                'rezim_podpory' => 1,
            ],
        ];
        parent::init();
    }
}
