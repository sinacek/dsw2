<?php
namespace OldDsw\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use OldDsw\Model\Table\ProgramyTable;

/**
 * OldDsw\Model\Table\ProgramyTable Test Case
 */
class ProgramyTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \OldDsw\Model\Table\ProgramyTable
     */
    public $Programy;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.OldDsw.Programy',
        'plugin.OldDsw.Fonds',
        'plugin.OldDsw.DotacniFondData',
        'plugin.OldDsw.GrantAttachmentSettings',
        'plugin.OldDsw.GrantRequests',
        'plugin.OldDsw.NastaveniAlokovaneCastky',
        'plugin.OldDsw.PrilohyFormulareNastaveni',
        'plugin.OldDsw.Purposes',
        'plugin.OldDsw.Roles',
        'plugin.OldDsw.Zadosti',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Programy') ? [] : ['className' => ProgramyTable::class];
        $this->Programy = TableRegistry::getTableLocator()->get('Programy', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Programy);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
