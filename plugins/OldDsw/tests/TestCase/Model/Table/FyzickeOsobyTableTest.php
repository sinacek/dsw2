<?php
namespace OldDsw\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use OldDsw\Model\Table\FyzickeOsobyTable;

/**
 * OldDsw\Model\Table\FyzickeOsobyTable Test Case
 */
class FyzickeOsobyTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \OldDsw\Model\Table\FyzickeOsobyTable
     */
    public $FyzickeOsoby;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.OldDsw.FyzickeOsoby',
        'plugin.OldDsw.OsobySPodilemFyzickeOsoby',
        'plugin.OldDsw.StatutarniOrganFyzickeOsoby',
        'plugin.OldDsw.Ucty',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('FyzickeOsoby') ? [] : ['className' => FyzickeOsobyTable::class];
        $this->FyzickeOsoby = TableRegistry::getTableLocator()->get('FyzickeOsoby', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->FyzickeOsoby);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
