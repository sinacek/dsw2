<?php
namespace OldDsw\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use OldDsw\Model\Table\StatutarniOrganFyzickeOsobyTable;

/**
 * OldDsw\Model\Table\StatutarniOrganFyzickeOsobyTable Test Case
 */
class StatutarniOrganFyzickeOsobyTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \OldDsw\Model\Table\StatutarniOrganFyzickeOsobyTable
     */
    public $StatutarniOrganFyzickeOsoby;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.OldDsw.StatutarniOrganFyzickeOsoby',
        'plugin.OldDsw.Accounts',
        'plugin.OldDsw.FyzickeOsobies',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('StatutarniOrganFyzickeOsoby') ? [] : ['className' => StatutarniOrganFyzickeOsobyTable::class];
        $this->StatutarniOrganFyzickeOsoby = TableRegistry::getTableLocator()->get('StatutarniOrganFyzickeOsoby', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->StatutarniOrganFyzickeOsoby);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
