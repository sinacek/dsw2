<?php
namespace OldDsw\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use OldDsw\Model\Table\UctyFyzickeOsobyTable;

/**
 * OldDsw\Model\Table\UctyFyzickeOsobyTable Test Case
 */
class UctyFyzickeOsobyTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \OldDsw\Model\Table\UctyFyzickeOsobyTable
     */
    public $UctyFyzickeOsoby;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.OldDsw.UctyFyzickeOsoby',
        'plugin.OldDsw.Ucties',
        'plugin.OldDsw.FyzickeOsobies',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('UctyFyzickeOsoby') ? [] : ['className' => UctyFyzickeOsobyTable::class];
        $this->UctyFyzickeOsoby = TableRegistry::getTableLocator()->get('UctyFyzickeOsoby', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UctyFyzickeOsoby);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
