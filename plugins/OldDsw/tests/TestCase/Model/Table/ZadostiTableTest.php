<?php
namespace OldDsw\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use OldDsw\Model\Table\ZadostiTable;

/**
 * OldDsw\Model\Table\ZadostiTable Test Case
 */
class ZadostiTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \OldDsw\Model\Table\ZadostiTable
     */
    public $Zadosti;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.OldDsw.Zadosti',
        'plugin.OldDsw.Ucets',
        'plugin.OldDsw.Fonds',
        'plugin.OldDsw.Programs',
        'plugin.OldDsw.Ucels',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Zadosti') ? [] : ['className' => ZadostiTable::class];
        $this->Zadosti = TableRegistry::getTableLocator()->get('Zadosti', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Zadosti);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
