<?php
namespace OldDsw\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use OldDsw\Model\Table\DefiniceProjektuTable;

/**
 * OldDsw\Model\Table\DefiniceProjektuTable Test Case
 */
class DefiniceProjektuTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \OldDsw\Model\Table\DefiniceProjektuTable
     */
    public $DefiniceProjektu;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.OldDsw.DefiniceProjektu',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('DefiniceProjektu') ? [] : ['className' => DefiniceProjektuTable::class];
        $this->DefiniceProjektu = TableRegistry::getTableLocator()->get('DefiniceProjektu', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DefiniceProjektu);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
