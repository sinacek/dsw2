<?php
namespace OldDsw\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use OldDsw\Model\Table\FondyTable;

/**
 * OldDsw\Model\Table\FondyTable Test Case
 */
class FondyTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \OldDsw\Model\Table\FondyTable
     */
    public $Fondy;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.OldDsw.Fondy',
        'plugin.OldDsw.GrantAttachmentSettings',
        'plugin.OldDsw.GrantRequests',
        'plugin.OldDsw.GridsColumnsSettings',
        'plugin.OldDsw.PrilohyFormulareNastaveni',
        'plugin.OldDsw.Programs',
        'plugin.OldDsw.Roles',
        'plugin.OldDsw.Zadosti',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Fondy') ? [] : ['className' => FondyTable::class];
        $this->Fondy = TableRegistry::getTableLocator()->get('Fondy', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Fondy);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
