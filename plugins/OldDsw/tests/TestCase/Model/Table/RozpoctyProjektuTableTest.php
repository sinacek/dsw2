<?php
namespace OldDsw\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use OldDsw\Model\Table\RozpoctyProjektuTable;

/**
 * OldDsw\Model\Table\RozpoctyProjektuTable Test Case
 */
class RozpoctyProjektuTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \OldDsw\Model\Table\RozpoctyProjektuTable
     */
    public $RozpoctyProjektu;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.OldDsw.RozpoctyProjektu',
        'plugin.OldDsw.Naklady',
        'plugin.OldDsw.VlastniZdroje',
        'plugin.OldDsw.Vynosy',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('RozpoctyProjektu') ? [] : ['className' => RozpoctyProjektuTable::class];
        $this->RozpoctyProjektu = TableRegistry::getTableLocator()->get('RozpoctyProjektu', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->RozpoctyProjektu);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
