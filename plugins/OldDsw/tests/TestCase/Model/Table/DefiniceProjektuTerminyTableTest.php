<?php
namespace OldDsw\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use OldDsw\Model\Table\DefiniceProjektuTerminyTable;

/**
 * OldDsw\Model\Table\DefiniceProjektuTerminyTable Test Case
 */
class DefiniceProjektuTerminyTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \OldDsw\Model\Table\DefiniceProjektuTerminyTable
     */
    public $DefiniceProjektuTerminy;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.OldDsw.DefiniceProjektuTerminy',
        'plugin.OldDsw.Zadosts',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('DefiniceProjektuTerminy') ? [] : ['className' => DefiniceProjektuTerminyTable::class];
        $this->DefiniceProjektuTerminy = TableRegistry::getTableLocator()->get('DefiniceProjektuTerminy', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DefiniceProjektuTerminy);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
