<?php
namespace OldDsw\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use OldDsw\Model\Table\RozpoctyProjektuNakladyInvesticniTable;

/**
 * OldDsw\Model\Table\RozpoctyProjektuNakladyInvesticniTable Test Case
 */
class RozpoctyProjektuNakladyInvesticniTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \OldDsw\Model\Table\RozpoctyProjektuNakladyInvesticniTable
     */
    public $RozpoctyProjektuNakladyInvesticni;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.OldDsw.RozpoctyProjektuNakladyInvesticni',
        'plugin.OldDsw.Zadosts',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('RozpoctyProjektuNakladyInvesticni') ? [] : ['className' => RozpoctyProjektuNakladyInvesticniTable::class];
        $this->RozpoctyProjektuNakladyInvesticni = TableRegistry::getTableLocator()->get('RozpoctyProjektuNakladyInvesticni', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->RozpoctyProjektuNakladyInvesticni);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
