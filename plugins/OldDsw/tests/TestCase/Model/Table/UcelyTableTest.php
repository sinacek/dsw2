<?php
namespace OldDsw\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use OldDsw\Model\Table\UcelyTable;

/**
 * OldDsw\Model\Table\UcelyTable Test Case
 */
class UcelyTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \OldDsw\Model\Table\UcelyTable
     */
    public $Ucely;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.OldDsw.Ucely',
        'plugin.OldDsw.Programs',
        'plugin.OldDsw.DotacniFondData',
        'plugin.OldDsw.GrantAttachmentSettings',
        'plugin.OldDsw.GrantRequests',
        'plugin.OldDsw.Hodnoceni',
        'plugin.OldDsw.PrilohyFormulareNastaveni',
        'plugin.OldDsw.Questionnaires',
        'plugin.OldDsw.RatingQuestionnaires',
        'plugin.OldDsw.RozvojData',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Ucely') ? [] : ['className' => UcelyTable::class];
        $this->Ucely = TableRegistry::getTableLocator()->get('Ucely', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Ucely);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
