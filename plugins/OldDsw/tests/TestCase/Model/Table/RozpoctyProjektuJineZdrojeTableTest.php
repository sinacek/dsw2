<?php
namespace OldDsw\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use OldDsw\Model\Table\RozpoctyProjektuJineZdrojeTable;

/**
 * OldDsw\Model\Table\RozpoctyProjektuJineZdrojeTable Test Case
 */
class RozpoctyProjektuJineZdrojeTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \OldDsw\Model\Table\RozpoctyProjektuJineZdrojeTable
     */
    public $RozpoctyProjektuJineZdroje;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.OldDsw.RozpoctyProjektuJineZdroje',
        'plugin.OldDsw.Zadosts',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('RozpoctyProjektuJineZdroje') ? [] : ['className' => RozpoctyProjektuJineZdrojeTable::class];
        $this->RozpoctyProjektuJineZdroje = TableRegistry::getTableLocator()->get('RozpoctyProjektuJineZdroje', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->RozpoctyProjektuJineZdroje);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
