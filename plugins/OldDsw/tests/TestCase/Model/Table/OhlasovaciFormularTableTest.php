<?php
namespace OldDsw\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use OldDsw\Model\Table\OhlasovaciFormularTable;

/**
 * OldDsw\Model\Table\OhlasovaciFormularTable Test Case
 */
class OhlasovaciFormularTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \OldDsw\Model\Table\OhlasovaciFormularTable
     */
    public $OhlasovaciFormular;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.OldDsw.OhlasovaciFormular',
        'plugin.OldDsw.Zadosts',
        'plugin.OldDsw.Prilohas',
        'plugin.OldDsw.AddedByUzivatels',
        'plugin.OldDsw.Prilohy',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('OhlasovaciFormular') ? [] : ['className' => OhlasovaciFormularTable::class];
        $this->OhlasovaciFormular = TableRegistry::getTableLocator()->get('OhlasovaciFormular', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->OhlasovaciFormular);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
