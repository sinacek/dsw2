<?php
namespace OldDsw\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use OldDsw\Model\Table\OhlasovaciFormularPrilohyTable;

/**
 * OldDsw\Model\Table\OhlasovaciFormularPrilohyTable Test Case
 */
class OhlasovaciFormularPrilohyTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \OldDsw\Model\Table\OhlasovaciFormularPrilohyTable
     */
    public $OhlasovaciFormularPrilohy;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.OldDsw.OhlasovaciFormularPrilohy',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('OhlasovaciFormularPrilohy') ? [] : ['className' => OhlasovaciFormularPrilohyTable::class];
        $this->OhlasovaciFormularPrilohy = TableRegistry::getTableLocator()->get('OhlasovaciFormularPrilohy', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->OhlasovaciFormularPrilohy);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
