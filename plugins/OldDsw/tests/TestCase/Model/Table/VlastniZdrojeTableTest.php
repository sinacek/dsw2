<?php
namespace OldDsw\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use OldDsw\Model\Table\VlastniZdrojeTable;

/**
 * OldDsw\Model\Table\VlastniZdrojeTable Test Case
 */
class VlastniZdrojeTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \OldDsw\Model\Table\VlastniZdrojeTable
     */
    public $VlastniZdroje;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.OldDsw.VlastniZdroje',
        'plugin.OldDsw.Requests',
        'plugin.OldDsw.RozpoctyProjektu',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('VlastniZdroje') ? [] : ['className' => VlastniZdrojeTable::class];
        $this->VlastniZdroje = TableRegistry::getTableLocator()->get('VlastniZdroje', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->VlastniZdroje);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
