<?php
namespace OldDsw\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use OldDsw\Model\Table\RozpoctyProjektuNakladyTable;

/**
 * OldDsw\Model\Table\RozpoctyProjektuNakladyTable Test Case
 */
class RozpoctyProjektuNakladyTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \OldDsw\Model\Table\RozpoctyProjektuNakladyTable
     */
    public $RozpoctyProjektuNaklady;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.OldDsw.RozpoctyProjektuNaklady',
        'plugin.OldDsw.Zadosts',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('RozpoctyProjektuNaklady') ? [] : ['className' => RozpoctyProjektuNakladyTable::class];
        $this->RozpoctyProjektuNaklady = TableRegistry::getTableLocator()->get('RozpoctyProjektuNaklady', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->RozpoctyProjektuNaklady);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
