<?php
namespace OldDsw\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use OldDsw\Model\Table\RozpoctyProjektuVlastniZdrojeTable;

/**
 * OldDsw\Model\Table\RozpoctyProjektuVlastniZdrojeTable Test Case
 */
class RozpoctyProjektuVlastniZdrojeTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \OldDsw\Model\Table\RozpoctyProjektuVlastniZdrojeTable
     */
    public $RozpoctyProjektuVlastniZdroje;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.OldDsw.RozpoctyProjektuVlastniZdroje',
        'plugin.OldDsw.Zadosts',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('RozpoctyProjektuVlastniZdroje') ? [] : ['className' => RozpoctyProjektuVlastniZdrojeTable::class];
        $this->RozpoctyProjektuVlastniZdroje = TableRegistry::getTableLocator()->get('RozpoctyProjektuVlastniZdroje', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->RozpoctyProjektuVlastniZdroje);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
