<?php
namespace OldDsw\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use OldDsw\Model\Table\PrilohyFormulareProhlaseniOVerejnePodporeTable;

/**
 * OldDsw\Model\Table\PrilohyFormulareProhlaseniOVerejnePodporeTable Test Case
 */
class PrilohyFormulareProhlaseniOVerejnePodporeTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \OldDsw\Model\Table\PrilohyFormulareProhlaseniOVerejnePodporeTable
     */
    public $PrilohyFormulareProhlaseniOVerejnePodpore;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.OldDsw.PrilohyFormulareProhlaseniOVerejnePodpore',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('PrilohyFormulareProhlaseniOVerejnePodpore') ? [] : ['className' => PrilohyFormulareProhlaseniOVerejnePodporeTable::class];
        $this->PrilohyFormulareProhlaseniOVerejnePodpore = TableRegistry::getTableLocator()->get('PrilohyFormulareProhlaseniOVerejnePodpore', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PrilohyFormulareProhlaseniOVerejnePodpore);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
