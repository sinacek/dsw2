// original from https://github.com/tangien/quilljs-textarea/blob/59daef9494ca76004f5fd98f7156dbb1486bf9a1/quill-textarea.js

function quilljs_textarea(elem = null, options = null) {
    let editorElems = [];
    if (elem) {
        editorElems = Array.prototype.slice.call(document.querySelectorAll(elem));
    } else {
        editorElems = Array.prototype.slice.call(document.querySelectorAll('[data-quilljs]'));
    }
    editorElems.forEach(function (el) {
        // added blacklist ability through data-noquilljs attribute
        if ((elem && el.hasAttribute("data-quilljs")) || el.hasAttribute('data-noquilljs')) {
            return;
        }
        const elemType = el.type;
        let placeholder = null;
        let editorDiv = el;
        if (elemType === 'textarea') {
            editorDiv = document.createElement('div');
            editorDiv.innerHTML = el.value;
            el.parentNode.insertBefore(editorDiv, el.nextSibling);
            el.style.display = "none";
            placeholder = el.placeholder;
        }
        const toolbarOptions = [
            ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
            ['blockquote', 'code-block'],

            [{'header': 1}, {'header': 2}],               // custom button values
            [{'list': 'ordered'}, {'list': 'bullet'}],
            [{'script': 'sub'}, {'script': 'super'}],      // superscript/subscript
            [{'indent': '-1'}, {'indent': '+1'}],          // outdent/indent
            [{'direction': 'rtl'}],                         // text direction

            [{'size': ['small', false, 'large', 'huge']}],  // custom dropdown
            [{'header': [1, 2, 3, 4, 5, 6, false]}],

            [{'color': []}, {'background': []}],          // dropdown with defaults from theme
            [{'align': []}],
            ['link', 'image', 'video'],
            ['clean']                                         // remove formatting button
        ];
        let default_options = {
            modules: {
                toolbar: {
                    container: toolbarOptions,
                    handlers: {
                        image: imageHandler
                    }
                }
            }
        };
        if (!options) {
            default_options = Object.assign({}, default_options, {
                theme: 'snow',
                placeholder: placeholder,
            });
        } else {
            if (!options.placeholder) {
                options.placeholder = placeholder;
            }
            default_options = Object.assign({}, options, default_options);
        }

        const editor = new Quill(editorDiv, default_options);
        editor.on('text-change', function (delta, oldDelta, source) {
            el.value = editor.root.innerHTML;
        });
    });
}

function imageHandler() {
    let range = this.quill.getSelection();
    let value = prompt('Vložte odkaz na obrázek');
    if (value) {
        this.quill.insertEmbed(range.index, 'image', value, Quill.sources.USER);
    }
}

(function () {
    quilljs_textarea();
})();