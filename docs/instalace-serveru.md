# Zběžná dokumentace instalace serveru

## Instalace

- Předpokládá se využití PHP 7.4+, MariaDB/MySQL DB, Nginx/Apache webserveru a Composer (PHP aplikace pro správu závislostí)
- Předpokládá se že k aplikaci budete přistupovat pomocí zabezpečeného protokolu HTTPS
- Instalace se řídí obecně požadavky frameworku CakePHP verze 3.8+ viz. [Installation - Cookbook](https://book.cakephp.org/3/en/installation.html)
- GIT repozitář umístíme do vhodné složky podle webového serveru a operačního systému
- Nginx `root` nebo Apache2 `DocumentRoot` nasměrujeme absolutní cestou do složky `webroot` v projektu
- V adresáři projektu spustíme 'composer install' ([Instrukce jak získat Composer](https://getcomposer.org/download/))
- Musí být instalováno a použito PHP verze 7.4 nebo vyšší, s aktivními rozšířeními [intl, mbstring, xml, mysql, curl, http, openssl]
- Přejmenujeme soubor config/app_local.example.php na config/app_local.php a upravíme/doplníme hodnoty pro připojení k DB a Security.salt
- V DB vytvoříme databázi a vložíme tabulky/klíče pomocí prázdného template ze souboru `docs/sql/schema.sql`
- V tabulce `organizations` vytvoříme vlastní organizaci
- V tabulce `domains` vytvoříme doménu s vazbou na vytvořenou organizaci a ve sloupci `is_enabled` vyplníme hodnotu `1`
- Nyní je aplikace instalována a můžete se zaregistrovat do aplikace standardně přes webové rozhraní, první registrovaný uživatel obdrží administrátorská oprávnění nad celou instalací DSW2

### Vycházíme z distribuce Debian Buster (10.3)

## APT

  - konfigurace viz. adresář etc/apt v repozitáři
  - využíváme backports a php buster/main z distribuce sury.org
  - zbežný seznam instalovaných balíků je v souboru etc/apt/dpkg_l a etc/apt/apt_list_installed

## Nginx

  - nginx je provozován za reverzní proxy, která zajišťuje load-balance a ssl/tls zabezpečení
  - konfigurace nginx dovoluje všem ip/dns záznamům přístup k aplikaci, filtrování se pak řídí nastavenými doménami v aplikaci
  - aplikace je v adresáři /var/www/html, root aplikace pak v podsložce webroot, vlastněná www-data:www-data

## PHP

  - php konfigurace viz. adresář etc/php
  - php je využíváno ve verzi 7.4 a variantě php-fpm s nastaveným poolem pro uživatele/skupinu www-data:www-data
  - konfigurace vazby mezi php soubory a php-fpm poolem je v konfiguraci etc/nginx/sites-enabled/default příp. v konfiguraci php poolu etc/php/7.4/php-fpm/pool.d/www.conf

## MariaDB / MySQL

  - využíváme mariadb-server verze 10.4 z oficiální distribuce mariadb (viz. příslušný apt repozitář na vpsfree.cz)
  - databáze nepoužívá jakkoli upravenou provozní konfiguraci
  - konfigurace spojení je v souboru config/app_local.php v sekci Datasources/default

## Redis

  - využíváme defaultní instalace / instance Redis serveru, bez autentifikace, jako cache-engine
  - spojení s Redis jde po localhost / loopback interface
  - konfigurace spojení je v souboru config/app.php v sekci Cache

## Cron

  - Pro automaticky zpracovávané akce je třeba nastavit cronjob s oprávněními uživatele PHP (viz. níže) a nastavit soubor `bin/cake` jako spustitelný (`chmod +x bin/cake`)
  - cronjob pak vypadá takto `0 * * * * /var/www/html/bin/cake cron`, tj. každý den, vždy o půnoci, provést automatické akce

## Firewall

  - pro zjednodušení používáme balík ufw s konfigurací, kdy je po IPv4 a IPv6 povolen jen port 80 a 22
  - po instalaci ufw stačí 2 příkazy, `ufw allow http` a `ufw allow ssh`, tím je běžný firewall kompletní

## Poznámky

  - Pro provoz aplikace není zapotřebí lokálně konfigurovaný postfix (vše lze řešit z aplikační vrstvy)
  - Pro automatickou instalaci bezpečnostních aktualizací je nastaven balík unattended-upgrades
