# Technický dokument: Nastavení parametrů organizace

*sekce označné WIP, jsou aktuálně ve vývoji a nemusí být kompletně zpřístupněny/zdokumentovány*

## Emaily

DSW2 e-maily jen odesílá, nepřijímá je.

Emaily mohou být posílány skrze server mx.otevrenamesta.cz, pokud máte zájem,
tak je potřeba se domluvit s Otevřenými Městy na poskytnutí přístupových údajů,
a parametrů pro SPF/DKIM/DMARC zamezpečení

Pokud chcete využít vlastní e-mailový server, musíte disponovat uživatelem s
úrovní oprávnění "Správce Dotačního Portálu" ve vaší Organizaci

Po přihlášení, v menu Admistrátor > Správa organizací, vyberte akci "Upravit" u
své organizace a v sekci "Nastavení e-mailů" vyplňte následující

- E-mailový server (např. mx.otevrenamesta.cz)
- Port SMTP (např. 25, 465, 587 nebo jiný)
- Nastavení zda se využívá TLS zabezpečení
- Uživatelské jméno a heslo pro přihlášení
- E-mailová adresa odesílatele (např. noreply@dotace.praha.eu)
- Reply-To, tj. adresa kam se mají doručovat potenciální odpovědi na aplikací odeslané e-maily

Otestovat nastavení e-mailů lze např. požádáním o obnovu hesla (jako nepřihlášený
uživatel na adrese {domena.tld}/password_recovery )

## Autorizace e-mailů

Pokud e-maily odesíláte skrze infrastrukturu mx.otevrenamesta.cz, pak je pro ověření e-mailů třeba autorizace,

1. záznamy SPF a DKIM, viz. níže v sekci Domény
2. adresa odesílatele (typicky `noreply@dotace.mojedomena.cz`)
3. adresy Reply-To, tj. pro sběr odpovědí od uživatelů

V případě vlastní infrastruktury byste toto již měli mít vyřešené

## Domény

Jako správce organizace, máte právo přidávat, domény, které náleží k vaší aplikaci

Pro aktivaci SSL/TLS certifikátu na vlastní doméně je potřeba kontaktovat nás na
dsw2@otevrenamesta.cz, pro manuální přidání a aktivaci certifikátu skrze proxy.otevrenamesta.cz

Pokud přidáváte vlastní doménu, nastavte ji prosím takto:
```
  dotace.mojedomena.cz. CNAME proxy.otevrenamesta.cz.
```
Tím získáme i SPF autorizaci, pokud budete chtít využívat i náš e-mailový server,
v e-mailové žádosti přidejte, že máte zájem o DKIM klíč, který následně přidáte do DNS takto:
```
  mail._domainkey.dotace.mojedomena.cz. TXT "v=DKIM1; k=rsa; p={zde klíč který vám zašleme}"
```

## Datové Schránky - ISDS - Odesílací Brána a Autentizační Služba

V nastavení Organizace, je potřeba definovat min. tyto parametry

1. ID datové schránky organizace - generované kontaktní údaje a nastavení cíle uživatelského
konceptu žádosti, pokud se podává skrze DS
2. Odesílací Brána
   1. V nastavení DS vaší organizace je třeba registrovat "Odesílací Brána"
   2. Informace jsou uvedeny v Technické Příloze č. 2, specifikace ISDS,
   viz. přiložené PDF "docs/isds/OdesilaciBrana_ISDS.pdf"
   3. V nastavení "Odesílací Brány" v prostředí ISDS jako návratovou URL zvolte `https://domena.moje-organizace.cz/user/isds_auth/${sessionId}`
      - tj. například `https://dotace.praha3.cz/user/isds_auth/${sessionId}`
   4. V nastavení organizace v DSW2 je pak potřeba zavést ID odesílací brány (atsID), certifikát a odpovídající privátní klíč, který je v ISDS zaveden
3. Autentizační Služba
   1. Je součástí nastavení Odesílací Brány
   2. Certifikát je společný pro Odesílací Bránu i Autentizační Službu
   3. Parametry Autentizační služby a doporučení jsou popsány níže
   4. Pokud je Autentizační Služba nastavena a zpřístupněna, v nastavení DSW2 není potřeba dále cokoli nastavovat

OB - Odesílací Brána - neumožňuje DSW2 odesílat zprávy z DS organizace, ale pouze

1. Přihlásit uživatele datovou schránkou (tj. ověřit vazbu mezi uživatelem DSW2 a ID datové schránky)
2. Vytvořit koncept vyplněné žádosti o dotaci a tento uživateli vložit do DS ke schválení a odeslání

AS - Autentizační Služba - (pokud je nakonfigurována) pak umožní ověřit identitu přihlášeného uživatele,
podle nastavení AS získá DSW2 jeden nebo více identifikátorů uživatele, jako je např.

- ID přihlášené datové schránky
- TYP datové schránky (fyzická osoba, právnická osoba, ovm)
- Název subjektu
- IČO subjektu

Pro ověření datové schránky žadatele, je potřeba mít povolen alespoň atribute "Identifikátor datové schránky",
pro ověření dalších informací pak použito vše co povolíte (typ schránky, stav, název schránky, název subjektu, ičo subjektu, atp.)

Dokumentace k nastavení OB a AS je např. zde https://www.mojedatovaschranka.cz/static/ISDS/help/page8.html#8_4_5

## WIP: Spisová služba

DSW2 podporuje Spisovou službu, která implementuje NSESSS (Národní standard pro elektronické systémy spisové služby),
pokud spisová služba, kterou využíváte, podporuje tento národní standard, stačí nastavit přístupové parametry, tj.

1. URL adresa REST služby
2. Přístupové údaje (uživatelské jméno, heslo, uživatelský certifikát)
3. Zabezpečení přenosu (např. pinning CA certifikátu PostSignum nebo jiného, který zabezpečuje veřejný REST endpoint)

Pokud vaše spisová služba NSESSS nepodporuje, a není možné aby jej podporovala (časově, finančně, ...), pak místo spisové služby,
máte následující varianty:

1. Konfigurovat DSW2 jako Spisovou službu, tj. registrovat "Certifikát spisové služby", pak by DSW2 odesílalo události skrze nastavenou DS
2. Použít e-mail pro notifikaci účastníků a soubory žádosti a příloh, příp. následných vyrozumění a upozornění, manuálně zavádět do vlastní spisovky

## WIP: Způsob odesílání vyrozumění nebo upozornění

DSW2 poskytuje informační servis v těchto místech dotačního procesu

1. Pokud po odeslání žádosti, je vyžadována úprava žádosti, po provedené formální kontrole
2. Při odeslání vyrozumění o přidělení nebo nepřidělení podpory
3. Při výzvě k doplnění chybějícího ohlášení činnosti/akce
4. Při výzvě k doplnění vyúčtování dotace

Tyto upozornění mohou odcházet pomocí jednotlivých dostupných komunikačních prostředků, v pořadí preference
1. Spisová služba (která automaticky odesílá pomocí DS)
2. E-mail

Technicky lze nastavit
1. Povolené možnosti (tj. zda odesílat e-mailem, spisovkou nebo oběma zároveň)
2. Preferované pořadí (pokud není vyžadováno ID datové schránky žadatele, tak možnost odeslat DS pokud je, v opačném případě e-mailem)

## Logo + Favicon

Odkaz na logo a favicon by měl vést na zdroj, který splňuje následující

1. Poskytuje data zabezpečeně (HTTPS)
2. Pro dané soubory, poskytuje správný Content-Type, tj. pro favicon ".ico" soubor s typem "image/x-icon", pro logo pak ".png" soubor s typem "image/png", ostatní nemusí správně fungovat ve všech prohlížečích

## Google Analytics

Pokud máte zájem sledovat provoz na vašem dotačním portálu, můžete v nastavení organizace vložit vlastní trackovací identifikátor (typicky začínající "UA-")

## Vlastní JS nebo CSS

Správa organizace vám dovoluje vložit si vlastní CSS nebo JS (samotný kód bez tagů 'script' nebo 'style'), tento se pak aplikuje na každé stránce jak pro přihlášené tak i nepřihlášené uživatele.
Toto je šikovné, pokud například chcete vložit sledování návštěvnosti od jiného poskytovatele než GA.
Za bezpečnost nebo funkčnost vašeho kódu DSW2 nenese žádnou zodpovědnost a nevaliduje správnost vloženého kódu.

# Procesy, které DSW2 aktuálně neimplementuje

## Helpdesk

DSW2 neobsahuje integrovaný helpdesk, a primární formou technické podpory DSW2 jsou issues v repozitáři (aktuálně [https://gitlab.com/otevrenamesta/praha3/dsw2])

Primární formou běžné uživatelské a obchodní podpory je pak e-mailová adresa `dsw2@otevrenamesta.cz`

Stejně tak DSW2 nepřijímá žádné e-maily, takže v obou případech nastavení e-mailů, je vhodné přidat "reply-to" adresu, pokud chcete sbírat informace, které vám uživatelé budou
odepisovat na odeslaná vyrozumění/oznámení/výzvy, a tuto případně i zavést do kontaktů organizace

Následně pak zodpovědné osoby za organizaci (nejen technické) mohou zpětnou vazbu koncentrovat a řešit ji s námi e-mailem nebo přes issues v gitlab projektu

## Kontrola uživatelských oprávnění

Pokud jste v systému určili nějakého uživatele jako administrátora (manažera konkrétní sekce nebo člena týmů, které se vyjadřují k dotačním žádostem,
a tento uživatel opustil pracovní úvazek, za jeho odstranění ze systému nesete odpovědnost sami.

DSW2 nemá a neplánujeme přidávat funkci integrace se zdrojem dat (AD/LDAP/...), pro synchronizaci uživatelských oprávnění

Pokud máte zájem o přihlašování uživatel nějakým SSO, tak kontaktujte Otevřená Města s požadavkem na vývoj a takovou komponentu bychom řešili individuálně
