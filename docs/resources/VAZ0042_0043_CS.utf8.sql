CREATE TABLE IF NOT EXISTS `csu_municipalities_to_csu_municipality_parts` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `csu_municipalities_number` int(11) NOT NULL,
 `csu_municipality_parts_number` int(11) NOT NULL,
 PRIMARY KEY (`id`),
 KEY `csu_municipalities_number` (`csu_municipalities_number`),
 KEY `csu_municipality_parts_number` (`csu_municipality_parts_number`)
) ENGINE=InnoDB AUTO_INCREMENT=16894 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_czech_ci COMMENT='vazba mezi obcemi a jejich částmi'

LOAD DATA INFILE 'VAZ0042_0043_CS.utf8.csv' INTO TABLE csu_municipalities_to_csu_municipality_parts FIELDS TERMINATED BY ',' ENCLOSED BY '"' LINES TERMINATED BY '\n' IGNORE 1 
ROWS (@i, @i, @i, @i, @part_no, @i, @i, @i, @city_no, @i) SET id=NULL, csu_municipalities_number=@city_no, csu_municipality_parts_number=@part_no;
