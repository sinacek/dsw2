

LOAD DATA INFILE 'CIS0044_CS.utf8.csv' INTO TABLE csu_municipality_parts FIELDS TERMINATED BY ',' ENCLOSED BY '"' LINES TERMINATED BY '\n' IGNORE 1 ROWS (@i, @i, @i, @number, @short_name, @long_name, @i, @i) SET id=NULL, short_name=@short_name, name=@long_name, number=@number;
