# Dotační Software 2 (DSW2)

![Dotační Software Logo](webroot/img/default64.png "Dotační Software 2")

Tento software je novou verzí původního DSW, který MČ Praha 3 uvolnila pod licencí GNU GPLv3
Původní software, můžete najít v tomto repozitáři ve větvi (branch) **oldstable**, ale již není provozován.

Tato verze je kromě opravy technických a bezpečnostních problémů zároveň připravena k provozu a poskytování
služeb na více doménách pro více obcí zároveň a vývoj a rozvoj aktuálně zajišťuje spolek Otevřená Města z.s.

## Konfigurace a Dokumentace

Konfiguraci aplikace DSW2 případně jednotlivého dotačního portálu / organizace, popisuje dokumentace ve složce [docs](../docs)

Dokumentace je ve formátu MarkDown (.md), pokud soubory neprocházíte v Gitlab/Github repozitáři, tak můžete ke zobrazení formátovaného textu použít například tuto webovou aplikaci [jbt.github.io/markdown-editor](https://jbt.github.io/markdown-editor/)

Především pak
  - Instalace a konfigurace serveru [instalace-serveru.md](../docs/instalace-serveru.md)
  - Nastavení organizace pro správce technické infrastruktury / ICT oddělení [tech-nastaveni-organizace.md](../docs/tech-nastaveni-organizace.md)

## Užitečné odkazy

  - Stručné shrnutí stavu projektu naleznete po spuštění portálu v cestě `/about`
    - tj. například https://dsw2.otevrenamesta.cz/about
  - Příručku správce dotačního portálu (nápovědu pro nastavení dotačního fondu, formulářů, uživatel, atp.) pak v cestě `/admin/docs`
    - tj. například https://dsw2.otevrenamesta.cz/admin/docs
