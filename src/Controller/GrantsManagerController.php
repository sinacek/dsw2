<?php

namespace App\Controller;

use App\Middleware\OrgDomainsMiddleware;
use App\Model\Entity\Program;
use App\Model\Entity\User;
use App\Model\Entity\UserRole;
use App\Model\Table\AppealsTable;
use App\Model\Table\FondsTable;
use App\Model\Table\ProgramsTable;
use App\Model\Table\RealmsTable;
use Cake\Http\Exception\ForbiddenException;
use Cake\ORM\Query;

/**
 * Manage fonds, realms, programs and appeals
 *
 * @property FondsTable $Fonds
 * @property RealmsTable $Realms
 * @property AppealsTable $Appeals
 * @property ProgramsTable $Programs
 */
class GrantsManagerController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Fonds');
        $this->loadModel('Realms');
        $this->loadModel('Appeals');
        $this->loadModel('Programs');
    }

    public function isAuthorized($user = null)
    {
        if (!($user instanceof User)) {
            $user = $this->Auth->user();
        }

        return parent::isAuthorized($user) && $user->isGrantsManager();
    }

    private function getFondsByUser(User $user)
    {
        $conditions = [];
        if (!$user->isSystemsManager()) {
            // now only portal manager or grant manager
            $conditions['Fonds.organization_id IN'] = array_intersect(
                $user->getOrganizationIdsWhereUserHasRoleWithFallbacks(UserRole::MANAGER_GRANTS, false, [UserRole::MANAGER_PORTALS]),
                [OrgDomainsMiddleware::getCurrentOrganizationId()]
            );
        }

        return $this->Fonds->find(
            'all',
            [
                'conditions' => $conditions,
                'contain' => [
                    'Realms',
                    'Realms.Programs',
                ],
            ]
        );
    }

    private function getRealmsByUser(User $user)
    {
        $conditions = [];
        if (!$user->isSystemsManager()) {
            // now only portal manager or grant manager
            $conditions['Fonds.organization_id IN'] = array_intersect(
                $user->getOrganizationIdsWhereUserHasRoleWithFallbacks(UserRole::MANAGER_GRANTS, false, [UserRole::MANAGER_PORTALS]),
                [OrgDomainsMiddleware::getCurrentOrganizationId()]
            );
        }

        return $this->Realms->find(
            'all',
            [
                'conditions' => $conditions,
                'contain' => [
                    'Fonds',
                    'Programs',
                    'Programs.Appeals',
                ],
            ]
        );
    }

    private function getTeamsByUser(User $user)
    {
        $conditions = [];
        if (!$user->isSystemsManager()) {
            $conditions['FormalCheckTeams.organization_id'] = OrgDomainsMiddleware::getCurrentOrganizationId();
        }

        return $this->Programs->FormalCheckTeams->find(
            'all',
            [
                'conditions' => $conditions,
            ]
        );
    }

    private function getProgramIdsByUser(User $user)
    {
        $programs = $this->getProgramsByUser($user)->contain('ChildPrograms')->find('threaded');
        $ids = [];
        foreach ($programs as $program) {
            $ids[] = $program->id;
            if (!empty($program->child_programs)) {
                foreach ($program->child_programs as $child_program) {
                    $ids[] = $child_program->id;
                }
            }
        }

        return $ids;
    }

    private function getProgramsTreeByUser(User $user, array $allowed_program_ids)
    {
        $map = $this->getProgramsByUser($user)
            ->contain(['ChildPrograms', 'ParentPrograms'])
            ->filter(function ($value) use ($allowed_program_ids) {
                return in_array($value->id, $allowed_program_ids, true) || in_array($value->parent_id, $allowed_program_ids, true);
            })
            ->map(function ($value) {
                /**@var $value Program */

                if (empty($value->child_programs)) {
                    return [
                        'value' => $value->id,
                        'text' => $value->name,
                        'data-section' => $value->getDataSection(),
                    ];
                }

                $rtn = [];

                foreach ($value->child_programs as $child_program) {
                    $child_program->parent_program = $value;
                    $child_program->realm = $value->realm;
                    $rtn[] = [
                        'value' => $child_program->id,
                        'text' => $child_program->name,
                        'data-section' => $child_program->getDataSection(),
                    ];
                }

                return $rtn;
            })->toArray();

        $flat = [];
        foreach ($map as $item) {
            $isSection = false;
            foreach ($item as $key => $value) {
                if (is_array($value)) {
                    $isSection = true;
                    $flat[] = $value;
                }
            }
            if (!$isSection) {
                $flat[] = $item;
            }
        }

        return $flat;
    }

    private function getProgramsByUser(User $user)
    {
        $conditions = [];
        $contain = [
            'Realms',
            'Realms.Fonds',
            'Appeals',
            'EvaluationCriteria',
        ];
        $query = $this->Programs->find(
            'all',
            [
                'conditions' => $conditions,
                'contain' => $contain,
            ]
        )->order(
            [
                'Programs.realm_id' => 'ASC',
            ]
        );
        if (!$user->isSystemsManager()) {
            // now only portal manager or grant manager
            $query->matching(
                'Realms.Fonds',
                function (Query $query) use ($user) {
                    return $query->where(
                        ['Fonds.organization_id IN' => array_intersect(
                            $user->getOrganizationIdsWhereUserHasRoleWithFallbacks(UserRole::MANAGER_GRANTS, false, [UserRole::MANAGER_PORTALS]),
                            [OrgDomainsMiddleware::getCurrentOrganizationId()]
                        )]
                    );
                }
            )->distinct('Programs.id');
        }

        return $query;
    }

    private function getAppealsByUser(User $user)
    {
        $query = $this->Appeals->find(
            'all',
            [
                'contain' => [
                    'Programs',
                    'Programs.Realms',
                    'Programs.Realms.Fonds',
                ],
            ]
        );

        if (!$user->isSystemsManager()) {
            $query->where(['Appeals.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId()]);
        }

        return $query;
    }

    private function getOrganizationsByUser(User $user)
    {
        $conditions = [];
        if (!$user->isSystemsManager()) {
            $conditions['Organizations.id IN'] = array_intersect(
                $user->getOrganizationIdsWhereUserHasRoleWithFallbacks(UserRole::MANAGER_PORTALS, false, [UserRole::MANAGER_GRANTS]),
                [OrgDomainsMiddleware::getCurrentOrganizationId()]
            );
        }

        return $this->Fonds->Organizations->find(
            'all',
            [
                'conditions' => $conditions,
                'contain' => [
                    'Fonds',
                    'Fonds.Realms',
                ],
            ]
        );
    }

    public function indexFonds()
    {
        $this->set('fonds', $this->getFondsByUser($this->getCurrentUser()));
    }

    public function indexRealms()
    {
        $this->set(
            'realms',
            $this->getRealmsByUser($this->getCurrentUser())->contain(
                'Programs',
                function (Query $q) {
                    return $q->where(['Programs.parent_id IS' => null]);
                }
            )
        );
        $this->set('crumbs', [__('Dotační Fondy') => 'admin_fonds']);
    }

    public function indexPrograms()
    {
        $this->set('programs', $this->getProgramsByUser($this->getCurrentUser())->contain(['ChildPrograms' => ['EvaluationCriteria', 'Forms'], 'Forms'])->find('threaded'));
        $this->set('crumbs', [__('Dotační Fondy') => 'admin_fonds', __('Oblasti Podpory') => 'admin_realms']);
    }

    public function indexAppeals()
    {
        $this->set('appeals', $this->getAppealsByUser($this->getCurrentUser()));
        $this->set('crumbs', [__('Dotační Fondy') => 'admin_fonds', __('Oblasti Podpory') => 'admin_realms', __('Programy a Pod-programy') => 'admin_programs']);
    }

    public function fondToggle(int $id = 0)
    {
        $fonds = $this->getFondsByUser($this->getCurrentUser())->find('list')->toArray();
        if (!in_array($id, array_keys($fonds))) {
            $this->Flash->error(__('Nemáte právo k úpravě tohoto Fondu'));
            $this->redirect(['action' => 'indexFonds']);

            return;
        }
        $fond = $this->Fonds->get($id);
        $fond->is_enabled = !$fond->is_enabled;
        $this->Fonds->save($fond);
        $this->Flash->success(__('Hotovo'));
        $this->redirect($this->referer());
    }

    public function fondDelete(int $id)
    {
        $fonds = $this->getFondsByUser($this->getCurrentUser())->find('list')->toArray();
        if (!in_array($id, array_keys($fonds))) {
            $this->Flash->error(__('Nemáte právo k úpravě tohoto Fondu'));

            return $this->redirect(['action' => 'indexFonds']);
        }
        $fond = $this->Fonds->get($id);
        if ($this->Fonds->delete($fond)) {
            $this->Flash->success(__('Dotační fond byl smazán úspěšně'));
        } else {
            $this->Flash->error($fond->getFirstError() ?? __('Nastala chyba při mazání dotačního fondu'));
        }

        return $this->redirect(['action' => 'indexFonds']);
    }

    public function fondCopy(int $id)
    {
        $fond = $this->Fonds->get(
            $id,
            [
                'conditions' => [
                    'organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                ],
                'contain' => [
                    'Realms',
                    'Realms.Programs',
                    'Realms.Programs.ChildPrograms',
                ],
            ]
        );

        $fond->unsetProperty('id')->unsetProperty('created')->unsetProperty('modified');
        $fond->isNew(true);
        $fond->set('is_enabled', false);
        $fond->name .= sprintf(" - %s %s", __('Kopie'), date('Y-m-d H:i:s'));
        foreach ($fond->realms as $realm) {
            $realm->unsetProperty('id')->unsetProperty('fond_id')->unsetProperty('modified')->unsetProperty('created');
            $realm->isNew(true);
            foreach ($realm->programs as $program) {
                $program->unsetProperty('id')->unsetProperty('realm_id')->unsetProperty('modified')->unsetProperty('created');
                $program->isNew(true);
                foreach ($program->child_programs as $child_program) {
                    $child_program->unsetProperty('id')->unsetProperty('realm_id')->unsetProperty('parent_id')->unsetProperty('created')->unsetProperty('modified');
                    $child_program->isNew(true);
                }
            }
        }

        if ($this->Fonds->save($fond)) {
            $this->Flash->success(__('Kopie vytvořena úspěšně'));
        } else {
            $this->Flash->error(__('Nebylo možné vytvořit kopii dotačního fondu'));
        }

        return $this->redirect(['action' => 'indexFonds']);
    }

    public function realmToggle(int $id = 0)
    {
        $realms = $this->getRealmsByUser($this->getCurrentUser())->find('list')->toArray();
        if (!in_array($id, array_keys($realms))) {
            throw new ForbiddenException(__('Nemáte právo k úpravě této oblasti podpory'));
        }
        $realm = $this->Realms->get($id);
        $realm->is_enabled = !$realm->is_enabled;
        $this->Realms->save($realm);
        $this->Flash->success(__('Hotovo'));
        $this->redirect($this->referer());
    }

    public function fondAddModify(int $id = 0)
    {
        if ($id > 0) {
            $allowed_fonds = $this->getFondsByUser($this->getCurrentUser())->find('list')->toArray();
            if (!in_array($id, array_keys($allowed_fonds))) {
                $this->Flash->error(__('Nemáte právo k úpravě tohoto Fondu'));
                $this->redirect(['action' => 'indexFonds']);

                return;
            }
        }
        $fond = $id > 0 ? $this->Fonds->get($id, ['contain' => ['Organizations']]) : $this->Fonds->newEntity();
        $organizations = $this->getOrganizationsByUser($this->getCurrentUser())->find('list')->toArray();

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $fond = $this->Fonds->patchEntity($fond, $this->getRequest()->getData());
            $fond->organization_id = OrgDomainsMiddleware::getCurrentOrganizationId();
            if ($this->Fonds->save($fond)) {
                $this->Flash->success(__('Hotovo'));
                $this->redirect($this->retrieveReferer(['action' => 'indexFonds']));
            } else {
                $this->Flash->error(__('Formulář obsahuje chyby'));
            }
        }

        $this->persistReferer();
        $this->set(compact('fond', 'organizations'));
        $this->set('crumbs', [__('Dotační fondy') => 'admin_fonds']);
    }

    public function realmAddModify(int $id = 0)
    {
        if ($id > 0) {
            $allowed_realms = $this->getRealmsByUser($this->getCurrentUser())->find('list')->toArray();
            if (!in_array($id, array_keys($allowed_realms))) {
                throw new ForbiddenException(__('Nemáte právo k úpravě této oblasti podpory'));
            }
        }
        $realm = $id > 0 ? $this->Realms->get($id) : $this->Realms->newEntity();
        $fonds = $this->getFondsByUser($this->getCurrentUser())->find('list')->toArray();

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $realm = $this->Realms->patchEntity($realm, $this->getRequest()->getData());
            if (!in_array($realm->fond_id, array_keys($fonds))) {
                $realm->fond_id = null;
            }
            if ($this->Realms->save($realm)) {
                $this->Flash->success(__('Hotovo'));
                $this->redirect($this->retrieveReferer(['action' => 'indexRealms']));
            } else {
                $this->Flash->error(__('Formulář obsahuje chyby'));
            }
        } elseif ($this->getRequest()->getParam('fond_id')) {
            $fond_id = $this->getRequest()->getParam('fond_id');
            if (in_array($fond_id, array_keys($fonds))) {
                $realm->fond_id = $fond_id;
            }
        }

        $this->persistReferer();
        $this->set(compact('realm', 'fonds'));
        $this->set('crumbs', [__('Dotační Fondy') => 'admin_fonds', __('Oblasti Podpory') => 'admin_realms']);
    }

    public function realmDelete(int $realm_id)
    {
        $allowed_realms = $this->getRealmsByUser($this->getCurrentUser())->find('list')->toArray();
        if (!in_array($realm_id, array_keys($allowed_realms))) {
            throw new ForbiddenException(__('Nemáte právo k úpravě této oblasti podpory'));
        }
        $realm = $this->Realms->get($realm_id);
        if ($message = $this->Realms->delete($realm)) {
            $this->Flash->success(__('Oblast podpory byla smazána'));
        } else {
            $this->Flash->error($realm->getFirstError() ?? __('Oblast podpory nelze smazat'));
        }
        $this->redirect(['action' => 'indexRealms']);
    }

    public function programDelete(int $program_id)
    {
        $allowed_programs = $this->getProgramsByUser($this->getCurrentUser())->find('list')->toArray();
        if (!in_array($program_id, array_keys($allowed_programs))) {
            $this->Flash->error(__('Nemáte právo k úpravě tohoto programu'));

            return $this->redirect(['action' => 'indexPrograms']);
        }
        $program = $this->Programs->get($program_id);
        if ($this->Programs->delete($program)) {
            $this->Flash->success(__('Program by smazán'));
        } else {
            $this->Flash->error($program->getFirstError() ?? __('Program nelze smazat'));
        }

        return $this->redirect(['action' => 'indexPrograms']);
    }

    public function programAddModify(int $id = 0)
    {
        if ($id > 0 && !in_array($id, $this->getProgramIdsByUser($this->getCurrentUser()))) {
            throw new ForbiddenException(__('Nemáte právo k úpravě tohoto programu'));
        }
        $this->persistReferer();

        $parent_id = $this->getRequest()->getParam('parent_id', null);
        $realm_id = $this->getRequest()->getParam('realm_id', null);

        $allowed_realms = $this->getRealmsByUser($this->getCurrentUser())->find('list')->toArray();
        $allowed_programs = $this->getProgramsByUser($this->getCurrentUser())->where(['Programs.parent_id IS' => null])->find('list')->toArray();
        $allowed_teams = $this->getTeamsByUser($this->getCurrentUser())->find('list')->toArray();

        $program = $id > 0 ? $this->Programs->get($id, [
            'contain' => [
                'Forms', 'EvaluationCriteria', 'ChildPrograms', 'ChildPrograms.Forms',
                'ChildPrograms.EvaluationCriteria', 'FormalCheckTeams',
            ],
        ]) : $this->Programs->newEntity();

        $evaluation_criteria = $this->Programs->EvaluationCriteria->find(
            'list',
            [
                'conditions' => [
                    'EvaluationCriteria.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                    'EvaluationCriteria.parent_id IS' => null,
                ],
            ]
        )->toArray();

        $forms = $this->Programs->Forms->find(
            'list',
            [
                'conditions' => [
                    'Forms.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                ],
            ]
        )->toArray();
        $this->set(compact('allowed_realms', 'allowed_teams', 'program', 'evaluation_criteria', 'forms'));
        $this->set('allowed_programs', array_filter($allowed_programs, function ($key) use ($program) {
            return $key !== $program->id;
        }, ARRAY_FILTER_USE_KEY));

        // Navigation

        $crumbs = [
            __('Dotační Fondy') => 'admin_fonds',
            __('Oblasti Podpory') => 'admin_realms',
        ];

        if (!empty($realm_id) && in_array($realm_id, array_keys($allowed_realms))) {
            $program->realm_id = $realm_id;
            $crumbs[$allowed_realms[$realm_id]] = ['action' => 'realmAddModify', $realm_id];
        }

        $crumbs[__('Programy a Pod-programy')] = 'admin_programs';

        if (!empty($parent_id) && in_array($parent_id, array_keys($allowed_programs))) {
            $program->parent_id = $parent_id;
            $crumbs[$allowed_programs[$parent_id]] = ['action' => 'programAddModify', $parent_id];
        }

        $this->set('crumbs', $crumbs);

        // submit handler

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $program = $this->Programs->patchEntity($program, $this->getRequest()->getData());
            if ($program->parent_id) {
                if (!in_array($program->parent_id, array_keys($allowed_programs))) {
                    $program->setError('parent_id', __('Nemáte právo k vytvoření pod-programu ve vybraném programu'));
                } else {
                    $parent = $this->Programs->get($program->parent_id);
                    $program->realm_id = $parent->realm_id;
                }
            }
            if (!empty($program->realm_id) && !in_array($program->realm_id, array_keys($allowed_realms))) {
                $program->setError(
                    'realm_id',
                    __('Nemáte právo k této oblasti podpory')
                );
            }
            if (!empty($program->evaluation_criteria_id) && !in_array($program->evaluation_criteria_id, array_keys($evaluation_criteria))) {
                $program->setError('evaluation_criteria_id', __('Nemáte právo k použití této skupiny Hodnotících kritérií'));
            }

            if (!in_array($program->formal_check_team_id, array_keys($allowed_teams))) {
                $program->formal_check_team_id = null;
                $program->formal_check_team = null;
            }

            if ($this->Programs->save($program)) {
                $this->Flash->success(__('Uloženo úspěšně'));
                $this->redirect(['action' => 'programAddModify', $program->parent_id ?? $program->id]);
            } else {
                $this->Flash->error(__('Formulář obsahuje chyby'));

                return;
            }
        }
    }

    public function appealAddModify(int $id = 0)
    {
        $allowed_programs = $this->getProgramsByUser($this->getCurrentUser())->find('list')->toArray();
        $allowed_appeals = $this->getAppealsByUser($this->getCurrentUser())->find('list')->toArray();
        if ($id > 0 && !in_array($id, array_keys($allowed_appeals))) {
            throw new ForbiddenException(__('Nemte právo k úpravě této výzvy'));
        }

        $programsTree = $this->getProgramsTreeByUser($this->getCurrentUser(), array_keys($allowed_programs));

        $appeal = $id > 0 ? $this->Appeals->get($id, ['contain' => ['Programs']]) : $this->Appeals->newEntity();

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $appeal = $this->Appeals->patchEntity($appeal, $this->getRequest()->getData());
            $appeal->organization_id = OrgDomainsMiddleware::getCurrentOrganizationId();
            if ($this->Appeals->save($appeal)) {
                $this->Flash->success(__('Hotovo'));
                $this->redirect(['action' => 'appealDetailSettings', $appeal->id]);
            } else {
                $this->Flash->error(__('Formulář obsahuje chyby'));
            }
        }

        $this->set(compact('appeal', 'allowed_programs', 'programsTree'));
        $this->set('crumbs', [__('Dotační Fondy') => 'admin_fonds', __('Oblasti Podpory') => 'admin_realms', __('Výzvy k podání dotačních žádostí') => 'admin_appeals']);
    }

    public function appealDetailSettings(int $id = 0)
    {
        $allowed_appeals = $this->getAppealsByUser($this->getCurrentUser())->find('list')->toArray();
        if ($id < 1 || !in_array($id, array_keys($allowed_appeals))) {
            throw new ForbiddenException(__('Nemte právo k úpravě této výzvy'));
        }
        $appeal = $this->Appeals->get($id, ['contain' => ['AppealsToPrograms', 'AppealsToPrograms.Programs']]);

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $appeal = $this->Appeals->patchEntity($appeal, $this->getRequest()->getData());
            if ($this->Appeals->save($appeal)) {
                $this->Flash->success(__('Hotovo'));
                $this->redirect(['action' => 'indexAppeals']);
            } else {
                $this->Flash->error(__('Formulář obsahuje chyby'));
            }
        }

        $this->set(compact('appeal'));
        $this->set('crumbs', [__('Dotační Fondy') => 'admin_fonds', __('Oblasti Podpory') => 'admin_realms', __('Výzvy k podání dotačních žádostí') => 'admin_appeals', h($appeal->name) => ['action' => 'appealAddModify', $appeal->id]]);
    }

    private function getAppealById(int $id)
    {
        $allowed_appeals = $this->getAppealsByUser($this->getCurrentUser())->find('list')->toArray();
        if ($id > 0 && !in_array($id, array_keys($allowed_appeals))) {
            throw new ForbiddenException(__('Nemte právo k úpravě této výzvy'));
        }

        return $this->Appeals->get($id);
    }

    public function appealToggle(int $id)
    {
        $appeal = $this->getAppealById($id);
        $appeal->is_active = !$appeal->is_active;
        $this->Appeals->save($appeal);

        return $this->redirect(['action' => 'indexAppeals']);
    }

    public function appealDelete(int $id)
    {
        $appeal = $this->getAppealById($id);
        if ($this->Appeals->delete($appeal)) {
            $this->Flash->success(__('Výzva byla smazána úspěšně'));
        }

        return $this->redirect(['action' => 'indexAppeals']);
    }
}
