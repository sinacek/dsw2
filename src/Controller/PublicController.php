<?php

namespace App\Controller;

use App\Middleware\OrgDomainsMiddleware;
use App\Model\Entity\Appeal;
use App\Model\Entity\Fond;
use App\Model\Entity\OrganizationSetting;
use App\Model\Table\AppealsTable;
use App\Model\Table\FondsTable;
use App\Model\Table\UsersTable;
use App\Model\Table\WikiCategoriesTable;
use Cake\Core\Configure;
use Cake\I18n\Time;
use Cake\ORM\Query;
use Cake\Routing\Router;

/**
 * Controller to handle public url, that require no authentication
 *
 * @property FondsTable $Fonds
 * @property AppealsTable $Appeals
 * @property WikiCategoriesTable $WikiCategories
 * @property UsersTable $Users
 */
class PublicController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow();
    }

    public function favicon()
    {
        return $this->redirect(OrganizationSetting::getFaviconFullPath());
    }

    public function index()
    {
    }

    public function about()
    {
        $this->viewBuilder()->setLayout('about');
    }

    public function adminDocs()
    {
        $this->loadModel('Fonds');
        /** @var Fond $fond */
        $fond = $this->Fonds->find('all', [
            'conditions' => [
                'organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
            ],
            'contain' => [
                'Realms',
                'Realms.Programs',
            ],
        ])->first();
        $realm = empty($fond->realms) ? null : reset($fond->realms);
        $program = empty($realm) || empty($realm->programs) ? null : reset($realm->programs);
        $form = $this->Fonds->Realms->Programs->Forms->find('list', [
            'conditions' => [
                'organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
            ],
        ])->first();
        $appeal = $this->getPublicAppeals()->first();
        $request = null;
        if ($this->getCurrentUser()) {
            $this->loadModel('Users');
            $user = $this->Users->get($this->getCurrentUser()->id)
                ->loadRequests();
            foreach ($user->requests ?? [] as $_request) {
                if ($_request->organization_id === OrgDomainsMiddleware::getCurrentOrganizationId()) {
                    $request = $_request;
                }
            }
        }
        $this->set('request', $request);

        $this->set(compact('fond', 'realm', 'program', 'form', 'appeal'));
        $this->set('organization', OrgDomainsMiddleware::getCurrentOrganization());
    }

    public function oldBrowser()
    {
    }

    public function exploreFonds()
    {
        $this->loadModel('Fonds');
        $fonds = $this->Fonds->find(
            'threaded',
            [
                'contain' => [
                    'Realms',
                    'Realms.Programs',
                    'Realms.Programs.ChildPrograms',
                ],
                'conditions' => [
                    'Fonds.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                    'Fonds.is_enabled' => true,
                ],
            ]
        )->matching(
            'Realms',
            function (Query $q) {
                return $q->where(['Realms.is_enabled' => true]);
            }
        )->distinct('Fonds.id');
        $this->set(compact('fonds'));
    }

    public function exploreAppeals()
    {
        $appeals = $this->getPublicAppeals();
        if ($appeals->isEmpty()) {
            $this->Flash->set(__('Aktuálně nejsou žádné aktivní výzvy k podání žádostí o veřejnou podporu (dotaci)'), ['element' => 'info']);
        }
        $threadedPrograms = $this->Appeals->Programs->find(
            'threaded',
            [
                'contain' => [
                    'Realms.Fonds',
                ],
            ]
        )->matching(
            'Realms.Fonds',
            function (Query $q) {
                return $q->where(['Fonds.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId()]);
            }
        );
        $this->set(compact('appeals', 'threadedPrograms'));
    }

    public function wiki()
    {
        $this->loadModel('WikiCategories');
        $this->set(
            'wikis',
            $this->WikiCategories->find(
                'all',
                [
                    'conditions' => [
                        'WikiCategories.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                    ],
                    'contain' => [
                        'Wikis' => function (Query $q) {
                            return $q->order(['Wikis.title' => 'ASC']);
                        },
                    ],
                    'order' => [
                        'WikiCategories.category_name' => 'ASC',
                    ],
                ]
            )
        );
    }

    public function sitemap()
    {
        $routes = [
            '/',
            '/explore/fonds',
            '/explore/appeals',
            '/login',
            '/register',
            '/password_recovery',
            '/csu/countries',
            '/csu/legal_forms',
            '/csu/municipalities',
            '/csu/municipality_parts',
        ];
        $urls = [];
        foreach ($routes as $route) {
            $urls[] = [
                'loc' => 'https://' . OrgDomainsMiddleware::getCurrentDomain()->domain . $route,
                'lastmod' => date('Y-m-d', strtotime('monday this week')),
                'changefreq' => 'weekly',
                'priority' => '0.5',
            ];
        }

        // Define a custom root node in the generated document.
        $this->set('_rootNode', 'urlset');
        $this->set(
            [
                // Define an attribute on the root node.
                '@xmlns' => 'http://www.sitemaps.org/schemas/sitemap/0.9',
                'url' => $urls,
            ]
        );
        $this->viewBuilder()->setLayout('ajax')->setClassName('Xml');
        $this->set('_serialize', ['@xmlns', 'url']);
        $this->RequestHandler->respondAs('xml');
    }

    public function rss()
    {
        $appeals = $this->getPublicAppeals();
        $entries = [];
        /**
         * @var Appeal $lastAppeal
         */
        $lastAppeal = null;

        foreach ($appeals as $appeal) {
            if ($lastAppeal === null) {
                $lastAppeal = $appeal;
            } elseif ($appeal->modified->diffInSeconds($lastAppeal->modified, false) > 0) {
                $lastAppeal = $appeal;
            }
            $id = Router::url(['controller' => 'Public', 'action' => 'exploreAppeals', '#' => $appeal->id, '_full' => true]);
            $entries[] = [
                'title' => $appeal->name,
                'link' => [
                    '@href' => $id,
                ],
                'updated' => $appeal->modified->toAtomString(),
                'summary' => strip_tags($appeal->description),
                'id' => $id,
            ];
        }

        $this->set('_rootNode', 'feed');
        $this->set(
            [
                '@xmlns' => 'http://www.w3.org/2005/Atom',
                'title' => OrganizationSetting::getSetting(OrganizationSetting::INDEX_HERO_TITLE) ?? OrgDomainsMiddleware::getCurrentOrganization()->name,
                'subtitle' => strip_tags(OrganizationSetting::getSetting(OrganizationSetting::INDEX_HERO_SUBTITLE)) ?? OrgDomainsMiddleware::getCurrentDomain()->domain,
                'updated' => $lastAppeal ? $lastAppeal->modified->toAtomString() : (new Time())->toAtomString(),
                'id' => Router::url(['controller' => 'Public', 'action' => 'exploreAppeals', '_full' => true]),
                'entry' => $entries,
                'link' => [
                    '@rel' => 'self',
                    '@href' => Router::url(['controller' => 'Public', 'action' => 'rss', '_full' => true]),
                ],
                'author' => [
                    'name' => OrgDomainsMiddleware::getCurrentOrganization()->name,
                ],
            ]
        );

        $this->viewBuilder()->setLayout('ajax')->setClassName('Xml');
        $this->set('_serialize', ['@xmlns', 'id', 'title', 'subtitle', 'updated', 'link', 'author', 'entry']);
        $this->RequestHandler->respondAs('xml');
    }

    /**
     * @return Query|Appeal[]
     */
    private function getPublicAppeals()
    {
        $this->loadModel('Appeals');

        return $this->Appeals->find(
            'threaded',
            [
                'contain' => [
                    'Programs',
                    'Programs.Realms.Fonds',
                ],
                'conditions' => [
                    'Appeals.is_active' => true,
                    'Appeals.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                ],
            ]
        )->distinct('Appeals.id')->order(['Appeals.id' => 'DESC']);
    }

    public function gdpr()
    {
    }
}
