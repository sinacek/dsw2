<?php

namespace App\Controller;

use App\Model\Table\CsuCountriesTable;
use App\Model\Table\CsuLegalFormsTable;
use App\Model\Table\CsuMunicipalitiesTable;
use App\Model\Table\CsuMunicipalityPartsTable;
use Cake\Http\Response;

/**
 * @property CsuMunicipalitiesTable $CsuMunicipalities
 * @property CsuCountriesTable $CsuCountries
 * @property CsuLegalFormsTable $CsuLegalForms
 * @property CsuMunicipalityPartsTable $CsuMunicipalityParts
 */
class CsuController extends AppController
{
    /**
     * @inheritDoc
     */
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow();

        $this->loadModel('CsuMunicipalities');
        $this->loadModel('CsuCountries');
        $this->loadModel('CsuLegalForms');
        $this->loadModel('CsuMunicipalityParts');
    }

    /**
     * @param string|null $output if json, json will be returned
     * @return Response
     */
    public function countries(?string $output = null): Response
    {
        $data = $this->CsuCountries->find('all');
        $this->set('data', $data);
        $this->set('title', __('Číselník států'));
        $this->set('crumbs', [__('Číselníky') => '#']);
        if ('json' === $output) {
            return $this->getResponse()->withType('application/json')->withStringBody(json_encode($data->toArray()));
        }

        return $this->render('csu_table');
    }

    /**
     * @param string|null $output if json, json will be returned
     * @return Response
     */
    public function legalForms(?string $output = null): Response
    {
        $data = $this->CsuLegalForms->find('all');
        $this->set('data', $data);
        $this->set('title', __('Číselník právních forem'));
        $this->set('crumbs', [__('Číselníky') => '#']);
        if ('json' === $output) {
            return $this->getResponse()->withType('application/json')->withStringBody(json_encode($data->toArray()));
        }

        return $this->render('csu_table');
    }

    /**
     * @param string|null $output if json, json will be returned
     * @return Response
     */
    public function municipalities(?string $output = null): Response
    {
        $data = $this->CsuMunicipalities->find('all');
        $this->set('data', $data);
        $this->set('title', __('Číselník obcí'));
        $this->set('crumbs', [__('Číselníky') => '#']);
        if ('json' === $output) {
            return $this->getResponse()->withType('application/json')->withStringBody(json_encode($data->toArray()));
        }

        return $this->render('csu_table');
    }

    /**
     * @param string|null $output if json, json will be returned
     * @return Response
     */
    public function municipalityParts(?string $output = null): Response
    {
        $data = $this->CsuMunicipalityParts->find('all', ['contain' => ['CsuMunicipalitiesToCsuMunicipalityParts', 'CsuMunicipalitiesToCsuMunicipalityParts.CsuMunicipalities']]);
        $this->set('data', $data);
        $this->set('csu_table_cols', [__('Nadřízená Obec') => 'csu_municipalities_to_csu_municipality_parts.0.csu_municipality']);
        $this->set('title', __('Číselník částí obcí'));
        $this->set('crumbs', [__('Číselníky') => '#']);
        if ('json' === $output) {
            return $this->getResponse()->withType('application/json')->withStringBody(json_encode($data->toArray()));
        }

        return $this->render('csu_table');
    }

    /**
     * @return Response
     */
    public function municipalityPartsList(): Response
    {
        $data = $this->CsuMunicipalityParts->find('list', ['keyField' => 'number'])->enableHydration(false);

        return $this->getResponse()->withType('application/json')->withStringBody(json_encode($data->toArray()));
    }
}
