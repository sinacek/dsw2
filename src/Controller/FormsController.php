<?php

namespace App\Controller;

use App\Form\AbstractFormController;
use App\Middleware\OrgDomainsMiddleware;
use App\Model\Entity\FormField;
use App\Model\Entity\FormFieldType;
use App\Model\Entity\Program;
use App\Model\Entity\User;
use App\Model\Table\FormsTable;
use Cake\Log\Log;
use Throwable;

/**
 * @property FormsTable $Forms
 */
class FormsController extends AppController
{
    public function isAuthorized($user = null)
    {
        /**
         * @var User $user
         */
        $user = $user instanceof User ? $user : $this->Auth->user();

        return parent::isAuthorized($user) && $user->isGrantsManager();
    }

    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Forms');
    }

    public function index()
    {
        $forms = $this->Forms->find(
            'all',
            [
                'conditions' => [
                    'Forms.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                ],
                'contain' => [
                    'FormFields' => [
                        'sort' => ['FormFields.field_order' => 'ASC'],
                    ],
                    'FormTypes',
                ],
            ]
        );

        $this->set(compact('forms'));
    }

    public function formCopy(int $id)
    {
        $form = $this->Forms->get(
            $id,
            [
                'conditions' => [
                    'Forms.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                ],
            ]
        );

        try {
            $form->getFormController()->duplicate($this->Forms);
            $this->Flash->success(__('Kopie vytvořena úspěšně'));
        } catch (Throwable $t) {
            Log::error($t->getMessage());
            Log::error($t->getTraceAsString());
            $this->Flash->error(__('Nastala chyba při vytváření kopie'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function formDelete(int $id)
    {
        $form = $this->Forms->get(
            $id,
            [
                'conditions' => [
                    'Forms.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                ],
            ]
        );
        if ($this->Forms->delete($form)) {
            $this->Flash->success(__('Smazáno úspěšně'));
        } else {
            $this->Flash->error(__('Formulář nebylo možné smazat'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function addModify(int $id = 0)
    {
        $form = $id === 0 ? $this->Forms->newEntity() : $this->Forms->get(
            $id,
            [
                'conditions' => [
                    'Forms.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                ],
                'contain' => [
                    'Programs',
                    'FormTypes',
                ],
            ]
        );
        $programs = $this->Forms->Programs->find(
            'all',
            [
                'conditions' => [
                    'Fonds.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                ],
                'contain' => [
                    'ChildPrograms',
                    'Realms',
                    'Realms.Fonds',
                    'ParentPrograms',
                ],
            ]
        )->filter(function ($value, $key) {
            return empty($value->child_programs);
        })->map(function ($value, $key) {
            /**@var $value Program */
            return [
                'value' => $value->id,
                'text' => $value->name,
                'data-section' => $value->getDataSection(),
            ];
        });
        $formTypes = $this->Forms->FormTypes->find('list')->toArray();

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $form = $this->Forms->patchEntity($form, $this->getRequest()->getData());
            $form->organization_id = OrgDomainsMiddleware::getCurrentOrganizationId();
            if ($this->Forms->save($form)) {
                $this->Flash->success(__('Uloženo úspěšně'));
                $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Formulář obsahuje chyby'));
            }
        }

        $this->set(compact('form', 'programs', 'formTypes'));
        $this->set('crumbs', [__('Formuláře') => 'admin_forms']);
    }

    public function fieldDelete(int $form_id, int $id)
    {
        $field = $this->Forms->FormFields->get(
            $id,
            [
                'conditions' => [
                    'FormFields.form_id' => $form_id,
                    'Forms.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                ],
                'contain' => 'Forms',
            ]
        );
        if (!$this->Forms->FormFields->delete($field)) {
            $this->Flash->error($field->getFirstError());
        }
        $this->redirect($this->referer());
    }

    public function fieldAddModify(int $form_id, int $id = 0)
    {
        $form_field = $id === 0 ? $this->Forms->FormFields->newEntity() : $this->Forms->FormFields->get(
            $id,
            [
                'conditions' => [
                    'form_id' => $form_id,
                    'Forms.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                ],
                'contain' => [
                    'Forms',
                ],
            ]
        );
        if ($form_field->isNew()) {
            $form_field->form_id = $form_id;
            $form_field->form = $this->Forms->get(
                $form_id,
                [
                    'conditions' => [
                        'Forms.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                    ],
                ]
            );
        }
        if (!$form_field->form->getFormController()->hasUserDefinedFields()) {
            $this->Flash->error(__('Tento formulář nepodporuje uživatelsky definovaná pole k vyplnění'));
            $this->redirect(['action' => 'index']);

            return;
        }
        $form_field_types = $this->Forms->FormFields->FormFieldTypes->find('list')->order(['weight' => 'DESC']);
        $form_fields_conditions = [
            'FormFields.form_id' => $form_id,
        ];

        if (!$form_field->isNew()) {
            $form_fields_conditions['FormFields.id !='] = $form_field->id;
        }
        $form_fields = $this->Forms->FormFields->find(
            'list',
            [
                'conditions' => $form_fields_conditions,
                'keyField' => 'field_order',
                'valueField' => 'name',
            ]
        )->order(['field_order' => 'ASC'])->toArray();

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $form_field = $this->Forms->FormFields->patchEntity($form_field, $this->getRequest()->getData());
            $form_field = FormFieldType::configureTableField($form_field, $this->getRequest()->getData());
            if (!in_array($form_field->form_field_type_id, [FormFieldType::FIELD_CHOICES], true)) {
                $form_field->choices = null;
            } elseif (!empty($this->getRequest()->getData('choices'))) {
                $form_field->choices = serialize($this->getRequest()->getData('choices'));
            } else {
                $form_field->setError('choices', __('Chybí definice možností k výběru'));
            }
            $form_field->field_order++;
            $this->fixFormFieldsOrder($form_field);
            if ($this->Forms->FormFields->save($form_field)) {
                $this->Flash->success(__('Uloženo úspěšně'));
                $this->redirect(['action' => 'formDetail', $form_field->form_id]);
            } else {
                $this->Flash->error(__('Formulář obsahuje chyby'));
            }
        }

        $form_field->field_order = $form_field->isNew() ? (empty($form_fields) ? 0 : max(array_keys($form_fields))) : --$form_field->field_order;
        FormFieldType::extractTableSettings($form_field);

        $this->set(compact('form_field', 'form_field_types', 'form_fields'));
        $this->set('crumbs', [__('Formuláře') => 'admin_forms', $form_field->form->name => ['action' => 'formDetail', $form_id]]);
    }

    public function formDetail(int $id)
    {
        $form = $this->Forms->get(
            $id,
            [
                'conditions' => [
                    'Forms.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                ],
            ]
        );

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $patchedForm = $form->getFormController()->saveSetting($this->getRequest()->getData());

            if ($this->Forms->save($patchedForm)) {
                $this->Flash->success(__('Úspěšně uloženo'));
            } else {
                Log::debug(json_encode($patchedForm->getErrors()));
                $this->Flash->error(__('Formulář obsahuje chyby'));
            }
        }

        $this->set('form', $form->getFormController());
        $this->set('crumbs', [__('Formuláře') => 'admin_forms']);
    }

    public function formPreview(int $id)
    {
        $this->formDetail($id);

        /** @var AbstractFormController $form */
        $form = $this->viewVars['form'];
        $this->set('crumbs', [__('Formuláře') => 'admin_forms', $form->getFormName() => ['action' => 'formDetail', 'id' => $id]]);
    }

    private function fixFormFieldsOrder(FormField $field)
    {
        $form_fields = $this->Forms->FormFields->find(
            'list',
            [
                'conditions' => ['form_id' => $field->form_id],
                'keyField' => 'field_order',
                'valueField' => 'id',
            ]
        );
        $form_fields = $form_fields->toArray();
        if (empty($field->field_order)) {
            // if no order defined, set order at the end of the form
            $field->field_order = max(array_keys($form_fields)) + 1;

            return;
        }
        if (isset($form_fields[$field->field_order])) {
            if ($form_fields[$field->field_order] === $field->id) {
                // update with same order, pass it
                return;
            } elseif (!empty($field->getErrors())) {
                // field contains errors, no changes should be made until corrected
                return;
            } else {
                // update other elements, so that provided field is inserted at indicated position/order
                $this->Forms->FormFields->getConnection()->transactional(
                    function () use ($form_fields, $field) {
                        $updates_in_order = [];
                        foreach ($form_fields as $order => $fieldId) {
                            if ($order < $field->field_order) {
                                continue;
                            } elseif ($order >= $field->field_order) {
                                $updates_in_order[$order + 1] = $fieldId;
                            }
                        }
                        krsort($updates_in_order);
                        foreach ($updates_in_order as $newOrder => $fieldId) {
                            $this->Forms->FormFields->updateAll(
                                ['field_order' => $newOrder],
                                ['FormFields.id' => $fieldId]
                            );
                        }
                        $this->Forms->FormFields->save($field);
                    }
                );
            }
        }
    }
}
