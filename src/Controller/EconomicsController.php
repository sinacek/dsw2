<?php
declare(strict_types=1);

namespace App\Controller;

use App\Middleware\OrgDomainsMiddleware;
use App\Model\Entity\RequestState;
use App\Model\Table\AppealsTable;
use App\Model\Table\RequestsTable;
use Cake\Http\Exception\ForbiddenException;

/**
 * @property-read AppealsTable $Appeals
 * @property-read RequestsTable $Requests
 */
class EconomicsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Appeals');
        $this->loadModel('Requests');
    }

    public function isAuthorized($user = null)
    {
        return parent::isAuthorized($user) && $this->getCurrentUser()->isEconomicsManager();
    }

    public function index()
    {
        $appeals = $this->Appeals->find(
            'list',
            [
            'conditions' => [
                'Appeals.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
            ],
            ]
        )->toArray();
        $states = RequestState::getLabels();

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $by_appeal_id = $this->getRequest()->getData('appeal_id');
            $by_state_id = $this->getRequest()->getData('state_id');

            if (
                (!empty($by_appeal_id) && !in_array(intval($by_appeal_id), array_keys($appeals), true))
                || (!empty($by_state_id) && !in_array(intval($by_state_id), array_keys($states), true))
            ) {
                throw new ForbiddenException();
            }

            $routeArray = ['action' => 'index'];
            if (!empty($by_appeal_id)) {
                $routeArray['appeal_id'] = $by_appeal_id;
            }
            if (!empty($by_state_id)) {
                $routeArray['state_id'] = $by_state_id;
            }

            $this->redirect($routeArray);

            return;
        }

        $conditions = [
            'Requests.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
        ];
        if ($this->getRequest()->getParam('appeal_id')) {
            $conditions['Requests.appeal_id'] = $this->getRequest()->getParam('appeal_id');
        }
        if ($this->getRequest()->getParam('state_id')) {
            $conditions['Requests.request_state_id'] = $this->getRequest()->getParam('state_id');
        } else {
            $conditions['Requests.request_state_id IN'] = [
                RequestState::STATE_WAITING_FOR_SETTLEMENT_TO_BE_PAID,
                RequestState::STATE_CONTRACT_SIGNED_READY_TO_PAYOUT,
                RequestState::STATE_CONTRACT_BREACH_CRIMINAL_PROCEEDINGS,
                RequestState::STATE_SETTLEMENT_SUBMITTED,
            ];
        }

        $requests = $this->Requests->find(
            'all',
            [
            'conditions' => $conditions,
            'contain' => [
                'Programs',
                'Programs.Realms',
                'RequestBudgets',
            ],
            ]
        );

        $this->set(compact('appeals', 'requests', 'states'));
    }
}
