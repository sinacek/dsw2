<?php

namespace App\Controller;

use App\Form\IdentityForm;
use App\Middleware\OrgDomainsMiddleware;
use App\Model\Entity\Identity;
use App\Model\Entity\OrganizationSetting;
use App\Model\Entity\User;
use App\Model\Entity\UserRole;
use App\Model\Table\CsuCountriesTable;
use App\Model\Table\CsuLegalFormsTable;
use App\Model\Table\CsuMunicipalitiesTable;
use App\Model\Table\CsuMunicipalityPartsTable;
use App\Model\Table\IdentitiesTable;
use App\Model\Table\PublicIncomeHistoriesTable;
use App\Model\Table\UsersTable;
use Cake\Cache\Cache;
use Cake\Http\Exception\NotFoundException;
use Cake\Http\Response;
use Cake\Log\Log;
use Cake\Routing\Router;
use Cake\Utility\Hash;
use Cake\View\Helper\HtmlHelper;
use Exception;
use OldDsw\Controller\Component\OldDswComponent;
use OldDsw\Model\Entity\Ucet;
use OldDsw\Model\Entity\Uzivatel;
use Throwable;

/**
 * Controller to handle basic user actions, such as login/register/logout/password_recovery,
 * managing user details, etc.
 *
 * @property UsersTable $Users
 * @property CsuCountriesTable $CsuCountries
 * @property CsuMunicipalitiesTable $CsuMunicipalities
 * @property CsuMunicipalityPartsTable $CsuMunicipalityParts
 * @property CsuLegalFormsTable $CsuLegalForms
 * @property IdentitiesTable $Identities
 * @property PublicIncomeHistoriesTable $PublicIncomeHistories
 * @property OldDswComponent $OldDsw
 */
class UsersController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['login', 'register', 'passwordRecovery', 'passwordRecoveryGo', 'verifyEmail', 'inviteAccept']);
        $this->loadModel('Users');
        $this->loadModel('CsuMunicipalities');
        $this->loadModel('CsuCountries');
        $this->loadModel('CsuMunicipalityParts');
        $this->loadModel('CsuLegalForms');
        $this->loadModel('Identities');
        $this->loadModel('PublicIncomeHistories');
    }

    public function isAuthorized($user = null)
    {
        if ('logout' === $this->getRequest()->getParam('action')) {
            return $this->getCurrentUser() instanceof User;
        }

        return parent::isAuthorized($user);
    }

    public function changeInfo()
    {
        try {
            $parsedReferer = Router::getRouteCollection()->parse($this->getRequest()->referer(true));
        } catch (Exception $e) {
            $parsedReferer = null;
        }
        if (!empty($parsedReferer) && ($parsedReferer['_name'] ?? null) === 'request_detail') {
            $this->persistReferer();
        }
        $identityForm = new IdentityForm();
        $identityForm->setCountries($this->CsuCountries);
        $identityForm->setCities($this->CsuMunicipalities);
        $identityForm->setCityParts($this->CsuMunicipalityParts);
        $identityForm->setLegalForms($this->CsuLegalForms);
        $identityForm->loadIdentities($this->Identities, $this->Auth->user());

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            if ($identityForm->execute($this->getRequest()->getData())) {
                $this->Flash->success(__('Vaše identita byla úspěšně uložena'));
                // update all non-locked requests to use latest identity version
                $maxIdentityVersion = $this->getCurrentUser()->getLastIdentityVersion();
                $this->Users->Requests->updateAll([
                    'user_identity_version' => $maxIdentityVersion,
                ], [
                    'user_id' => $this->getCurrentUser()->id,
                    'is_locked' => false,
                ]);
                $this->redirect($this->retrieveReferer());

                return;
            } elseif (!empty($identityForm->getErrors())) {
                $this->Flash->error(__('Formulář obsahuje chyby, opravte je prosím a znovu zkuste uložit'));
            } else {
                $this->Flash->error(__('Nastala chyba při ukládání vaší identity, zkuste to prosím znovu později'));
            }

            $flash_counter = 0;
            $form_errors = [];
            foreach ($this->getCurrentUser()->getIdentityMissingFields() ?? [] as $missing_identity_field) {
                if ($flash_counter < 3) {
                    $this->Flash->error(sprintf("%s: %s", __('Chybí vyplnit'), Identity::getFieldLabel($missing_identity_field)));
                }
                $form_errors[$missing_identity_field] = __('Chybí vyplnit');
                $flash_counter++;
            }
            $identityForm->setErrors(array_merge_recursive($identityForm->getErrors(), Hash::expand($form_errors)));

            if (count($identityForm->getErrors()) === 0) {
                $this->redirect($this->getRequest()->getRequestTarget());
            }
        }

        $this->set(compact('identityForm'));
    }

    /**
     * Význam zkratek v XML https://wwwinfo.mfcr.cz/ares/xml_doc/schemas/documentation/zkr_103.txt
     * Dokumentace služby Basic Multi https://wwwinfo.mfcr.cz/ares/ares_xml_basic.html.cz
     *
     * @param string $ico
     * @param string $type
     * @return Response|null
     */
    public function aresFetch(string $ico, string $type)
    {
        $ico = preg_replace("/[^0-9]/", "", $ico);
        if (empty($ico)) {
            throw new NotFoundException();
        }
        $cacheKey = 'ares_' . md5($ico) . ($type === 'bas' ? '' : $type);
        $cacheData = Cache::read($cacheKey);

        if (!empty($cacheData) && mb_strpos($cacheData, 'Chyba') !== false) {
            Cache::delete($cacheKey);
            $cacheData = false;
        }

        if ($cacheData === false) {
            // darv_or neobsahuje podnikatele
            // darv_std neobsahuje podily v PO
            // darv_bas je výpis z více rejstříků kombinovaný
            try {
                $cacheData = @file_get_contents('https://wwwinfo.mfcr.cz/cgi-bin/ares/darv_' . $type . '.cgi?stdadr=true&ico=' . $ico);
            } catch (Throwable $t) {
                $cacheData = false;
            }
            if (!empty($cacheData)) {
                Cache::write($cacheKey, $cacheData);
            } else {
                return $this->getResponse()->withStatus(404);
            }
        }

        return $this->getResponse()->withType('xml')->withStringBody($cacheData);
    }

    public function shareMyAccount()
    {
        $origin = $this->getCurrentUser()->getOriginIdentity($this->getRequest());
        $this->Users->loadInto($origin, ['AllowedToUsers', 'AllowedToUsers.AllowedUser']);

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $postData = $this->getRequest()->getData('allowed_users_emails', []);
            $postData = !is_array($postData) ? [] : $postData;
            $allowedUsersToDelete = $origin->patchAllowedToUsers($postData, $this->Users);
            if ($this->Users->save($origin)) {
                foreach ($allowedUsersToDelete as $usersToUserEntity) {
                    $this->Users->AllowedUsers->delete($usersToUserEntity);
                }
                $this->Flash->success(__('Uloženo úspěšně'));
                $this->getCurrentUser()->setOriginIdentity(
                    $this->getRequest(),
                    $this->Users->find('withRoles', ['conditions' => ['Users.id' => $origin->id]])->firstOrFail()
                );
            } else {
                $this->Flash->error(__('Formulář obsahuje chyby'));
            }
        }

        $existing_shares = [];
        foreach ($origin->allowed_to_users as $existing_share) {
            $existing_shares[$existing_share->allowed_user->email] = $existing_share->allowed_user->email;
        }

        $this->set(compact('origin', 'existing_shares'));
    }

    public function switchToAllowed(int $target_user_id)
    {
        $target_user = $this->Users->find('forSwitch', [
            'conditions' => [
                'Users.id' => $target_user_id,
            ],
        ])->firstOrFail();

        if (!($this->getCurrentUser()->getOriginIdentity($this->getRequest()))->switchToUser($this->getRequest(), $target_user)) {
            $this->Flash->error(__('Nemáte oprávnění zastupovat tohoto uživatele'));

            return $this->redirect('/');
        }

        return $this->redirect('/');
    }

    public function login()
    {
        if ($this->Auth->user() instanceof User) {
            // already logged in
            $this->redirect('/');

            return;
        }

        $this->set('user', $this->Users->newEntity());

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            /**
             * @var User $user
             */
            $user = $this->Auth->identify();

            // OldDsw provide guidance on account migration
            $is_email = filter_var($this->getRequest()->getData('email'), FILTER_VALIDATE_EMAIL);
            if (OrganizationSetting::getSetting(OrganizationSetting::HAS_DSW) === true && !$is_email && !($user instanceof User)) {
                $this->loadComponent('OldDsw.OldDsw');
                $old_dsw_user = $this->OldDsw->authenticate($this->getRequest()->getData('email'), $this->getRequest()->getData('password'));
                if ($old_dsw_user instanceof Uzivatel) {
                    $user_by_email = $this->Users->find(
                        'all',
                        [
                            'conditions' => [
                                'email' => $old_dsw_user->email,
                            ],
                        ]
                    )->first();
                    if ($user_by_email instanceof User) {
                        $this->Flash->set(sprintf(__('Přihlaste se prosím svým e-mailem %s a heslem k novému účtu'), $old_dsw_user->email), ['element' => 'info']);
                    } else {
                        $this->Flash->set(sprintf(__('Pokusili jste se přihlásit starým uživatelským účtem. Vytvořte si prosím nový účet s e-mailem %s a získáte přístup ke svým historickým žádostem'), $old_dsw_user->email), ['element' => 'info']);
                        $this->Flash->success((new HtmlHelper($this->viewBuilder()->build()))->link(__('Registrovat se'), ['_name' => 'register']), ['escape' => false]);
                    }

                    return;
                }
            }

            if (!($user instanceof User)) {
                $this->Flash->error(__('Přihlášení se nezdařilo'));

                return;
            }

            $this->getRequest()->getSession()->delete(User::SESSION_ORIGIN_IDENTITY);

            if (!$user->is_enabled) {
                // do not send recovery email, if disabled user is not yet member of this portal
                if ($user->hasRole(UserRole::USER_PORTAL_MEMBER, OrgDomainsMiddleware::getCurrentOrganizationId())) {
                    // regenerate email token first
                    $user->regenerateEmailToken();
                    $this->Users->save($user);
                    // send verify_email
                    $this->sendMailSafe('User', 'verifyEmail', [$user]);
                }

                $this->Flash->error(__('Váš účet je zakázán. Musíte nejdříve aktivovat svůj účet odkazem zaslaným Vám do e-mailu.'));

                return;
            }
            if (
                !OrgDomainsMiddleware::getCurrentOrganization()->getSettingValue(OrganizationSetting::ENABLE_LOGIN_USERS, true)
                && !$user->hasRoles(UserRole::ALL_MANAGER, OrgDomainsMiddleware::getCurrentOrganizationId(), true)
            ) {
                $this->Flash->error(__('V tuto chvíli je zakázáno přihlašování uživatel, zkuste to prosím později'));

                return;
            }
            // create member role if not yet created
            $user->createMemberRoleIfNotExists(OrgDomainsMiddleware::getCurrentOrganizationId());
            // this will touch db only if previous call made any field dirty
            try {
                $this->Users->save($user);
            } catch (Exception $ignore) {
                // rare case duplicate index violation
                // this shall not prevent user from being logged in
            }

            $this->Auth->setUser($user);
            $this->Flash->success(__('Vítejte'));
            $this->redirect($this->Auth->redirectUrl());

            return;
        }
    }

    public function register()
    {
        if ($this->Auth->user()) {
            $this->redirect('/');

            return;
        }

        if (!OrgDomainsMiddleware::getCurrentOrganization()->getSettingValue(OrganizationSetting::ENABLE_REGISTRATION, true)) {
            $this->Flash->error(__('Registrace uživatelů aktuálně není povolena, zkuste to prosím později'));
            $this->redirect('/');

            return;
        }

        $user = $this->Users->newEntity();

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $this->Users->patchEntity($user, $this->getRequest()->getData());

            $user->regenerateEmailToken();
            $user->regenerateSalt();
            $user->set('is_enabled', false);
            $user->setNewPassword($this->getRequest()->getData('password'));

            if ($this->Users->save($user)) {
                if ($this->Users->find('all')->count() === 1) {
                    // set first user as systems manager to avoid default user with default password config
                    $user->set('user_roles', [$this->Users->UserRoles->get(UserRole::MANAGER_SYSTEMS)]);
                    $user->set('is_enabled', true);
                    $this->Users->save($user);
                } else {
                    // create association between this portal and the user on successfull registration
                    $user->createMemberRoleIfNotExists(OrgDomainsMiddleware::getCurrentOrganizationId());
                    $this->Users->save($user);
                }

                if ($this->sendMailSafe('User', 'verifyEmail', [$user])) {
                    $this->Flash->success(__('Registrace proběhla úspěšně. Následujte prosím instrukce odeslané vám do e-mailu.'));
                }

                $this->redirect('/');

                return;
            } else {
                $this->Flash->error(__('Registrační formulář obsahuje chyby'));
            }
        }

        $this->set(compact('user'));
    }

    public function changePassword()
    {
        $user = $this->Users->get($this->Auth->user('id'));

        if ($user->id !== $user->getOriginIdentity($this->getRequest())->id) {
            $this->Flash->error(__('Pro změnu hesla, se musíte nejdříve přepnout zpět do svého účtu'));
            $this->redirect('/');

            return;
        }

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            // check old password first
            if (password_verify($this->getRequest()->getData('old_password') . $user->salt, $user->password)) {
                $new_password = $this->getRequest()->getData('new_password');
                if (empty($new_password)) {
                    $user->setError('new_password', __('Nové heslo nesmí být prázdné'));
                } else {
                    $user->regenerateSalt();
                    $user->setNewPassword($new_password);
                    if ($this->Users->save($user)) {
                        $this->Flash->success(__('Heslo bylo změněno úspěšně'));
                        $this->redirect('/');
                    } else {
                        $this->Flash->error(__('Chyba při ukládání'));
                    }
                }
            } else {
                $user->setError('old_password', __('Aktuální heslo nesouhlasí'));
            }
        }

        $this->set(compact('user'));
    }

    public function logout()
    {
        $this->Auth->logout();
        $this->getRequest()->getSession()->destroy();
        $this->Flash->success(__('Odhlášení proběhlo úspěšně'));
        $this->redirect('/');
    }

    public function verifyEmail()
    {
        $user = $this->Users->find(
            'all',
            [
                'conditions' => [
                    'Users.id' => $this->getRequest()->getParam('requestId'),
                    'Users.mail_verification_code' => $this->getRequest()->getParam('token'),
                ],
                'contain' => [
                    'StareUcty',
                ],
            ]
        )->first();
        if (!($user instanceof User)) {
            $this->Flash->error(__('Neplatný odkaz pro obnovu hesla. Nechte si prosím zaslat nový.'));

            return $this->redirect(['_name' => 'password_recovery']);
        }
        // previous will fail hard if user is not matched
        $user->regenerateEmailToken();
        $user->set('is_enabled', true);
        if ($this->Users->save($user)) {
            if (OrganizationSetting::getSetting(OrganizationSetting::HAS_DSW) === true) {
                $this->loadComponent('OldDsw.OldDsw');
                // pair old dsw accounts to user by verified email address
                try {
                    /**
                     * @var Ucet[] $ucty
                     */
                    $ucty = $this->OldDsw->getAccounts()->where(['Ucty.email' => $user->email])->toArray();
                    $uzivatele = $this->OldDsw->getAccountsByUsersEmail($user->email);
                    // keep old verified accounts that were set by portal manager for example
                    foreach ($user->stare_ucty as $stary_ucet) {
                        $user->stare_ucty[$stary_ucet->id] = $stary_ucet;
                    }
                    // add all newly matched accounts by verified email
                    foreach (($ucty + $uzivatele) as $ucet) {
                        // add unique
                        $user->stare_ucty[$ucet->id] = $ucet;
                    }
                    // reset keys
                    $user->stare_ucty = array_values($user->stare_ucty);
                    // force save association
                    $user->setDirty('stare_ucty');
                    $this->Users->save($user);
                } catch (Throwable $t) {
                    Log::error($t->getMessage(), $t);
                }
            }

            $this->Flash->success(__('Vaše e-mailová adresa byla ověřena. Nyní se můžete přihlásit'));
        } else {
            $this->Flash->error(__('Nebylo možné povolit váš účet'));
        }

        return $this->redirect(['_name' => 'login']);
    }

    public function passwordRecovery()
    {
        if ($this->Auth->user()) {
            $this->redirect('/');

            return;
        }

        $oneShotStoreKey = 'passwordRecovery_oneShot';
        $oneShotCode = $this->getOrGenerateOneShotCode($oneShotStoreKey);
        $this->set(compact('oneShotCode'));

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            if ($this->getRequest()->getData('oneShot') != $oneShotCode) {
                $this->Flash->error(__('Obnovte stránku a odešlete formulář znovu, prosím'));

                return;
            }
            $this->regenerateOneShotCode($oneShotStoreKey);

            /**
             * @var User $user_by_email
             */
            $user_by_email = $this->Users->find(
                'all',
                [
                    'conditions' => [
                        'Users.email' => $this->getRequest()->getData('email'),
                    ],
                    'contain' => [
                        'UsersToRoles',
                    ],
                ]
            )->first();

            // OldDsw provide guidance on account migration
            $is_email = filter_var($this->getRequest()->getData('email'), FILTER_VALIDATE_EMAIL);
            if (OrganizationSetting::getSetting(OrganizationSetting::HAS_DSW) === true && $is_email && !($user_by_email instanceof User)) {
                $this->loadComponent('OldDsw.OldDsw');
                $old_dsw_accounts = $this->OldDsw->getAccountsByUsersEmail($this->getRequest()->getData('email'));
                if (!empty($old_dsw_accounts)) {
                    $this->Flash->set(sprintf(__('Pokusili jste se obnovit heslo ke starému uživatelskému účtu. Vytvořte si prosím nový účet s e-mailem %s a získáte přístup ke svým historickým žádostem'), $this->getRequest()->getData('email')), ['element' => 'info']);
                    $this->Flash->success((new HtmlHelper($this->viewBuilder()->build()))->link(__('Registrovat se'), ['_name' => 'register']), ['escape' => false]);

                    return;
                }
            }

            if (empty($user_by_email) || !($user_by_email->hasRole(UserRole::USER_PORTAL_MEMBER, OrgDomainsMiddleware::getCurrentOrganizationId()))) {
                $this->Flash->success(__('Pokračujte prosím otevřením odkazu v e-mailu, který jsme vám právě zaslali'));

                return;
            }

            $user_by_email->regenerateEmailToken();
            $this->Users->save($user_by_email);
            if ($this->sendMailSafe('User', 'forgotPassword', [$user_by_email])) {
                $this->Flash->success(__('Pokračujte prosím otevřením odkazu v e-mailu, který jsme vám právě zaslali'));
            }
        }
    }

    public function publicIncomeHistories()
    {
        $sources = $this->PublicIncomeHistories->PublicIncomeSources->find(
            'list',
            [
                'conditions' => [
                    'OR' => [
                        'PublicIncomeSources.organization_id IS' => null,
                        'PublicIncomeSources.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                    ],
                ],
            ]
        )->order(['organization_id' => 'DESC'])->toArray();
        $history = $this->PublicIncomeHistories->newEntity();

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $history = $this->PublicIncomeHistories->patchEntity(
                $history,
                [
                    'user_id' => $this->Auth->user('id'),
                ] + $this->getRequest()->getData()
            );
            if ($this->PublicIncomeHistories->save($history)) {
                $this->Flash->success(__('Přidáno úspěšně'));
            } else {
                $this->Flash->error(__('Při ukládání nastala chyba'));
            }
        }
        $histories = $this->PublicIncomeHistories->find(
            'all',
            ['conditions' => [
                'PublicIncomeHistories.user_id' => $this->Auth->user('id'),
            ], 'contain' => ['PublicIncomeSources']]
        )->order(['fiscal_year' => 'DESC']);

        $this->set(compact('histories', 'history', 'sources'));
    }

    public function incomeHistoryDelete(int $id)
    {
        $history = $this->PublicIncomeHistories->get(
            $id,
            [
                'conditions' => [
                    'PublicIncomeHistories.user_id' => $this->Auth->user('id'),
                ],
            ]
        );
        if ($this->PublicIncomeHistories->delete($history)) {
            $this->Flash->success(__('Smazáno úspěšně'));
        } else {
            $this->Flash->error(__('Záznam nebylo možné smazat'));
        }

        return $this->redirect(['action' => 'publicIncomeHistories']);
    }

    public function inviteAccept()
    {
        if ($this->Auth->user()) {
            $this->redirect('/');

            return;
        }
        /**
         * @var User $user
         */
        $user = $this->Users->find(
            'all',
            [
                'conditions' => [
                    'Users.mail_verification_code' => $this->getRequest()->getParam('token'),
                    'Users.id' => $this->getRequest()->getParam('requestId'),
                ],
            ]
        )->first();

        if (empty($user)) {
            $this->Flash->error(__('Neplatný odkaz pozvánky'));
            $this->redirect('/');

            return;
        }

        if ($user->password != 'INVITE') {
            $user->regenerateEmailToken();
            $this->Users->save($user);
            $this->Flash->error(__('Neplatný odkaz na pozvánku'));
            $this->redirect('/');

            return;
        }
        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $new_password = $this->getRequest()->getData('new_password');
            $new_password_check = $this->getRequest()->getData('new_password_check');
            if (empty($new_password)) {
                $user->setError('new_password', __('Heslo musí být vyplněno'));
            }
            if (empty($new_password_check)) {
                $user->setError('new_password_check', __('Heslo musí být vyplněno'));
            }
            if (strcmp($new_password, $new_password_check) != 0) {
                $user->setError('new_password', __('Hesla nejsou stejná'));
                $user->setError('new_password_check', __('Hesla nejsou stejná'));
            }
            if ($user->hasErrors()) {
                $this->Flash->error(__('Formulář obsahuje chyby'));
            } else {
                $user->regenerateSalt();
                $user->setNewPassword($new_password);
                $user->is_enabled = true;
                $user->regenerateEmailToken();
                if ($this->Users->save($user)) {
                    $this->Flash->success(__('Vaše e-mailová adresa byla ověřena. Nyní se můžete přihlásit'));
                } else {
                    $this->Flash->error(__('Nebylo možné povolit váš účet'));
                }
                $this->redirect(['_name' => 'login']);

                return;
            }
        }
        $this->set(compact('user'));
    }

    public function passwordRecoveryGo()
    {
        if ($this->Auth->user()) {
            $this->redirect('/');

            return;
        }

        /**
         * @var User $user
         */
        $user = $this->Users->find(
            'all',
            [
                'conditions' => [
                    'Users.mail_verification_code' => $this->getRequest()->getParam('token'),
                    'Users.id' => $this->getRequest()->getParam('requestId'),
                ],
            ]
        )->first();

        if (!($user instanceof User)) {
            $this->Flash->error(__('Neplatný odkaz pro obnovu hesla. Nechte si prosím zaslat nový.'));

            $this->redirect(['_name' => 'password_recovery']);

            return;
        }

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            if (!empty($this->getRequest()->getData('password'))) {
                if (!empty($this->getRequest()->getData('password_2'))) {
                    if (strcmp($this->getRequest()->getData('password'), $this->getRequest()->getData('password_2')) !== 0) {
                        $user->setError('password', __('Hesla nejsou stejná'));
                    }
                }
                $user->set('mail_verification_code', null);
                $user->regenerateSalt();
                $user->setNewPassword($this->getRequest()->getData('password'));
                $user->is_enabled = true;
                if ($this->Users->save($user)) {
                    $this->Flash->success(__('Nové heslo nastaveno, nyní se můžete přihlásit'));
                    $this->redirect(['_name' => 'login']);
                } else {
                    $this->Flash->error(__('Nebylo možné nastavit nové heslo'));
                }
            }
        }

        $this->set(compact('user'));
    }
}
