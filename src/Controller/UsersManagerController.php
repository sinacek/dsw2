<?php

namespace App\Controller;

use App\Middleware\OrgDomainsMiddleware;
use App\Model\Entity\OrganizationSetting;
use App\Model\Entity\User;
use App\Model\Entity\UserRole;
use App\Model\Table\UsersTable;
use Cake\Http\Exception\ForbiddenException;
use Cake\ORM\Query;
use OldDsw\Controller\Component\OldDswComponent;

/**
 * @property UsersTable $Users
 * @property OldDswComponent $OldDsw
 */
class UsersManagerController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Users');
        if (OrganizationSetting::getSetting(OrganizationSetting::HAS_DSW) === true) {
            $this->loadComponent('OldDsw.OldDsw');
        }
    }

    public function isAuthorized($user = null)
    {
        /**
         * @var User $user
         */
        $user = $this->Auth->user();

        return parent::isAuthorized($user) && $user->isUsersManager();
    }

    private function canModifyUser(User $otherUser, ?int $otherUserRole = null, bool $flashError = true, bool $redirectToIndex = true): bool
    {
        if ($this->getCurrentUser()->isSystemsManager()) {
            return true;
        }

        if ($this->getCurrentUser()->isPortalsManager() && $otherUser->id === $this->getCurrentUser()->id) {
            return true;
        }

        $canModify = true;
        $errorMessage = null;

        if ($canModify && $this->getCurrentUser()->id === $otherUser->id) {
            $errorMessage = __('Není možné upravovat vlastní role');
            $canModify = false;
        }

        if (
            $canModify && (!$otherUser->hasRole(UserRole::USER_PORTAL_MEMBER, OrgDomainsMiddleware::getCurrentOrganizationId())
                || $otherUser->hasRole(UserRole::MANAGER_SYSTEMS))
        ) {
            $errorMessage = __('Tento uživatel není ve vaší pravomoci');
            $canModify = false;
        }

        $currentUserOrganizations = $this->getCurrentUser()->getOrganizationIdsWhereUserHasRoleWithFallbacks(UserRole::MANAGER_USERS, false, [UserRole::MANAGER_PORTALS]);

        if ($canModify && empty($currentUserOrganizations)) {
            $errorMessage = __('K této akci nemáte oprávnění');
            $canModify = false;
        }

        if ($canModify && !empty($otherUserRole) && !in_array($otherUserRole, array_keys($this->getRolesCurrentUserCanModify()->toArray()))) {
            $errorMessage = __('Nemáte právo přidělovat tuto roli');
            $canModify = false;
        }

        if ($flashError && !empty($errorMessage)) {
            $this->Flash->error($errorMessage);
        }

        if (!$canModify && $redirectToIndex) {
            $this->redirect(['action' => 'index']);
        }

        return $canModify;
    }

    private function getRolesCurrentUserCanModify(): Query
    {
        $roles = $this->Users->UserRoles->find('list')->where(['UserRoles.id !=' => UserRole::USER_PORTAL_MEMBER]);

        if (!$this->getCurrentUser()->isSystemsManager()) {
            $roles->where(['UserRoles.id !=' => UserRole::MANAGER_SYSTEMS]);
            if (!$this->getCurrentUser()->isPortalsManager()) {
                $roles->where(['UserRoles.id !=' => UserRole::MANAGER_PORTALS]);
            }
        }

        return $roles;
    }

    private function getOrganizationsCurrentUserCanModify(): Query
    {
        $organizations = $this->Users->UsersToRoles->Organizations->find('list');

        if (!$this->getCurrentUser()->isSystemsManager()) {
            // simplify, only show / allow modify of users for currently opened portal, not all portals current user can manage
            $organizations->where(['Organizations.id' => OrgDomainsMiddleware::getCurrentOrganizationId()]);
        }

        return $organizations;
    }

    public function index()
    {
        /**
         * @var User $user
         */
        $user = $this->Auth->user();
        $orgs = $user->getOrganizationIdsWhereUserHasRoleWithFallbacks(UserRole::MANAGER_USERS, false, [UserRole::MANAGER_PORTALS, UserRole::MANAGER_SYSTEMS]);

        if (empty($orgs) && !$this->getCurrentUser()->isSystemsManager()) {
            throw new ForbiddenException();
        }

        $users = $this->Users->find(
            'all',
            [
                'contain' => [
                    'UsersToRoles',
                ],
            ]
        );
        if (!$this->getCurrentUser()->isSystemsManager()) {
            // is not global users manager, global portals manager or systems manager
            // allow only specific organizations to be fetched
            $users->matching(
                'UsersToRoles',
                function (Query $q) {
                    return $q->where(['UsersToRoles.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId()]);
                }
            )->distinct('Users.id');
        }

        $organizations = $this->Users->UsersToRoles->Organizations->find('list');

        $this->set('organizations', $organizations->toArray());
        $this->set('roles', $this->Users->UserRoles->find('list')->toArray());
        $this->set('users', $users->toArray());
    }

    public function loginAs(int $userId)
    {
        $user = $this->Users->find(
            'withRoles',
            [
                'conditions' => [
                    'Users.id' => $userId,
                ],
            ]
        )->first();
        if ($user instanceof User && $this->canModifyUser($user)) {
            $this->Auth->setUser($user);
        }
        $this->redirect('/');
    }

    public function oldLink(int $user_id)
    {
        if (!OrganizationSetting::getSetting(OrganizationSetting::HAS_DSW) || !isset($this->OldDsw) || $user_id < 1) {
            $this->redirect(['action' => 'index']);

            return;
        }
        $user = $this->Users->get(
            $user_id,
            [
                'contain' => [
                    'StareUcty',
                    'UsersToRoles',
                ],
            ]
        );
        if (!$this->canModifyUser($user)) {
            return;
        }
        $ucty = $this->OldDsw->getAccounts()->find(
            'list',
            [
                'valueField' => 'nameWithYear',
            ]
        );

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $user = $this->Users->patchEntity($user, $this->getRequest()->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('Uloženo úspěšně'));
            } else {
                $this->Flash->error(__('Formulář obsahuje chyby'));
            }
        }

        $this->set(compact('user', 'ucty'));
    }

    public function invite()
    {
        $user = $this->Users->newEntity();
        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $user = $this->Users->find(
                'all',
                [
                    'conditions' => [
                        'Users.email' => $this->getRequest()->getData('email'),
                    ],
                ]
            )->contain(['UsersToRoles'])->first();

            if (empty($user)) {
                $user = $this->Users->newEntity($this->getRequest()->getData());
            }

            if (($user->isNew() && empty($user->getError('email'))) || ($user->salt === 'INVITE')) {
                if ($user->isNew()) {
                    // create new with correct params
                    $data = [
                        'email' => $user->email,
                        'salt' => 'INVITE',
                        'password' => 'INVITE',
                        'is_enabled' => false,
                    ];
                    $user = $this->Users->newEntity($data);
                }

                // regenerate always
                $user->regenerateEmailToken();
                $user->createMemberRoleIfNotExists(OrgDomainsMiddleware::getCurrentOrganizationId());

                if ($this->Users->save($user)) {
                    if ($this->sendMailSafe('User', 'invite', [$user])) {
                        $this->Flash->success(sprintf(__('Uživatel %s byl úspěšně pozván'), $user->email));
                    }
                    $this->redirect(['action' => 'edit', $user->id]);
                } else {
                    $this->Flash->error(__('Uživatele s danou e-mailovou adresou, není možné pozvat'));
                }
            } else {
                $this->Flash->error(__('Uživatel s touto e-mailovou adresou již existuje, nelze jej pozvat'));
            }
        }
        $this->set(compact('user'));
        $this->set('crumbs', [__('Správa Uživatelů') => 'admin_users']);
    }

    public function edit(int $id)
    {
        $user = $this->Users->get(
            $id,
            [
                'contain' => [
                    'UsersToRoles',
                    'UsersToRoles.UserRoles',
                    'UsersToRoles.Organizations',
                ],
            ]
        );

        if (!$this->canModifyUser($user)) {
            return;
        }

        $organizations = $this->getOrganizationsCurrentUserCanModify();
        $roles = $this->getRolesCurrentUserCanModify();

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $user = $this->Users->patchEntity($user, $this->getRequest()->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('Uloženo úspěšně'));
            } else {
                $this->Flash->error(__('Formulář obsahuje chyby'));
            }
        }

        $this->set('organizations', $organizations->toArray());
        $this->set('user', $user);
        $this->set('roles', $roles->toArray());
        $this->set('role', $this->Users->UsersToRoles->newEntity());
        $this->set('crumbs', [__('Správa Uživatelů') => 'admin_users']);
    }

    public function roleAdd(int $id)
    {
        $user = $this->Users->get(
            $id,
            [
                'contain' => [
                    'UsersToRoles',
                    'UsersToRoles.UserRoles',
                    'UsersToRoles.Organizations',
                ],
            ]
        );
        $userToRole = $this->Users->UsersToRoles->newEntity($this->getRequest()->getData());
        $this->redirect($this->referer());

        if (!$this->canModifyUser($user, $userToRole->user_role_id, true, false)) {
            return;
        }

        $organizations = $this->getOrganizationsCurrentUserCanModify();

        if (!in_array($userToRole->organization_id, array_keys($organizations->toArray()))) {
            throw new ForbiddenException(__('Nemáte právo upravovat tuto organizaci'));
        }

        if ($user->hasRole($userToRole->user_role_id, $userToRole->organization_id)) {
            $this->Flash->success(__('Uživatel již danou roli má'));
        } else {
            $user->users_to_roles[] = $userToRole;
            $user->setDirty('users_to_roles');
            if ($this->Users->save($user)) {
                $this->Flash->success(__('Role úspěšně přidána'));
            } else {
                $this->Flash->error(__('Chyba při ukládání'));
            }
        }
    }

    public function roleDelete(int $user_id, int $role_id)
    {
        $user = $this->Users->get(
            $user_id,
            [
                'contain' => [
                    'UsersToRoles',
                    'UsersToRoles.UserRoles',
                    'UsersToRoles.Organizations',
                ],
            ]
        );
        $userToRole = $this->Users->UsersToRoles->get($role_id);
        $this->redirect($this->referer());

        if (!$this->canModifyUser($user, $userToRole->user_role_id, true, false)) {
            return;
        }

        $organizations = $this->getOrganizationsCurrentUserCanModify();

        if (!in_array($userToRole->organization_id, array_keys($organizations->toArray()))) {
            $this->Flash->error(__('Nemáte právo upravovat tuto organizaci'));
            $this->redirect(['action' => 'index']);

            return;
        }

        if ($this->Users->UsersToRoles->delete($userToRole)) {
            $this->Flash->success(__('Úspěšně odstraněno'));
        } else {
            $this->Flash->error(__('Chyba při odstraňování'));
        }
    }
}
