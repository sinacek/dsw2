<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use App\Model\Entity\User;
use App\Model\Entity\UserRole;
use App\Model\Table\DomainsTable;
use Cake\Controller\Controller;
use Cake\Core\Configure;
use Cake\Http\Exception\ForbiddenException;
use Cake\Mailer\MailerAwareTrait;
use Exception;
use Throwable;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 *
 * @property DomainsTable $Domains
 */
class AppController extends Controller
{
    use MailerAwareTrait;

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     * @throws Exception
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent(
            'RequestHandler',
            [
                'enableBeforeRedirect' => false,
            ]
        );
        $this->loadComponent('Flash');

        $this->loadComponent('Security');

        $this->loadComponent(
            'Auth',
            [
                'loginAction' => [
                    'controller' => 'Users',
                    'action' => 'login',
                    'prefix' => false,
                ],
                'authError' => __('Musíte se nejdříve přihlásit'),
                'authenticate' => [
                    'FormSalt' => [
                        'userModel' => 'Users',
                        'fields' => [
                            'username' => 'email',
                            'password',
                        ],
                        'finder' => 'withRoles',
                    ],
                ],
                'loginRedirect' => ['controller' => 'Public', 'action' => 'index'],
                'logoutRedirect' => '/',
                'unauthorizedRedirect' => '/',
                'storage' => 'Session',
                'authorize' => [
                    'Controller',
                ],
            ]
        );

        $this->loadModel('Domains');

        if ($this->getCurrentUser() instanceof User && $this->getCurrentUser()->hasRole(UserRole::MANAGER_SYSTEMS)) {
            Configure::write('debug', true);
        }
    }

    public function getCurrentUser(): ?User
    {
        return $this->Auth->user();
    }

    public function getCurrentUserId(): ?int
    {
        $user = $this->getCurrentUser();

        return ($user instanceof User) ? $user->id : null;
    }

    public function isAuthorized($user = null)
    {
        if ($user === null) {
            $user = $this->Auth->user();
        }

        return !empty($user) && $user instanceof User && $user->is_enabled;
    }

    public function isAuthorizedToOrganization(int $organization_id = null, int $role_id = UserRole::MANAGER_SYSTEMS)
    {
        /**
         * @var User $user
         */
        $user = $this->Auth->user();

        if (empty($user) || !($user instanceof User) || !$user->is_enabled) {
            return false;
        }

        return $user->hasRole($role_id, $organization_id);
    }

    public function checkIsAuthorizedToOrganizationOrThrow(int $organization_id = null, int $role_id = UserRole::MANAGER_SYSTEMS, $error_class = ForbiddenException::class)
    {
        if (!$this->isAuthorizedToOrganization($organization_id, $role_id)) {
            if (!$this->isAuthorizedToOrganization()) {
                throw new $error_class();
            }
        }
    }

    protected function getOrGenerateOneShotCode(string $storeKey)
    {
        $oneShotCode = $this->getRequest()->getSession()->read($storeKey);
        if (empty($oneShotCode)) {
            return $this->regenerateOneShotCode($storeKey);
        }

        return $oneShotCode;
    }

    protected function regenerateOneShotCode(string $storeKey)
    {
        $oneShotCode = random_str('alphanum', 32);
        $this->getRequest()->getSession()->write($storeKey, $oneShotCode);

        return $oneShotCode;
    }

    protected function persistReferer()
    {
        $this->getRequest()->getSession()->write('refer.' . md5($this->getRequest()->getRequestTarget()), $this->referer());
    }

    protected function retrieveReferer($default = null)
    {
        return $this->getRequest()->getSession()->read('refer.' . md5($this->getRequest()->getRequestTarget())) ?? $default ?? $this->referer();
    }

    public function sendMailSafe(string $mailer, string $mailAction, array $args = []): bool
    {
        try {
            $this->getMailer($mailer)->send($mailAction, $args);

            return true;
        } catch (Throwable $t) {
            $this->Flash->error(__('Odesílání e-mailu selhalo, chyba: ') . $t->getMessage());
        }

        return false;
    }
}
