<?php

namespace App\Controller;

use App\Middleware\OrgDomainsMiddleware;
use App\Model\Entity\User;
use App\Model\Table\EvaluationCriteriaTable;

/**
 * @property EvaluationCriteriaTable $EvaluationCriteria
 */
class EvaluationCriteriaController extends AppController
{
    public function isAuthorized($user = null)
    {
        /**
         * @var User $user
         */
        $user = $this->Auth->user();

        return parent::isAuthorized($user) && $user->isGrantsManager();
    }

    public function index()
    {
        $this->set(
            'criteria',
            $this->EvaluationCriteria->find(
                'all',
                [
                    'conditions' => [
                        'EvaluationCriteria.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                        'EvaluationCriteria.parent_id IS' => null,
                    ],
                ]
            )
        );
    }

    public function criteriumAddModify(int $id = 0)
    {
        if ($id > 0) {
            $criterium = $this->EvaluationCriteria->get(
                $id,
                [
                    'conditions' => [
                        'EvaluationCriteria.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                    ],
                    'contain' => [
                        'ChildEvaluationCriteria',
                    ],
                ]
            );
        } else {
            $criterium = $this->EvaluationCriteria->newEntity([
                'parent_id' => $this->getRequest()->getParam('parent_id', null),
            ]);
        }
        $criteria = $this->EvaluationCriteria->find(
            'list',
            [
                'conditions' => [
                    'EvaluationCriteria.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                    // allow only 2 level, group name and group leafs
                    'EvaluationCriteria.parent_id IS' => null,
                ],
            ]
        )->toArray();

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $criterium = $this->EvaluationCriteria->patchEntity($criterium, $this->getRequest()->getData());
            $criterium->organization_id = OrgDomainsMiddleware::getCurrentOrganizationId();
            if ($criterium->parent_id && !in_array($criterium->parent_id, array_keys($criteria))) {
                $criterium->parent_id = null;
            }
            if ($this->EvaluationCriteria->save($criterium)) {
                $this->Flash->success(__('Hotovo'));
                $this->redirect($this->retrieveReferer(['action' => 'index']));
            } else {
                $this->Flash->error(__('Formulář obsahuje chyby'));
            }
        } elseif ($this->getRequest()->getParam('parent_id')) {
            $for_parent = $this->getRequest()->getParam('parent_id');
            if (in_array($for_parent, array_keys($criteria))) {
                $criterium->parent_id = $for_parent;
            }
        }

        $this->persistReferer();
        $this->set(compact('criterium', 'criteria'));
        $crumbs = [__('Hodnotící kritéria') => 'admin_evaluation_criteria'];
        if (!empty($criterium->parent_id)) {
            $crumbs[$criteria[$criterium->parent_id]] = ['action' => 'criteriumAddModify', $criterium->parent_id];
        }
        $this->set('crumbs', $crumbs);
    }

    public function criteriumCopy(int $id)
    {
        $criterium = $this->EvaluationCriteria->get(
            $id,
            [
                'conditions' => [
                    'EvaluationCriteria.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                ],
                'contain' => [
                    'ChildEvaluationCriteria',
                ],
            ]
        );
        $criterium->unsetProperty('id')->unsetProperty('created')->unsetProperty('modified');
        $criterium->name .= sprintf(" - %s %s", __('Kopie'), date('Y-m-d H:i:s'));
        $criterium->isNew(true);
        foreach ($criterium->child_evaluation_criteria as $index => $childCriterium) {
            $childCriterium->unsetProperty('id')->unsetProperty('modified')->unsetProperty('created')->unsetProperty('parent_id');
            $childCriterium->isNew(true);
        }
        if ($this->EvaluationCriteria->save($criterium)) {
            $this->Flash->success(__('Kopie vytvořena úspěšně'));
        } else {
            $this->Flash->error(__('Nebylo možné vytvořit kopii'));
        }

        return $this->redirect($criterium->parent_id ? ['action' => 'addModify', $criterium->id] : ['action' => 'index']);
    }

    public function criteriumDelete(int $id = 0)
    {
        if (empty($id)) {
            $this->redirect(['action' => 'index']);

            return;
        }
        $criterium = $this->EvaluationCriteria->get(
            $id,
            [
                'conditions' => [
                    'EvaluationCriteria.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                ],
            ]
        );
        $this->EvaluationCriteria->delete($criterium);
        $this->redirect(['action' => 'index']);
    }
}
