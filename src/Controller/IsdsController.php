<?php

namespace App\Controller;

use App\Controller\Component\IsdsComponent;
use App\Model\Entity\Identity;
use App\Model\Entity\OrganizationSetting;
use App\Model\Table\IdentitiesTable;
use Cake\Log\Log;
use Exception;
use Throwable;

/**
 * @property IsdsComponent $Isds
 * @property IdentitiesTable $Identities
 */
class IsdsController extends AppController
{
    public const ISDS_APP_TOKEN = 'isds.app_token';
    public const ISDS_TIME_LIMITED_ID = 'isds.time_limited_id';
    public const ISDS_VERIFIED_DS_ID = 'isds.verified.ds_id';
    public const ISDS_VERIFIED_USER_TYPE = 'isds.verified.user_type';
    public const ISDS_VERIFIED_DS_TYPE = 'isds.verified.db_type';
    public const ISDS_VERIFIED_SUBJECT_NAME = 'isds.verified.full_user_name';
    public const ISDS_VERIFIED_FIRM_NAME = 'isds.verified.firm_name';
    public const ISDS_VERIFIED_ICO = 'isds.verified.ico';
    public const ISDS_RAW_USER_ATTRIBUTES = 'isds.raw.user_attributes';
    public const ISDS_CONCEPT_DM_ID = 'isds.concept.dm_id';
    public const ISDS_CONCEPT_STATUS_CODE = 'isds.concept.status_code';
    public const ISDS_CONCEPT_STATUS_MESSAGE = 'isds.concept.status_message';

    public const MAP_IDENTITY_STORAGE_ISDS_ATRRIBUTE = [
        Identity::ISDS_VERIFIED_BUSINESS_ID => self::ISDS_VERIFIED_ICO,
        Identity::ISDS_VERIFIED_BUSINES_NAME => self::ISDS_VERIFIED_FIRM_NAME,
        Identity::ISDS_VERIFIED_DATABOX_ID => self::ISDS_VERIFIED_DS_ID,
        Identity::ISDS_VERIFIED_DATABOX_TYPE => self::ISDS_VERIFIED_DS_TYPE,
        Identity::CONTACT_DATABOX => self::ISDS_VERIFIED_DS_ID,
    ];

    public const ISDS_AFTER_AUTH_HANDLER = 'isds.after_auth_handler';

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Isds');
        $this->loadModel('Identities');
    }

    public function authRedirect()
    {
        $atsId = $this->Isds->filter_atsId(OrganizationSetting::getSetting(OrganizationSetting::DS_ATS_ID));
        if (empty($atsId)) {
            $this->Flash->error(sprintf(__('%s není nastaveno, kontaktujte prosím správce dotačního portálu'), 'Datová Schránka atsId'));
            // not referer, to prevent infinite loop
            return $this->redirect('/');
        }
        $url = $this->Isds->getAtsLoginUrl($atsId, $this->getRequest()->getSession()->read(self::ISDS_APP_TOKEN));

        return $this->redirect($url);
    }

    public function authCallback(string $sessionId)
    {
        $sessionId = $this->Isds->filter_sessionId($sessionId);
        if (empty($sessionId)) {
            $this->Flash->error(__('Portál Datových Schránek neposkytl platnou odpověď, akce není možná'));

            return $this->redirect('/');
        }

        list($cert, $key) = $this->Isds->getCertificateAndPrivateKey();
        if (empty($cert) || empty($key)) {
            return $this->redirect('/');
        }

        $xml = $this->Isds->getAuthConfirmationRequest($sessionId);
        $url = $this->Isds->getAuthConfirmationRequestUrl();
        $response = null;
        $parsed = null;
        try {
            $response = $this->Isds->execSoapAction($url, $cert, $key, $xml);
            Log::debug('isds returned raw xml ' . $response);
            $parsed = $this->Isds->extractAuthConfirmationResponse($response);
            Log::debug('isds returned: ' . json_encode($parsed));
            if (!empty($parsed['timeLimitedId'])) {
                $this->getRequest()->getSession()->write(self::ISDS_TIME_LIMITED_ID, $parsed['timeLimitedId']);
            } else {
                throw new Exception('timeLimitedId not found');
            }
            if (isset($parsed['appToken'])) {
                $this->getRequest()->getSession()->write(self::ISDS_APP_TOKEN, $parsed['appToken']);
            }
            //else {
            //    throw new Exception('appToken not found');
            //}
            if (isset($parsed['dbID'])) {
                $this->getRequest()->getSession()->write(self::ISDS_VERIFIED_DS_ID, $parsed['dbID']);
            }
            if (isset($parsed['firmName'])) {
                $this->getRequest()->getSession()->write(self::ISDS_VERIFIED_FIRM_NAME, $parsed['firmName']);
            }
            if (isset($parsed['ic'])) {
                $this->getRequest()->getSession()->write(self::ISDS_VERIFIED_ICO, $parsed['ic']);
            }
            if (isset($parsed['fullUserName'])) {
                $this->getRequest()->getSession()->write(self::ISDS_VERIFIED_SUBJECT_NAME, $parsed['fullUserName']);
            }
            if (isset($parsed['dbType'])) {
                $this->getRequest()->getSession()->write(self::ISDS_VERIFIED_DS_TYPE, $parsed['dbType']);
            }
            if (isset($parsed['userType'])) {
                $this->getRequest()->getSession()->write(self::ISDS_VERIFIED_USER_TYPE, $parsed['userType']);
            }
            if (isset($parsed['conceptDmId'])) {
                $this->getRequest()->getSession()->write(self::ISDS_CONCEPT_DM_ID, $parsed['conceptDmId']);
            }
            if (isset($parsed['conceptStatusCode'])) {
                $this->getRequest()->getSession()->write(self::ISDS_CONCEPT_STATUS_CODE, $parsed['conceptStatusCode']);
            }
            if (isset($parsed['conceptStatusMessage'])) {
                $this->getRequest()->getSession()->write(self::ISDS_CONCEPT_STATUS_MESSAGE, $parsed['conceptStatusMessage']);
            }
            $this->getRequest()->getSession()->write(self::ISDS_RAW_USER_ATTRIBUTES, $parsed);
        } catch (Throwable $t) {
            $this->getRequest()->getSession()->delete(self::ISDS_APP_TOKEN);
            $this->getRequest()->getSession()->delete(self::ISDS_TIME_LIMITED_ID);
            $this->Flash->error(__('Nebylo možné získat identifikátor ze systému Datových Schránek'));

            Log::error($t->getMessage());
            Log::error($t->getTraceAsString());
            Log::error('response: ' . $response);
            Log::error('parsed: ' . json_encode($parsed));

            return $this->redirect('/');
        }

        // return to user handler to perform whatever was meant to be done after ISDS auth
        return $this->redirect($this->getRequest()->getSession()->consume(self::ISDS_AFTER_AUTH_HANDLER) ?? ['action' => 'storeVerifiedAttributes']);
    }

    public function storeVerifiedAttributes()
    {
        if ($this->Isds->storeVerifiedAttributes($this)) {
            $this->Flash->success(__('Údaje z datové schránky byly úspěšně získány'));
        } else {
            $this->Flash->error(__('Údaje z datové schránky bohužel nebylo možné získat'));
        }
        $this->redirect(['_name' => 'update_self_info']);
    }
}
