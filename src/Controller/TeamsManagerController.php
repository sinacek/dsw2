<?php

namespace App\Controller;

use App\Middleware\OrgDomainsMiddleware;
use App\Model\Entity\Program;
use App\Model\Entity\User;
use App\Model\Entity\UserRole;
use App\Model\Table\ProgramsTable;
use App\Model\Table\TeamsTable;
use App\Model\Table\UsersTable;
use Cake\Database\Query;
use Cake\Utility\Hash;

/**
 * @property UsersTable $Users
 * @property TeamsTable $Teams
 * @property ProgramsTable $Programs
 */
class TeamsManagerController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Users');
        $this->loadModel('Teams');
        $this->loadModel('Programs');
    }

    public function isAuthorized($user = null)
    {
        /**
         * @var User $user
         */
        $user = $this->Auth->user();

        return parent::isAuthorized($user) && $user->isUsersManager();
    }

    public function index()
    {
        $this->set('teams', $this->finder()->find('all'));
    }

    private function finder()
    {
        return $this->Teams->find(
            'all',
            [
                'conditions' => [
                    'Teams.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                ],
                'contain' => [
                    'Users',
                    'FormalCheckPrograms',
                    'PriceProposalPrograms',
                    'PriceApprovalPrograms',
                    'CommentsPrograms',
                    'RequestManagerPrograms',
                ],
            ]
        );
    }

    public function addModify(int $id = 0)
    {
        $team = $id > 0 ? $this->finder()->where(['Teams.id' => $id])->firstOrFail() : $this->Teams->newEntity();
        $team->organization_id = OrgDomainsMiddleware::getCurrentOrganizationId();
        $managerList = $this->Users->find(
            'list',
            [
                'contain' => [
                    'UsersToRoles',
                ],
            ]
        )->matching(
            'UsersToRoles',
            function (Query $q) {
                return $q->where(
                    [
                        // the user has at least once signed in on this portal
                        'UsersToRoles.user_role_id' => UserRole::USER_PORTAL_MEMBER,
                        'UsersToRoles.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                    ]
                );
            }
        );
        $user = $this->getCurrentUser();
        $allowed_programs = $this->Programs->find(
            'all',
            [
                'conditions' => [],
                'contain' => [
                    'ParentPrograms',
                    'ChildPrograms',
                    'Realms',
                    'Realms.Fonds',
                    'Appeals',
                    'EvaluationCriteria',
                ],
            ]
        )->order(
            [
                'Programs.realm_id' => 'ASC',
            ]
        );
        if (!$user->isSystemsManager()) {
            $allowed_programs->matching(
                'Realms.Fonds',
                function (\Cake\ORM\Query $query) use ($user) {
                    return $query->where(
                        ['Fonds.organization_id IN' => array_intersect(
                            $user->getOrganizationIdsWhereUserHasRoleWithFallbacks(UserRole::MANAGER_GRANTS, false, [UserRole::MANAGER_PORTALS]),
                            [OrgDomainsMiddleware::getCurrentOrganizationId()]
                        )]
                    );
                }
            )->distinct('Programs.id');
        }

        $allowed_programs = $allowed_programs->filter(function ($value, $key) {
            return empty($value->child_programs);
        })->map(function ($value, $key) {
            /**@var $value Program */
            return [
                'value' => $value->id,
                'text' => $value->name,
                'data-section' => $value->getDataSection(),
            ];
        });

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $team = $this->Teams->patchEntity($team, $this->getRequest()->getData());
            if ($this->Teams->save($team)) {
                $this->Flash->success(__('Uloženo úspěšně'));
                $this->redirect(['action' => 'addModify', $team->id]);
            } else {
                $this->Flash->error(__('Formulář obsahuje chyby'));
            }
        }

        $this->set(compact('team', 'managerList', 'allowed_programs'));
        $this->set('crumbs', [__('Struktura Dotačního Úřadu') => 'admin_teams']);
    }
}
