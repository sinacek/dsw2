<?php

namespace App\Controller;

use App\Middleware\OrgDomainsMiddleware;
use App\Model\Entity\Domain;
use App\Model\Entity\OrganizationSetting;
use App\Model\Entity\User;
use App\Model\Entity\UserRole;
use App\Model\Table\OrganizationsTable;
use Cake\Utility\Hash;

/**
 * @property OrganizationsTable $Organizations
 */
class OrganizationsController extends AppController
{

    /**
     * @param User $user
     * @return bool
     */
    public function isAuthorized($user = null)
    {
        if (!parent::isAuthorized($user)) {
            return false;
        }

        if (
            !$this->isAuthorizedToOrganization()
            && !$this->isAuthorizedToOrganization(null, UserRole::MANAGER_PORTALS)
        ) {
            return false;
        }

        return true;
    }

    public function index()
    {
        $conditions = [];
        if (!$this->isAuthorizedToOrganization()) {
            $conditions['Organizations.id IN'] = $this->Auth->user()->getOrganizationIdsWhereUserHasRole();
        }
        $organizations = $this->Organizations->find(
            'all',
            [
                'contain' => [
                    'Domains',
                ],
                'conditions' => $conditions,
            ]
        );
        if ($organizations->count() === 1) {
            $this->redirect(['action' => 'edit', $organizations->firstOrFail()->id]);
        }
        $this->set(
            'organizations',
            $organizations
        );
    }

    public function create()
    {
        $this->checkIsAuthorizedToOrganizationOrThrow();
        $organization = $this->Organizations->newEntity();

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $organization = $this->Organizations->patchEntity(
                $organization,
                $this->getRequest()->getData(),
                [
                    'associated' => [
                        'Domains',
                    ],
                ]
            );
            if ($this->Organizations->save($organization)) {
                $this->Flash->success(__('Uloženo'));
                $this->redirect(['action' => 'edit', $organization->id]);
            } else {
                $this->Flash->error(__('Chyba při ukládání'));
            }
        }

        $this->set(compact('organization'));
    }

    public function domainAdd(int $organization_id)
    {
        $this->checkIsAuthorizedToOrganizationOrThrow($organization_id, UserRole::MANAGER_PORTALS);
        $domain = $this->Organizations->Domains->newEntity();
        $domain->organization_id = $organization_id;

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $domain = $this->Organizations->Domains->patchEntity($domain, $this->getRequest()->getData());
            if ($this->Organizations->Domains->save($domain)) {
                $this->Flash->success(__('Uloženo'));
                $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Chyba při ukládání'));
            }
        }

        $this->set(compact('domain'));
    }

    public function edit($id = null)
    {
        $this->checkIsAuthorizedToOrganizationOrThrow($id, UserRole::MANAGER_PORTALS);
        $organization = $this->Organizations->get(
            $id ?? OrgDomainsMiddleware::getCurrentOrganizationId(),
            [
                'contain' => [
                    'OrganizationSettings',
                ],
            ]
        );

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            OrgDomainsMiddleware::flushCache();
            $settings = $this->getRequest()->getData('settings');
            foreach (Hash::flatten($settings) as $key => $value) {
                foreach ($organization->organization_settings as $setting) {
                    if ($setting->name === $key) {
                        if ($setting->value !== $value) {
                            $setting->set('value', $value);
                            $organization->setDirty('organization_settings');
                        }
                        continue 2;
                    }
                }
                if (OrganizationSetting::getDefaultValue($key) === $value || empty($value)) {
                    continue;
                }
                $new_setting = $this->Organizations->OrganizationSettings->newEntity();
                $new_setting->name = $key;
                $new_setting->value = $value;
                $new_setting->organization_id = $organization->id;
                $this->Organizations->OrganizationSettings->save($new_setting);
                $organization->organization_settings[] = $new_setting;
            }
            $this->getRequest()->withoutData('settings');
            $organization = $this->Organizations->patchEntity($organization, $this->getRequest()->getData());
            if ($this->Organizations->save($organization)) {
                $this->Flash->success(__('Uloženo'));
            } else {
                $this->Flash->error(__('Chyba při ukládání'));
            }

            $organization = $this->Organizations->get(
                $id,
                [
                    'contain' => [
                        'OrganizationSettings',
                    ],
                ]
            );
        }

        $this->set(compact('organization'));
    }

    public function delete($id = null)
    {
        $this->checkIsAuthorizedToOrganizationOrThrow($id, UserRole::MANAGER_PORTALS);
        if ($this->Organizations->delete($this->Organizations->get($id))) {
            $this->Flash->success(__('Smazáno'));
        } else {
            $this->Flash->error(__('Chyba při odstranění'));
        }
        OrgDomainsMiddleware::flushCache();

        return $this->redirect($this->referer());
    }

    public function domainDelete(int $organization_id, $id = null)
    {
        $this->checkIsAuthorizedToOrganizationOrThrow($organization_id, UserRole::MANAGER_PORTALS);
        /** @var Domain $domain */
        $domain = $this->Organizations->Domains->find('byOrganization', ['organization_id' => $organization_id, 'id' => $id])->firstOrFail();

        if ($this->getRequest()->host() == $domain->domain) {
            $this->Flash->error(__('Nelze provést ze shodné domény'));
        } elseif ($this->Organizations->Domains->delete($domain)) {
            $this->Flash->success(__('Smazáno'));
        } else {
            $this->Flash->error(__('Chyba při odstraňování'));
        }
        OrgDomainsMiddleware::flushCache();

        return $this->redirect($this->referer());
    }

    public function domainTrigger(int $organization_id, $id = null)
    {
        $this->checkIsAuthorizedToOrganizationOrThrow($organization_id, UserRole::MANAGER_PORTALS);
        /** @var Domain $domain */
        $domain = $this->Organizations->Domains->find('byOrganization', ['organization_id' => $organization_id, 'id' => $id])->firstOrFail();
        $domain->is_enabled = !$domain->is_enabled;

        if ($this->getRequest()->host() == $domain->domain) {
            $this->Flash->error(__('Nelze provést ze shodné domény'));
        } elseif ($this->Organizations->Domains->save($domain)) {
            $this->Flash->success(__('Uloženo'));
        } else {
            $this->Flash->error(__('Chyba při ukládání'));
        }
        OrgDomainsMiddleware::flushCache();

        return $this->redirect($this->referer());
    }
}
