<?php

namespace App\Controller;

use App\Model\Entity\User;
use App\Model\Table\I18nMessagesTable;
use Cake\Cache\Cache;

/**
 * @property I18nMessagesTable $I18nMessages
 */
class TranslationsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('I18nMessages');
    }

    /**
     * @param  null|User $user
     * @return bool
     */
    public function isAuthorized($user = null)
    {
        return parent::isAuthorized($user) && ($user->isSystemsManager() || $user->isPortalsManager());
    }

    public function index()
    {
        $this->set('messages', $this->I18nMessages->find('all'));
    }

    public function addModify(int $id = 0)
    {
        $message = $id > 0 ? $this->I18nMessages->get($id) : $this->I18nMessages->newEntity();

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $message = $this->I18nMessages->patchEntity($message, $this->getRequest()->getData());
            if (empty(trim($message->value_0))) {
                // ability to remove translation
                $message->value_0 = null;
            }
            if ($this->I18nMessages->save($message)) {
                $this->Flash->success(__('Překlad byl uložen úspěšně'));
                $this->redirect(['action' => 'index']);
                Cache::clear(false, '_cake_core_');
            } else {
                $this->Flash->error(__('Nastala chyba při ukládání'));
            }
        }

        $this->set('crumbs', [__('Překlady') => 'admin_translations']);
        $this->set(compact('message'));
    }
}
