<?php

namespace App\Controller;

use App\Controller\Component\IsdsComponent;
use App\Middleware\OrgDomainsMiddleware;
use App\Model\Entity\Appeal;
use App\Model\Entity\Fond;
use App\Model\Entity\Form;
use App\Model\Entity\Identity;
use App\Model\Entity\OrganizationSetting;
use App\Model\Entity\Request;
use App\Model\Entity\RequestBudget;
use App\Model\Entity\RequestState;
use App\Model\Entity\User;
use App\Model\Entity\UserRole;
use App\Model\Table\BudgetItemsTable;
use App\Model\Table\FondsTable;
use App\Model\Table\OrganizationsTable;
use App\Model\Table\PublicIncomeHistoriesTable;
use App\Model\Table\RequestFilledFieldsTable;
use App\Model\Table\RequestsTable;
use Cake\Core\Configure;
use Cake\Filesystem\File;
use Cake\I18n\FrozenTime;
use Cake\I18n\Number;
use Cake\Log\Log;
use OldDsw\Controller\Component\OldDswComponent;
use OldDsw\Model\Entity\Zadost;
use Throwable;
use function Zend\Diactoros\normalizeUploadedFiles;

/**
 * @property RequestsTable $Requests
 * @property RequestFilledFieldsTable $RequestFilledFields
 * @property PublicIncomeHistoriesTable $PublicIncomeHistories
 * @property OldDswComponent $OldDsw
 * @property FondsTable $Fonds
 * @property IsdsComponent $Isds
 * @property BudgetItemsTable $BudgetItems
 * @property OrganizationsTable $Organizations
 */
class UserRequestsController extends AppController
{
    public function isAuthorized($user = null)
    {
        /**
         * @var User $user
         */
        $user = $user instanceof User ? $user : $this->Auth->user();

        return parent::isAuthorized($user);
    }

    public function initialize()
    {
        parent::initialize();
        if ($this->getCurrentUser() instanceof User && $this->getCurrentUser()->hasRoles([UserRole::MANAGER_PORTALS, UserRole::MANAGER_SYSTEMS], OrgDomainsMiddleware::getCurrentOrganizationId(), true)) {
            Configure::write('debug', true);
        }
        $this->loadModel('Requests');
        $this->loadModel('BudgetItems');
        $this->loadModel('Fonds');
        $this->loadModel('PublicIncomeHistories');
        $this->loadModel('RequestFilledFields');
        $this->loadModel('Organizations');
        if (OrganizationSetting::getSetting(OrganizationSetting::HAS_DSW) === true) {
            $this->loadComponent('OldDsw.OldDsw');
        }
        $this->loadComponent('Isds');
    }

    public function attachmentAdd(int $request_id)
    {
        $request = $this->getRequestById($request_id);
        if (!RequestState::canUserEditRequest($request->request_state_id)) {
            $this->redirect(['action' => 'requestDetail', $request->id]);

            return;
        }
        $file = $this->Requests->Files->newEntity();

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $filedata = $this->request->getData('filedata');
            $uploadedFiles = normalizeUploadedFiles([$filedata]);

            $uploadedFile = $uploadedFiles[0];

            if ($uploadedFile->getError() !== UPLOAD_ERR_OK || $uploadedFile->getSize() <= 1) {
                $file->setError('filedata', 'File upload failed');
                $this->Flash->error(__('File upload failed'));
            } else {
                $fileMoveResult = $file->fileUploadMove($filedata, $request->user_id);

                if (is_string($fileMoveResult)) {
                    $file->filepath = $fileMoveResult;
                    $file->original_filename = $uploadedFile->getClientFilename();
                    $file->filesize = $uploadedFile->getSize();
                    $file->user_id = $request->user_id;

                    $request->files[] = $file;
                    $request->setDirty('files');

                    if ($this->Requests->save($request, ['associated' => ['Files']])) {
                        $this->Flash->success(__('Soubor byl úspěšně nahrán'));
                        $this->redirect(['action' => 'requestDetail', $request_id]);
                    } else {
                        $this->Flash->error(__('Error adding new version of file'));
                    }
                }
            }
        }

        $this->set(compact('request', 'file'));
    }

    public function fileDownload(int $request_id, int $file_id)
    {
        $request = $this->getRequestById($request_id);
        $file = $request->findAttachment($file_id);
        $target = ROOT . DS . 'files' . DS . $file->filepath;
        $_file = new File($target);

        if ($_file->exists()) {
            return $this->getResponse()->withFile($target, ['name' => urlencode($file->original_filename), 'download' => true]);
        }

        return $this->getResponse()->withStatus(404);
    }

    public function attachmentDelete(int $request_id, int $attachment_id)
    {
        $request = $this->getRequestById($request_id);
        if (RequestState::canUserEditRequest($request->request_state_id)) {
            $file = $request->findAttachment($attachment_id);
            if ($this->Requests->Files->delete($file)) {
                $file->deletePhysically();
            }
        }
        $this->redirect(['action' => 'requestDetail', $request_id]);
    }

    public function formFillReset(int $request_id, int $form_id)
    {
        $request = $this->getRequestById($request_id);
        $form = $request->getFormById($form_id);

        $status = false;

        if ($form instanceof Form && RequestState::canUserEditRequest($request->request_state_id)) {
            $requestForm = $form->getFormController($request);
            $status = $requestForm->deleteSavedData();
        }

        if ($status === true) {
            $this->Flash->success(__('Vyplněné hodnoty formuláře byly kompletně vymazány'));
        } else {
            $this->Flash->error(__('Data formuláře nemohla být vymazána'));
        }

        return $this->redirect(['action' => 'formFill', 'id' => $request->id, 'form_id' => $form->id]);
    }

    public function formFill(int $request_id, int $form_id)
    {
        $request = $this->getRequestById($request_id);
        $form = $request->getFormById($form_id);
        if (empty($form)) {
            $this->Flash->error(__('Formulář nebyl nalezen'));

            $this->redirect(['action' => 'requestDetail', $request_id]);

            return;
        }
        $form = $this->getFormById($form_id);

        $requestForm = $form->getFormController($request);
        $requestForm->setUser($this->getCurrentUser());

        if (RequestState::canUserEditRequest($request->request_state_id) && $this->getRequest()->is(['post', 'put', 'patch'])) {
            if ($requestForm->execute($this->getRequest()->getData())) {
                $this->Flash->success(__('Formulář byl uložen'));
                $this->redirect($this->retrieveReferer() ?? ['_name' => 'request_detail', 'id' => $request_id]);
            } else {
                $this->Flash->error(__('Formulář obsahuje chyby'));
            }
        }

        $this->persistReferer();
        $this->set(compact('request', 'requestForm'));
        $this->set('crumbs', [__('Mé žádosti o podporu') => 'user_requests', $request->name => ['action' => 'requestDetail', $request_id]]);
    }

    public function getPdf(int $id)
    {
        $request = $this->getRequestById($id);
        if (
            $request->request_state_id === RequestState::STATE_NEW ||
            ($request->request_state_id === RequestState::STATE_READY_TO_SUBMIT && OrganizationSetting::getSetting(OrganizationSetting::ALLOW_MANUAL_SUBMIT_OF_REQUESTS) === false)
        ) {
            $this->Flash->error(__('Tuto žádost ještě není možné stáhnout'));
            $this->redirect($this->referer());

            return;
        }
        $this->set('request', $request);

        Configure::write('debug', false);
        Configure::write(
            'CakePdf',
            [
                'engine' => [
                    'className' => 'CakePdf.WkHtmlToPdf',
                    'viewport-size' => '1920x1080',
                    'margin-bottom' => 0,
                    'margin-top' => 0,
                    'margin-right' => 0,
                    'margin-left' => 0,
                ],
                'orientation' => 'portrait',
                'download' => true,
                'margin' => [
                    'bottom' => 0,
                    'left' => 0,
                    'right' => 0,
                    'top' => 0,
                ],

            ]
        );
        $this->viewBuilder()->setOptions(
            [
                'pdfConfig' => [
                    'filename' => urlencode($request->name) . '.pdf',
                ],
            ]
        );
        $this->viewBuilder()->setClassName('CakePdf.Pdf');
        $this->viewBuilder()->setTemplate('/UserRequests/get_pdf');
    }

    public function index()
    {
        $this->set(
            'requests',
            $this->Requests->find(
                'all',
                [
                    'conditions' => [
                        'Requests.user_id' => $this->getCurrentUser()->id,
                        'Requests.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                    ],
                    'contain' => [
                        'Appeals',
                        'Programs',
                    ],
                ]
            )
        );

        if (OrganizationSetting::getSetting(OrganizationSetting::HAS_DSW) === true) {
            $this->set('old_requests', $this->OldDsw->getZadostiByAccountIds($this->getCurrentUser()->getStareUctyIds(), true));
        }
    }

    public function dswZadost(int $id = 0, bool $render = true)
    {
        if (!OrganizationSetting::getSetting(OrganizationSetting::HAS_DSW) || !isset($this->OldDsw) || $id < 1) {
            $this->redirect(['action' => 'index']);

            return;
        }

        $zadost = $this->OldDsw->getZadost($id, $this->getCurrentUser()->getStareUctyIds());
        $this->set(compact('zadost'));
        if ($render === true) {
            $this->set('crumbs', [__('Mé žádosti o podporu') => ['action' => 'index']]);
            $this->render('OldDsw.OldDsw/zadost');
        }
    }

    public function dswOhlasit(int $id = 0)
    {
        $this->dswZadost($id, false);
        $zadost = isset($this->viewVars['zadost']) ? $this->viewVars['zadost'] : null;
        if (!($zadost instanceof Zadost)) {
            $this->Flash->error(__('Není možné provést ohlášení k žádosti, které nebylo vyhověno'));
            $this->redirect(['action' => 'dswZadost', $id]);

            return;
        }
        $ohlaseni = $this->OldDsw->Zadosti->OhlasovaciFormular->newEntity();

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $ohlaseni = $this->OldDsw->Zadosti->OhlasovaciFormular->patchEntity($ohlaseni, $this->getRequest()->getData());
            $ohlaseni->zadost_id = $zadost->id;
            $ohlaseni->added_by_uzivatel_id = $zadost->pridano_kym;
            $ohlaseni->added_when = FrozenTime::now();
            if ($this->OldDsw->Zadosti->OhlasovaciFormular->save($ohlaseni)) {
                $this->Flash->success(__('Ohlášení bylo úspěšně provedeno'));
                $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Formulář obsahuje chyby'));
            }
        }

        $this->set('crumbs', [__('Mé žádosti o podporu') => ['action' => 'index'], h($zadost->nazev) => ['action' => 'dswZadost', $id]]);
        $this->set(compact('ohlaseni'));
    }

    public function requestDetail(int $request_id)
    {
        $request = $this->getRequestById($request_id);
        $public_income_history = $this->PublicIncomeHistories->find(
            'list',
            [
                'conditions' => [
                    'PublicIncomeHistories.user_id' => $request->user_id,
                    'PublicIncomeHistories.fiscal_year >=' =>
                        intval(date('Y')) - OrgDomainsMiddleware::getCurrentOrganization()->getSettingValue(OrganizationSetting::SUBSIDY_HISTORY_YEARS, true),
                ],
            ]
        )->toArray();

        $isReadyToSubmit = false;
        // validate identity
        if ($request->user_identity_version !== 0 && $this->getCurrentUser()->isIdentityCompleted($request->user_identity_version)) {
            // validate budget is
            if ($request->validateBudgetRequirements() === RequestBudget::BUDGET_OK) {
                if ($request->appeal->checkRequestLimit($request->request_budget->requested_amount, $request->program_id)) {
                    $formsFilled = true;
                    foreach ($request->getForms() as $form) {
                        $formsFilled = $formsFilled && $request->isFormCompleted($form->id);
                    }
                    if ($formsFilled) {
                        $isReadyToSubmit = true;
                    }
                }
            }
        }
        $newState = $request->request_state_id;
        if (!$isReadyToSubmit) {
            $newState = RequestState::STATE_NEW;
        } elseif ($request->request_state_id === RequestState::STATE_NEW) {
            $newState = RequestState::STATE_READY_TO_SUBMIT;
        }
        if ($newState !== $request->request_state_id) {
            // persist only if state changed
            $request->setCurrentStatus($newState, $this->getCurrentUserId());
            $this->Requests->save($request);
        }

        $this->set(compact('request', 'public_income_history'));
        $this->set('crumbs', [__('Mé žádosti o podporu') => 'user_requests']);
    }

    public function requestSubmitDataboxResult(int $request_id)
    {
        $request = $this->getRequestById($request_id);

        $result_dm_id = $this->getRequest()->getSession()->read(IsdsController::ISDS_CONCEPT_DM_ID);
        $result_message = $this->getRequest()->getSession()->read(IsdsController::ISDS_CONCEPT_STATUS_MESSAGE);
        $result_code = $this->getRequest()->getSession()->read(IsdsController::ISDS_CONCEPT_STATUS_CODE);

        if (empty($result_dm_id)) {
            $this->Flash->error(sprintf("%s %s (%s)", __('Žádost nebyla podána z důvodu:'), $result_message, $result_code));
            $this->redirect(['action' => 'requestDetail', 'id' => $request_id]);

            return;
        }

        $request->setCurrentStatus(RequestState::STATE_SUBMITTED, $this->getCurrentUserId());
        if ($this->Requests->save($request)) {
            $this->Flash->success(__('Žádost byla úspěšně odeslána pomocí datové schránky'));
        } else {
            Log::error('requestSubmitDataboxResult saving error, ' . json_encode($request->getErrors()));
        }
        $this->redirect(['action' => 'index']);
    }

    public function requestSubmitDatabox(int $request_id)
    {
        $request = $this->getRequestById($request_id);
        if ($request->request_state_id !== RequestState::STATE_READY_TO_SUBMIT || !$request->appeal->canUserSubmitRequest($request)) {
            $this->Flash->error(__('Tuto žádost není možné odeslat'));
            $this->redirect(['action' => 'requestDetail', 'id' => $request_id]);

            return;
        }

        $this->getRequest()->getSession()->write(IsdsController::ISDS_AFTER_AUTH_HANDLER, ['action' => 'requestSubmitDatabox', 'controller' => 'UserRequests', 'id' => $request_id]);
        $timeLimitedId = $this->getRequest()->getSession()->consume(IsdsController::ISDS_TIME_LIMITED_ID);
        if (empty($timeLimitedId)) {
            $this->redirect(['_name' => 'isds_auth']);

            return;
        }

        // we have timeLimitedId, so we can first check the target box against identity
        $_session_dbID = trim($this->getRequest()->getSession()->read(IsdsController::ISDS_VERIFIED_DS_ID));
        if (!empty($_session_dbID)) {
            $request->loadIdentities();
            $_request_dbID = null;
            $_request_verified_dbID = null;
            foreach ($request->identities ?? [] as $identity) {
                if ($identity->name === Identity::CONTACT_DATABOX) {
                    $_request_dbID = trim($identity->value);
                } elseif ($identity->name === Identity::ISDS_VERIFIED_DATABOX_ID) {
                    $_request_verified_dbID = trim($identity->value);
                }
            }
            if (!in_array($_session_dbID, [$_request_verified_dbID, $_request_dbID], true) && (!empty($_request_dbID) || !empty($_request_verified_dbID))) {
                $this->Flash->error(__('Datová schránka, kterou jste se pokusili žádost odeslat, nesouhlasí s identitou žadatele'));
                $this->Flash->error(sprintf(__('Odesíláte z Datové Schránky ID "%s" ale v identitě je uvedeno "%s"'), $_session_dbID, $_request_verified_dbID ?? $_request_dbID));

                $this->redirect(['action' => 'index']);

                return;
            }
            $this->Isds->storeVerifiedAttributes($this);
        }

        // put concept into databox and redirect to it
        $pdfData = null;

        $request = $this->getRequestById($request_id);
        $this->set('request', $request);
        Configure::write(
            'CakePdf',
            [
                'engine' => 'CakePdf.WkHtmlToPdf',
                'orientation' => 'portrait',
                'download' => true,
            ]
        );
        $this->viewBuilder()->setOptions(
            [
                'pdfConfig' => [
                    'disable-smart-shrinking' => 'disable-smart-shrinking',
                ],
            ]
        );
        $this->viewBuilder()->setClassName('CakePdf.Pdf');
        $this->viewBuilder()->setTemplate('/UserRequests/get_pdf');
        $pdfData = base64_encode($this->render()->getBody()->__toString());

        $url = $this->Isds->getSetConceptRequestUrl();
        $xml = $this->Isds->getSetConceptRequest(OrganizationSetting::getSetting(OrganizationSetting::DS_ID), __('Žádost o poskytnutí dotace č.') . $request_id, $request_id, $pdfData);

        list($cert, $key) = $this->Isds->getCertificateAndPrivateKey();

        if (empty($cert) || empty($key)) {
            $this->Flash->error(__('Datová Schránka dotačního portálu není nastavena správně'));
            $this->redirect(['action' => 'index']);

            return;
        }

        $response = null;
        $parsed = null;
        $conceptId = null;
        try {
            $response = $this->Isds->execSoapAction($url, $cert, $key, $xml, ['ExtWS', $timeLimitedId]);
            $parsed = $this->Isds->extractSetConceptResponse($response);

            $conceptId = $parsed[0];
            //Log::debug(json_encode($parsed + [$this->getRequest()->getRequestTarget()]));
        } catch (Throwable $t) {
            Log::error($t->getMessage());
            Log::error($t->getTraceAsString());
        }

        if (!empty($conceptId)) {
            $this->getRequest()->getSession()->write(IsdsController::ISDS_AFTER_AUTH_HANDLER, ['controller' => 'UserRequests', 'action' => 'requestSubmitDataboxResult', 'id' => $request_id]);
            $this->redirect($this->Isds->getKonceptViewUrl($conceptId));

            return;
        }

        $this->redirect(['action' => 'requestDetail', $request_id]);
    }

    public function requestDelete(int $request_id)
    {
        $request = $this->getRequestById($request_id);
        if ($request->user_id !== $this->getCurrentUser()->getOriginIdentity($this->getRequest())->id) {
            $this->Flash->error(__('Žádost může být smazána pouze vlastníkem'));
        } elseif (RequestState::canBeDeleted($request->request_state_id) && $this->Requests->delete($request)) {
            $this->Flash->success(__('Smazáno úspěšně'));
        } else {
            $this->Flash->error(__('Žádost nebylo možné smazat'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function requestBudget(int $request_id)
    {
        $request = $this->getRequestById($request_id);
        $budget = $request->request_budget ?? $this->Requests->RequestBudgets->newEntity(
                [
                    'request_id' => $request_id,
                ]
            );

        if (RequestState::canUserEditRequest($request->request_state_id) && $this->getRequest()->is(['post', 'put', 'patch'])) {
            $data = $budget->cleanPostData($this->getRequest()->getData());
            $budget = $this->Requests->RequestBudgets->patchEntity(
                $budget,
                $data,
                [
                    'associated' => [
                        'Incomes',
                        'Costs',
                        'OwnSources',
                        'OtherSubsidies',
                    ],
                ]
            );
            $budget->recountItems();
            if (!$request->appeal->checkRequestLimit($budget->requested_amount, $request->program_id)) {
                $budget->setError('requested_amount', sprintf("%s %s", __('Žádáte o částku vyšší než je možné, maximum v tomto programu je'), Number::currency($request->appeal->getMaxRequestBudget($request->program_id), 'CZK')));
            }
            if ($this->Requests->RequestBudgets->save($budget)) {
                $budget->deleteRemovedItems($this->BudgetItems);
                $this->Flash->success(__('Rozpočet uložen úspěšně'));
                $this->redirect(['action' => 'requestDetail', $request->id]);
            } else {
                $this->Flash->error(__('Formulář obsahuje chyby'));
            }
        }

        $this->set(compact('request', 'budget'));
        $this->set('crumbs', [__('Mé žádosti o podporu') => 'user_requests', $request->name => ['action' => 'requestDetail', $request_id]]);
    }

    private array $_allowed_programs = [];
    private array $_program_descriptions = [];
    private array $_program_id_to_appeal_id = [];

    function hasSelectableItems($data)
    {
        $rtn = false;
        foreach ($data as $key => $value) {
            if (is_numeric($key)) {
                $rtn = true;
            }
            if (is_array($value)) {
                $rtn = $rtn || $this->hasSelectableItems($value);
            }
        }
        return $rtn;
    }

    public function getAppealsToPrograms()
    {
        $tree = [];

        $appeals_conditions = [
            'Appeals.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
        ];
        if (!$this->getCurrentUser()->isGrantsManager()) {
            $appeals_conditions['Appeals.is_active'] = true;
        }

        /** @var Appeal[] $appeals */
        $appeals = $this->Requests->Appeals->find('all', [
            'conditions' => $appeals_conditions,
            'contain' => [
                'Programs',
            ],
        ])->toArray();

        foreach ($appeals as $appeal) {
            $allowed_programs = [];
            foreach ($appeal->programs as $program) {
                $allowed_programs[] = $program->id;
                $this->_allowed_programs[] = $program->id;
                $this->_program_descriptions[$program->id] = $program->description;
                $this->_program_id_to_appeal_id[$program->id] = $appeal->id;
            }

            /** @var Fond[] $fonds */
            $fonds = $this->Fonds->find('all', [
                'conditions' => [
                    'Fonds.is_enabled' => true,
                    'Fonds.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                ],
                'contain' => [
                    'Realms.Programs.ChildPrograms',
                ],
            ])->toArray();

            foreach ($fonds as $fond) {
                $fond_data = [];
                foreach ($fond->realms as $realm) {
                    $realm_data = [];
                    foreach ($realm->programs as $program) {
                        if (!empty($program->child_programs)) {
                            $realm_data[$program->name] = [];
                            foreach ($program->child_programs as $child_program) {
                                if (in_array($child_program->id, $allowed_programs, true) || in_array($program->id, $allowed_programs, true)) {
                                    $realm_data[$program->name][$child_program->id] = $child_program->name;
                                }
                            }
                        } elseif (empty($program->parent_id)) {
                            if (in_array($program->id, $allowed_programs, true)) {
                                $realm_data[$program->id] = $program->name;
                            }
                        }
                    }
                    if (!empty($realm_data) && $this->hasSelectableItems($realm_data)) {
                        if ($realm->is_hidden) {
                            $fond_data = $fond_data + $realm_data;
                        } else {
                            $fond_data[$realm->name] = $realm_data;
                        }
                    }
                }
                if (!empty($fond_data) && $this->hasSelectableItems($fond_data)) {
                    if ($fond->is_hidden) {
                        $appeal_data = $fond_data;
                    } else {
                        $appeal_data[$fond->name] = $fond_data;
                    }
                }
            }

            if (!empty($appeal_data)) {
                $tree[$appeal->name] = $appeal_data;
            }
        }

        // strip appeal name if only one is open
        if (count($tree) === 1) {
            $tree = array_pop($tree);
        }
        // strip single fond name if only one is contained in appeal
        if (count($tree) === 1) {
            $tree = array_pop($tree);
        }

        return $tree;
    }

    public function addModify(?int $id = null)
    {
        $request = $id > 1 ? $this->Requests->get($id, [
            'conditions' => [
                'Requests.user_id' => $this->getCurrentUser()->id,
            ],
            'contain' => ['RequestBudgets'],
        ]) : $this->Requests->newEntity([
            'request_state_id' => RequestState::STATE_NEW,
        ], ['associated' => ['RequestBudgets']]);

        $appeals_conditions = [
            'Appeals.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
            'Appeals.open_to >=' => date('Y-m-d'),
        ];
        if (!$this->getCurrentUser()->isGrantsManager()) {
            $appeals_conditions['Appeals.is_active'] = true;
        }

        $appeals = $this->Requests->Appeals->find(
            'all',
            [
                'conditions' => $appeals_conditions,
                'contain' => [
                    'Programs',
                ],
            ]
        )->toArray();
        /** @var Appeal[] $appeals */
        if (empty($appeals) && $request->isNew()) {
            $this->Flash->error(__('Aktuálně není žádná otevřená výzva, ve které byste mohli žádat o podporu'));

            $this->redirect(['action' => 'index']);

            return;
        }

        $appeals_programs = $this->getAppealsToPrograms();
        $program_descriptions = $this->_program_descriptions;

        if (in_array($request->request_state_id, [RequestState::STATE_NEW, RequestState::STATE_READY_TO_SUBMIT], true) && $this->getRequest()->is(['post', 'put', 'patch'])) {
            $request_program_id = $this->getRequest()->getData('program_id');
            if (!empty($request_program_id) && in_array($request_program_id, $this->_allowed_programs)) {
                $request = $this->Requests->patchEntity(
                    $request,
                    [
                        'program_id' => $request_program_id,
                        'name' => $this->getRequest()->getData('name'),
                        'organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                        'user_id' => $this->Auth->user('id'),
                        'appeal_id' => isset($this->_program_id_to_appeal_id[$request_program_id]) ? $this->_program_id_to_appeal_id[$request_program_id] : null,
                        'user_identity_version' => $this->getCurrentUser()->getLastIdentityVersion(false),
                    ]
                );
                if (!($request->request_budget instanceof RequestBudget)) {
                    $request->request_budget = $this->Requests->RequestBudgets->newEntity();
                }
                $this->Requests->RequestBudgets->patchEntity($request->request_budget, ['requested_amount' => floatval($this->getRequest()->getData('request_budget.requested_amount'))]);
                $request->setDirty('request_budget', $request->request_budget->isDirty());

                if ($this->Requests->save($request, ['associated' => ['RequestBudgets']])) {
                    $this->Flash->success(__('Žádost byla úspěšně uložena'));
                    $this->redirect(['action' => 'requestDetail', $request->id]);
                } else {
                    $this->Flash->error(__('Nebylo možné uložit žádost, formulář obsahuje chyby'));
                    Log::debug(json_encode($request->getErrors()));
                }
            }
        }

        $crumbs = [__('Mé žádosti o podporu') => 'user_requests'];
        if (!$request->isNew()) {
            $crumbs[h($request->name)] = ['_name' => 'request_detail', 'id' => $request->id];
        }

        $this->set(compact('request', 'appeals', 'appeals_programs', 'program_descriptions', 'crumbs'));
    }

    private function getRequestById(int $request_id): Request
    {
        return $this->Requests->get(
            $request_id,
            [
                'conditions' => [
                    'Requests.user_id' => $this->Auth->user('id'),
                    'Requests.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                ],
                'contain' => [
                    'Programs',
                    'Programs.Forms',
                    'Programs.Forms.FormFields',
                    'Programs.ParentPrograms',
                    'Programs.ParentPrograms.Forms',
                    'Programs.ParentPrograms.Forms.FormFields',
                    'Programs.Realms',
                    'Programs.ParentPrograms.Realms',
                    'Programs.Realms.Fonds',
                    'Programs.ParentPrograms.Realms.Fonds',
                    'Appeals',
                    'Appeals.AppealsToPrograms',
                    'Files',
                    'RequestBudgets',
                    'RequestBudgets.Incomes',
                    'RequestBudgets.Costs',
                    'RequestBudgets.OwnSources',
                    'RequestBudgets.OtherSubsidies',
                    'Users' => [
                        'PublicIncomeHistories',
                        'PublicIncomeHistories.PublicIncomeSources',
                    ],
                ],
            ]
        );
    }

    private function getFormById(int $form_id): Form
    {
        return $this->Requests->Programs->Forms->get(
            $form_id,
            [
                'contain' => [
                    'FormFields',
                    'FormFields.FormFieldTypes',
                ],
            ]
        );
    }
}
