<?php

namespace App\Controller;

use App\Middleware\OrgDomainsMiddleware;
use App\Model\Entity\User;
use App\Model\Table\WikisTable;
use Cake\Database\Query;

/**
 * @property-read WikisTable $Wikis
 */
class WikiManagerController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Wikis');
    }

    public function isAuthorized($user = null)
    {
        /**
         * @var User $user
         */
        $user = $this->Auth->user();

        return parent::isAuthorized($user) && $user->isWikiManager();
    }

    public function index()
    {
        $this->set(
            'wikis',
            $this->Wikis->WikiCategories->find(
                'all',
                [
                    'conditions' => [
                        'WikiCategories.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                    ],
                    'order' => [
                        'WikiCategories.category_name' => 'ASC',
                    ],
                    'contain' => [
                        'Wikis' => function (Query $query) {
                            return $query->order(['Wikis.title' => 'ASC']);
                        },
                    ],
                ]
            )
        );
    }

    public function addModify(int $id = 0)
    {
        $page = $id > 0 ? $this->Wikis->get(
            $id,
            [
                'conditions' => [
                    'WikiCategories.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                ],
                'contain' => [
                    'WikiCategories',
                ],
            ]
        ) : $this->Wikis->newEntity();
        $allowed_categories = $this->Wikis->WikiCategories->find(
            'list',
            [
                'conditions' => [
                    'WikiCategories.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                ],
                'order' => [
                    'WikiCategories.category_name' => 'ASC',
                ],
            ]
        )->toArray();

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $page = $this->Wikis->patchEntity($page, $this->getRequest()->getData());
            if (!in_array($page->wiki_category_id, array_keys($allowed_categories))) {
                $page->wiki_category_id = null;
            }
            if ($this->Wikis->save($page)) {
                $this->Flash->success(__('Uloženo úspěšně'));
            } else {
                $this->Flash->error(__('Formulář obsahuje chyby'));
            }
        }

        $this->set(compact('page', 'allowed_categories'));
        $this->set('crumbs', [__('Správa nápovědy') => 'admin_wiki']);
    }

    public function categoryAddModify(int $id = 0)
    {
        $category = $id > 0 ? $this->Wikis->WikiCategories->get(
            $id,
            [
                'conditions' => [
                    'WikiCategories.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                ],
            ]
        ) : $this->Wikis->WikiCategories->newEntity(
            [
                'organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
            ]
        );

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $category = $this->Wikis->WikiCategories->patchEntity($category, $this->getRequest()->getData());
            if ($this->Wikis->WikiCategories->save($category)) {
                $this->Flash->success(__('Uloženo úspěšně'));
                $this->redirect(['_name' => 'admin_wiki']);
            }
        }

        $this->set(compact('category'));
    }

    public function delete(int $id = 0)
    {
        $page = $this->Wikis->get(
            $id,
            [
                'conditions' => [
                    'WikiCategories.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                ],
                'contain' => [
                    'WikiCategories',
                ],
            ]
        );
        if ($this->Wikis->delete($page)) {
            $this->Flash->success(__('Smazáno úspěšně'));
        } else {
            $this->Flash->error(__('Chyba při ukládání'));
        }
        $this->redirect(['action' => 'index']);
    }

    public function categoryDelete(int $id = 0)
    {
        $category = $this->Wikis->WikiCategories->get(
            $id,
            [
                'conditions' => [
                    'WikiCategories.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                ],
            ]
        );
        if ($this->Wikis->WikiCategories->delete($category)) {
            $this->Flash->success(__('Smazáno úspěšně'));
        } else {
            $this->Flash->error(__('Chyba při ukládání'));
        }
        $this->redirect(['action' => 'index']);
    }
}
