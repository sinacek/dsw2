<?php

namespace App\Controller\Component;

use App\Controller\AppController;
use App\Controller\IsdsController;
use App\Controller\UserRequestsController;
use App\Model\Entity\Identity;
use App\Model\Entity\OrganizationSetting;
use Cake\Controller\Component;
use DOMDocument;
use Exception;
use InvalidArgumentException;
use RuntimeException;

class IsdsComponent extends Component
{
    const DOMAIN_PRODUCTION = 'mojedatovaschranka.cz';
    const DOMAIN_TEST = 'czebox.cz';
    const SUBDOMAIN_CERT = 'cert';

    const ACTION_AUTH_CONFIRMATION_REQUEST = '/asws/atsEndpoint11';
    const ACTION_SETCONCEPT_REQUEST = '/asws/konceptEndpoint';
    const ACTION_ATS_LOGIN = '/as/login?atsId=';
    const ACTION_AS_KONCEPT_VIEW = '/as/koncept/view?konceptId=';

    const SOAP_AUTH_CONFIRMATION_REQUEST = <<<EOF
<?xml version = "1.0" ?>
<SOAP-ENV:Envelope
        xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"
        SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
    <SOAP-ENV:Body>
        <m:authConfirmationRequest xmlns:m="http://agw-as.cz/ats-ws/v1_1">
            <m:sessionId>REPLACETOKEN</m:sessionId>
        </m:authConfirmationRequest>
    </SOAP-ENV:Body>
</SOAP-ENV:Envelope>
EOF;
    const SOAP_SETCONCEPT_REQUEST = <<<EOF
<?xml version = "1.0" ?>
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:kon="http://isds.czechpoint.cz/v20/koncept">
   <soapenv:Body>
      <kon:SetConcept>
         <kon:dmEnvelope>
            <kon:dbIDRecipient>TARGET_DS_ID</kon:dbIDRecipient>
            <kon:dmAnnotation>CONCEPT_DESCRIPTION</kon:dmAnnotation>
            <kon:dmPersonalDelivery>false</kon:dmPersonalDelivery>
            <kon:dmAllowSubstDelivery>true</kon:dmAllowSubstDelivery>
         </kon:dmEnvelope>
         <kon:dmFiles>
            <kon:dmFile dmMimeType="application/pdf" dmFileMetaType="application/pdf" dmFileDescr="zadost-REQUESTID.pdf">
               <kon:dmEncodedContent>PDFDATA</kon:dmEncodedContent>
            </kon:dmFile>
         </kon:dmFiles>
      </kon:SetConcept>
   </soapenv:Body>
</soapenv:Envelope>
EOF;

    // priloha 2, WS_vyhledavani_datovych_schranek.pdf, str. 9-10
    const DB_TYPES = [
        10 => [
            'short' => 'OVM',
            'long' => 'DS orgánu veřejné moci (OVM)',
        ],
        13 => [
            'short' => 'OVM_REQ',
            'long' => 'podřízená DS typu OVM vzniklá na žádost, pro potřeby mateřského OVM, přebírá IČO mateřské schránky typu OVM',
        ],
        14 => [
            'short' => 'OVM_FO',
            'long' => 'DS fyzické osoby v roli Orgánu veřejné moci',
        ],
        15 => [
            'short' => 'OVM_PFO',
            'long' => 'DS podnikající fyzické osoby v roli Orgánu veřejné moci (např. notář nebo exekutor)',
        ],
        16 => [
            'short' => 'OVM_PO',
            'long' => 'DS orgánu veřejné moci, vzniklá změnou typu poté, co byl subjekt odpovídající existující schránce typu PO nebo PO_REQ zapsán do Rejstříku OVM.',
        ],
        20 => [
            'short' => 'PO',
            'long' => 'DS právnické osoby (z ROS)',
        ],
        22 => [
            'short' => 'PO_REQ',
            'long' => 'DS právnické osoby, vzniklá na žádost (takový typ subjektu, kterému nebyla automaticky zřízena DS typu PO)',
        ],
        30 => [
            'short' => 'PFO',
            'long' => 'DS podnikající fyzické osoby, včetně cizinců',
        ],
        31 => [
            'short' => 'PFO_ADVOK',
            'long' => 'DS advokáta',
        ],
        32 => [
            'short' => 'PFO_DANPOR',
            'long' => 'DS daňového poradce',
        ],
        33 => [
            'short' => 'PFO_INSSPR',
            'long' => 'DS insolvenčního správce',
        ],
        34 => [
            'short' => 'PFO_AUDITOR',
            'long' => 'DS statutárního auditora (OSVČ nebo zaměstnance - pak nemá IČO)',
        ],
        40 => [
            'short' => 'FO',
            'long' => 'DS fyzické osoby',
        ],
    ];

    public function useTestEnvironment(): bool
    {
        return OrganizationSetting::getSetting(OrganizationSetting::DS_TEST_ENVIRONMENT) === true;
    }

    public function getEnvironmentUrl(?string $subdomain = null): string
    {
        return 'https://' . ($subdomain ?? 'www') . '.' . ($this->useTestEnvironment() ? self::DOMAIN_TEST : self::DOMAIN_PRODUCTION);
    }

    public function getEndpointUrl(string $soapAction, ?string $subdomain = null)
    {
        return $this->getEnvironmentUrl($subdomain) . $soapAction;
    }

    public function filter_sessionId(string $sessionId): string
    {
        // if sessionId is full GET query
        if (!empty($sessionId) && mb_strpos($sessionId, '&') !== -1) {
            // sessionId will be the first of GET params, extract it
            $sessionId = explode('&', $sessionId)[0];
        }

        return preg_replace('/[^0-9a-zA-Z-]/', '', $sessionId);
    }

    public function filter_atsId(?string $atsId): string
    {
        return preg_replace('/[^0-9a-zA-Z]/', '', $atsId ?? '');
    }

    public function filter_pem_certificate(string $certificate): ?string
    {
        if (strpos($certificate, '-----BEGIN CERTIFICATE-----') === false) {
            return null;
        }
        if (strpos($certificate, '-----END CERTIFICATE-----') === false) {
            return null;
        }
        if (openssl_x509_read($certificate) === false) {
            return null;
        }

        return $certificate;
    }

    public function filter_pem_privatekey(string $private_key, ?string $certificate): ?string
    {
        if (strpos($private_key, '-----BEGIN PRIVATE KEY-----') === false) {
            return null;
        }
        if (strpos($private_key, '-----END PRIVATE KEY-----') === false) {
            return null;
        }
        if (empty($certificate)) {
            return null;
        }
        if ($this->filter_pem_certificate($certificate) === null) {
            return null;
        }
        if (openssl_x509_check_private_key($certificate, $private_key) === false) {
            return null;
        }

        return $private_key;
    }

    public function getAuthConfirmationRequest(string $sessionId): string
    {
        return str_replace('REPLACETOKEN', $sessionId, self::SOAP_AUTH_CONFIRMATION_REQUEST);
    }

    public function getSetConceptRequest(string $targetDataboxId, string $conceptDescription, int $requestId, string $pdfData)
    {
        $data = self::SOAP_SETCONCEPT_REQUEST;
        $data = str_replace('TARGET_DS_ID', $targetDataboxId, $data);
        $data = str_replace('CONCEPT_DESCRIPTION', $conceptDescription, $data);
        $data = str_replace('REQUESTID', strval($requestId), $data);

        return str_replace('PDFDATA', $pdfData, $data);
    }

    public function getAuthConfirmationRequestUrl()
    {
        return $this->getEndpointUrl(self::ACTION_AUTH_CONFIRMATION_REQUEST, self::SUBDOMAIN_CERT);
    }

    public function getSetConceptRequestUrl()
    {
        return $this->getEndpointUrl(self::ACTION_SETCONCEPT_REQUEST, self::SUBDOMAIN_CERT);
    }

    public function getAtsLoginUrl(string $atsId, ?string $requestAppToken)
    {
        $atsId = $this->filter_atsId($atsId);
        if (empty($atsId)) {
            throw new InvalidArgumentException('atsId not set');
        }

        return $this->getEndpointUrl(self::ACTION_ATS_LOGIN) . $atsId . (empty($requestAppToken) ? '' : '&appToken=' . $requestAppToken);
    }

    public function getKonceptViewUrl(string $conceptId)
    {
        if (empty($conceptId)) {
            throw new InvalidArgumentException('conceptId not set');
        }

        return $this->getEndpointUrl(self::ACTION_AS_KONCEPT_VIEW . $conceptId);
    }

    /**
     * @param string $url
     * @param string $certificate
     * @param string $private_key
     * @param string $xml
     * @param array $basic_auth [username, password] array if necessary
     * @return string|null
     * @throws Exception if curl error occured
     */
    public function execSoapAction(string $url, string $certificate, string $private_key, string $xml, array $basic_auth = []): ?string
    {
        $tmpcert = tmpfile();
        $tmpkey = tmpfile();
        try {
            fwrite($tmpcert, $certificate);
            fwrite($tmpkey, $private_key);

            $headers = [
                "Content-type: text/xml;",
                "Content-length: " . strlen($xml),
                "Connection: close",
            ];

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_VERBOSE, 0);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_SSLCERT, stream_get_meta_data($tmpcert)['uri']);
            curl_setopt($ch, CURLOPT_SSLKEY, stream_get_meta_data($tmpkey)['uri']);
            curl_setopt($ch, CURLOPT_SSLKEYTYPE, 'PEM');
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            if (!empty($basic_auth)) {
                curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
                curl_setopt($ch, CURLOPT_USERPWD, join(':', $basic_auth));
            }

            $response = curl_exec($ch);

            if (curl_errno($ch) !== 0) {
                throw new Exception(curl_error($ch));
            }

            //Log::debug(json_encode(curl_getinfo($ch)));

            return $response;
        } finally {
            // ensure temp files with sensitive data are deleted
            fclose($tmpcert);
            fclose($tmpkey);
        }
    }

    public function getCertificateAndPrivateKey(): array
    {
        $cert = $this->filter_pem_certificate(OrganizationSetting::getSetting(OrganizationSetting::DS_CERTIFICATE));
        $key = $this->filter_pem_privatekey(OrganizationSetting::getSetting(OrganizationSetting::DS_CERTKEY), $cert);
        if (empty($cert)) {
            $this->getController()->Flash->error(sprintf(__('%s není správně nastaveno, kontaktujte prosím správce dotačního portálu'), 'Datová Schránka Certifikát pro Odesílací Bránu (PEM)'));
        }
        if (empty($key)) {
            $this->getController()->Flash->error(sprintf(__('%s není správně nastaveno, kontaktujte prosím správce dotačního portálu'), 'Datová Schránka Soukromý Klíč pro Odesílací Bránu (PEM)'));
        }

        return [$cert, $key];
    }

    public function extractAuthConfirmationResponse(string $xml): array
    {
        $ret = [];
        $xml = $this->safelyParseXmlString($xml);
        $attributes = $xml->getElementsByTagName('attribute');
        for ($count = 0; $count < $attributes->count(); $count++) {
            $attribute = $attributes->item($count);
            if ($attribute->hasAttributes()) {
                $name = $attribute->attributes->getNamedItem('name');
                $val = $attribute->attributes->getNamedItem('value');
                if (!empty($name) && !empty($val)) {
                    $ret[$name->nodeValue] = $val->nodeValue;
                }
            }
        }

        return $ret;
    }

    public function extractSetConceptResponse(string $xml): array
    {
        $xml = $this->safelyParseXmlString($xml);

        $dmIdTag = $xml->getElementsByTagName('dmID')->item(0);
        $statusCode = $xml->getElementsByTagName('dmStatusCode')->item(0);
        $statusMessage = $xml->getElementsByTagName('dmStatusMessage')->item(0);

        $dmIdTag = $dmIdTag ? $dmIdTag->nodeValue : null;
        $statusCode = $statusCode ? $statusCode->nodeValue : null;
        $statusMessage = $statusMessage ? $statusMessage->nodeValue : null;

        return [$dmIdTag, $statusCode, $statusMessage];
    }

    /**
     * Modified version of simplesamlphp/saml2
     * https://github.com/simplesamlphp/saml2/blob/121aec484db5331d2c743f4ce89c42b9be3f8127/src/SAML2/DOMDocumentFactory.php#L28
     *
     * @param string $xml
     * @return DOMDocument|null
     */
    public function safelyParseXmlString(string $xml): ?DOMDocument
    {
        if (trim($xml) === '') {
            throw new InvalidArgumentException('Empty XML string provided');
        }

        $entityLoader = libxml_disable_entity_loader(true);
        $internalErrors = libxml_use_internal_errors(true);
        libxml_clear_errors();

        $domDocument = new DOMDocument();
        $options = LIBXML_DTDLOAD | LIBXML_DTDATTR | LIBXML_NONET | LIBXML_PARSEHUGE;
        if (defined('LIBXML_COMPACT')) {
            $options |= LIBXML_COMPACT;
        }

        $loaded = $domDocument->loadXML($xml, $options);

        libxml_use_internal_errors($internalErrors);
        libxml_disable_entity_loader($entityLoader);

        if (!$loaded) {
            $error = libxml_get_last_error();
            libxml_clear_errors();

            throw new $error();
        }

        libxml_clear_errors();

        foreach ($domDocument->childNodes as $child) {
            if ($child->nodeType === XML_DOCUMENT_TYPE_NODE) {
                throw new RuntimeException(
                    'Dangerous XML detected, DOCTYPE nodes are not allowed in the XML body'
                );
            }
        }

        return $domDocument;
    }

    /**
     * @param AppController|IsdsController|UserRequestsController $controller
     * @return bool
     */
    public function storeVerifiedAttributes(AppController $controller): bool
    {
        $storedAtLeastOneVerified = false;
        foreach (IsdsController::MAP_IDENTITY_STORAGE_ISDS_ATRRIBUTE as $identityAttributeName => $selfAttributeName) {
            $storedValue = $controller->getRequest()->getSession()->read($selfAttributeName);
            $controller->loadModel('Identities');
            $identity = $controller->Identities->find(
                'all',
                ['conditions' => [
                    'Identities.user_id' => $controller->getCurrentUser()->id,
                    'Identities.name' => $identityAttributeName,
                ]]
            )->first();
            // do not delete databox if no verified info obtained from ISDS
            if (!empty($identity) && empty($storedValue) && !in_array($identityAttributeName, [Identity::CONTACT_DATABOX])) {
                // delete previously verified if not currently verified
                $controller->Identities->delete($identity);
            } else {
                if (empty($identity)) {
                    $identity = $controller->Identities->newEntity(
                        [
                            'value' => $storedValue,
                            'name' => $identityAttributeName,
                            'user_id' => $controller->getCurrentUser()->id,
                        ]
                    );
                }
                if ($controller->Identities->save($identity)) {
                    $storedAtLeastOneVerified = true;
                }
            }
        }

        return $storedAtLeastOneVerified;
    }
}
