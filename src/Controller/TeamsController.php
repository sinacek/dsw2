<?php

namespace App\Controller;

use App\Middleware\OrgDomainsMiddleware;
use App\Model\Entity\OrganizationSetting;
use App\Model\Entity\Request;
use App\Model\Entity\RequestState;
use App\Model\Entity\User;
use App\Model\Table\RequestsTable;
use Cake\Datasource\EntityInterface;
use Cake\Filesystem\File;
use Cake\Log\Log;
use DateTimeInterface;

/**
 * To perform all lifecycle tasks related to requests
 *
 * @property-read RequestsTable $Requests
 */
class TeamsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Requests');
    }

    /**
     * @param User $user
     * @return bool
     */
    public function isAuthorized($user = null)
    {
        return parent::isAuthorized($user) && $user->hasAtLeastOneTeam();
    }

    public function index()
    {
    }

    public function indexFormalControl()
    {
        $this->set('requests', $this->getSuitableRequests(
            $this->getCurrentUser()->getProgramIdsForFormalControl(),
            [
                RequestState::STATE_NEW,
                RequestState::STATE_READY_TO_SUBMIT,
                RequestState::STATE_SUBMITTED,
                RequestState::STATE_FORMAL_CHECK_APPROVED,
                RequestState::STATE_FORMAL_CHECK_REJECTED,
            ]
        ));
        $this->set('crumbs', [__('Mé týmy') => 'my_teams']);
    }

    public function indexComments()
    {
        $this->set('requests', $this->getSuitableRequests($this->getCurrentUser()->getProgramIdsForComments(), [RequestState::STATE_FORMAL_CHECK_APPROVED]));
        $this->set('crumbs', [__('Mé týmy') => 'my_teams']);
    }

    public function indexProposals()
    {
        $this->set('requests', $this->getSuitableRequests($this->getCurrentUser()->getProgramIdsForComments(), [RequestState::STATE_FORMAL_CHECK_APPROVED]));
        $this->set('crumbs', [__('Mé týmy') => 'my_teams']);
    }

    public function indexApprovals()
    {
        $this->set('requests', $this->getSuitableRequests($this->getCurrentUser()->getProgramIdsForComments(), [RequestState::STATE_FORMAL_CHECK_APPROVED]));
        $this->set('crumbs', [__('Mé týmy') => 'my_teams']);
    }

    public function indexManagers()
    {
        $this->set('requests', $this->getSuitableRequests($this->getCurrentUser()->getProgramIdsForComments(), [RequestState::STATE_REQUEST_APPROVED, RequestState::STATE_READY_TO_SIGN_CONTRACT]));
        $this->set('crumbs', [__('Mé týmy') => 'my_teams']);
    }

    public function requestDetailReadOnly(int $id)
    {
        $conditions = [
            'Requests.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
        ];

        $conditions['Requests.id'] = $id;

        $contain = [
            'Identities',
            'Appeals',
            'Programs',
            'Programs.EvaluationCriteria',
            'Programs.ParentPrograms',
            'Programs.ParentPrograms.EvaluationCriteria',
            'Programs.Realms',
            'Programs.ParentPrograms.Realms',
            'Programs.Realms.Fonds',
            'Programs.ParentPrograms.Realms.Fonds',
            'Users',
            'RequestBudgets',
            'Evaluations',
            'Evaluations.EvaluationCriteria',
            'Files',
        ];

        if ($id > 0) {
            $contain['RequestBudgets'] = [
                'OwnSources',
                'Incomes',
                'Costs',
                'OtherSubsidies',
            ];
            $contain['Users'] = [
                'PublicIncomeHistories',
                'PublicIncomeHistories.PublicIncomeSources',
            ];
            $contain[] = 'Programs.Forms';
            $contain[] = 'Programs.Forms.FormFields';
            $contain[] = 'Programs.ParentPrograms.Forms';
            $contain[] = 'Programs.ParentPrograms.Forms.FormFields';
        }

        $request = $this->Requests->find(
            'all',
            [
                'conditions' => $conditions,
                'contain' => $contain,
            ]
        )->firstOrFail();
        $this->set(compact('request'));
    }

    public function downloadAttachment(int $request_id, int $file_id)
    {
        $request = $this->getRequestOrFail($this->getCurrentUser()->getAllTeamProgramIds(), [], $request_id);
        foreach ($request->files as $file) {
            if ($file->id === $file_id) {
                $_file = new File(ROOT . DS . 'files' . DS . $file->filepath);
                if ($_file->exists()) {
                    return $this->getResponse()->withFile(
                        $_file->path,
                        [
                            'name' => urlencode($file->original_filename),
                            'download' => true,
                        ]
                    );
                }
            }
        }

        return $this->getResponse()->withStatus(404);
    }

    public function formalControlCheckRequest(int $id)
    {
        $request = $this->getRequestOrFail($this->getCurrentUser()->getProgramIdsForFormalControl(), [RequestState::STATE_READY_TO_SUBMIT, RequestState::STATE_SUBMITTED, RequestState::STATE_FORMAL_CHECK_APPROVED, RequestState::STATE_FORMAL_CHECK_REJECTED], $id);

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $request = $this->Requests->patchEntity($request, $this->getRequest()->getData());
            // patchEntity changed internal request_state_id
            $request->setCurrentStatus($request->request_state_id, $this->getCurrentUserId());
            if ($this->Requests->save($request)) {
                if (RequestState::canUserEditRequest($request->request_state_id) && $request->lock_when instanceof DateTimeInterface) {
                    Log::debug('current request state id ' . $request->request_state_id . ', lock_when: ' . $request->lock_when->nice());
                    $this->sendMailSafe('User', 'requestFormalControlReturned', [$request->user, $request]);
                }
                $this->Flash->success(__('Hotovo'));
                $this->redirect(['action' => 'indexFormalControl']);
            }
        }

        $this->set(compact('request'));
    }

    public function formalControlSendEmail(int $id)
    {
        $request = $this->getRequestOrFail($this->getCurrentUser()->getProgramIdsForFormalControl(), [RequestState::STATE_READY_TO_SUBMIT, RequestState::STATE_NEW], $id);
        if (RequestState::canUserEditRequest($request->request_state_id) && $request->lock_when instanceof DateTimeInterface) {
            $this->sendMailSafe('User', 'requestFormalControlReturned', [$request->user, $request]);
            $this->Flash->success(__('E-mail byl odeslán'));
        } else {
            $this->Flash->error(__('Tato žádost je již uzamčena, e-mail není možné odeslat'));
        }

        return $this->redirect($this->getRequest()->referer());
    }

    public function formalControlManualSubmit(int $id)
    {
        $request = $this->getRequestOrFail($this->getCurrentUser()->getProgramIdsForFormalControl(), [RequestState::STATE_READY_TO_SUBMIT, RequestState::STATE_SUBMITTED], $id);
        if (!OrganizationSetting::getSetting(OrganizationSetting::ALLOW_MANUAL_SUBMIT_OF_REQUESTS)) {
            $this->Flash->error(__('Vaše organizace nemá tuto funkci povolenou'));
            $this->redirect(['action' => 'indexFormalControl']);

            return;
        }

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $request = $this->Requests->patchEntity($request, $this->getRequest()->getData());
            $request->setCurrentStatus(RequestState::STATE_SUBMITTED, $this->getCurrentUserId());
            if ($this->Requests->save($request)) {
                $this->Flash->success(__('Hotovo'));
                $this->redirect(['action' => 'indexFormalControl']);
            } else {
                $this->Flash->error($request->getFirstError());
            }
        }
        $this->set(compact('request'));
    }

    public function commentsEvaluate(int $id)
    {
        $request = $this->getRequestOrFail($this->getCurrentUser()->getProgramIdsForComments(), [RequestState::STATE_FORMAL_CHECK_APPROVED], $id);
        $evaluation = $this->Requests->Evaluations->newEntity();
        foreach ($request->evaluations as $candidate) {
            if ($candidate->user_id === $this->getCurrentUser()->id) {
                $evaluation = $candidate;
                $evaluation->set('criterium', unserialize($evaluation->responses));
            }
        }

        $criteria = $this->Requests->Evaluations->EvaluationCriteria->find(
            'all',
            [
                'conditions' => [
                    'EvaluationCriteria.parent_id' => $request->program->evaluation_criteria_id ?? $request->program->parent_program->evaluation_criteria_id,
                ],
            ]
        )->toArray();

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $evaluation->user_id = $this->getCurrentUser()->id;
            // preference on closest to request
            $evaluation->evaluation_criteria_id = $request->program->evaluation_criteria_id ?? $request->program->parent_program->evaluation_criteria_id;
            $evaluation->request_id = $request->id;
            $points = $this->getRequest()->getData('criterium');
            $evaluation->set('responses', serialize($points));
            if (is_array($points)) {
                $evaluation->points = array_sum($points);
            }
            $evaluation->comment = $this->getRequest()->getData('comment');
            if ($this->Requests->Evaluations->save($evaluation)) {
                $this->Flash->success(__('Hotovo'));
                $this->redirect(['action' => 'indexComments']);
            } else {
                $this->Flash->error(__('Formulář obsahuje chyby'));
            }
        }

        $this->set(compact('request', 'evaluation', 'criteria'));
    }

    public function proposalsEdit(int $id)
    {
        $request = $this->getRequestOrFail($this->getCurrentUser()->getProgramIdsForComments(), [RequestState::STATE_FORMAL_CHECK_APPROVED], $id);
        if (!empty($request->final_evaluation)) {
            $request->criterium = unserialize($request->final_evaluation);
        }

        $criteria = $this->Requests->Evaluations->EvaluationCriteria->find(
            'all',
            [
                'conditions' => [
                    'EvaluationCriteria.parent_id' => $request->program->evaluation_criteria_id ?? $request->program->parent_program->evaluation_criteria_id,
                ],
            ]
        )->toArray();

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $request = $this->Requests->patchEntity($request, $this->getRequest()->getData());
            $request->setCurrentStatus($request->request_state_id, $this->getCurrentUserId());
            $eval_proposal = $this->getRequest()->getData('criterium');
            if (is_array($eval_proposal)) {
                $request->final_evaluation = serialize($eval_proposal);
            }
            if ($this->Requests->save($request)) {
                $this->Flash->success(__('Hotovo'));
                $this->redirect(['action' => 'indexProposals']);
            }
        }

        $this->set(compact('request', 'criteria'));
    }

    public function approvalsEdit(int $id)
    {
        $request = $this->getRequestOrFail($this->getCurrentUser()->getProgramIdsForComments(), [RequestState::STATE_FORMAL_CHECK_APPROVED], $id);
        if (!empty($request->final_evaluation)) {
            $request->criterium = unserialize($request->final_evaluation);
        }

        $criteria = $this->Requests->Evaluations->EvaluationCriteria->find(
            'all',
            [
                'conditions' => [
                    'EvaluationCriteria.parent_id' => $request->program->evaluation_criteria_id ?? $request->program->parent_program->evaluation_criteria_id,
                ],
            ]
        )->toArray();

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $request = $this->Requests->patchEntity($request, $this->getRequest()->getData());
            $eval_proposal = $this->getRequest()->getData('criterium');
            if (is_array($eval_proposal)) {
                $request->final_evaluation = serialize($eval_proposal);
            }
            $request->setCurrentStatus($request->final_subsidy_amount < 1 ? RequestState::STATE_CLOSED_FINISHED : RequestState::STATE_REQUEST_APPROVED, $this->getCurrentUserId());
            if ($this->Requests->save($request)) {
                $this->Flash->success(__('Hotovo'));
                $this->redirect(['action' => 'indexProposals']);
            }
        }

        $this->set(compact('request', 'criteria'));
    }

    public function managersEdit(int $id)
    {
        $request = $this->getRequestOrFail($this->getCurrentUser()->getProgramIdsForComments(), [RequestState::STATE_REQUEST_APPROVED, RequestState::STATE_READY_TO_SIGN_CONTRACT], $id);

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            if ($request->request_state_id === RequestState::STATE_REQUEST_APPROVED) {
                $request = $this->Requests->patchEntity($request, $this->getRequest()->getData());
                if ($this->getRequest()->getData('STATE_READY_TO_SIGN_CONTRACT') !== null) {
                    $request->setCurrentStatus(RequestState::STATE_READY_TO_SIGN_CONTRACT, $this->getCurrentUserId());
                }
                if ($this->Requests->save($request)) {
                    $this->Flash->success(__('Hotovo'));
                }
            } elseif ($this->getRequest()->getData('STATE_CONTRACT_SIGNED') !== null) {
                $request->setCurrentStatus(RequestState::STATE_CONTRACT_SIGNED_READY_TO_PAYOUT, $this->getCurrentUserId());
                if ($this->Requests->save($request)) {
                    $this->Flash->success(__('Hotovo'));
                    $this->redirect(['action' => 'indexManagers']);
                }
            }
        }

        $this->set(compact('request'));
    }

    /**
     * @param array $allowed_program_ids
     * @param array $request_state_ids
     * @param int $id
     * @return Request|EntityInterface
     */
    private function getRequestOrFail(array $allowed_program_ids, array $request_state_ids, int $id)
    {
        return $this->getSuitableRequests($allowed_program_ids, $request_state_ids, $id)->firstOrFail();
    }

    private function getSuitableRequests(array $allowed_program_ids, array $request_state_ids, ?int $request_id = null)
    {
        $conditions = [
            'Requests.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
        ];

        if (!empty($request_state_ids)) {
            $conditions['Requests.request_state_id IN'] = $request_state_ids;
        }

        if (!empty($allowed_program_ids)) {
            $conditions['OR'] = [
                'Programs.id IN' => $allowed_program_ids,
                'ParentPrograms.id IN' => $allowed_program_ids,
            ];
        }

        if (!empty($request_id)) {
            $conditions['Requests.id'] = $request_id;
        }

        $contain = [
            'Programs',
            'Programs.EvaluationCriteria',
            'Programs.ParentPrograms',
            'Programs.ParentPrograms.EvaluationCriteria',
            'Programs.Realms',
            'Programs.ParentPrograms.Realms',
            'Programs.Realms.Fonds',
            'Programs.ParentPrograms.Realms.Fonds',
            'Users',
            'RequestBudgets',
            'Evaluations',
            'Appeals',
            'Files',
            'Evaluations.EvaluationCriteria',
        ];

        if ($request_id > 0) {
            $contain['RequestBudgets'] = [
                'OwnSources',
                'Incomes',
                'Costs',
                'OtherSubsidies',
            ];
            $contain['Users'] = [
                'PublicIncomeHistories',
                'PublicIncomeHistories.PublicIncomeSources',
            ];
            $contain[] = 'Programs.Forms';
            $contain[] = 'Programs.Forms.FormFields';
            $contain[] = 'Programs.ParentPrograms.Forms';
            $contain[] = 'Programs.ParentPrograms.Forms.FormFields';
        }

        return $this->Requests->find(
            'all',
            [
                'conditions' => $conditions,
                'contain' => $contain,
            ]
        );
    }
}
