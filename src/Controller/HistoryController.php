<?php

namespace App\Controller;

use App\Middleware\OrgDomainsMiddleware;
use App\Model\Entity\HistoryIdentity;
use App\Model\Entity\OrganizationSetting;
use App\Model\Entity\User;
use App\Model\Table\HistoriesTable;
use App\Model\Table\HistoryIdentitiesTable;
use Cake\Filesystem\File;
use Cake\Http\Exception\NotFoundException;
use Cake\Http\Response;
use Cake\I18n\FrozenDate;
use Cake\Log\Log;
use OldDsw\Controller\Component\OldDswComponent;

/**
 * @property HistoriesTable $Histories
 * @property HistoryIdentitiesTable $HistoryIdentities
 * @property OldDswComponent $OldDsw
 */
class HistoryController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Histories');
        $this->loadModel('HistoryIdentities');
        if (OrganizationSetting::getSetting(OrganizationSetting::HAS_DSW)) {
            $this->loadComponent('OldDsw.OldDsw');
        }
    }

    public function isAuthorized($user = null)
    {
        if (!($user instanceof User)) {
            $user = $this->Auth->user();
        }

        if (!parent::isAuthorized($user)) {
            return false;
        }

        if ($user->isHistoryManager()) {
            return true;
        }

        if (in_array($this->getRequest()->getParam('action'), ['index', 'dsw', 'dswZadosti', 'dswDetail', 'dswZadost'], true)) {
            return $user->hasAtLeastOneTeam();
        }

        return false;
    }

    public function index()
    {
        $identities = $this->HistoryIdentities->find(
            'all',
            [
                'contain' => [
                    'Histories',
                ],
                'conditions' => [
                    'HistoryIdentities.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                ],
            ]
        );

        $this->set(compact('identities'));
    }

    public function addModifyIdentity(int $id = 0)
    {
        $identity = $id > 0 ? $this->HistoryIdentities->get($id, [
            'conditions' => [
                'HistoryIdentities.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
            ],
        ]) : $this->HistoryIdentities->newEntity();

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $identity = $this->HistoryIdentities->patchEntity($identity, $this->getRequest()->getData());
            if (empty($identity->get('name'))) {
                $identity = $this->HistoryIdentities->patchEntity($identity, ['name' => sprintf("%s %s", $identity->first_name, $identity->last_name)]);
            }

            $identity->organization_id = OrgDomainsMiddleware::getCurrentOrganizationId();

            if ($this->HistoryIdentities->save($identity)) {
                $this->Flash->success(__('Úspěšně uloženo'));
                $this->redirect(['action' => 'index']);

                return;
            } else {
                $this->Flash->error(__('Formulář obsahuje chyby'));
            }
        }

        $this->set(compact('identity'));
        $this->set('crumbs', [__('Historie žadatelů') => ['action' => 'index']]);
    }

    public function detail(int $id)
    {
        $identity = $this->HistoryIdentities->get($id, [
            'conditions' => [
                'HistoryIdentities.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
            ],
            'contain' => [
                'Histories',
            ],
        ]);

        $this->set(compact('identity'));
        $this->set('crumbs', [__('Historie žadatelů') => ['action' => 'index']]);
    }

    public function deleteHistory(int $identity_id, int $history_id)
    {
        $identity = $this->HistoryIdentities->get($identity_id, [
            'conditions' => [
                'HistoryIdentities.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
            ],
        ]);
        $history = $this->Histories->get($history_id, [
            'conditions' => [
                'Histories.history_identity_id' => $identity->id,
            ],
        ]);
        $this->Histories->delete($history);

        return $this->redirect(['action' => 'detail', 'id' => $identity->id]);
    }

    public function addModifyHistory(int $identity_id, $history_id = null)
    {
        $identity = $this->HistoryIdentities->get($identity_id, [
            'conditions' => [
                'HistoryIdentities.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
            ],
        ]);
        $history = $history_id > 0 ? $this->Histories->get($history_id, [
            'conditions' => [
                'Histories.history_identity_id' => $identity->id,
            ],
        ]) : $this->Histories->newEntity();

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $history->history_identity_id = $identity->id;
            $history = $this->Histories->patchEntity($history, $this->getRequest()->getData());
            if ($this->Histories->save($history)) {
                $this->Flash->success(__('Úspěšně uloženo'));
                $this->redirect(['action' => 'detail', 'id' => $identity->id]);

                return;
            } else {
                $this->Flash->error(__('Formulář obsahuje chyby'));
                $this->Flash->error($history->getFirstError());
            }
        }

        $this->set(compact('identity', 'history'));
    }

    public function uploadIdentities()
    {
        $this->set('crumbs', [__('Historie žadatelů') => ['action' => 'index']]);
        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            if (empty($this->getRequest()->getData('filedata')) || $this->getRequest()->getData('filedata.error') !== UPLOAD_ERR_OK) {
                $this->Flash->error(__('Soubor nebyl nahrán nebo došlo k chybě při nahrávání'));

                return;
            }

            $fpath = $this->getRequest()->getData('filedata.tmp_name');
            $rowcount = 0;
            $new_identities_count = 0;
            $new_histories_count = 0;
            $handle = null;

            $COL_NAME = 0;
            $COL_DATABOX_ID = 1;
            $COL_BUSINESS_ID = 2;
            $COL_VAT_ID = 3;
            $COL_FIRST_NAME = 4;
            $COL_LAST_NAME = 5;
            $COL_DATE_OF_BIRTH = 6;
            $COL_SUBSIDY_AMOUNT_CZK = 7;
            $COL_PROJECT_TITLE = 8;
            $COL_SUBSIDY_YEAR = 9;
            $COL_NOTES = 10;

            try {
                if (($handle = fopen($fpath, "r")) !== false) {
                    while (($orig_line = fgets($handle)) !== false) {
                        $rowcount++;
                        if ($rowcount === 1) {
                            continue;
                        }
                        $utf8_line = autoUTF($orig_line);
                        $csv_values = str_getcsv($utf8_line, ",", '"', "\\");
                        $columns = count($csv_values);

                        $history_identity = null;
                        $history = null;

                        if ($columns === 7 || $columns === 11) {
                            // identity
                            $or_conditions = [];
                            if (!empty($csv_values[$COL_DATE_OF_BIRTH])) {
                                $or_conditions[] = [
                                    'HistoryIdentities.first_name' => trim(h($csv_values[$COL_FIRST_NAME])),
                                    'HistoryIdentities.last_name' => trim(h($csv_values[$COL_LAST_NAME])),
                                    'HistoryIdentities.date_of_birth' => FrozenDate::parseDate($csv_values[$COL_DATE_OF_BIRTH]),
                                ];
                            }
                            if (!empty($csv_values[$COL_BUSINESS_ID])) {
                                $po_conditions = [
                                    'HistoryIdentities.ico' => trim(h($csv_values[$COL_BUSINESS_ID])),
                                ];
                                if (!empty($csv_values[$COL_VAT_ID])) {
                                    $po_conditions['HistoryIdentities.dic'] = trim(h($csv_values[$COL_VAT_ID]));
                                }
                                $or_conditions[] = $po_conditions;
                            }
                            if (!empty($csv_values[$COL_DATABOX_ID])) {
                                $or_conditions[] = [
                                    'HistoryIdentities.databox_id' => trim(h($csv_values[$COL_DATABOX_ID])),
                                ];
                            }

                            $history_identity = $this->HistoryIdentities->find('all', [
                                'conditions' => [
                                    'HistoryIdentities.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                                    'OR' => $or_conditions,
                                ],
                            ])->first();
                            if (empty($history_identity)) {
                                $history_identity = $this->HistoryIdentities->newEntity([
                                    'organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                                    'name' => $csv_values[$COL_NAME],
                                    'first_name' => $csv_values[$COL_FIRST_NAME],
                                    'last_name' => $csv_values[$COL_LAST_NAME],
                                    'date_of_birth' => FrozenDate::parseDate($csv_values[$COL_DATE_OF_BIRTH]),
                                    'ico' => $csv_values[$COL_BUSINESS_ID],
                                    'dic' => $csv_values[$COL_VAT_ID],
                                    'databox_id' => $csv_values[$COL_DATABOX_ID],
                                ]);
                                if (!$this->HistoryIdentities->save($history_identity)) {
                                    $this->Flash->error(sprintf("%s %d", __('Žadatele nelze uložit, řádek: '), $rowcount));
                                    $this->Flash->error(json_encode($history_identity->getErrors()));
                                    $this->Flash->error(json_encode($csv_values));

                                    return;
                                }
                                $new_identities_count++;
                            }
                        }
                        if ($columns === 11 && $history_identity instanceof HistoryIdentity) {
                            $history_conditions = [
                                'Histories.history_identity_id' => $history_identity->id,
                                'Histories.year' => intval($csv_values[$COL_SUBSIDY_YEAR]),
                                'Histories.czk_amount' => intval($csv_values[$COL_SUBSIDY_AMOUNT_CZK]),
                            ];
                            $history = $this->Histories->find('all', [
                                'conditions' => $history_conditions,
                            ]);
                            if ($history->count() > 1) {
                                // more than 1 history matched
                                $history_conditions['Histories.project_name'] = trim(h($csv_values[$COL_PROJECT_TITLE]));
                                $history = $this->Histories->find('all', [
                                    'conditions' => $history_conditions,
                                ])->first();
                            } elseif ($history->count() === 1) {
                                $history = $history->firstOrFail();
                            } else {
                                $history = $this->Histories->newEntity([
                                    'history_identity_id' => $history_identity->id,
                                    'czk_amount' => $history_conditions['Histories.czk_amount'],
                                    'project_name' => $history_conditions['Histories.project_name'] ?? trim(h(strip_tags($csv_values[$COL_PROJECT_TITLE]))),
                                    'year' => $history_conditions['Histories.year'],
                                    'notes' => trim(h(strip_tags($csv_values[$COL_NOTES]))),
                                ]);
                                if (!$this->Histories->save($history)) {
                                    $this->Flash->error(sprintf("%s %d", __('Poskytnutou podporu nelze uložit, řádek: '), $rowcount));
                                    $this->Flash->error(json_encode($history->getErrors()));
                                    $this->Flash->error(json_encode($csv_values));

                                    return;
                                }
                                $new_histories_count++;
                            }
                        }

                        if (empty($history_identity)) {
                            $this->Flash->error(sprintf(__('Špatný počet sloupců na řádku %d (nalezeno %d, očekáváno 7 nebo 11)'), $rowcount, $columns));

                            return;
                        }
                    }
                }
                $this->Flash->success(sprintf(__('Úspěšně upraveno %d žadatelů (%d nových žadatelů, %d nových záznamů o podpoře)'), $rowcount, $new_identities_count, $new_histories_count));
                $this->redirect(['action' => 'index']);
            } catch (\Throwable $t) {
                Log::error($t->getMessage());
                Log::error($t->getTraceAsString());
                $this->Flash->error(__('Došlo k chybě při čtení nahraného souboru'));

                return;
            } finally {
                if ($handle !== null) {
                    fclose($handle);
                    unlink($fpath);
                }
            }
        }
    }

    public function dsw()
    {
        if (!OrganizationSetting::getSetting(OrganizationSetting::HAS_DSW) || !isset($this->OldDsw)) {
            $this->redirect(['action' => 'index']);

            return;
        }

        $accounts = $this->OldDsw->getAccounts();

        $this->set(compact('accounts'));
        $this->render('OldDsw.OldDsw/listing');
    }

    public function dswZadosti()
    {
        if (!OrganizationSetting::getSetting(OrganizationSetting::HAS_DSW) || !isset($this->OldDsw)) {
            $this->redirect(['action' => 'index']);

            return;
        }

        $zadosti = $this->OldDsw->getZadosti();

        $this->set(compact('zadosti'));
        $this->render('OldDsw.OldDsw/listing_zadosti');
    }

    /**
     * @param int $id dsw.ucty.id
     */
    public function dswDetail(int $id)
    {
        if (!OrganizationSetting::getSetting(OrganizationSetting::HAS_DSW) || !isset($this->OldDsw) || $id < 1) {
            $this->redirect(['action' => 'index']);

            return;
        }

        $account = $this->OldDsw->getAccountById($id);
        $this->set(compact('account'));
        $this->render('OldDsw.OldDsw/detail');
    }

    public function dswHideAccount(int $id)
    {
        if (!OrganizationSetting::getSetting(OrganizationSetting::HAS_DSW) || !isset($this->OldDsw) || $id < 1) {
            $this->redirect(['action' => 'index']);

            return;
        }

        $account = $this->OldDsw->getAccountById($id);
        $account->is_hidden = true;
        $this->OldDsw->Ucty->save($account);

        $this->redirect($this->referer());
    }

    /**
     * @param int $id dsw.prilohy.id
     * @return Response|null
     */
    public function dswDownload(int $id)
    {
        if (!OrganizationSetting::getSetting(OrganizationSetting::HAS_DSW) || !isset($this->OldDsw) || $id < 1) {
            return $this->redirect(['action' => 'index']);
        }

        /**
         * Original files archive should be placed in OldDsw plugin directory, with permissions, so it is readable
         * by web-server user/group
         * eg. folder uploads should be in path /var/www/html/plugins/OldDsw/uploads
         * paths in DSW1 DB are prefixed with '/uploads/' so we look up in the plugin folder directly
         */

        // This will throw if record is not found, resulting in NotFoundException
        $priloha = $this->OldDsw->getPriloha($id);
        $old_dsw_plugin_dir = ROOT . DS . 'plugins' . DS . 'OldDsw' . DS;
        $file = new File($old_dsw_plugin_dir . $priloha->filename);
        if ($file->exists()) {
            return $this->getResponse()->withFile(
                $file->path,
                [
                    'name' => urlencode($priloha->orig_filename),
                    'download' => true,
                ]
            );
        }
        // handle case when file is not present physically, or permissions are incorrect (we cannot tell the difference)
        return $this->getResponse()->withStatus(404);
    }

    /**
     * @param int $id dsw.zadosti.id
     */
    public function dswZadost(int $id)
    {
        if (!OrganizationSetting::getSetting(OrganizationSetting::HAS_DSW) || !isset($this->OldDsw) || $id < 1) {
            $this->redirect(['action' => 'index']);

            return;
        }

        $zadost = $this->OldDsw->getZadost($id);
        $this->set(compact('zadost'));
        $this->render('OldDsw.OldDsw/zadost');
    }

    public function dswHideZadost(int $id)
    {
        if (!OrganizationSetting::getSetting(OrganizationSetting::HAS_DSW) || !isset($this->OldDsw) || $id < 1) {
            $this->redirect(['action' => 'index']);

            return;
        }

        $zadost = $this->OldDsw->getZadost($id);
        $zadost->is_hidden = true;
        $this->OldDsw->Zadosti->save($zadost);

        $this->redirect($this->referer());
    }
}
