<?php

use App\Model\Entity\Realm;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $realms Realm[]
 */
$this->assign('title', __('Oblasti Podpory'));
echo $this->element('simple_datatable');
?>
<h1><?= $this->fetch('title') ?></h1>
<?= $this->Html->link('Vytvořit novou oblast podpory', ['action' => 'realmAddModify'], ['class' => 'btn btn-success m-2']) ?>
<table class="table" id="dtable">
    <thead>
    <tr>
        <th><?= __('ID') ?></th>
        <th><?= __('Název Oblasti') ?></th>
        <th><?= __('Dotační Fond') ?></th>
        <th><?= __('Stav') ?></th>
        <th><?= __('Podřízené Programy') ?></th>
        <th><?= __('Akce') ?></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($realms as $realm): ?>
        <tr>
            <td><?= $realm->id ?></td>
            <td><?= $this->Html->link($realm->name, ['action' => 'realmAddModify', $realm->id]) ?></td>
            <td><?= $this->Html->link($realm->fond->name, ['action' => 'fondAddModify', $realm->fond_id]) ?></td>
            <td>
                <?= $realm->is_enabled ? 'Povolen' : 'Zakázán' ?> <br/>
                <?= $this->Html->link($realm->is_enabled ? '(Zakázat)' : '(Povolit)', ['action' => 'realmToggle', $realm->id], ['class' => $realm->is_enabled ? 'text-danger' : 'text-success']) ?>
            </td>
            <td>
                <?php foreach ($realm->programs as $program): ?>
                    <?= $this->Html->link($program->name, ['action' => 'programAddModify', $program->id]) ?> <br/>
                <?php endforeach; ?>
                <?= $this->Html->link('<i class="fas fa-plus-circle"></i> Přidat nový podřízený program', ['action' => 'programAddModify', 'realm_id' => $realm->id], ['escape' => false, 'class' => 'text-success']) ?>
            </td>
            <td>
                <?= $this->Html->link('Upravit', ['action' => 'realmAddModify', $realm->id]) ?>,
                <?= $this->Form->postLink('Smazat', ['action' => 'realmDelete', $realm->id], ['class' => 'text-danger', 'confirm' => __('Opravdu chcete smazat celou oblast podpory vč. podřízených programů a nastavení?')]) ?>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
