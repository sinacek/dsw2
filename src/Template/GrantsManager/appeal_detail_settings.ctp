<?php

use App\Model\Entity\Appeal;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $appeal Appeal
 */
$this->assign('title', __('Podrobné nastavení výzvy'));
?>
<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col-md-8">
                <h2><?= $this->fetch('title') ?></h2>
            </div>
            <div class="col-md text-right">
                <?= $this->Html->link(__('Přeskočit'), ['action' => 'indexAppeals'], ['class' => 'btn btn-success']) ?>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="alert alert-info">
            <?= __('Nastavení zde slouží pouze k následné kontrole při schvalování žádostí, navrhování výše podpory nebo jednání komisí/výborů/atp.') ?>
        </div>
        <?php
        echo $this->Form->create($appeal);

        $counter = 0;
        foreach ($appeal->appeals_to_programs as $link) {
            $prefix = sprintf('appeals_to_programs.%d.', $counter);
            echo $this->Form->control($prefix . 'id', ['type' => 'hidden', 'value' => $link->id]);
            ?>
            <h4><?= $link->program->name ?></h4>
            <div class="row">
                <div class="col-md">
                    <?= $this->Form->control($prefix . 'budget_size', ['label' => __('Celkový objem financí na tento program v této výzvě'), 'type' => 'number', 'empty' => true]); ?>
                </div>
                <div class="col-md">
                    <?= $this->Form->control($prefix . 'max_request_budget', ['label' => __('Maximální částka o kterou lze žádat v tomto programu'), 'type' => 'number', 'empty' => true]); ?>
                </div>
                <div class="col-md">
                    <?= $this->Form->control($prefix . 'evaluation_deadline', ['label' => __('Poslední den, kdy lze hodnotit žádosti v tomto programu'), 'type' => 'date', 'empty' => true]); ?>
                </div>
            </div>
            <?php
            echo '<hr/>';
            $counter++;
        }

        echo $this->Form->submit(__('Uložit'), ['class' => 'btn btn-success float-right']);
        echo $this->Form->end();
        ?>
    </div>
</div>
