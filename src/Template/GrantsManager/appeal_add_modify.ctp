<?php

use App\Model\Entity\Appeal;
use App\Model\Entity\Program;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $appeal Appeal
 * @var $allowed_programs Program[]
 * @var $programsTree
 */
$this->assign('title', $appeal->id > 0 ? __('Upravit Výzvu') : __('Vytvořit novou výzvu'));

echo $this->element('simple_treeselect');

?>
    <h1><?= $this->fetch('title') ?></h1>
<?php
echo $this->Form->create($appeal);
echo $this->Form->control('name', ['label' => __('Název výzvy')]);
echo $this->Form->control('description', ['label' => __('Popis výzvy')]);
echo $this->Form->control('link', ['label' => __('Odkaz na detailní informace k této výzvě'), 'type' => 'text']);
echo $this->Form->control('is_active', ['type' => 'checkbox', 'label' => __('Aktivní?')]);
echo $this->Form->control('open_from', ['label' => __('Datum začátku příjmu žádostí (první den kdy lze podat žádost)'), 'type' => 'date']);
echo $this->Form->control('open_to', ['label' => __('Datum ukončení příjmu žádostí (první den kdy už nelze podat žádost)'), 'type' => 'date']);
echo $this->Form->control('programs._ids', ['options' => $programsTree, 'label' => __('Obsažené Dotační Programy'), 'class' => 'treeselect']);
echo $this->Form->submit(__('Uložit'), ['class' => 'btn btn-success']);
echo $this->Form->end();
