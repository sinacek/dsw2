<?php

use App\Model\Entity\Fond;
use App\Model\Entity\Realm;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $fonds Fond[]
 * @var $realm Realm
 */

$this->assign('title', $realm->id > 0 ? __('Upravit Oblast Podpory') : __('Vytvořit novou Oblast Podpory'))
?>
    <h1><?= $this->fetch('title') ?></h1>

<?php
echo $this->Form->create($realm);
echo $this->Form->control('name', ['label' => __('Jméno')]);
echo $this->Form->control('description', ['label' => __('Popis')]);
echo $this->Form->control('is_enabled', ['type' => 'checkbox', 'label' => __('Aktivní?')]);
echo $this->Form->control('is_hidden', ['type' => 'checkbox', 'label' => __('Skrýt ve veřejné struktuře dotačních fondů?')]);
echo $this->Form->control('fond_id', ['options' => $fonds]);
echo $this->Form->submit(__('Uložit'), ['class' => 'btn btn-success']);
echo $this->Form->end();
echo $this->Html->link(__('Zahodit změny a vrátit se na seznam oblastí podpory'),['action'=>'indexRealms'],['class'=>'btn btn-warning mt-2']);