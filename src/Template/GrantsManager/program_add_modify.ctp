<?php

use App\Model\Entity\EvaluationCriterium;
use App\Model\Entity\Program;
use App\Model\Entity\Realm;
use App\Model\Entity\Team;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $program Program
 * @var $allowed_realms Realm[]
 * @var $allowed_programs Program[]
 * @var $evaluation_criteria EvaluationCriterium[]
 * @var $allowed_teams Team[]
 * @var $forms array
 */

echo $this->element('simple_select2');
$this->assign('title', $program->id > 0 ? $program->parent_id > 0 ? __('Upravit Pod-program') : __('Upravit Program') : __('Vytvořit nový Program nebo Pod-Program'));

echo $this->Form->create($program);
?>
<div class="card mt-2">
    <div class="card-header">
        <h1><?= $this->fetch('title') ?></h1>
        <?php
        echo $this->Form->button(__('Uložit'), ['class' => 'btn btn-success m-2', 'type' => 'submit']);
        if ($program->parent_id) {
            echo $this->Html->link(__('Zahodit změny a vrátit se k nadřazenému programu'), ['action' => 'programAddModify', $program->parent_id], ['class' => 'btn btn-warning m-2']);
        }
        ?>
    </div>
    <div class="card-body">
        <?php
        echo $this->Form->control('name', ['label' => __('Název')]);
        echo $this->Form->control('description', ['label' => __('Popis')]);
        echo $this->Form->control('parent_id', ['options' => $allowed_programs, 'empty' => __('Program není podřízený'), 'label' => __('Nadřízený Program')]);
        echo $this->Form->control('realm_id', ['options' => $allowed_realms, 'empty' => false, 'label' => __('Oblast Podpory')]);
        echo $this->Form->control('evaluation_criteria_id', ['options' => $evaluation_criteria, 'empty' => __('Ponechte prázdné, pokud se žádosti v tomto programu nehodnotí podle kritérií'), 'label' => __('Hodnotící kritéria')]);
        echo $this->Form->control('forms._ids', ['options' => $forms, 'empty' => true, 'label' => __('Formuláře, které musí žadatel vyplnit před odevzdáním žádosti'), 'class' => 'select2']);
        ?>
    </div>
</div>

<div class="card mt-2">
    <h2 class="card-header"><?= __('Nastavení dotačního procesu') ?></h2>
    <div class="card-body">
        <?php
        echo $this->Form->control('requires_balance_sheet', ['label' => __('Povolit sekci "Ekonomická rozvaha" v rozpočtu projektu'), 'type' => 'checkbox']);
        echo $this->Form->control('requires_budget', ['label' => __('Povolit sekci "Vlastní zdroje a dotace z jiných zdrojů" v rozpočtu projektu'), 'type' => 'checkbox']);
        echo $this->Form->control('requires_extended_budget', ['label' => __('V sekci rozpočtu, požadovat rozepsat nákladové položky jako náklad celkem a požadovaná částka s nepovinným komentářem'), 'type' => 'checkbox']);
        ?>
        <hr/>
        <?php
        echo $this->Form->control('formal_check_team_id', ['options' => $allowed_teams, 'empty' => __('Ponechte prázdné, pokud se formální kontrola neprovádí'), 'label' => __('Kdo provádí Formální kontrolu?')]);
        echo $this->Form->control('price_proposal_team_id', ['options' => $allowed_teams, 'empty' => __('Ponechte prázdné, pokud se výše dotace nenavrhuje dopředu'), 'label' => __('Kdo navrhuje výši dotace?')]);
        echo $this->Form->control('price_approval_team_id', ['options' => $allowed_teams, 'empty' => __('Ponechte prázdné, pokud se výše dotace neschvaluje před jednáním zastupitelstva/rady'), 'label' => __('Kdo schvaluje výši dotace?')]);
        echo $this->Form->control('comments_team_id', ['options' => $allowed_teams, 'empty' => __('Ponechte prázdné, pokud se neprovádí hodnocení dle hodnotících kritérií'), 'label' => __('Kdo provádí hodnocení dotace?')]);
        echo $this->Form->control('request_manager_team_id', ['options' => $allowed_teams, 'empty' => __('Ponechte prázdné, pokud vše bude provádět správce dotačního portálu'), 'label' => __('Kdo může nastavit výsledný status dotace?')]);
        ?>
    </div>
</div>
<?php
echo $this->Form->end();
if (!$program->isNew()): ?>

    <div class="card mt-2">
        <div class="card-header">
            <h2><?= __('Podprogramy') ?></h2>
            <?= $this->Html->link(__('Přidat nový podprogram'), ['action' => 'programAddModify', 'parent_id' => $program->id], ['class' => 'btn btn-success mb-2']) ?>
        </div>
        <div class="card-body">
            <table id="dtable" class="table">
                <thead>
                <tr>
                    <th><?= __('Název') ?></th>
                    <th><?= __('Formuláře') ?></th>
                    <th><?= __('Hodnotící kritéria') ?></th>
                    <th><?= __('Akce') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($program->child_programs as $childProgram): ?>
                    <tr>
                        <td><?= $this->Html->link($childProgram->name, ['action' => 'programAddModify', $childProgram->id]) ?></td>
                        <td>
                            <?php foreach (empty($childProgram->forms) ? $program->forms : $childProgram->forms as $form) {
                                echo sprintf("%s,<br/>", $this->Html->link($form->name, ['controller' => 'Forms', 'action' => 'formDetail', $form->id], ['target' => '_blank']));
                            } ?>
                        </td>
                        <td>
                            <?php
                            if ($childProgram->evaluation_criterium) {
                                echo $this->Html->link($childProgram->evaluation_criterium->name, ['action' => 'criteriumAddModify', 'controller' => 'EvaluationCriteria', $childProgram->evaluation_criteria_id]);
                            } else if ($program->evaluation_criterium) {
                                echo $this->Html->link($program->evaluation_criterium->name, ['action' => 'criteriumAddModify', 'controller' => 'EvaluationCriteria', $program->evaluation_criterium->id]);
                            }
                            ?>
                        </td>
                        <td>
                            <?= $this->Html->link(__('Otevřít'), ['action' => 'programAddModify', $childProgram->id]) ?>
                            ,
                            <?= $this->Form->postLink(__('Smazat Podprogram'), ['action' => 'programDelete', $childProgram->id], ['class' => 'text-danger', 'confirm' => __('Opravdu chcete smazat podprogram?')]) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
<?php endif; ?>

<script type="text/javascript">
    $(function () {
        $("#requires-balance-sheet").change(function () {
            $("#requires-extended-budget").prop('disabled', !$(this).is(':checked'));
        }).change();
    });
</script>