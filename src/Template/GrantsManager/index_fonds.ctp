<?php

use App\Model\Entity\Fond;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $fonds Fond[]
 */
$this->assign('title', __('Dotační Fondy'));
echo $this->element('simple_datatable');
?>
<h1><?= $this->fetch('title') ?></h1>
<?= $this->Html->link(__('Vytvořit nový dotační fond'), ['action' => 'fondAddModify'], ['class' => 'btn btn-success m-2']) ?>
<table class="table" id="dtable">
    <thead>
    <tr>
        <th><?= __('ID') ?></th>
        <th><?= __('Název Fondu') ?></th>
        <th><?= __('Stav') ?></th>
        <th><?= __('Oblasti Podpory') ?></th>
        <th><?= __('Akce') ?></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($fonds as $fond): ?>
        <tr>
            <td><?= $fond->id ?></td>
            <td><?= $this->Html->link($fond->name, ['action' => 'fondAddModify', $fond->id]) ?></td>
            <td>
                <?= $fond->is_enabled ? __('Povolen') : __('Zakázán') ?> <br/>
                <?= $this->Html->link($fond->is_enabled ? sprintf("(%s)", __('Zakázat')) : sprintf("(%s)", __('Povolit')), ['action' => 'fondToggle', $fond->id], ['class' => $fond->is_enabled ? 'text-danger' : 'text-success']) ?>
            </td>
            <td>
                <?php foreach ($fond->realms as $realm): ?>
                    <?= $this->Html->link($realm->name, ['action' => 'realmAddModify', $realm->id]) ?> <br/>
                <?php endforeach; ?>
                <?= $this->Html->link('<i class="fas fa-plus-circle"></i> ' . __('Přidat novou oblast'), ['action' => 'realmAddModify', 'fond_id' => $fond->id], ['escape' => false, 'class' => 'text-success']) ?>
            </td>
            <td>
                <?= $this->Html->link(__('Upravit'), ['action' => 'fondAddModify', $fond->id]) ?>,
                <?= $this->Html->link(__('Vytvořit kopii'), ['action' => 'fondCopy', $fond->id], ['class'=>'text-nowrap text-success']) ?>,
                <?= $this->Form->postLink(__('Smazat'), ['action' => 'fondDelete', $fond->id], ['class' => 'text-danger text-nowrap', 'confirm' => __('Opravdu chcete smazat celý dotační fond?')]) ?>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
