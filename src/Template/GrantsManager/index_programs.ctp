<?php

use App\Model\Entity\Program;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $programs Program[]
 */
$this->assign('title', __('Dotační Programy'));
echo $this->element('simple_datatable');
?>
<h1><?= $this->fetch('title') ?></h1>
<h6><?= __('Jsou zobrazeny pouze programy, pod-programy můžete spravovat otevřením skupiny') ?></h6>
<?= $this->Html->link(__('Přidat nový Program'), ['action' => 'programAddModify'], ['class' => 'btn btn-success m-2']) ?>
<table class="table" id="dtable">
    <thead>
    <tr>
        <th><?= __('Název') ?></th>
        <th><?= __('Pod-programy') ?></th>
        <th><?= __('Formuláře') ?></th>
        <th><?= __('Oblast Podpory') ?></th>
        <th><?= __('Fond') ?></th>
        <th><?= __('Hodnotící kritéria') ?></th>
        <th><?= __('Akce') ?></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($programs as $program): ?>
        <tr>
            <td>
                <?= $this->Html->link($program->name, ['action' => 'programAddModify', $program->id]) ?>
            </td>
            <td>
                <?php foreach ($program->child_programs as $child_program): ?>
                    <?= $this->Html->link(h($child_program->name), ['action' => 'programAddModify', $child_program->id]) ?>
                    <br/>
                <?php endforeach; ?>
            </td>
            <td>
                <?php if (!empty($program->forms)): ?>
                    <?php foreach ($program->forms as $form) {
                        echo sprintf("%s,<hr/>", $this->Html->link($form->name, ['controller' => 'Forms', 'action' => 'formDetail', $form->id], ['target' => '_blank']));
                    } ?>
                <?php endif; ?>
            </td>
            <td><?= $this->Html->link($program->realm->name, ['action' => 'realmAddModify', $program->realm_id]) ?></td>
            <td><?= $this->Html->link($program->realm->fond->name, ['action' => 'fondAddModify', $program->realm->fond_id]) ?></td>
            <td><?= $program->evaluation_criterium ? $this->Html->link($program->evaluation_criterium->name, ['action' => 'criteriumAddModify', 'controller' => 'EvaluationCriteria', $program->evaluation_criteria_id]) : '' ?></td>
            <td>
                <?= $this->Html->link(__('Otevřít'), ['action' => 'programAddModify', $program->id]) ?>,
                <?= $this->Form->postLink(__('Smazat program'), ['action' => 'programDelete', $program->id], ['confirm' => __('Opravdu chcete smazat program včetně pod-programů a jejich nastavení?'), 'class' => 'text-danger text-nowrap']) ?>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
