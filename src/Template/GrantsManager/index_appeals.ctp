<?php

use App\Model\Entity\Appeal;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $appeals Appeal[]
 */
$this->assign('title', __('Dotační Výzvy'));
?>
<h1><?= $this->fetch('title') ?></h1>
<?= $this->Html->link(__('Vytvořit novou výzvu'), ['action' => 'appealAddModify'], ['class' => 'btn btn-success m-2']) ?>
<table class="table">
    <thead>
    <tr>
        <th><?= __('ID') ?></th>
        <th><?= __('Název') ?></th>
        <th><?= __('Programy') ?></th>
        <th><?= __('Platnost') ?></th>
        <th><?= __('Akce') ?></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($appeals as $appeal): ?>
        <tr>
            <td><?= $appeal->id ?></td>
            <td><?= $this->Html->link($appeal->name, ['action' => 'appealAddModify', $appeal->id]) ?></td>
            <td>
                <?php foreach ($appeal->programs as $program): ?>
                    <?= $this->Html->link($program->name, ['action' => 'programAddModify', $program->id]) ?> <br/>
                <?php endforeach; ?>
            </td>
            <td>
                <?= $appeal->open_from->nice() ?> - <?= $appeal->open_to->nice() ?> <br/><br/>
                <?= $appeal->is_active ? __('Aktivní') : __('Neaktivní') ?> <br/>
                <?= $this->Html->link($appeal->is_active ? sprintf('(%s)', __('Deaktivovat')) : sprintf("(%s)", __('Aktivovat')), ['action' => 'appealToggle', $appeal->id], ['class' => ($appeal->is_active ? 'text-danger' : 'text-sucess')]) ?>
            </td>
            <td>
                <?= $this->Html->link(__('Upravit'), ['action' => 'appealAddModify', $appeal->id]) ?>,
                <?= $this->Form->postLink(__('Smazat'), ['action' => 'appealDelete', $appeal->id], ['class' => 'text-danger', 'confirm' => __('Opravdu chcete smazat tuto výzvu?')]) ?>
                ,<br/><?= $this->Html->link(__('Nastavení financí a termínů'), ['action' => 'appealDetailSettings', $appeal->id], ['class' => 'text-success']) ?>
            </td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2">
                <?= $appeal->description ?>
            </td>
            <td colspan="2">
                <?= $this->Html->link($appeal->link, $appeal->link, ['target' => '_blank']) ?>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
