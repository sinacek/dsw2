<?php
/**
 * @var AppView $this
 * @var Request $request
 * @var string $orgName
 * @var string $title
 */
$this->assign('title', $title);

use App\Model\Entity\Request;
use App\View\AppView;

echo __d('email', 'Dobrý den') . ',' . PHP_EOL . PHP_EOL;

echo sprintf(__d('email', 'Vaše žádost %s neprošla formální kontrolou'), $request->name) . PHP_EOL . PHP_EOL;

if ($request->lock_when) {
    echo __d('email', 'Na opravu žádosti máte čas do') . ' ' . $request->lock_when->format('d. m. Y') . PHP_EOL;
    echo __d('email', 'Pokud žádost neopravíte a znovu neodešlete, bude pravděpodobně vyřazena z dalšího zpracování') . PHP_EOL . PHP_EOL;
}

if ($request->lock_comment) {
    echo __d('email', 'Odůvodnění:') . PHP_EOL;
    echo trim(str_replace("<br>", PHP_EOL, strip_tags($request->lock_comment, '<br>')));
}
