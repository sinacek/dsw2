<?php
/**
 * @var AppView $this
 * @var int $user_id
 * @var string $code
 * @var string $orgName
 */

use App\View\AppView; ?>
<?= __d('email', 'Obnovení hesla') ?>

-----------------------------

<?php
$link = $this->Url->build(['_name' => 'password_recovery_go',
    'token' => $code,
    'requestId' => $user_id
], ['escape' => false, 'fullBase' => true]);
?>
<?= __d('email', 'Dobrý den') ?>,

<?= __d('email', 'pro nastavení nového hesla ke svému účtu v systému') ?> <?= h($orgName) ?>, <?= __d('email', 'klikněte na následující odkaz') ?>

<?= $link ?>
