<?php
/**
 * @var AppView $this
 * @var int $user_id
 * @var string $code
 * @var string $orgName
 */

use App\View\AppView; ?>
<?= __d('email', 'Ověření e-mailové adresy') ?>

-----------------------------

<?php
$link = $this->Url->build(['_name' => 'verify_email',
    'token' => $code,
    'requestId' => $user_id
], ['escape' => false, 'fullBase' => true]);
?>
<?= __d('email', 'Dobrý den') ?>,

<?= __d('email', 'pro aktivaci svého uživatelského účtu v systému') ?> <?= h($orgName) ?>, <?= __d('email', 'otevřete následující odkaz') ?>

<?= $link ?>
