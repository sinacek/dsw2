<?php
/**
 * @var AppView $this
 * @var Request $request
 * @var string $orgName
 * @var string $title
 */
$this->assign('title', $title);

use App\Model\Entity\Request;
use App\View\AppView;

?>
<p><?= __d('email', 'Dobrý den') ?>,</p>
<p>
    <?= sprintf(__d('email', 'Vaše žádost %s neprošla formální kontrolou'), $request->name) ?>
</p>
<?php if ($request->lock_when): ?>
    <p>
        <strong><?= __d('email', 'Na opravu žádosti máte čas do') . ' ' . $request->lock_when->format('d. m. Y') ?></strong>
        <br/>
        <?= __d('email', 'Pokud žádost neopravíte a znovu neodešlete, bude pravděpodobně vyřazena z dalšího zpracování') ?>
    </p>
<?php endif; ?>
<?php if ($request->lock_comment): ?>
    <p>
        <strong><?= __d('email', 'Odůvodnění:') ?></strong>
    </p>
    <p>
        <?= str_replace('<p><br></p>', '', $request->lock_comment) ?>
    </p>
<?php endif; ?>
