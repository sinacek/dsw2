<?php
/**
 * @var AppView $this
 * @var int $user_id
 * @var string $code
 * @var string $orgName
 * @var string $title
 */

$this->assign('title', $title);

use App\View\AppView; ?>
<?php
$link = $this->Url->build(['_name' => 'password_recovery_go',
    'token' => $code,
    'requestId' => $user_id
], ['escape' => false, 'fullBase' => true]);
?>
<p><?= __d('email', 'Dobrý den') ?>,</p>
<p><?= __d('email', 'pro nastavení nového hesla ke svému účtu v systému') ?> <?= h($orgName) ?>
    , <?= __d('email', 'klikněte na následující odkaz') ?></p>
<p><?= $this->Html->link($link) ?></p>