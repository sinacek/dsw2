<?php
/**
 * @var AppView $this
 * @var Request $request
 * @var string $orgName
 * @var string $title
 */
$this->assign('title', $title);

use App\Model\Entity\Request;
use App\View\AppView;

?>
<p><?= __d('email', 'Dobrý den') ?>,</p>
<p>
    <?= sprintf(__d('email', 'Vaše žádost %s je nyní ve stavu %s'), $request->name, $request->getCurrentStateLabel()) ?>
</p>
