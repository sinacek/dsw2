<?php

use App\Model\Entity\Form;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $form Form
 * @var $programs array
 * @var $formTypes array
 */

echo $this->element('simple_treeselect');
$this->assign('title', $form->id > 0 ? __('Úprava formuláře') : __('Vytvoření formuláře'));

?>
    <h1><?= $this->fetch('title') ?></h1>
<?php
echo $this->Form->create($form);
echo $this->Form->control('name', ['label' => __('Název Formuláře (veřejný)')]);
echo $this->Form->control('form_type_id', ['label' => __('Typ Formuláře'), 'options' => $formTypes]);
echo $this->Form->control('programs._ids', ['class' => 'treeselect', 'options' => $programs, 'label' => __('Dotační programy, pro které je formulář povinný')]);
echo $this->Form->submit(__('Uložit'), ['class' => 'btn btn-success']);
echo $this->Form->end();
if ($form->id) {
    echo $this->Html->link(__('Zahodit změny a vrátit se k detailu formuláře'), ['action' => 'formDetail', $form->id], ['class' => 'btn btn-warning mt-2']);
}
?>
