<?php

use App\Form\AbstractFormController;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $form AbstractFormController
 */

$this->assign('title', $form->getFormDefinition()->name);
$formID = $form->getFormDefinition()->id;
$nav =
    ($form->hasUserDefinedFields() ? $this->Html->link(__('Přidat nové pole'), ['action' => 'fieldAddModify', $formID], ['class' => 'btn btn-success mb-2']) : '') .
    $this->Html->link(__('Úprava formuláře'), ['action' => 'addModify', $formID], ['class' => 'btn btn-warning mb-2 ml-2']) .
    $this->Html->link(__('Náhled na finální formulář'), ['action' => 'formPreview', $formID], ['class' => 'btn btn-info mb-2 ml-2']) .
    $this->Html->link(__('Vytvořit kopii formuláře'), ['action' => 'formCopy', $formID], ['class' => 'btn btn-primary mb-2 ml-2']);
?>
<div class="card">
    <div class="card-header">
        <h1><?= $this->fetch('title') ?></h1>

        <?= $nav ?>
    </div>
    <div class="card-body">
        <?= $form->renderFormSettings($this) ?>
    </div>
    <div class="card-footer">
        <?= count($form->getFieldsInOrder()) > 0 ? $nav : '' ?>
    </div>
</div>
