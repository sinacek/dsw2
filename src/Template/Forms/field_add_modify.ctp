<?php

use App\Model\Entity\FormField;
use App\Model\Entity\FormFieldType;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $form_field FormField
 * @var $form_field_types array
 * @var $form_fields array
 */

echo $this->element('simple_select2');
$this->assign('title', $form_field->id > 0 ? __('Úprava pole') : __('Vytvoření formulářového pole'));

?>
<h1><?= $this->fetch('title') ?></h1>
<h4><?= __('Formulář') ?>: <?= h($form_field->form->name) ?></h4>
<?php
echo $this->Form->create($form_field);
echo $this->Form->control('name', ['label' => __('Název pole / Textový obsah')]);
echo $this->Form->control('is_required', ['label' => __('Je vyplnění pole požadováno?'), 'type' => 'checkbox']);
echo $this->Form->control('description', ['label' => __('Popis pole / uživatelská vysvětlivka')]);
echo $this->Form->control('form_field_type_id', ['options' => $form_field_types, 'label' => __('Typ formulářového pole')]);
echo $this->Form->control('choices', ['label' => sprintf("%s (%s)", __('Možnosti výběru'), __('Napište text jedné možnosti a stiskněte enter, pak napište další a opakujte postup')), 'empty' => true, 'class' => 'select2tags', 'options' => [], 'multiple' => 'multiple']);
?>
<div class="card mt-2 mb-2">
    <p class="card-header"><?= __('Nastavení tabulky') ?></p>
    <div class="card-body">
        <strong><?= __('Řádky tabulky') ?></strong>
        <small><?= __('Uveďte buď počet řádku nebo příslušný počet popisů, každý řádek textu je řádkem v tabulce') ?></small>
        <?php
        echo $this->Form->control('table_rows_count', ['type' => 'number', 'label' => __('Počet řádků tabulky')]);
        echo $this->Form->control('table_rows_labels', ['type' => 'textarea', 'label' => __('Popis jednotlivých řádků tabulky, řádek textu = řádek v tabulce'), 'data-noquilljs' => 'data-noquilljs']);
        echo $this->Form->control('table_rows_header', ['type' => 'text', 'label' => __('Popis prvního sloupce tabulky (pouze pokud jsou řádky definovány popisky, ne jen počtem)')])
        ?>
        <strong><?= __('Sloupce tabulky') ?></strong>
        <?= $this->Form->error('columns') ?>
        <table class="table">
            <thead>
            <tr>
                <th><?= __('Nadpis sloupce') ?></th>
                <th><?= __('Typ sloupce') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php for ($i = 0; $i < 10; $i++): ?>
                <tr>
                    <td><?= $this->Form->control(sprintf('columns.%d.title', $i), ['type' => 'text', 'label' => false]) ?></td>
                    <td><?= $this->Form->control(sprintf('columns.%d.type', $i), ['options' => FormFieldType::TABLE_COLUMN_TYPES, 'label' => false]) ?></td>
                </tr>
            <?php endfor; ?>
            </tbody>
        </table>
        <strong><?= __('Součet tabulky') ?></strong>
        <?= $this->Form->control('table_show_sums', ['type' => 'checkbox', 'label' => __('Zobrazit součet pod jednotlivými sloupci, pokud jsou číselné')]) ?>
        <?= $this->Form->control('table_show_sums_label', ['type' => 'text', 'label' => __('Popis řádku se součty'), 'default' => __('Celkem')]) ?>
    </div>
    <div class="d-none">
        <?= $this->Form->control('table_settings', ['class' => '', 'required' => false]) ?>
    </div>
</div>
<?php
echo $this->Form->control('field_order', ['options' => $form_fields, 'label' => __('Předcházející formulářové pole'), 'empty' => __('Začátek formuláře')]);
echo $this->Form->submit(__('Uložit'), ['class' => 'btn btn-success']);
echo $this->Form->end();
?>

<script type="text/javascript">
    const TYPES_WITH_CHOICES = <?= json_encode([FormFieldType::FIELD_CHOICES]) ?>;
    const TYPES_WITH_TABLE = <?= json_encode([FormFieldType::FIELD_TABLE]) ?>;
    $(function () {
        $("#form-field-type-id").change(function () {
            let $value = parseInt($(this).val());
            $("#choices").closest(".form-group").toggle(TYPES_WITH_CHOICES.includes($value));
            $("#table-settings").closest(".card").toggle(TYPES_WITH_TABLE.includes($value));
        }).change();

        $("#table-rows-labels").on('change keyup paste', function () {
            $("#table-rows-count").val($(this).val().split(/\r\n|\r|\n/).length);
        });
    });
</script>
