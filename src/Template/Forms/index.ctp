<?php

use App\Model\Entity\Form;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $forms Form[]
 */

$this->assign('title', __('Formuláře'));
echo $this->element('simple_datatable');
?>
<h1><?= $this->fetch('title') ?></h1>
<?= $this->Html->link(__('Přidat formulář'), ['action' => 'addModify'], ['class' => 'btn btn-success mb-2']) ?>
<table class="table" id="dtable">
    <thead>
    <tr>
        <th><?= __('ID') ?></th>
        <th><?= __('Název') ?></th>
        <th><?= __('Položky formuláře') ?></th>
        <th><?= __('Akce') ?></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($forms as $formDefinition): ?>
        <?php $form = $formDefinition->getFormController(); ?>
        <tr>
            <td><?= $formDefinition->id ?></td>
            <td><?= $this->Html->link(h($form->getFormName()), ['action' => 'formDetail', $formDefinition->id]) ?></td>
            <td>
                <ul>
                    <?php if ($form->hasUserDefinedFields()): ?>
                        <?php foreach ($form->getFieldsInOrder() as $field): ?>
                            <li><?= $this->Html->link($field->name, ['action' => 'fieldAddModify', 'id' => $formDefinition->id, 'field_id' => $field->id]) ?></li>
                        <?php endforeach; ?>
                        <li>
                            <?= $this->Html->link('<i class="fas fa-plus-circle"></i> Přidat nové pole', ['action' => 'fieldAddModify', 'id' => $formDefinition->id, 'field_id' => 0], ['escape' => false, 'class' => 'text-success']) ?>
                        </li>
                    <?php endif; ?>
                </ul>
            </td>
            <td>
                <?= $this->Html->link(__('Vytvořit kopii'), ['action' => 'formCopy', $formDefinition->id], ['class' => 'text-nowrap text-success']) ?>
                <?= $this->Form->postLink(__('Smazat'), ['action' => 'formDelete', $formDefinition->id], ['class' => 'text-danger', 'confirm' => __('Opravdu chcete smazat formulář se všemi nastavenými položkami?')]) ?>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
