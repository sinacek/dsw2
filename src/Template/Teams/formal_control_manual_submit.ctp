<?php

use App\Model\Entity\Request;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $request Request
 */

?>
    <div class="card">
        <div class="card-header">
            <?= __('Označit jako odeslanou žádost') ?>
        </div>
        <div class="card-body">
            <?php
            echo $this->Form->create($request);
            echo $this->Form->control('reference_number', ['label' => __('Číslo jednací / Spisová značka')]);
            echo $this->Form->submit(__('Uložit'), ['class' => 'btn btn-success']);
            echo $this->Form->end();
            ?>
        </div>
    </div>
    <hr/>
<?php
echo $this->element('request_full_table', compact('request'));