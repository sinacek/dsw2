<?php

use App\Model\Entity\Request;
use App\Model\Entity\RequestState;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $request Request
 */
$this->assign('title', __('Finalizace'));

?>

    <div class="card">
        <div class="card-header">
            <?= __('Nastavit výsledek zpracování žádosti') ?>
        </div>
        <div class="card-body">
            <?php

            if ($request->request_state_id === RequestState::STATE_REQUEST_APPROVED) {
                echo $this->Form->create($request);
                echo $this->Form->control('final_subsidy_amount', ['label' => __('Výsledná částku podpory (Kč)'), 'default' => $request->final_subsidy_amount ?? $request->request_budget->requested_amount]);
                echo $this->Form->control('comment', ['label' => __('Výsledné slovní hodnocení žádosti'), 'type' => 'textarea', 'data-noquilljs' => 'data-noquilljs']);
                echo $this->Form->control('purpose', ['label' => __('Výsledná formulace účelu, na který bude dotace poskytnuta'), 'type' => 'textarea', 'data-noquilljs' => 'data-noquilljs']);

                echo $this->Form->submit(__('Uložit výsledek zpracování žádosti'), ['class' => 'btn btn-success m-2']);
                echo $this->Form->submit(__('Uložit a nastavit do stavu "Připraveno k podpisu smlouvy"'), ['type' => 'submit', 'name' => 'STATE_READY_TO_SIGN_CONTRACT', 'escape' => 'false', 'class' => 'btn btn-warning m-2']);
                echo $this->Form->end();
            }
            if ($request->request_state_id === RequestState::STATE_READY_TO_SIGN_CONTRACT) {
                echo $this->Form->create($request);
                echo $this->Form->button(__('Nastavit stav "Smlouva podepsána"'), ['type' => 'submit', 'class' => 'btn btn-success', 'name' => 'STATE_CONTRACT_SIGNED']);
                echo $this->Form->end();
            }
            ?>
        </div>
    </div>
    <hr/>

<?php
echo $this->element('request_full_table', compact('request'));