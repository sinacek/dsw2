<?php

use App\Model\Entity\Request;
use App\Model\Entity\RequestState;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $requests Request[]
 */

$this->assign('title', __('Hodnocení'));
echo $this->element('simple_datatable');
?>

<table id="dtable" class="table">
    <thead>
    <tr>
        <th><?= __('ID') ?></th>
        <th><?= __('Stav žádosti') ?></th>
        <th><?= __('Název žádosti') ?></th>
        <th><?= __('Žadatel') ?></th>
        <th><?= __('Program') ?></th>
        <th><?= __('Oblast podpory') ?></th>
        <th><?= __('Fond') ?></th>
        <th><?= __('Moje hodnocení') ?></th>
        <th><?= __('Akce') ?></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($requests as $request): ?>
        <tr>
            <td><?= $request->id ?></td>
            <td><?= RequestState::getLabelByStateId($request->request_state_id) ?></td>
            <td><?= $request->name ?></td>
            <td><?= $this->getUserIdentity($request->user_id, $request->user_identity_version, 'default') ?></td>
            <td><?= $request->program->name ?></td>
            <td><?= $request->program->realm->name ?></td>
            <td><?= $request->program->realm->fond->name ?></td>
            <td>
                <?php $evaluated = false;
                foreach ($request->evaluations as $evaluation) {
                    if ($evaluation->user_id === $this->getCurrentUser()->id) {
                        $evaluated = true;
                        echo $evaluation->points . '/' . $evaluation->evaluation_criterium->max_points;
                    }
                }
                if (!$evaluated) {
                    echo '<span class="badge badge-danger">Nehodnoceno</span>';
                }
                ?>
            </td>
            <td>
                <?php
                if ($request->request_state_id === RequestState::STATE_FORMAL_CHECK_APPROVED) {
                    echo $this->Html->link($evaluated ? __('Upravit hodnocení') : __('Ohodnotit žádost'), ['_name' => 'comments_evaluate', $request->id], ['class' => $evaluated ? 'text-success' : 'btn btn-success']);
                }
                ?>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
