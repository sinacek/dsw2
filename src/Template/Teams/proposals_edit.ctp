<?php

use App\Model\Entity\EvaluationCriterium;
use App\Model\Entity\Request;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $request Request
 * @var $criteria EvaluationCriterium[]
 */

$hasEvaluations = false;
if (empty($request->criterium)) {
    $request->criterium = [];
    foreach ($request->evaluations as $evaluation) {
        $hasEvaluations = true;
        foreach (unserialize($evaluation->responses) as $criterium_id => $points) {
            if (!isset($request->criterium[$criterium_id])) {
                $request->criterium[$criterium_id] = [0, 0];
            }
            $request->criterium[$criterium_id][0] += $points;
            $request->criterium[$criterium_id][1]++;
        }
    }
    foreach ($request->criterium as $criterium_id => $stats) {
        $request->criterium[$criterium_id] = ($stats[1] > 0) ? $stats[0] / $stats[1] : 0;
    }
}

?>

    <div class="card">
        <div class="card-header">
            <?= __('Navrhnout úpravu výsledku žádosti') ?>
        </div>
        <div class="card-body">
            <?php
            echo $this->Form->create($request);
            echo $this->Form->control('final_subsidy_amount', ['label' => __('Návrh na výslednou částku podpory (Kč)'), 'default' => $request->final_subsidy_amount ?? $request->request_budget->requested_amount]);
            echo $this->Form->control('comment', ['label' => __('Návrh na slovní hodnocení žádosti'), 'type' => 'textarea', 'data-noquilljs' => 'data-noquilljs']);
            echo $this->Form->control('purpose', ['label' => __('Návrh na úpravu účelu, na který bude dotace poskytnuta'), 'type' => 'textarea', 'data-noquilljs' => 'data-noquilljs']);
            ?>
            <hr/>
            <strong><?= __('Návrh výsledného bodového hodnocení žádosti (zobrazený je průměr ze všech proběhlých hodnocení pro každé z kritérií)') ?></strong>
            <?php
            $hasCriteria = false;
            $criteriaSum = 0;
            foreach ($criteria as $criterium) {
                $hasCriteria = true;
                echo $this->Form->control('criterium.' . $criterium->id, [
                    'type' => 'number',
                    'min' => 0,
                    'step' => 0.01,
                    'max' => $criterium->max_points,
                    'label' => $criterium->name . sprintf(' (max. %d)', $criterium->max_points) . sprintf('<br/><span class="text-muted">%s</span>', $criterium->description),
                    'default' => 0,
                    'escape' => false]);
                $criteriaSum += $criterium->max_points;
            }
            if ($hasCriteria) {
                echo $this->Form->control('criterium.sum', [
                    'type' => 'number',
                    'min' => 0,
                    'step' => 0.01,
                    'max' => $criteriaSum,
                    'label' => __('Výsledný součet bodů') . sprintf(" (max. %d)", $criteriaSum),
                    'default' => array_sum($request->criterium)
                ]);
            }

            echo $this->Form->submit(__('Uložit návrhy'), ['class' => 'btn btn-success']);
            echo $this->Form->end();
            ?>
        </div>
    </div>
    <hr/>

<?php
echo $this->element('request_full_table', compact('request'));