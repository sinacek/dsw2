<?php
/**
 * @var $this \App\View\AppView
 */

use App\Middleware\OrgDomainsMiddleware;

$this->assign('title', __('Mé týmy'));

$team_roles = [
    'formal_check_programs' => __('Provádí formální kontrolu v těchto programech'),
    'comments_programs' => __('Provádí hodnocení v těchto programech'),
    'price_proposal_programs' => __('Navrhuje výši podpory, komentář, účel a bodové ohodnocení v těchto programech'),
    'price_approval_programs' => __('Schvaluje žádosti před jednáním nejvyššího orgánu v těchto programech'),
    'request_manager_programs' => __('Finalizuje stav žádostí v těchto programech')
];

?>

    <div class="row bg-light p-3">
        <div class="col-md">
            <?= __('Přejít na') ?>
        </div>
        <?php
        if ($this->isTeamFormalControlor()) {
            echo '<div class="col-md text-center">';
            echo $this->Html->link(__('Formální kontrola'), ['_name' => 'team_formal_control_index'], ['class' => 'btn btn-info']);
            echo '</div>';
        }

        if ($this->isTeamEvaluator()) {
            echo '<div class="col-md text-center">';
            echo $this->Html->link(__('Hodnotitelé'), ['_name' => 'team_evaluators_index'], ['class' => 'btn btn-warning']);
            echo '</div>';
        }

        if ($this->isTeamProposals()) {
            echo '<div class="col-md text-center">';
            echo $this->Html->link(__('Navrhovatelé'), ['_name' => 'team_proposals_index'], ['class' => 'btn btn-primary']);
            echo '</div>';
        }

        if ($this->isTeamApprovals()) {
            echo '<div class="col-md text-center">';
            echo $this->Html->link(__('Schvalovatelé'), ['_name' => 'team_approvals_index'], ['class' => 'btn btn-success']);
            echo '</div>';
        }

        if ($this->isTeamManagers()) {
            echo '<div class="col-md text-center">';
            echo $this->Html->link(__('Finalizace'), ['_name' => 'team_managers_index'], ['class' => 'btn btn-danger']);
            echo '</div>';
        }
        ?>
    </div>
    <hr/>

    <h2><?= __('Jste členem těchto týmů') ?></h2>
    <hr/>

<?php foreach ($this->getCurrentUser()->teams as $team): ?>
    <?php if ($team->organization_id !== OrgDomainsMiddleware::getCurrentOrganizationId()) {
        continue;
    } ?>
    <div class="card mt-3">
        <div class="card-header">
            <h2 class="card-title"><?= $team->name ?></h2>
        </div>
        <div class="card-body">
            <ul>

                <?php foreach ($team_roles as $fieldname => $label): ?>
                    <?php if (!empty($team->{$fieldname})): ?>
                        <li class="mt-2">
                            <?= $label ?>:
                            <?= $this->asHtmlList($team->$fieldname) ?>
                        </li>
                    <?php endif; ?>
                <?php endforeach; ?>

            </ul>
        </div>
    </div>
<?php endforeach; ?>
