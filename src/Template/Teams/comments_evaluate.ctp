<?php

use App\Model\Entity\Evaluation;
use App\Model\Entity\EvaluationCriterium;
use App\Model\Entity\Request;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $request Request
 * @var $evaluation Evaluation
 * @var $criteria EvaluationCriterium[]
 */
?>

<div class="card">
    <div class="card-header">
        <?= __('Ohodnotit žádost') ?>
    </div>
    <div class="card-body">
        <?php
        echo $this->Form->create($evaluation);

        foreach ($criteria as $criterium) {
            echo $this->Form->control('criterium.' . $criterium->id, [
                'type' => 'number',
                'min' => 0,
                'max' => $criterium->max_points,
                'label' => $criterium->name . sprintf(' (max. %d bodů)', $criterium->max_points) . sprintf('<br/><span>%s</span>', $criterium->description),
                'default' => 0,
                'escape' => false]);
        }

        echo '<hr/>';
        echo $this->Form->control('comment', ['type' => 'textarea', 'label' => __('Vlastní komentář (nebude nikde zobrazeno)')]);
        echo $this->Form->submit(__('Uložit hodnocení'), ['class' => 'btn btn-success']);
        echo $this->Form->end();
        ?>
    </div>
</div>
</div>
<hr/>
<div class="container-fluid">
    <?php
    echo $this->element('request_full_table', compact('request'));
    ?>
</div>
