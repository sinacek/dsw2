<?php

use App\Model\Entity\OrganizationSetting;
use App\Model\Entity\Request;
use App\Model\Entity\RequestState;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $requests Request[]
 */

$this->assign('title', __('Formální kontrola'));
echo $this->element('simple_datatable');

?>

<table id="dtable" class="table">
    <thead>
    <tr>
        <th><?= __('ID') ?></th>
        <th><?= __('Stav žádosti') ?></th>
        <th><?= __('Název žádosti') ?></th>
        <th><?= __('Žadatel') ?></th>
        <th><?= __('Program') ?></th>
        <th><?= __('Oblast podpory') ?></th>
        <th><?= __('Fond') ?></th>
        <th><?= __('Akce') ?></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($requests as $request): ?>
        <?php
        if (!OrganizationSetting::getSetting(OrganizationSetting::ALLOW_MANUAL_SUBMIT_OF_REQUESTS)
            && in_array($request->request_state_id, [RequestState::STATE_READY_TO_SUBMIT, RequestState::STATE_NEW], true) && empty($request->lock_when)) {
            continue;
        }
        ?>
        <tr>
            <td><?= $this->Html->link($request->id, ['_name' => 'my_teams_request_detail', 'id' => $request->id]) ?></td>
            <td><?= RequestState::getLabelByStateId($request->request_state_id) ?></td>
            <td><?= $this->Html->link($request->name, ['_name' => 'my_teams_request_detail', 'id' => $request->id]) ?></td>
            <td>
                <?= $this->getUserIdentity($request->user_id, $request->user_identity_version, 'default') ?>
                <br/>
                <?= $request->user->email ?>
            </td>
            <td><?= $request->program->name ?></td>
            <td><?= $request->program->realm ? $request->program->realm->name : 'N/A' ?></td>
            <td><?= $request->program->realm ? $request->program->realm->fond->name : 'N/A' ?></td>
            <td>
                <?php
                if (in_array($request->request_state_id, [RequestState::STATE_READY_TO_SUBMIT, RequestState::STATE_NEW], true)) {
                    if ($request->canTransitionTo(RequestState::STATE_SUBMITTED) && OrganizationSetting::getSetting(OrganizationSetting::ALLOW_MANUAL_SUBMIT_OF_REQUESTS)) {
                        echo $this->Html->link(__('Označit jako odeslanou žádost'), ['_name' => 'formal_control_manual_submit', $request->id], ['class' => 'btn btn-success']);
                    } elseif (!empty($request->lock_when)) {
                        echo sprintf("%s %s", __('Opravu žádosti může žadatel provést do:'), $request->lock_when->nice());
                        echo '<br/>';
                        echo $this->Html->link(__('Znovu odeslat e-mail žadateli'), ['_name' => 'formal_control_send_notice', $request->id], ['class' => 'btn btn-warning']);
                    }
                }

                if (!RequestState::canUserEditRequest($request->request_state_id) && $request->request_state_id !== RequestState::STATE_SUBMITTED && $request->canTransitionTo(RequestState::STATE_SUBMITTED)) {
                    echo $this->Form->postButton(__('Vrátit žádost mezi žádosti k formální kontrole'), ['_name' => 'formal_control_check_request', $request->id], ['class' => 'btn btn-warning', 'data' => ['request_state_id' => RequestState::STATE_SUBMITTED]]);
                }

                if ($request->request_state_id === RequestState::STATE_SUBMITTED) {
                    echo $this->Html->link(__('Zkontrolovat obsah žádosti'), ['_name' => 'formal_control_check_request', $request->id], ['class' => 'btn btn-success w-100']);
                }
                ?>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
