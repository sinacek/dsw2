<?php

use App\Model\Entity\Request;
use App\Model\Entity\RequestState;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $request Request
 */
?>

    <div class="card">
        <div class="card-header">
            <?= __('Zkontrolovat obsah žádosti') ?>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <h2><?= __('Žádost je v pořádku') ?></h2>
                    <?php
                    echo $this->Form->create($request);
                    echo $this->Form->hidden('request_state_id', ['value' => RequestState::STATE_FORMAL_CHECK_APPROVED]);
                    echo $this->Form->submit(__('Potvrdit správnost'), ['class' => 'btn btn-success']);
                    echo $this->Form->end();
                    ?>
                    <hr/>
                    <h2><?= __('Žádost nevyhovuje a není možné ji opravit') ?></h2>
                    <?php
                    echo $this->Form->create($request);
                    echo $this->Form->hidden('request_state_id', ['value' => RequestState::STATE_FORMAL_CHECK_REJECTED]);
                    echo $this->Form->submit(__('Vyřadit žádost jako nevyhovující'), ['class' => 'btn btn-danger']);
                    echo $this->Form->end();
                    ?>
                </div>
                <div class="col">
                    <h2><?= __('Vrátit žádost k doplnění') ?></h2>
                    <?php
                    echo $this->Form->create($request);
                    echo $this->Form->control('lock_when', ['label' => __('Datum, do kdy má žadatel čas žádost opravit'), 'type' => 'date', 'default' => date('Y-m-d', strtotime('+7 days'))]);
                    echo $this->Form->control('lock_comment', ['label' => __('Vysvětlení, proč žádost není v pořádku'), 'type' => 'textarea']);
                    echo $this->Form->hidden('request_state_id', ['value' => RequestState::STATE_NEW]);
                    echo $this->Form->submit(__('Vrátit k doplnění'), ['class' => 'btn btn-warning']);
                    echo $this->Form->end();
                    ?>
                </div>
            </div>
        </div>
    </div>
    <hr/>

<?php
echo $this->element('request_full_table', compact('request'));
