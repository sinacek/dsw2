<?php

use App\Model\Entity\Request;
use App\Model\Entity\RequestState;
use App\View\AppView;
use Cake\I18n\Number;

/**
 * @var $this AppView
 * @var $requests Request[]
 */

$this->assign('title', __('Schvalovatelé'));
echo $this->element('simple_datatable');
?>

<table id="dtable" class="table">
    <thead>
    <tr>
        <th><?= __('ID') ?></th>
        <th><?= __('Název žádosti') ?></th>
        <th><?= __('Žadatel') ?></th>
        <th><?= __('Program') ?></th>
        <th><?= __('Oblast podpory') ?></th>
        <th><?= __('Navržená částka') ?></th>
        <th><?= __('Slovní hodnocení') ?></th>
        <th><?= __('Bodové hodnocení') ?></th>
        <th><?= __('Akce') ?></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($requests as $request): ?>
        <tr>
            <td><?= $request->id ?></td>
            <td><?= $request->name ?></td>
            <td><?= $this->getUserIdentity($request->user_id, $request->user_identity_version, 'default') ?></td>
            <td><?= $request->program->name ?></td>
            <td><?= $request->program->realm->name ?></td>
            <td>
                <?= Number::currency($request->final_subsidy_amount ?? $request->request_budget->requested_amount, 'CZK') ?>
            </td>
            <td>
                <?= $request->comment ?>
            </td>
            <td>
                <?php
                $points_sum = 0;
                $evaluated_times = 0;
                foreach ($request->evaluations as $evaluation) {
                    $points_sum += $evaluation->points;
                    $evaluated_times++;
                }
                if (empty($request->final_evaluation) && $evaluated_times > 0) {
                    echo (round($points_sum / $evaluated_times, 2)) . '/' . $request->evaluations[0]->evaluation_criterium->max_points;
                } else if (!empty($request->final_evaluation)) {
                    echo unserialize($request->final_evaluation)['sum'] . '/' . ($request->program->evaluation_criterium ?? $request->program->parent_program->evaluation_criterium)->max_points;
                }
                ?>
            </td>
            <td>
                <?php
                if ($request->request_state_id === RequestState::STATE_FORMAL_CHECK_APPROVED) {
                    echo $this->Html->link(__('Upravit / Schválit návrhy'), ['_name' => 'approvals_edit', $request->id], ['class' => 'btn btn-success']);
                }
                ?>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
