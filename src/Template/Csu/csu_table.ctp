<?php

use App\View\AppView;
use Cake\Utility\Hash;

/**
 * @var $this AppView
 * @var $data array
 */
if (isset($title)) {
    $this->assign('title', $title);
}

echo $this->element('simple_datatable');
?>

<table class="table" id="dtable">
    <thead>
    <tr>
        <th><?= __('Kód') ?></th>
        <th><?= __('Zkrácený název') ?></th>
        <th><?= __('Úplný název') ?></th>
        <?php foreach ($csu_table_cols ?? [] as $colHeader => $colPath): ?>
            <th><?= $colHeader ?></th>
        <?php endforeach; ?>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($data as $row): ?>
        <tr>
            <td><?= $row->number ?></td>
            <td><?= $row->short_name ?></td>
            <td><?= $row->name ?></td>
            <?php foreach ($csu_table_cols ?? [] as $colHeader => $colPath): $rowArray = $row->toArray(); ?>
                <td><?= Hash::get($rowArray, $colPath . '.name') ?></td>
            <?php endforeach; ?>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
