<?php

use App\Model\Entity\HistoryIdentity;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $identity HistoryIdentity
 */
$this->assign('title', $identity->isNew() ? __('Vytvořit Identitu Žadatele') : __('Upravit Identitu Žadatele'));
?>
<div class="card">
    <div class="card-header">
        <h1><?= $this->fetch('title') ?></h1>
    </div>
    <div class="card-body">
        <div class="alert alert-info">
            <strong><?= __('Minimální identifikace:') ?></strong><br/>
            <?= __('Fyzická osoba: Křestní jméno, příjmení a datum narození') ?><br/>
            <?= __('Právnická osoba: Název žadatele a IČO') ?>
        </div>
        <?php
        echo $this->Form->create($identity);
        ?>
        <legend><?= __('Identifikace třetí stranou') ?></legend>
        <?php
        echo $this->Form->control('databox_id', ['label' => __('ID Datové Schránky'), 'type' => 'string']);
        ?>
        <legend><?= __('Identifikace Právnické Osoby') ?></legend>
        <?php
        echo $this->Form->control('name', ['label' => __('Název žadatele'), 'required' => false]);
        echo $this->Form->control('ico', ['label' => __('IČO')]);
        echo $this->Form->control('dic', ['label' => __('DIČ')]);
        ?>
        <legend><?= __('Identifikace Fyzické Osoby') ?></legend>
        <?php
        echo $this->Form->control('first_name', ['label' => __('Křestní jméno')]);
        echo $this->Form->control('last_name', ['label' => __('Příjmení')]);
        echo $this->Form->control('date_of_birth', ['label' => __('Datum narození'), 'empty' => true]);
        ?>
    </div>
    <div class="card-footer">
        <?php
        echo $this->Form->submit(__('Uložit'), ['class' => 'btn btn-success']);
        echo $this->Form->end();
        ?>
    </div>
</div>
