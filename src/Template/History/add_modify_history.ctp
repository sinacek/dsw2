<?php

use App\Model\Entity\History;
use App\Model\Entity\HistoryIdentity;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $identity HistoryIdentity
 * @var $history History
 */

$this->assign('title', $history->isNew() ? __('Přidat nový záznam o podpoře') : __('Upravit záznam o podpoře'));
?>

<div class="card">
    <div class="card-header">
        <h3><?= $this->fetch('title') ?></h3>
    </div>
    <div class="card-body">
        <?php
        echo $this->Form->create($history);
        echo $this->Form->control('year', ['label' => __('Rok poskytnutí podpory'), 'type' => 'number']);
        echo $this->Form->control('czk_amount', ['label' => __('Částka podpory (zaokrouhleno na celé Kč)'), 'type' => 'number']);
        echo $this->Form->control('project_name', ['label' => __('Název podpořeného projektu')]);
        echo $this->Form->control('notes', ['label' => __('Poznámky k tomuto záznamu'), 'data-noquilljs' => 'data-noquilljs']);
        ?>
    </div>
    <div class="card-footer">
        <?php
        echo $this->Form->submit(__('Uložit'), ['class' => 'btn btn-success']);
        echo $this->Form->end();
        ?>
    </div>
</div>
