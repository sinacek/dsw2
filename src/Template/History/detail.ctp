<?php

use App\Model\Entity\HistoryIdentity;
use App\View\AppView;
use Cake\I18n\Number;

/**
 * @var $this AppView
 * @var $identity HistoryIdentity
 */

$this->assign('title', h($identity->name));
?>
<div class="card">
    <div class="card-header">
        <h3><?= h($identity->name) ?></h3>
        <ul>
            <li><strong><?= __('IČO') ?> / <?= __('DIČ') ?>:</strong> <?= $identity->formatIcoDic() ?></li>
            <li><strong><?= __('ID Datové Schránky') ?>:</strong> <?= h($identity->databox_id) ?></li>
            <li><strong><?= __('Jméno / Příjmení') ?>:</strong> <?= h($identity->formatFirstLastName()) ?></li>
            <li><strong><?= __('Datum narození') ?>
                    :</strong> <?= $identity->date_of_birth ? $identity->date_of_birth->nice() : '' ?></li>
        </ul>
    </div>
    <div class="card-body">
        <div class="row mb-2">
            <div class="col">
                <h4><?= __('Historie poskytnuté podpory') ?></h4>
            </div>
            <div class="col text-right">
                <?= $this->Html->link(__('Přidat'), ['action' => 'addModifyHistory', 'identity_id' => $identity->id], ['class' => 'btn btn-success']) ?>
            </div>
        </div>
        <table class="table">
            <thead>
            <tr>
                <th><?= __('Rok') ?></th>
                <th><?= __('Částka (CZK)') ?></th>
                <th><?= __('Název projektu') ?></th>
                <th><?= __('Poznámky') ?></th>
                <th><?= __('Akce') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($identity->histories as $history): ?>
                <tr>
                    <td><?= $history->year ?></td>
                    <td><?= Number::currency($history->czk_amount, 'CZK') ?></td>
                    <td><?= h($history->project_name) ?></td>
                    <td><?= h($history->notes) ?></td>
                    <td>
                        <?= $this->Html->link(__('Upravit'), ['action' => 'addModifyHistory', 'identity_id' => $identity->id, 'id' => $history->id]) ?>
                        ,
                        <?= $this->Html->link(__('Smazat'), ['action' => 'deleteHistory', 'identity_id' => $identity->id, 'id' => $history->id]) ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
