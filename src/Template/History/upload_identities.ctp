<?php
/**
 * @var $this \App\View\AppView
 */

$this->assign('title', __('Nahrát žadatele hromadně'));
?>
<div class="card">
    <div class="card-header">
        <?= $this->fetch('title') ?>
    </div>
    <div class="card-body">
        <?php
        echo $this->Form->create(null, ['type' => 'file']);
        echo $this->Form->control('filedata', ['type' => 'file', 'label' => __('CSV soubor s žadateli a poskytnutou podporou'), 'required' => 'required']);
        ?>
        <div class="alert alert-info">
            <strong><?= __('Žadatele lze nahrát pomocí souboru CSV (pro Excel: Soubor > Uložit jako > CSV)') ?></strong><br/>
            <?= __('Vzorové příklady (název souboru není důležitý, důležité je jen pořadí a obsah sloupců)') ?>
            <ul>
                <li>
                    <?= __('Žadatelé bez poskytnuté podpory') ?>
                    <ul>
                        <li><?= __('Windows:') ?> <?= $this->Html->link('zadatele.cp1250.csv', 'https://gitlab.com/otevrenamesta/praha3/dsw2/-/raw/master/docs/history-templates/zadatele.cp1250.csv?inline=false', ['download' => 'zadatele.cp1250.csv', 'target' => '_blank']) ?></li>
                        <li><?= __('Linux:') ?> <?= $this->Html->link('zadatele.utf8.csv', 'https://gitlab.com/otevrenamesta/praha3/dsw2/-/raw/master/docs/history-templates/zadatele.utf8.csv?inline=false', ['download' => 'zadatele.utf8.csv', 'target' => '_blank']) ?></li>
                    </ul>
                </li>
                <li>
                    <?= __('Žadatelé vč. poskytnuté podpory') ?>
                    <ul>
                        <li><?= __('Windows:') ?> <?= $this->Html->link('zadatele-s-podporou.cp1250.csv', 'https://gitlab.com/otevrenamesta/praha3/dsw2/-/raw/master/docs/history-templates/zadatele-s-podporou.cp1250.csv?inline=false', ['download' => 'zadatele-s-podporou.cp1250.csv', 'target' => '_blank']) ?></li>
                        <li><?= __('Linux:') ?> <?= $this->Html->link('zadatele-s-podporou.utf8.csv', 'https://gitlab.com/otevrenamesta/praha3/dsw2/-/raw/master/docs/history-templates/zadatele-s-podporou.utf8.csv?inline=false', ['download' => 'zadatele-s-podporou.utf8.csv', 'target' => '_blank']) ?></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <div class="card-footer">
        <?php
        echo $this->Form->submit(__('Uložit'), ['class' => 'btn btn-success']);
        echo $this->Form->end();
        echo $this->element('filesize_check');
        ?>
    </div>
</div>
