<?php

use App\Model\Entity\HistoryIdentity;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $identities HistoryIdentity[]
 **/
$this->assign('title', __('Historie žadatelů'));
echo $this->element('simple_datatable');
?>
<div class="row mb-2">
    <div class="col">
        <h1><?= $this->fetch('title') ?></h1>
    </div>
    <div class="col text-right">
        <?= $this->Html->link(__('Přidat nového žadatele'), ['action' => 'addModifyIdentity'], ['class' => 'btn btn-success mr-2']) ?>
        <?= $this->Html->link(__('Nahrát žadatele hromadně'), ['action' => 'uploadIdentities'], ['class' => 'btn btn-primary mr-2']) ?>
    </div>
</div>
<table class="table" id="dtable">
    <thead>
    <tr>
        <th><?= __('ID') ?></th>
        <th><?= __('Název Žadatele') ?></th>
        <th><?= __('IČO') . '/' . __('DIČ') ?></th>
        <th><?= __('Datová Schránka') ?></th>
        <th><?= __('Datum narození') ?></th>
        <th><?= __('Počet Historických Záznamů') ?></th>
        <th><?= __('Akce') ?></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($identities as $identity): ?>
        <tr>
            <td><?= $identity->id ?></td>
            <td><?= h($identity->name) . $identity->formatFirstLastName(true) ?></td>
            <td><?= $identity->formatIcoDic() ?></td>
            <td><?= h($identity->databox_id) ?> </td>
            <td><?= $identity->date_of_birth ? $identity->date_of_birth->nice() : '' ?></td>
            <td><?= count($identity->histories) ?></td>
            <td>
                <?= $this->Html->link(__('Otevřít'), ['action' => 'detail', 'id' => $identity->id]) ?>,
                <?= $this->Html->link(__('Upravit'), ['action' => 'addModifyIdentity', 'id' => $identity->id]) ?>,
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
