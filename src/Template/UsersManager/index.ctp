<?php

use App\Model\Entity\User;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $users User[]
 * @var $organizations array [org_id => org_name]
 * @var $roles array [role_id => role_name]
 */
$this->assign('title', __('Uživatelé'));
echo $this->element('simple_datatable');
?>
<h1><?= $this->fetch('title') ?></h1>
<?= $this->Html->link(__('Pozvat nového uživatele'), ['_name' => 'admin_users_invite'], ['class' => 'btn btn-success mb-2']) ?>
<table class="table" id="dtable">
    <thead>
    <tr>
        <th><?= __('ID') ?></th>
        <th><?= __('E-mail') ?></th>
        <th><?= __('Povolen') ?></th>
        <th><?= __('Organizace / Role') ?></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($users as $user): ?>
        <tr>
            <td><?= $user->id ?></td>
            <td><?= $this->Html->link($user->email, ['action' => 'edit', $user->id]) ?></td>
            <td><?= $user->is_enabled ? 'Povolen' : 'Zakázán' ?></td>
            <td>
                <?php
                foreach ($user->users_to_roles as $users_to_role) {
                    printf("%s%s<br/>", $roles[$users_to_role->user_role_id], $users_to_role->organization_id ? ' v ' . $organizations[$users_to_role->organization_id] : '');
                }
                ?>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>