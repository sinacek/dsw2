<?php

use App\Model\Entity\OrganizationSetting;
use App\Model\Entity\User;
use App\Model\Entity\UsersToRole;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $user User
 * @var $role UsersToRole
 * @var $organizations array [org_id => org_name]
 * @var $roles array [role_id => role_name]
 */
$this->assign('title', __('Uživatel') . ': ' . h($user->email));
?>
<div class="card mt-2">
    <div class="card-header">
        <h4><?= $this->fetch('title') ?></h4>

        <?= $this->Html->link('<i class="fas fa-dungeon"></i> ' . __('Vstoupit do uživatele'), ['action' => 'loginAs', $user->id], ['escape' => false, 'class' => 'btn btn-primary']) ?>
        <?php
        if ($this->getSiteSetting(OrganizationSetting::HAS_DSW) === true) {
            echo $this->Html->link('Spojit se starými identitami', ['action' => 'oldLink', $user->id], ['class' => 'btn btn-success']) . '<br/>';
        }
        ?>
    </div>
    <div class="card-body">
        <ul class="list-group list-group-flush">
            <li class="list-group-item">
                <?= __('Uživatel ověřil svou e-mailovou adresu') ?>: <?= $user->is_enabled ? __('ANO') : __('NE') ?>
            </li>
        </ul>
    </div>
    <div class="card-footer">
        <?= __('Poslední úprava uživatele') ?> : <?= $user->modified->nice() ?>
    </div>
</div>

<div class="card mt-2">
    <div class="card-header">
        <h4><?= __('Uživatelské role') ?></h4>
    </div>
    <div class="card-body">
        <table class="table">
            <thead>
            <tr>
                <th><?= __('Role') ?></th>
                <th><?= __('Organizace') ?></th>
                <th><?= __('Akce') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($user->users_to_roles as $users_to_role): ?>
                <tr>
                    <td><?= $users_to_role->user_role->role_name ?></td>
                    <td><?= $users_to_role->organization ? $users_to_role->organization->name : 'Globální' ?></td>
                    <td>
                        <?= $this->Html->link('Smazat', ['action' => 'roleDelete', $user->id, $users_to_role->id], ['class' => 'text-danger']) ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="card-footer">
        <h4><?= __('Přidat novou roli') ?></h4>
        <?php
        echo $this->Form->create($role, ['url' => ['action' => 'roleAdd', $user->id]]);
        echo $this->Form->control('organization_id', ['options' => $organizations]);
        echo $this->Form->control('user_role_id', ['options' => $roles]);
        echo $this->Form->submit('Přidat novou roli', ['class' => 'btn btn-success mb-2']);
        echo $this->Form->end();
        ?>
    </div>
</div>
