<?php

use App\Model\Entity\User;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $user User
 * @var $ucty array
 */

$this->assign('title', h($user->email));
echo $this->element('simple_select2');

echo $this->Form->create($user);

echo $this->Form->control('stare_ucty._ids', ['options' => $ucty, 'label' => __('Účty ze staré verze DSW'), 'class' => 'select2', 'multiple' => true]);

echo $this->Form->submit(__('Uložit'), ['class' => 'btn btn-success']);
echo $this->Form->end();