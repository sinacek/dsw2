<?php

use App\Model\Entity\Request;
use App\View\AppView;
use Cake\I18n\Number;

/**
 * @var $this AppView
 * @var $requests Request[]
 * @var $file File
 */

echo $this->Form->create($file, ['type' => 'file']);
echo $this->Form->control('filedata', ['label' => __('Vyberte prosím soubor přílohy'), 'type' => 'file']);
echo $this->Form->submit(__('Nahrát'), ['class' => 'btn btn-success']);
echo $this->Form->end();
echo $this->element('filesize_check');
