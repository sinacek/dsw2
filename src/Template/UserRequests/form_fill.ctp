<?php

use App\Form\AbstractFormController;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $requestForm AbstractFormController
 */

$this->assign('title', $requestForm->getFormName());


$submit = $this->Form->button(__('Uložit vyplněný formulář'), ['class' => 'btn btn-success m-2', 'type' => 'submit']);
$backbutton = $this->Html->link(__('Neukládat a Vrátit se zpět k žádosti'), ['action' => 'requestDetail', $requestForm->getUserRequest()->id], ['class' => 'btn btn-warning m-2']);
$resetButton = $this->Form->postLink(__('Vymazat data a začít znovu'), ['action' => 'formFillReset', 'form_id' => $requestForm->getFormDefinition()->id, 'id' => $requestForm->getUserRequest()->id], ['class' => 'btn btn-danger m-2', 'confirm' => __('Opravdu chcete vymazat všechna vyplněné informace?')]);
echo $this->Form->create($requestForm, ['type' => $requestForm->getHtmlHelperFormType()]);
?>
<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col-md-7">
                <h1><?= $this->fetch('title') ?></h1>
            </div>
            <div class="col-md text-right">
                <?php
                echo $submit;
                echo $backbutton;
                echo $resetButton;
                ?>
            </div>
        </div>
    </div>
    <div class="card-body">
        <?= $requestForm->render($this); ?>
    </div>
    <div class="card-footer text-right">
        <?php
        echo $submit;
        echo $backbutton;
        echo $this->Form->end();
        echo $resetButton;
        ?>
    </div>
</div>
<?= $this->element('filesize_check'); ?>
