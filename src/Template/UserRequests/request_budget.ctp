<?php

use App\Model\Entity\BudgetItemType;
use App\Model\Entity\Request;
use App\Model\Entity\RequestBudget;
use App\View\AppView;
use Cake\I18n\Number;

/**
 * @var $this AppView
 * @var $request Request
 * @var $budget RequestBudget
 */

$this->assign('title', __('Rozpočet projektu'));

echo $this->Form->create($budget);
?>

<div class="card mb-3">
    <div class="card-header">
        <h2 class="card-title"><?= $this->fetch('title') ?></h2>
        <?php if ($request->program->requires_budget || $request->program->requires_balance_sheet): ?>
            <span><?= __('Celkové částky jsou automaticky počítány z jednotlivých položek níže na této stránce') ?></span>
        <?php endif; ?>
    </div>
    <div class="card-header font-weight-bold">
        <?php
        echo $this->Form->control('requested_amount', ['label' => __('Požadovaná výše dotace (Kč)')]);
        ?>
    </div>
    <div class="card-body">
        <div class="card-text">
            <?php
            if ($request->program->requires_balance_sheet) {
                echo $this->Form->control('total_income', ['label' => __('Celkové výnosy projektu (Kč)'), 'disabled' => 'disabled', 'type' => 'text', 'value' => Number::currency($budget->total_income, 'CZK'), 'required' => false]);
                echo $this->Form->control('total_costs', ['label' => __('Celkové náklady na projekt (Kč)'), 'disabled' => 'disabled', 'required' => false]);
            }

            if ($request->program->requires_budget) {
                echo $this->Form->control('total_own_sources', ['label' => __('Vlastní zdroje celkem (Kč)'), 'disabled' => 'disabled', 'required' => false]);
                echo $this->Form->control('total_other_subsidy', ['label' => __('Dotace z jiných zdrojů celkem (Kč)'), 'disabled' => 'disabled', 'required' => false]);
            }
            ?>
        </div>
    </div>
</div>

<div class="navbar navbar-dark text-right sticky-top bg-dark rounded mb-2">
    <?= $this->Form->submit(__('Uložit'), ['class' => 'btn btn-success nav-item']) ?>
</div>

<?php if ($request->program->requires_balance_sheet): ?>
    <div class="card mb-3">
        <div class="card-header">
            <h2 class="card-title"><?= __('Ekonomická rozvaha') ?></h2>
            <span><?= __('Zde můžete uvést jednotlivé položky výnosů a nákladů projektu') . '. ' . __('V další sekci této stránky pak můžete uvést položkově vlastní zdroje nebo dotace z jiných zdrojů, které se týkají stejného projektu') ?></span>
        </div>
        <div class="card-body">
            <div class="card-text row">
                <div class="col-md<?= $request->program->requires_extended_budget ? '-12' : ' border-right border-dark' ?>">
                    <h4 class="text-center"><?= __('Náklady') ?></h4>
                    <?php $this->budgetRows(BudgetItemType::TYPE_COST, $budget, 10, ['extended' => boolval($request->program->requires_extended_budget)]) ?>
                </div>
                <div class="col-md">
                    <h4 class="text-center"><?= __('Výnosy') ?></h4>
                    <?php $this->budgetRows(BudgetItemType::TYPE_INCOME, $budget) ?>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <?php
            echo $this->Form->control('balance_sum', ['label' => __('Rozdíl výnosů a nákladů'), 'disabled' => 'disabled', 'type' => 'text', 'value' => Number::currency($budget->total_income - $budget->total_costs, 'CZK')]);
            ?>
        </div>
    </div>
    <div class="navbar navbar-dark text-right sticky-top bg-dark rounded mb-2">
        <?= $this->Form->submit(__('Uložit'), ['class' => 'btn btn-success nav-item float-right']) ?>
    </div>
<?php endif; ?>

<?php if ($request->program->requires_budget): ?>
    <div class="card mb-3">
        <div class="card-header">
            <h2 class="card-title"><?= __('Vlastní zdroje a dotace z jiných zdrojů') ?></h2>
            <span><?= __('Pokud projekt financujete z vlastních prostředků, např. členských příspěvků, nebo jste na stejný projekt požádali nebo obdrželi dotaci z jiného zdroje') ?></span>
        </div>
        <div class="card-body">
            <div class="card-text row">
                <div class="col-md border-right border-dark">
                    <h4 class="text-center"><?= __('Vlastní zdroje') ?></h4>
                    <?php $this->budgetRows(BudgetItemType::TYPE_OWN_SOURCE, $budget) ?>
                </div>
                <div class="col-md">
                    <h4 class="text-center"><?= __('Dotace z jiných zdrojů') ?> (<span
                                class="small"><?= __('Včetně podaných žádostí') ?></span>)</h4>
                    <?php $this->budgetRows(BudgetItemType::TYPE_SUBSIDY, $budget) ?>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <?php
            echo $this->Form->control('sources_sum', ['label' => __('Součet vlastních zdrojů a dalších dotačních zdrojů'), 'disabled' => 'disabled', 'type' => 'text', 'value' => Number::currency($budget->total_other_subsidy + $budget->total_own_sources, 'CZK')]);
            ?>
        </div>
    </div>
    <div class="navbar navbar-dark text-right sticky-top rounded bg-dark">
        <?= $this->Form->submit(__('Uložit'), ['class' => 'btn btn-success nav-item float-right']) ?>
    </div>
<?php endif; ?>

<?php
echo $this->Form->end();
?>
<script type="text/javascript">
    const TYPE_INCOME = <?= BudgetItemType::TYPE_INCOME ?>;
    const TYPE_COST = <?= BudgetItemType::TYPE_COST ?>;
    const TYPE_OWN_SOURCE = <?= BudgetItemType::TYPE_OWN_SOURCE ?>;
    const TYPE_OTHER_SUBSIDY = <?= BudgetItemType::TYPE_SUBSIDY ?>;
    const TYPE_REQUESTED_AMOUNT = 0;
    let current_income = <?= floatval($budget->total_income) ?>;
    let current_costs = <?= floatval($budget->total_costs) ?>;
    let current_own_sources = <?= floatval($budget->total_own_sources) ?>;
    let current_other_subsidy = <?= floatval($budget->total_other_subsidy) ?>;
    let current_total_balance = 0;
    let current_total_sources = 0;
    $(function () {
        $("input[data-source-type]").on('change keyup bind', function () {
            let $type = parseInt($(this).data('source-type'));
            let $sum = 0.0;
            $("input[data-source-type=" + $type + "]").each(function () {
                $sum += parseFloat($(this).val());
            })
            $sum = $sum.toFixed(2);
            let $target = false;
            switch ($type) {
                case TYPE_INCOME:
                    current_income = $sum;
                    $target = "#total-income";
                    current_total_balance = current_income - current_costs;
                    break;
                case TYPE_COST:
                    current_costs = $sum;
                    $target = "#total-costs";
                    current_total_balance = current_income - current_costs;
                    break;
                case TYPE_OWN_SOURCE:
                    current_own_sources = $sum;
                    $target = '#total-own-sources';
                    current_total_sources = current_other_subsidy + current_own_sources;
                    break;
                case TYPE_OTHER_SUBSIDY:
                    current_other_subsidy = $sum;
                    $target = '#total-other-subsidy';
                    current_total_sources = current_other_subsidy + current_own_sources;
                    break;
                case TYPE_REQUESTED_AMOUNT:
                    $target = '#requested-amount';
                    break;
            }
            if ($target) {
                $($target).val($sum);
            }
            current_total_balance = parseFloat(current_total_balance).toFixed(2);
            current_total_sources = parseFloat(current_total_sources).toFixed(2);
            $("#sources-sum").val(current_total_sources);
            $("#balance-sum").val(current_total_balance);
        });
        $.each([TYPE_INCOME, TYPE_COST, TYPE_OTHER_SUBSIDY, TYPE_OWN_SOURCE], function (index, value) {
            $("input[data-source-type=" + value + "]").first().trigger('change');
        });
    });
</script>
