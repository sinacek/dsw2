<?php

use App\Model\Entity\Request;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $request Request
 **/

echo $this->element('request_full_table', ['request' => $request]);