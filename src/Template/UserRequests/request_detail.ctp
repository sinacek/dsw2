<?php

use App\Model\Entity\Identity;
use App\Model\Entity\OrganizationSetting;
use App\Model\Entity\Request;
use App\Model\Entity\RequestBudget;
use App\Model\Entity\RequestState;
use App\View\AppView;
use Cake\I18n\Number;

/**
 * @var $this AppView
 * @var $request Request
 * @var $public_income_history array
 */

$this->assign('title', h($request->name));
?>
<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col">
                <h1 class="d-inline"><?= h($request->name) ?></h1>
            </div>
            <div class="col text-right">
                <?= sprintf("%s %d", __('žádost č.'), $request->id) ?><br/>
                <?= $this->Html->link(__('Nastavení'), ['_name' => 'request_settings', 'id' => $request->id], ['class' => 'btn btn-outline-success']) ?>
            </div>
        </div>
        <div class="row">
            <div class="col text-right">
                <?php
                if (!RequestState::canUserEditRequest($request->request_state_id)
                    || ($request->request_state_id === RequestState::STATE_READY_TO_SUBMIT
                        && $this->getSiteSetting(OrganizationSetting::ALLOW_MANUAL_SUBMIT_OF_REQUESTS, false) === true)
                ) {
                    echo $this->Html->link(
                        __('Stáhnout jako PDF'),
                        ['action' => 'getPdf', $request->id],
                        ['class' => 'btn btn-success m-2']
                    );
                }
                if ($this->isRequestOwner($request) && $request->request_state_id === RequestState::STATE_READY_TO_SUBMIT) {
                    echo $this->Html->link(
                        __('Odeslat žádost Datovou schránkou'),
                        ['action' => 'requestSubmitDatabox', 'id' => $request->id],
                        ['class' => 'm-2 btn btn-info ' . ($request->appeal->canUserSubmitRequest($request) ? '' : 'disabled')]
                    );
                }
                ?>
            </div>
        </div>
    </div>
    <div class="card-body">
        <table class="table">
            <thead>
            <tr>
                <th><?= __('Požadavek') ?></th>
                <th><?= __('Stav požadavku') ?></th>
                <th><?= __('Akce') ?></th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>
                    <?= __('Vyplnit svou kompletní identitu') ?>
                </td>
                <td>
                    <?php
                    $identityMissingFields = $this->getCurrentUser()->getIdentityMissingFields($request->user_identity_version);
                    $identityCompleted = empty($identityMissingFields);

                    $class = $identityCompleted ? 'fa-check text-success' : 'fa-times text-danger';
                    $text = $identityCompleted ? __('Hotovo') : __('Nesplněno');
                    ?><i class="fas font-weight-bold <?= $class ?>"></i> <?= $text ?>
                    <?php
                    if (!empty($identityMissingFields)) {
                        $counter = 0;
                        foreach ($identityMissingFields as $missingFieldId) {
                            if ($counter < 3) {
                                echo '<br/><span class="text-danger">' . sprintf("%s: %s", __('Chybí vyplnit'), Identity::getFieldLabel($missingFieldId)) . '</span>';
                            }
                            $counter++;
                        }
                        if ($counter >= 3) {
                            echo '<br/>' . sprintf(__('Celkem %d chyb'), $counter);
                        }
                    }
                    ?>
                </td>
                <td>
                    <?= $this->Html->link(__('Vyplnit identitu'), ['_name' => 'update_self_info'], ['class' => 'btn btn-success']) ?>
                </td>
            </tr>

            <tr>
                <td>
                    <?= sprintf("%s %d až %d", __('Poskytnout informace o přijaté veřejné podpoře za roky'), intval(date('Y')) - $this->getSiteSetting(OrganizationSetting::SUBSIDY_HISTORY_YEARS, true), date('Y')) ?>
                </td>
                <td>
                    <?php if (empty($public_income_history)): ?>
                        <span class="text-warning">
                            <i class="fas fa-exclamation-triangle"></i> <?= __('Nebyla nalezeny žádné historické záznamy'); ?>
                        </span>
                    <?php else: ?>
                        <span class="">
                            <?= __('Historie obsahuje alespoň jednu položku') ?>
                        </span>
                    <?php endif; ?>
                </td>
                <td>
                    <?= $this->Html->link(__('Upravit historii'), ['_name' => 'update_self_history'], ['class' => 'btn btn-success']) ?>
                </td>
            </tr>


            <tr>
                <td>
                    <?= __('Vyplnit rozpočet projektu') ?>
                </td>
                <td>
                    <?php
                    switch ($request->validateBudgetRequirements()) {
                        default:
                        case RequestBudget::BUDGET_MISSING:
                        case RequestBudget::BUDGET_MISSING_REQUESTED_AMOUNT:
                            ?>
                            <i class="fas fa-times text-danger"></i> <?= __('Rozpočet nebyl vyplněn'); ?>
                            <?php
                            break;
                        case RequestBudget::BUDGET_MISSING_BALANCE_SHEET:
                            ?>
                            <i class="fas fa-exclamation-triangle text-warning"></i> <?= __('V rozpočtu projektu nejsou žádné položky (výnosy / náklady)') ?>
                            <?php
                            break;
                        case RequestBudget::BUDGET_MISSING_BUDGET:
                            ?>
                            <i class="fas fa-exclamation-triangle text-warning"></i> <?= __('V rozpočtu projektu nejsou žádné položky (vlastní zdroje / jiné dotace)') ?>
                            <?php
                            break;
                        case RequestBudget::BUDGET_OK:
                            ?>
                            <i class="fas fa-check text-success"></i> <?= sprintf(__('Rozpočet je vyplněn, žádáte o %s'), Number::currency($request->request_budget->requested_amount, 'CZK')) ?>
                            <?php
                            break;
                        case RequestBudget::BUDGET_REQUESTED_HIGHER_THAN_ALLOWED:
                            ?>
                            <i class="fas fa-times text-danger"></i> <?= sprintf("%s %s", __('Žádáte o částku vyšší než je možné, maximum v tomto programu je'), Number::currency($request->appeal->getMaxRequestBudget($request->program_id), 'CZK')) ?>
                            <?php
                            break;
                    }
                    ?>
                </td>
                <td>
                    <?= $this->Html->link(__('Upravit rozpočet'), ['_name' => 'request_budget', $request->id], ['class' => 'btn btn-success']) ?>
                </td>
            </tr>

            <?php
            foreach ($request->getForms() as $form) {
                ?>
                <tr>
                    <td><?= sprintf("%s: %s", __("Vyplnit Formulář"), $form->name) ?></td>
                    <td><?php
                        $isComplete = $request->isFormCompleted($form->id);
                        $class = $isComplete ? 'fa-check text-success' : 'fa-times text-danger';
                        $text = $isComplete ? __('Hotovo') : __('Nesplněno');
                        ?><i class="fas font-weight-bold <?= $class ?>"></i> <?= $text ?>
                    </td>
                    <td>
                        <?= $this->Html->link(__('Vyplnit formulář'), ['action' => 'formFill', 'id' => $request->id, 'form_id' => $form->id], ['class' => 'btn btn-success']) ?>
                    </td>
                </tr>
                <?php
            }
            ?>
            <tr>
                <td>
                    <?= __('Nahrát nepovinné přílohy') ?>
                </td>
                <td>
                    <p class="text-success"><i class="fas fa-check text-success"></i> <?= __('Nemusíte nic nahrávat') ?>
                    </p>
                    <?php foreach ($request->files ?? [] as $attachment): ?>
                        <?= $this->Html->link($attachment->original_filename, ['action' => 'fileDownload', $request->id, $attachment->id], ['target' => '_blank']); ?>
                        <?= $this->Form->postLink('<i class="fas fa-times"></i>', ['action' => 'attachmentDelete', $request->id, $attachment->id], ['class' => 'text-danger', 'title' => __('Smazat'), 'escape' => false, 'confirm' => __('Opravdu chcete smazat tuto přílohu?')]) ?>
                        <br/>
                    <?php endforeach; ?>
                </td>
                <td>
                    <?= $this->Html->link(__('Nahrát novou přílohu'), ['action' => 'attachmentAdd', $request->id], ['class' => 'btn btn-outline-success']) ?>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="card-footer">
        <strong><?= $request->program->name ?></strong>
        <div>
            <?= $request->program->description ?>
        </div>
        <strong><?= __('Dotační Výzva') ?>: <?= $request->appeal->name ?></strong>
        <div>
            <?= $request->appeal->description ?>
        </div>
    </div>
</div>
