<?php

use App\Model\Entity\OrganizationSetting;
use App\Model\Entity\Request;
use App\Model\Entity\RequestState;
use App\View\AppView;
use OldDsw\Model\Entity\Zadost;

/**
 * @var $this AppView
 * @var $requests Request[]
 * @var $old_requests Zadost[]
 */

$this->assign('title', __('Mé žádosti o podporu'));
?>
<h1><?= $this->fetch('title') ?></h1>
<?= $this->Html->link(__('Vytvořit novou žádost o podporu'), ['action' => 'addModify'], ['class' => 'btn btn-success mb-2']) ?>
<hr/>
<div class="row no-gutters">
    <?php foreach ($requests as $request): ?>
        <div class="col-xl-4 col-md-6 col-12 mt-2 pl-2 d-flex">
            <div class="card flex-fill">
                <div class="card-header">

                    <div class="row">
                        <div class="col">
                            <h4 class="d-inline"><?= h($request->name) ?></h4>
                        </div>
                        <div class="mr-2">
                            č. <?= $request->id ?>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <?= sprintf("%s: %s", __('Výzva'), $request->appeal->name) ?> <br/>
                    <?= sprintf("%s: %s", __('Program podpory'), $request->program->name) ?> <br/>

                    <?php if (RequestState::canUserEditRequest($request->request_state_id) && !empty($request->lock_when)): ?>
                        <hr/>
                        <div class="alert alert-danger">
                            <?= __('Žádost bude automaticky uzavřena') ?>: <?= $request->lock_when->format('d.m.Y') ?>
                            <br/>
                            <?= __('Důvod neschválení žádosti o dotaci') ?>:
                            <div>
                                <?= $request->lock_comment ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if (!RequestState::canUserEditRequest($request->request_state_id)): ?>
                        <?php if (!empty($request->reference_number)): ?>
                            <?= __('Přidělené číslo jednací: ') . $request->reference_number ?>
                            <hr/>
                        <?php endif; ?>
                        <br/>
                        <div class="alert alert-success">
                            <?= __('Žádost byla úspěšně odeslána a nyní probíhá její schvalování. O dalším postupu vás budeme informovat e-mailem a/nebo datovou schránkou') ?>
                            <br/><br/>
                            <?= sprintf("%s: %s", __('Aktuální stav žádosti'), $request->getCurrentStateLabel()) ?>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="card-footer">

                    <?php
                    if ($this->isRequestOwner($request) && $request->request_state_id === RequestState::STATE_READY_TO_SUBMIT) {
                        echo $this->Html->link(
                            __('Odeslat žádost Datovou schránkou'),
                            ['action' => 'requestSubmitDatabox', 'id' => $request->id],
                            ['class' => 'm-2 w-100 btn btn-info ' . ($request->appeal->canUserSubmitRequest($request) ? '' : 'disabled')]
                        );
                    }

                    if (!RequestState::canUserEditRequest($request->request_state_id)
                        || ($request->request_state_id === RequestState::STATE_READY_TO_SUBMIT
                            && $this->getSiteSetting(OrganizationSetting::ALLOW_MANUAL_SUBMIT_OF_REQUESTS, false) === true)
                    ) {
                        echo $this->Html->link(
                            __('Stáhnout jako PDF'),
                            ['action' => 'getPdf', $request->id],
                            ['class' => 'btn btn-success m-2 w-100']
                        );
                    }

                    if (in_array($request->request_state_id, [RequestState::STATE_PAID_READY_FOR_SETTLEMENT], true)) {
                        echo $this->Html->link(
                            __('Provést ohlášení akce / díla'),
                            '#',
                            ['class' => ' btn btn-primary disabled m-2 w-100']
                        );
                    }

                    if (RequestState::canUserEditRequest($request->request_state_id)) {
                        echo $this->Html->link(
                            __('Upravit žádost'),
                            ['action' => 'requestDetail', $request->id],
                            ['class' => 'btn btn-success m-2 w-100']
                        );
                    }

                    if ($this->isRequestOwner($request)
                        && in_array($request->request_state_id, [RequestState::STATE_NEW, RequestState::STATE_READY_TO_SUBMIT])
                    ) {
                        echo $this->Form->postLink(
                            __('Smazat žádost'),
                            ['action' => 'requestDelete', $request->id],
                            ['class' => 'text-danger m-3 d-block', 'confirm' => __('Opravdu chcete smazat tuto žádost?')]);
                    }
                    ?>

                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>
<hr/>

<?php if ($this->getSiteSetting(OrganizationSetting::HAS_DSW) === true && !empty($old_requests)): ?>
    <?= $this->element('simple_datatable'); ?>
    <div class="card">
        <div class="card-header">
            <h3><?= __('Archiv žádostí') ?></h3>
        </div>
        <div class="card-body table-responsive">
            <?= $this->element('OldDsw.zadosti', ['zadosti' => $old_requests]); ?>
        </div>
    </div>
<?php endif; ?>
