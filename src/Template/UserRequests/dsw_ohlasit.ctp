<?php

use App\View\AppView;
use OldDsw\Model\Entity\OhlasovaciFormular;
use OldDsw\Model\Entity\Zadost;

/**
 * @var $this AppView
 * @var $zadost Zadost
 * @var $ohlaseni OhlasovaciFormular
 **/
$this->assign('title', __('Ohlásit akci/činnost'));

echo $this->Form->create($ohlaseni);
?>
<div class="card">
    <div class="card-header">
        <h1 class="card-title"><?= $this->fetch('title') ?></h1>
        <div class="card-subtitle">
            <?= __('Žádost') ?>: <?= $zadost->nazev ?>
        </div>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md">
                <?php
                echo $this->Form->control('typ', ['options' => OhlasovaciFormular::TYPY_OHLASENI, 'label' => __('Typ ohlášení'), 'type' => 'radio']);
                ?>
            </div>
            <div class="col-md">
                <?php
                echo $this->Form->control('datum_zahajeni', ['empty' => true, 'label' => sprintf("%s (%s)", __('Datum zahájení'), __('nepovinné, pokud se shoduje s datem ukončení'))]);
                echo $this->Form->control('datum_ukonceni', ['label' => __('Datum ukončení'), 'default' => date('Y-m-d')]);
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md">
                <?php
                echo $this->Form->control('cerpana_castka', ['type' => 'number', 'label' => __('Čerpaná částka'), 'max' => intval($zadost->konecna_castka)]);
                ?>
            </div>
            <div class="col-md">
                <?php
                echo $this->Form->control('pocet_ucastniku', ['type' => 'number', 'label' => __('Předpokládaný počet účastníků'), 'required' => false, 'default' => 0]);
                ?>
            </div>
        </div>
        <?php
        echo $this->Form->control('misto_konani', ['label' => __('Místo konání'), 'data-noquilljs' => 'data-noquilljs']);
        ?>
        <div class="row">
            <div class="col-md">
                <?php
                echo $this->Form->control('garant', ['label' => __('Garant akce')]);
                ?>
            </div>
            <div class="col-md">
                <?php
                echo $this->Form->control('garant_telefon', ['label' => __('Tel. garanta akce')]);
                ?>
            </div>
            <div class="col-md">
                <?php
                echo $this->Form->control('garant_email', ['label' => __('Email garanta akce'), 'type' => 'email']);
                ?>
            </div>
        </div>
        <?php
        echo $this->Form->control('poznamka', ['label' => __('Poznámka k ohlášení'), 'data-noquilljs' => 'data-noquilljs', 'required' => false, 'default' => ' ']);
        ?>
        <?php
        echo $this->Form->submit(__('Uložit'), ['class' => 'btn btn-success']);
        echo $this->Form->end();
        ?>
    </div>
</div>