<?php

use App\Model\Entity\Request;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $request Request
 * @var $appeals_programs array
 * @var $program_descriptions array
 */
$this->assign('title', $request->isNew() ? __('Nová žádost o podporu') : __('Nastavení'));
echo $this->element('simple_chained_select', ['data' => $appeals_programs, 'selectedKey' => $request->program_id]);
?>
<div class="card">
    <h1 class="card-header"><?= $this->fetch('title') ?></h1>
    <div class="card-body">
        <?php
        echo $this->Form->create($request);
        echo $this->Form->control('program_id', ['options' => [], 'type' => 'select', 'class' => 'chainedSelect', 'label' => __('Vyberte si Dotační Výzvu, Oblast podpory a Program, ve kterém chcete žádat'), 'required' => 'required']);
        echo $this->Form->control('name', ['label' => __('Název projektu')]);
        echo $this->Form->control('request_budget.requested_amount', ['label' => sprintf("%s<br/><small>%s</small>", __('Požadovaná výše dotace (Kč)'), __('Částku můžete později upravit')), 'type' => 'number', 'default' => 0, 'required' => false, 'escape' => false]);
        echo $this->Form->submit($request->isNew() ? __('Vytvořit novou žádost') : __('Uložit'), ['class' => 'btn btn-success']);
        echo $this->Form->end();
        ?>
    </div>
    <div class="card-footer">
    </div>
</div>
<script type="text/javascript">
    const PROGRAM_DESCRIPTIONS = <?= json_encode($program_descriptions) ?>;

    function chained_select_callback(id) {
        let $target = $(".card-footer");
        if (id in PROGRAM_DESCRIPTIONS) {
            $target.html("<strong><?= __('Popis programu') ?></strong><p>" + PROGRAM_DESCRIPTIONS[id] + "</p>");
        } else {
            $target.html("<p class='alert alert-danger'><?= __('Není vybrán program') ?></p>");
        }
    }
</script>
