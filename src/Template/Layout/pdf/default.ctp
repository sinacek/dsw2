<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <title>
    </title>
    <style type="text/css">
        * {
            font-family: sans-serif;
        }

        <?= file_get_contents(WWW_ROOT.DS.'css'.DS.'bootstrap-4.4.1.min.css') ?>
    </style>
</head>
<body>
<div class="container-fluid clearfix">
    <?= $this->fetch('content') ?>
</div>
</body>
</html>
