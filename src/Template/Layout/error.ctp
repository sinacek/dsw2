<?php
/**
 * @var $this AppView
 */

use App\View\AppView; ?>
<!DOCTYPE html>
<html lang="cs">
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="referrer" content="origin-when-cross-origin">
    <title>
        <?= $this->fetch('title') ?>
    </title>
</head>
<body>
<?= $this->fetch('content') ?>
</body>
</html>
