<?php
/**
 * @var $this AppView
 */

use App\View\AppView;

?>
<!DOCTYPE html>
<html lang="cs">
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="referrer" content="origin-when-cross-origin">
    <title>
        Dotační Software 2 - Shrnutí projektu
    </title>

    <?= $this->Html->script([
        'jquery-3.5.1.min.js',
        'popper-1.16.0.min.js',
        'bootstrap-4.4.1.min.js',
        'jquery.fancybox-3.5.7.min.js',
        'font-awesome-5.13.0.all.min.js',
    ]) ?>

    <?= $this->Html->css([
        'bootstrap-4.4.1.min.css',
        'jquery.fancybox-3.5.7.min.css',
        'font-awesome-5.13.0.all.min.css',
    ]) ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

    <style type="text/css">
        html {
            scroll-behavior: smooth;
        }

        body {
            background-image: url('/about_assets/background.jpg');
            background-repeat: no-repeat;
            background-attachment: fixed;
        }

        .bg-opaque {
            background-color: rgba(255, 255, 255, 0.9);
        }

        .w-35 {
            width: 35% !important;
        }

        .w-30 {
            width: 30% !important;
        }

        .w-20 {
            width: 20% !important;
        }

        i.circle {
            border-radius: 50%;
            border: 2px solid;
            color: white;
            display: inline-block;
            font-size: 1.5rem;
            padding: 0 .75rem;
            font-style: normal;
        }
    </style>
</head>
<body id="body" data-spy="scroll" data-target="#about-navbar" data-offset="0">

<nav id="about-navbar" class="navbar navbar-dark bg-dark navbar-expand-lg sticky-top">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText"
            aria-controls="navbarText" aria-expanded="false" aria-label="Otevřít navigační menu">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarText">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a href="/" title="Zpět na dotační portál" class="nav-link nav-action">
                    <i aria-hidden="true" class="fas fa-arrow-left"></i>
                    <span class="d-md-inline-block d-lg-none">Zpět na dotační portál</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link active" href="#body">Dotační Software 2</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#about">O projektu</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#get-started">Vyzkoušet si</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#process">Dotační proces</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#features">Funkce</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#pricing">Ceník</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#gallery">Fotogalerie</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#partners">Partneři</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#links">Další informace</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#contacts">Kontakty</a>
            </li>
        </ul>
    </div>
</nav>

<div class="container-fluid clearfix">
    <?= $this->Flash->render() ?>

    <?= $this->fetch('content') ?>
</div>
</body>
</html>
