<?php

use App\Model\Entity\Request;
use App\View\AppView;
use Cake\I18n\Number;

/**
 * @var $this AppView
 * @var $appeals array
 * @var $states array
 * @var $requests Request[]
 */

echo $this->element('simple_datatable');
?>

<div class="card" id="economics-filter">
    <div class="card-header">
        <?php
        echo $this->Form->create(null);
        ?>
        <div class="row">
            <div class="col-md">
                <?= $this->Form->control('state_id', ['options' => $states, 'class' => 'select2', 'label' => __('Stav žádosti'), 'empty' => true, 'value' => intval($this->getRequest()->getParam('state_id'))]) ?>
                <?= $this->Form->submit(__('Filtrovat')) ?>
            </div>
            <div class="col-md">
                <?= $this->Form->control('appeal_id', ['options' => $appeals, 'class' => 'select2', 'label' => __('Dotační výzva'), 'empty' => true, 'value' => intval($this->getRequest()->getParam('appeal_id'))]) ?>
            </div>
        </div>
        <?php
        echo $this->Form->end();
        ?>
        <script type="text/javascript">
            let $filterForm = undefined;
            $(function () {
                $filterForm = $("#economics-filter form");
                $("select", $filterForm).change(function () {
                    $filterForm.submit();
                });
            });
        </script>
    </div>
</div>
<hr/>

<table class="table" id="dtable">
    <thead>
    <tr>
        <th><?= __('ID') ?></th>
        <th><?= __('Stav žádosti') ?></th>
        <th><?= __('Žadatel') ?></th>
        <th><?= __('Program') ?></th>
        <th><?= __('Oblast podpory') ?></th>
        <th><?= __('Podpora celkem') ?></th>
        <th><?= __('Vyplaceno') ?></th>
        <th><?= __('Ohlášeno?') ?></th>
        <th><?= __('Vyúčtováno?') ?></th>
        <th><?= __('Akce') ?></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($requests as $request): ?>
        <tr>
            <td><?= $request->id ?></td>
            <td><?= $request->getCurrentStateLabel() ?></td>
            <td><?= $this->getUserIdentity($request->user_id, $request->user_identity_version, 'default') ?></td>
            <td><?= $request->program->name ?></td>
            <td><?= $request->program->realm->name ?></td>
            <td><?= Number::currency($request->final_subsidy_amount ?? $request->request_budget->requested_amount ?? 0, 'CZK') ?></td>
            <td class="text-center"><?= $this->element('simple_color_indicator', ['bool' => $request->subsidy_paid > 0]) ?></td>
            <td class="text-center"><?= $this->element('simple_color_indicator', ['bool' => $request->is_reported]) ?></td>
            <td class="text-center"><?= $this->element('simple_color_indicator', ['bool' => $request->is_settled]) ?></td>
            <td></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
