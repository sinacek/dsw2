<?php

use App\Model\Entity\User;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $user User
 */
$this->assign('title', __('Registrace'));
?>
    <h1><?= $this->fetch('title') ?></h1>
<?php
echo $this->Form->create($user);

echo $this->Form->control('email', ['autofocus' => 'autofocus']);
echo $this->Form->control('password', ['label' => __('Heslo')]);

echo $this->Form->submit(__('Registrovat se'), ['class' => 'btn btn-success']);
echo $this->Form->end();
?>