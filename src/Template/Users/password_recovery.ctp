<?php

/**
 * @var $this AppView
 * @var $oneShotCode string
 */

use App\View\AppView;

$this->assign('title', __('Zapomenuté heslo'));
?>
    <h1><?= $this->fetch('title') ?></h1>

<?php
echo $this->Form->create(null);
echo $this->Form->control('email', ['type' => 'email', 'required' => 'required', 'label' => __('E-mailová adresa registrovaného uživatele'), 'autofocus' => 'autofocus']);
echo $this->Form->hidden('oneShot', ['value' => $oneShotCode]);
echo $this->Form->submit(__('Vyžádat odkaz na obnovení hesla'), ['class' => 'btn btn-success']);
echo $this->Form->end();