<?php
/**
 * @var $this AppView
 * @var $history PublicIncomeHistory
 * @var $histories PublicIncomeHistory[]
 * @var $sources array
 */

use App\Model\Entity\PublicIncomeHistory;
use App\View\AppView;
use Cake\I18n\Number;

$this->assign('title', __('Historie přijaté veřejné podpory'));

?>
<h1><?= __('Přidat nový záznam historie') ?></h1>
<?php
echo $this->Form->create($history);
echo $this->Form->control('project_name', ['required' => 'required', 'label' => __('Název projektu')]);
echo $this->Form->control('fiscal_year', ['type' => 'number', 'min' => 2000, 'max' => intval(date('Y')), 'default' => intval(date('Y')) - 1, 'label' => __('Rok poskytnutí')]);
echo $this->Form->control('amount_czk', ['type' => 'number', 'label' => __('Výše podpory v Kč'), 'required' => true]);
echo $this->Form->control('public_income_source_id', ['options' => $sources, 'label' => __('Zdroj podpory')]);
echo $this->Form->submit(__('Přidat'), ['class' => 'btn btn-success']);
echo $this->Form->end();
?>
<hr/>
<h2><?= $this->fetch('title') ?></h2>

<?php
$lastYear = null;
foreach ($histories

as $row) {
if ($row->fiscal_year != $lastYear) {
$lastYear = $row->fiscal_year;
if ($lastYear != null) {
    echo '</tbody></table>';
}

echo sprintf('<h3>%s</h3>', $row->fiscal_year);

?>
<table class="table">
    <thead>
    <tr>
        <th><?= __('Zdroj') ?></th>
        <th><?= __('Název / Popis projektu') ?></th>
        <th><?= __('Výše podpory (Kč)') ?></th>
        <th><?= __('Akce') ?></th>
    </tr>
    </thead>
    <tbody>
    <?php

    }
    ?>
    <tr>
        <td><?= $row->public_income_source->source_name ?></td>
        <td><?= $row->project_name ?></td>
        <td><?= Number:: currency($row->amount_czk, 'CZK') ?></td>
        <td><?= $this->Html->link(__('Smazat'), ['action' => 'incomeHistoryDelete', $row->id], ['class' => 'text-danger']) ?></td>
    </tr>
    <?php
    } ?>
    </tbody>
</table>
