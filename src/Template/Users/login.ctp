<?php

use App\Model\Entity\OrganizationSetting;
use App\Model\Entity\User;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $user User
 */
$this->assign('title', __('Přihlášení'));
?>
    <h1><?= $this->fetch('title') ?></h1>
<?php
echo $this->Form->create($user);

$email_extras = [];
if ($this->getSiteSetting(OrganizationSetting::HAS_DSW) === true) {
    $email_extras = [
        'type' => 'text',
        'label' => __('Email / Uživatelské jméno')
    ];
}

echo $this->Form->control('email', ['autofocus' => 'autofocus'] + $email_extras);
echo $this->Form->control('password', ['label' => __('Heslo')]);

echo $this->Form->submit(__('Přihlásit se'), ['class' => 'btn btn-success']);
echo $this->Form->end();
?>