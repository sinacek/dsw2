<?php

/**
 * @var $this AppView
 * @var $user User
 */

use App\Model\Entity\User;
use App\View\AppView;

$this->assign('title', __('Nastavení nového hesla'));
?>
    <h1><?= $this->fetch('title') ?></h1>

<?php
echo $this->Form->create($user);
echo $this->Form->control('email', ['disabled' => 'disabled']);
echo $this->Form->control('password', ['required' => 'required', 'label' => __('Nové heslo')]);
echo $this->Form->control('password_2', ['required' => 'required', 'label' => __('Nové heslo znovu, pro ověření')]);
echo $this->Form->submit(__('Nastavit'));
echo $this->Form->end();
