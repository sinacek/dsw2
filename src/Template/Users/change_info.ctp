<?php
/**
 * @var $this AppView
 * @var $identityForm IdentityForm
 */

use App\Controller\Component\IsdsComponent;
use App\Form\IdentityForm;
use App\Model\Entity\Identity;
use App\View\AppView;
use Cake\Routing\Router;

$this->assign('title', __('Moje identita'));
echo $this->element('simple_select2');
?>
<?= $this->Form->create($identityForm);
if (empty($identityForm->getErrors())) {
    $identityForm->setErrors(['preload-ico' => ['Zadejte IČ']]);
}
$identityForm->setFormHelper($this->Form);
$submitButton = $this->Form->submit(__('Uložit informace'), ['class' => 'btn btn-success m-2']);
echo $submitButton;
?>
<div class="card m-2" id="ares-loader">
    <div class="card-body">
        <?= $identityForm->control(Identity::IS_PO) ?>
        <div class="fo-hide">
            <?= $this->Form->control('preload-ico', ['type' => 'number', 'label' => __('Vyplnit z ARES podle IČO')]) ?>
            <?= $this->Html->link(__('Načíst data z ARES'), '#', ['class' => 'btn btn-success', 'id' => 'ares-load']) ?>
        </div>
    </div>
</div>

<div class="card m-2" id="fo-basic">
    <h6 class="card-header"><?= __('Fyzická osoba nepodnikající') ?></h6>
    <div class="card-body">
        <div class="row">
            <div class="col-md-2">
                <?= $identityForm->control(Identity::FO_DEGREE_BEFORE); ?>
            </div>
            <div class="col-md">
                <?= $identityForm->control(Identity::FO_FIRSTNAME); ?>
            </div>
            <div class="col-md">
                <?= $identityForm->control(Identity::FO_SURNAME); ?>
            </div>
            <div class="col-md-2">
                <?= $identityForm->control(Identity::FO_DEGREE_AFTER); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md">
                <?= $identityForm->control(Identity::FO_DATE_OF_BIRTH); ?>
            </div>
            <div class="col-md">
                <?= $identityForm->control(Identity::FO_VAT_ID); ?>
            </div>
        </div>
    </div>
</div>

<div class="card m-2" id="po-basic">
    <h6 class="card-header"><?= __('Právnická osoba / Fyzická osoba podnikající s IČO') ?></h6>
    <div class="card-body">
        <div class="row">
            <div class="col-md">
                <?= $identityForm->control(Identity::PO_FULLNAME) ?>
            </div>
            <div class="col-md">
                <?= $identityForm->control(Identity::PO_CORPORATE_TYPE) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md">
                <?= $identityForm->control(Identity::PO_BUSINESS_ID) ?>
            </div>
            <div class="col-md">
                <?= $identityForm->control(Identity::PO_VAT_ID) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md">
                <?= $identityForm->control(Identity::PO_REGISTRATION_SINCE) ?>
            </div>
            <div class="col-md">
                <?= $identityForm->control(Identity::PO_REGISTRATION_DETAILS) ?>
            </div>
        </div>
    </div>
</div>

<div class="card m-2" id="po-statutory">
    <h6 class="card-header">
        <?= __('Statutární orgán zastupující právnickou osobu žadatele') ?>
    </h6>
    <div class="card-body">
        <div class="row">
            <div class="col-md">
                <?= $identityForm->control(Identity::STATUTORY_FULLNAME) ?>
            </div>
            <div class="col-md">
                <?= $identityForm->control(Identity::STATUTORY_REPRESENTATION_REASON) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md">
                <?= $identityForm->control(Identity::STATUTORY_EMAIL) ?>
            </div>
            <div class="col-md">
                <?= $identityForm->control(Identity::STATUTORY_PHONE) ?>
            </div>
        </div>
    </div>
</div>

<div class="card m-2" id="po-interests">
    <h6 class="card-header"><?= __('Osoby s podílem v této PO') ?></h6>
    <div class="card-body">
        <div class="alert alert-info">
            <?= __('Tento formulář po vás vyžadujeme, kvůli naplnění zákonné povinnosti evidence vlastnické struktury a skutečného majitele u příjemců dotací.') ?>
            <?= __('Tato povinnost je evidována v')
            . ' '
            . $this->Html->link(__('Zákonu č. 253/2008 Sb. proti praní peněz'), 'https://www.zakonyprolidi.cz/cs/2008-253#p29b')
            . ' a '
            . $this->Html->link(__('Zákonu č. 250/2000 Sb. o rozpočtových pravidlech územních rozpočtů'), 'https://www.zakonyprolidi.cz/cs/2015-24#cl1')
            ?>
            <hr/>
            <strong>
                <?=
                __('Pro právnické osoby, které nemají vlastníky, ale zakladatele, uveďte prosím zakladatele.')
                . ' '
                . __('Pro ostatní (například sdružení osob bez vlastníka), zde prosím uveďte osoby, které mají jednotlivě nebo (pokud jednají ve shodě) společně podíl vyšší než 25% na základním kapitálu, hlasovacích právech nebo zisku osoby žadatele.')
                ?>
            </strong>
            <hr/>
            <?= __('Více informací') ?>:
            <ul>
                <li><?= $this->Html->link('Certifix: Dotační úřady - poskytovatelé dotací z českých rozpočtů', 'https://www.certifix.eu/proc-je-certifikace-uzitecna-pro/#poskytovatele-dotaci-cr') ?></li>
                <li><?= $this->Html->link('Frank Bold: Kdo je skutečným majitelem?', 'https://www.fbadvokati.cz/cs/clanky/2209-evidence-skutecnych-majitelu-koho-se-tyka-jak-se-registrovat-a-dalsi-otazky#:~:text=Kdo%20je%20skute%C4%8Dn%C3%BDm%20majitelem') ?></li>
            </ul>
        </div>
        <small><?= __('Nepovinné pokud jste fyzická podnikající osoba.') ?>
            <br/><?= __('V každém podílu musí být vyplněno alespoň Velikost, Název vlastníka a státní příslušnost') ?>
        </small>
        <table class="table table-bordered with-inputs" id="po-interests-table">
            <colgroup>
                <col class="w-10">
                <col class="w-25">
                <col class="w-20">
                <col class="w-10">
                <col class="w-25">
            </colgroup>
            <thead>
            <tr>
                <th><?= __('Velikost podílu') ?></th>
                <th><?= __('Název vlastníka <br/>(jméno a příjmení / název obchodní korporace)') ?></th>
                <th><?= __('IČO <br/>(právnická osoba)') ?></th>
                <th><?= __('Datum narození <br/>(fyzická osoba)') ?></th>
                <th><?= __('Stát <br/>(sídlo / trvalé bydliště)') ?></th>
            </tr>
            </thead>
            <?php for ($i = 0; $i < IdentityForm::MAX_NUMBER_OF_FILLED_INTERESTS; $i++): ?>
                <tr class="tr-inputs <?= $identityForm->getNumberOfFilledInterests() > $i ? 'visible' : 'd-none' ?>"
                    data-po-interest="<?= $i ?>">
                    <?php foreach (Identity::FULL_INTEREST_TEMPLATE as $interestColumn): ?>
                        <td>
                            <?= $identityForm->control($interestColumn, ['row' => $i]) ?>
                        </td>
                    <?php endforeach; ?>
                </tr>
            <?php endfor ?>
            <tr>
                <td colspan="2">
                    <div class="row no-gutters">
                        <div class="col">
                            <a id="po-interest-add" class="btn btn-success w-100"
                               style="margin: 0; border: 0; color: white;">+</a>
                        </div>
                        <div class="col">
                            <a id="po-interest-remove" class="btn btn-danger w-100"
                               style="margin: 0; border: 0; color: white;">-</a>
                        </div>
                    </div>
                </td>
                <td colspan="3"></td>
            </tr>
        </table>

        <script type="text/javascript">
            let currentOwnedCount = <?= $identityForm->getNumberOfFilledInterests() ?>;
            $(function () {
                $("#po-interest-remove").click(function () {
                    let $target = $("#po-interests-table tr.visible").last();
                    $target.toggleClass('visible').toggleClass('d-none');
                    $("input, select", $target).val("");
                });
                $("#po-interest-add").click(function () {
                    let $target = $("#po-interests-table tr.d-none").first();
                    $target.toggleClass('visible').toggleClass('d-none');
                    $(".select2-container", $target).css('width', '100%');
                });
            });
        </script>

        <style type="text/css">
            table.with-inputs a {
                border-radius: 0;
                border: 0;
            }

            table.with-inputs td input {
                width: 100%;
                height: 100%;
                border: 0;
            }

            table.with-inputs label {
                display: none;
            }

            table.with-inputs .form-group {
                margin: 0;
            }

            table.with-inputs td {
                padding: .25rem;
            }
        </style>
    </div>
</div>

<div class="card m-2" id="po-owned-interests">
    <h6 class="card-header"><?= __('Právnické osoby v nichž má žadatel přímý obchodní podíl') ?></h6>
    <div class="card-body">
        <div class="alert alert-info">
            <?= __('Pokud žadatel vlastní obchodní podíl v jiné právnické osobě, je povinen podle zákona tuto skutečnost nezamlčet, při žádosti o dotaci') ?>
        </div>
        <table class="table table-bordered with-inputs" id="po-owned-interests-table">
            <colgroup>
                <col class="w-25">
                <col class="w-25">
                <col class="w-50">
            </colgroup>
            <thead>
            <tr>
                <th><?= __('Velikost podílu') ?></th>
                <th><?= __('IČO vlastněné společnosti') ?></th>
                <th><?= __('Název vlastněné společnosti') ?></th>
                <th><?= __('Stát <br/>(sídlo společnosti)') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php for ($i = 0; $i < IdentityForm::MAX_NUMBER_OF_OWNED_INTERESTS; $i++): ?>
                <tr class="tr-inputs <?= $identityForm->getNumberOfFilledInterests() > $i ? 'visible' : 'd-none' ?>"
                    data-po-owned-interest="<?= $i ?>">
                    <?php foreach (Identity::FULL_OWNED_INTERESTS_TEMPLATE as $interestColumn): ?>
                        <td>
                            <?= $identityForm->control($interestColumn, ['row' => $i]) ?>
                        </td>
                    <?php endforeach; ?>
                </tr>
            <?php endfor ?>
            <tr>
                <td>
                    <div class="row no-gutters">
                        <div class="col">
                            <a id="po-owned-interest-add" class="btn btn-success w-100"
                               style="margin: 0; border: 0; color: white;">+</a>
                        </div>
                        <div class="col">
                            <a id="po-owned-interest-remove" class="btn btn-danger w-100"
                               style="margin: 0; border: 0; color: white;">-</a>
                        </div>
                    </div>
                </td>
                <td colspan="3"></td>
            </tr>
            </tbody>
        </table>

        <script type="text/javascript">
            let currentCount = <?= $identityForm->getNumberOfFilledOwnedInterests() ?>;
            $(function () {
                $("#po-owned-interest-remove").click(function () {
                    let $target = $("#po-owned-interests-table tr.visible").last();
                    $target.toggleClass('visible').toggleClass('d-none');
                    $("input", $target).val("");
                });
                $("#po-owned-interest-add").click(function () {
                    let $target = $("#po-owned-interests-table tr.d-none").first();
                    $target.toggleClass('visible').toggleClass('d-none');
                });
            });
        </script>
    </div>
</div>

<div class="card m-2" id="address-residence">
    <h6 class="card-header"><?= __('Trvalá Adresa / Sídlo Právnické Osoby') ?></h6>
    <div class="card-body">
        <div class="row">
            <div class="col-md">
                <?= $identityForm->control(Identity::RESIDENCE_ADDRESS_STREET); ?>
            </div>
            <div class="col-md">
                <?= $identityForm->control(Identity::RESIDENCE_ADDRESS_CITY); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md">
                <?= $identityForm->control(Identity::RESIDENCE_ADDRESS_ZIP); ?>
            </div>
            <div class="col-md">
                <?= $identityForm->control(Identity::RESIDENCE_ADDRESS_CITY_PART); ?>
            </div>
        </div>
        <?= $this->Form->control('has-postal', ['type' => 'checkbox', 'label' => __('Zadat jinou doručovací adresu')]) ?>
    </div>
</div>

<div class="card m-2" id="address-postal">
    <h6 class="card-header"><?= __('Doručovací Adresa') ?></h6>
    <div class="card-body">
        <div class="row">
            <div class="col-md">
                <?= $identityForm->control(Identity::POSTAL_ADDRESS_STREET); ?>
            </div>
            <div class="col-md">
                <?= $identityForm->control(Identity::POSTAL_ADDRESS_CITY); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md">
                <?= $identityForm->control(Identity::POSTAL_ADDRESS_ZIP); ?>
            </div>
            <div class="col-md">
                <?= $identityForm->control(Identity::POSTAL_ADDRESS_CITY_PART); ?>
            </div>
        </div>
    </div>
</div>

<div class="card m-2" id="contact">
    <h6 class="card-header"><?= __('Kontaktní informace') ?></h6>
    <div class="card-body">
        <div class="row">
            <div class="col-md">
                <?= $identityForm->control(Identity::CONTACT_PHONE) ?>
            </div>
            <div class="col-md">
                <?= $identityForm->control(Identity::CONTACT_EMAIL, ['default' => $this->getUser('email')]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md">
                <?= $identityForm->control(Identity::CONTACT_DATABOX) ?>
            </div>
            <div class="col-md pt-4">
                <?= $this->Html->link(__('Získat ověřené informace přihlášením do datové schránky'), ['_name' => 'isds_auth'], ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
        <div class="row">
            <?php
            foreach (Identity::ALL_VERIFIED_INFO as $verifiedInfoFieldId) {
                $verifiedValue = $identityForm->getData($verifiedInfoFieldId);
                if (!empty($verifiedValue) && $verifiedInfoFieldId === Identity::ISDS_VERIFIED_DATABOX_TYPE) {
                    $verifiedValue = IsdsComponent::DB_TYPES[intval($verifiedValue)]['long'];
                }
                $label = Identity::getFieldLabel($verifiedInfoFieldId);
                if (!empty($verifiedValue)): ?>
                    <div class="col-md-6">
                        <?= $this->Form->control($verifiedInfoFieldId, ['disabled' => 'disabled', 'value' => $verifiedValue, 'label' => $label, 'type' => 'text']) ?>
                    </div>
                <?php
                endif;
            }
            ?>
        </div>
    </div>
</div>

<div class="card m-2" id="bank">
    <h6 class="card-header"><?= __('Bankovní spojení') ?></h6>
    <div class="card-body">
        <?= $identityForm->control(Identity::BANK_NAME) ?>
        <div class="form-row">
            <div class="col-md-8 col-7 m-0 p-0">
                <?= $identityForm->control(Identity::BANK_NUMBER) ?>
            </div>
            <div class="col-md-4 col-5 m-0 p-0">
                <?= $identityForm->control(Identity::BANK_CODE, ['type' => 'bank_code', 'prepend' => '<div class="input-group-text">/</div>']) ?>
            </div>
        </div>
    </div>
</div>

<div class="card m-2" id="note">
    <h6 class="card-header"><?= __('Upřesnění informací o žadateli') ?></h6>
    <div class="card-text p-2">
        <div class="col">
            <?= $identityForm->control(Identity::NOTE, ['data-noquilljs' => 'data-noquilljs']) ?>
        </div>
    </div>
</div>

<?php
echo $submitButton;
echo $this->Form->end();
?>
<script type="text/javascript">
    let $lastResponse = null;
    let $orResponse = null;
    let $feedbackDiv = null;
    let $cityParts = [];
    const $currentValues = <?= json_encode($identityForm->getSelectsCurrentValues()) ?>;
    const $citiesToParts = <?= json_encode($identityForm->getCitiesToPartsMap()) ?>;
    const $aresURL = "<?= mb_substr(Router::url(['_name' => 'ares', 'ico' => '0']), 0, -1) ?>";
    const $ORaresURL = "<?= mb_substr(Router::url(['_name' => 'ares_or', 'ico' => '0']), 0, -1) ?>";
    const poCards = [
        'note',
        'bank',
        'address-postal',
        'address-residence',
        'contact',
        'po-basic',
        'ares-loader',
        'po-interests',
        'po-owned-interests'
    ];
    const foCards = [
        'note',
        'bank',
        'address-postal',
        'address-residence',
        'contact',
        'fo-basic',
        'ares-loader',
        'po-owned-interests'
    ];

    function preloadOR() {
        if (!$orResponse) {
            return;
        }


    }

    function preloadData() {
        if (!$lastResponse) {
            $feedbackDiv?.removeClass('valid-feedback').addClass('invalid-feedback').text('<?= __('Načtení se nezdařilo') ?>').toggle(true);
            return;
        }
        let $kodPF = $($lastResponse.getElementsByTagName('D:KPF')).first().text();
        let $psc = $($lastResponse.getElementsByTagName('D:PSC')).first().text();
        let $fullname = $($lastResponse.getElementsByTagName('D:OF')).first().text();
        let $ico = $($lastResponse.getElementsByTagName('D:ICO')).first().text();
        let $ulice = $($lastResponse.getElementsByTagName('D:NU')).first().text();
        let $cisloDomovni = $($lastResponse.getElementsByTagName('D:CD')).first().text();
        let $cisloPopisne = $($lastResponse.getElementsByTagName('D:TCD')).first().text();

        let $kodObce = $($lastResponse.getElementsByTagName('U:KO')).first().text();
        let $kodCasti = $($lastResponse.getElementsByTagName('U:KCO')).first().text();

        let $vznik = $($lastResponse.getElementsByTagName('D:DV')).first().text();
        let $vlozka = $($lastResponse.getElementsByTagName('D:OV')).first().text();
        let $soud = $($lastResponse.getElementsByTagName('D:T')).first().text();
        let $urad = $($lastResponse.getElementsByTagName('D:NZU')).first().text();

        $("#po-corporate-type").val($kodPF).trigger('change').addClass('is-valid');
        $("#po-fullname").val($fullname).addClass('is-valid');
        $("#address-residence-zip").val($psc).addClass('is-valid');
        $("#address-residence-street").val($ulice + " " + $cisloDomovni + ($cisloPopisne ? "/" + $cisloPopisne : "")).addClass('is-valid');
        $("#po-business-id").val($ico).addClass('is-valid');
        $("#address-residence-city").val($kodObce).trigger('change').addClass('is-valid');
        $("#address-residence-city-part").val($kodCasti).trigger('change').addClass('is-valid');
        $("#po-registration-since").val($vznik).trigger('change').addClass('is-valid');
        $("#po-registration-details").val($vlozka ? ($vlozka + ", " + $soud) : $urad).trigger('change').addClass('is-valid');

        //Priznaky_subjektu
        let $priznaky = $($lastResponse.getElementsByTagName('D:PSU')).first().text();
        if ($priznaky[1] === 'A') {
            $orResponse = null;
            $.ajax({
                url: $ORaresURL + $ico
            }).done(function (data) {
                $orResponse = data;
                preloadOR();
            });
        }
    }

    function loadAresData() {
        let $input = $("#preload-ico");
        let ico = $input.val();
        ico = ico.replace(/\D/g, '');
        if (!ico) {
            $input.addClass('is-invalid').attr('placeholder', '<?= __('Musíte vyplnit IČO') ?>');
            return;
        }
        $input.removeClass('is-invalid');
        $("#aresLoadingModal").modal('show');
        $.ajax({
            url: $aresURL + ico
        }).done(function (data) {
            $lastResponse = data;
            preloadData();
            $("#aresLoadingModal").modal('hide');
            if ($feedbackDiv) {
                $feedbackDiv.removeClass('invalid-feedback').addClass('valid-feedback').text('<?= __('Načtení se zdařilo') ?>').toggle(true);
            }
        }).fail(function () {
            $lastResponse = null;
            $("#aresLoadingModal").modal('hide');
            if ($feedbackDiv) {
                $feedbackDiv.removeClass('valid-feedback').addClass('invalid-feedback').text('<?= __('Načtení se nezdařilo') ?>').toggle(true);
            }
        });
    }

    function sideloadCityParts($elm, $data = null) {
        try {
            if ($elm.data('select2')) {
                $elm.select2('destroy');
            }
        } catch (ignore) {
        }
        try {
            $elm.empty();
        } catch (ignore) {
        }

        let $defaultValue = null;
        if ($elm.prop('id') && $elm.prop('id') in $currentValues) {
            let $rawValue = $currentValues[$elm.prop('id')];
            let $value = parseInt($rawValue);
            if ($value > 0) {
                $defaultValue = $rawValue;
            }
        }

        $data = $data ? $data : $.map($cityParts, function (name, key) {
            if (key === $defaultValue) {
                return {'id': key, 'text': name, 'selected': true};
            }
            return {'id': key, 'text': name};
        });
        let $placeholder = [{
            id: '',
            text: '<?= __('Prosím vyberte si z možností'); ?>'
        }];
        $data = $placeholder.concat($data);

        $elm.select2({
            data: $data,
            theme: 'classic',
            allowClear: true,
            placeholder: '<?= __('Prosím vyberte si z možností'); ?>',
            minimumInputLength: 3
        });
    }

    $(document).ready(function () {
        let $preloadIco = $("#preload-ico");
        $preloadIco.removeClass('is-invalid');
        $feedbackDiv = $preloadIco.closest('.form-group').find('.invalid-feedback');
        $feedbackDiv.toggle(false);
        $("#is-po").change(function () {
            let isPO = $(this).is(':checked')
            let shownCards = isPO ? poCards : foCards;
            $(".fo-hide").toggle(isPO);
            $("form .card").not("#address-postal").each(function () {
                $(this).toggle($.inArray($(this).attr('id'), shownCards) > -1);
            });
        }).trigger('change');
        $("#ares-load").click(function () {
            loadAresData();
        });
        $("#po-corporate-type").change(function () {
            let showStatutory = false;
            if ($("#is-po").is(':checked')) {
                let thisVal = parseInt($(this).val());
                if (thisVal < 100 || thisVal > 110) {
                    showStatutory = true;
                }
            }
            $("#po-statutory").toggle(showStatutory);
            $("#po-interests").toggle(showStatutory);
        }).change();
        $("#has-postal").change(function () {
            $("#address-postal").toggle($(this).is(':checked'));
        }).trigger('change');
        $preloadIco.keypress(function (e) {
            if (e.which === 13) {
                e.preventDefault();
                loadAresData();
            }
        });
        $(".city-parts-sideload").each(function () {
            const $thisElement = $(this);
            $.get('<?= Router::url(['_name' => 'csu_municipality_parts_list_json'])?>', function (data) {
                $cityParts = data;
                sideloadCityParts($thisElement);
            });
        });
        $("#address-residence-city, #address-postal-city").change(function () {
            const $forCity = parseInt($(this).val());

            const $links = $citiesToParts.filter(function (link) {
                return link['csu_municipalities_number'] === $forCity;
            }).filter(function () {
                return true;
            });

            let $matchingParts = [];
            $.each($links, function ($index, $link) {
                const $key = $link['csu_municipality_parts_number'];
                if ($key in $cityParts) {
                    $matchingParts[$key] = $cityParts[$key];
                }
            });

            const $target = $(this).attr('id') === 'address-residence-city' ? $("#address-residence-city-part") : $("#address-postal-city-part");
            sideloadCityParts($target, $.map($matchingParts, function (name, key) {
                if (name !== undefined) {
                    return {'id': key, 'text': name};
                }
            }));
        });
    })
</script>

<div id="aresLoadingModal" class="modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?= __('Vyčkejte prosím') ?></h5>
            </div>
            <div class="modal-body">
                <p><?= __('Načtení dat z ARES typicky trvá jen několik vteřin') ?></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= __('Zrušit') ?></button>
            </div>
        </div>
    </div>
</div>
