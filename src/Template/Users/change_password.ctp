<?php
/**
 * @var $this AppView
 * @var $user User
 */

use App\Model\Entity\User;
use App\View\AppView;

$this->assign('title', __('Změna hesla'));
?>
    <h1><?= $this->fetch('title') ?></h1>
<?php
echo $this->Form->create($user);
echo $this->Form->control('old_password', ['label' => __('Aktuální heslo'), 'type' => 'password']);
echo $this->Form->control('new_password', ['label' => __('Nové heslo'), 'type' => 'password']);
echo $this->Form->submit(__('Uložit'), ['class' => 'btn btn-success']);
echo $this->Form->end();