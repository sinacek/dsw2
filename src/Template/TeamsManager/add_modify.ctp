<?php

use App\Model\Entity\Program;
use App\Model\Entity\Team;
use App\Model\Entity\User;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $managerList User[]
 * @var $team Team
 * @var $allowed_programs Program[]
 */
$this->assign('title', $team->isNew() ? __('Vytvořit novou org. jednotku') : __('Organizační jednotka') . ': ' . $team->name);
echo $this->element('simple_datatable');
echo $this->element('simple_select2');
echo $this->element('simple_treeselect');
?>
    <div class="card mt-2">
        <div class="card-header">
            <h1><?= $this->fetch('title') ?></h1>
        </div>
        <div class="card-body">
            <?php
            echo $this->Form->create($team);
            echo $this->Form->control('name', ['label' => __('Název')]);
            echo $this->Form->control('users._ids', ['label' => __('Členové'), 'options' => $managerList]);
            ?>
        </div>
    </div>

    <nav class="navbar navbar-dark bg-dark sticky-top rounded-bottom">
        <?= $this->Form->button(__('Uložit'), ['class' => 'btn btn-success ml-auto', 'type' => 'submit']); ?>
    </nav>

    <div class="card mt-2">
        <div class="card-header">
            <h4 class="font-weight-bold">
                <?= __('Role') ?>: <?= __('Formální kontrola') ?>
            </h4>

            <div class="card-subtitle">
                Pravomoci formální kontroly
                <ul>
                    <li>Nastavit neodeslanou žádost manuálně do stavu "odesláno" a doplnit číslo jednací (spisovou
                        značku)
                    </li>
                    <li>Kontrolovat odeslané (datovou schránkou / spisovou službou) žádosti a tyto vracet k opravě s
                        komentářem, nebo postoupit k dalšímu procesu
                    </li>
                </ul>
            </div>
        </div>
        <div class="card-body">
            <?php
            echo $this->Form->control('formal_check_programs._ids', ['options' => $allowed_programs, 'label' => __('Provádí Formální Kontrolu v těchto Programech'), 'class' => 'treeselect']);
            ?>
        </div>
    </div>

    <div class="card mt-2">
        <div class="card-header">
            <h4 class="font-weight-bold">
                <?= __('Role') ?>: <?= __('Hodnotitelé') ?>
            </h4>

            <div class="card-subtitle">
                Pravomoci hodnotitelů
                <ul>
                    <li>Pokud jsou nastavena hodnotící kritéria, hodnotit těmito jednotlivé žádosti</li>
                    <li>Sami pro sebe si evidovat nepovinnou slovní poznámku k dané žádosti (např. jako příprava na
                        jednání
                        komise)
                    </li>
                </ul>
            </div>
        </div>
        <div class="card-body">
            <?php
            echo $this->Form->control('comments_programs._ids', ['options' => $allowed_programs, 'label' => __('Provádí hodnocení v těchto Programech'), 'class' => 'treeselect']);
            ?>
        </div>
    </div>

    <div class="card mt-2">
        <div class="card-header">
            <h4 class="font-weight-bold">
                <?= __('Role') ?>: <?= __('Navrovatelé') ?>
            </h4>

            <div class="card-subtitle">
                Pravomoci navrhovatelů
                <ul>
                    <li>Navrhnout rozdílnou výši poskytnuté podpory od požadované</li>
                    <li>Navrhnout slovní komentář k dané žádosti, pro následné vyrozumění a jednání dalších orgánů
                        (např.
                        rady / zastupitelstva)
                    </li>
                    <li>Navrhnout upravenou formulaci účelu, na který je podpora poskytována</li>
                    <li>Navrhnout výsledné bodové hodnocení, pokud jsou pro daný program stanovena hodnotící kritéria
                    </li>
                </ul>
            </div>
        </div>
        <div class="card-body">
            <?php
            echo $this->Form->control('price_proposal_programs._ids', ['options' => $allowed_programs, 'label' => __('Navrhují výši dotace v těchto Programech'), 'class' => 'treeselect']);
            ?>
        </div>
    </div>

    <div class="card mt-2">
        <div class="card-header">
            <h4 class="font-weight-bold">
                <?= __('Role') ?>: <?= __('Schvalovatelé') ?>
            </h4>

            <div class="card-subtitle">
                Pravomoci schvalovatelů
                <ul>
                    <li>Potvrdit (případně ještě upravit před uložením) navrženou výši podpory, slovní komentář a
                        formulaci
                        účelu poskytnutí podpory
                    </li>
                    <li>Pokud je potvrzená částka podpory 0 Kč pak se žádost automaticky ukončuje</li>
                </ul>
            </div>
        </div>
        <div class="card-body">
            <?php
            echo $this->Form->control('price_approval_programs._ids', ['options' => $allowed_programs, 'label' => __('Schvalují vyši dotace v těchto Programech'), 'class' => 'treeselect']);
            ?>
        </div>
    </div>

    <div class="card mt-2">
        <div class="card-header">
            <h4 class="font-weight-bold">
                <?= __('Role') ?>: <?= __('Finalizace') ?>
            </h4>

            <div class="card-subtitle">
                Pravomoci týmu finalizace
                <ul>
                    <li>Po jednání zodpovědných orgánů nastavit stav schválených žádosti (zda byly finálně poskytnuty
                        nebo
                        ne)
                    </li>
                    <li>Pokud proběhla změna na jednání zastupitelstva / rady, pak může ještě upravit schválené
                        parametry
                        (částku podpory, slovní hodnocení a účel podpory)
                    </li>
                    <li>
                        Odesílat a formulovat vyrozumění o poskytnutí nebo neposkytnutí podpory (platí jen pro spisovou
                        službu)
                    </li>
                    <li>Po podpisu smlouvy pak tým nastavuje zda byla úspěšně podepsána smlouva a dotace je určena k
                        vyplacení, nebo zda podpis neproběhl a proces se ukončuje
                    </li>
                </ul>
            </div>
        </div>
        <div class="card-body">
            <?php
            echo $this->Form->control('request_manager_programs._ids', ['options' => $allowed_programs, 'label' => __('Nastavují výsledný status žádostí v těchto Programech'), 'class' => 'treeselect']);
            ?>
        </div>
    </div>

<?php
echo $this->Form->end();
?>
