<?php

use App\Model\Entity\Team;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $teams Team[]
 */
$this->assign('title', __('Struktura Dotačního Úřadu'));
echo $this->element('simple_datatable');
?>
<h1><?= $this->fetch('title') ?></h1>
<h6><?= __('Zde vytvořené organizační jednotky nejsou veřejné, slouží jen pro organizaci práce v rámci dotačního sofware') ?></h6>
<?= $this->Html->link(__('Přidat novou organizační jednotku'), ['action' => 'addModify'], ['class' => 'btn btn-success mb-2']) ?>

<table class="table" id="dtable">
    <thead>
    <tr>
        <th><?= __('Název') ?></th>
        <th><?= __('Členové') ?></th>
        <th><?= __('Akce') ?></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($teams as $team): ?>
        <tr>
            <td><?= $this->Html->link($team->name, ['action' => 'addModify', $team->id]) ?></td>
            <td>
                <ul>
                    <?php foreach ($team->users as $user): ?>
                        <li><?= $user->email ?></li>
                    <?php endforeach; ?>
                </ul>
            </td>
            <td>
                <?= $this->Html->link(__('Otevřít / Upravit'), ['action' => 'addModify', $team->id]) ?>,
                <?= $this->Html->link(__('Vytvořit kopii'), ['action' => 'teamCopy', $team->id], ['class' => 'text-success']) ?>
                ,
                <?= $this->Html->link(__('Smazat'), ['action' => 'teamDelete', $team->id], ['class' => 'text-danger']) ?>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
