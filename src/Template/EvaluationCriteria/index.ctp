<?php

use App\Model\Entity\EvaluationCriterium;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $criteria EvaluationCriterium[]
 */

echo $this->element('simple_datatable');
$this->assign('title', __('Hodnotící kritéria'));
?>
<h1><?= $this->fetch('title') ?></h1>
<h6><?= __('Jsou zobrazeny pouze skupiny, podřízená kritéria spravujte otevřením skupiny') ?></h6>
<?= $this->Html->link(__('Přidat Hodnotící kritérium'), ['action' => 'criteriumAddModify'], ['class' => 'btn btn-success m-2']) ?>
<table class="table" id="dtable">
    <thead>
    <tr>
        <th><?= __('Název') ?></th>
        <th><?= __('Maximální počet bodů') ?></th>
        <th><?= __('Akce') ?></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($criteria as $criterium): ?>
        <tr>
            <td><?= $this->Html->link($criterium->name, ['action' => 'criteriumAddModify', $criterium->id]) ?></td>
            <td><?= $criterium->max_points ?></td>
            <td>
                <?= $this->Html->link(__('Otevřít'), ['action' => 'criteriumAddModify', $criterium->id]) ?>,
                <?= $this->Html->link(__('Vytvořit kopii'), ['action' => 'criteriumCopy', $criterium->id], ['class' => 'text-success']) ?>
                ,
                <?= $this->Form->postLink(__('Smazat skupinu'), ['action' => 'criteriumDelete', $criterium->id], ['confirm' => __('Opravdu chcete smazat celou skupinu kritérií'), 'class' => 'text-danger']) ?>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
