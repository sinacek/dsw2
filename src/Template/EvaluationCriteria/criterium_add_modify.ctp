<?php

use App\Model\Entity\EvaluationCriterium;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $criterium EvaluationCriterium
 * @var $criteria EvaluationCriterium[]
 */

$this->assign('title', $criterium->id > 0 ? __('Upravit Hodnotící kritérium') : __('Přidat Hodnotící kritérium'));
?>
    <h1><?= $this->fetch('title') ?></h1>
<?php
echo $this->Form->create($criterium);
echo $this->Form->control('name', ['label' => __('Název')]);
echo $this->Form->control('description', ['label' => __('Popis')]);
echo $this->Form->control('parent_id', ['options' => $criteria, 'label' => sprintf("%s (%s)", __('Nadřazené kriterium'), __('Ponechte prázdné, pokud vytváříte novou skupinu kritérií')), 'empty' => true, 'required' => false]);
echo $this->Form->control('max_points', ['label' => __('Maximální počet bodů'), 'default' => 0]);
echo $this->Form->submit(__('Uložit'), ['class' => 'btn btn-success']);
echo $this->Form->end();
if ($criterium->parent_id) {
    echo $this->Html->link(__('Zahodit změny a vrátit se ke správě skupiny'), ['action' => 'criteriumAddModify', $criterium->parent_id], ['class' => 'btn btn-warning mt-2']);
} else {
    echo $this->Html->link(__('Zahodit změny a vrátit se zpět na seznam hodnotících kritérií'), ['action' => 'index'], ['class' => 'btn btn-warning mt-2']);
}
?>

<?php if ($criterium->id > 0 && $criterium->parent_id === null): ?>
    <hr/>
    <h2><?= __('Podřízená kritéria') ?></h2>
    <?= $this->Html->link(__('Přidat podřízené kritérium'), ['action' => 'criteriumAddModify', 'parent_id' => $criterium->id], ['class' => 'btn btn-success mb-2']) ?>
    <table class="table">
        <thead>
        <tr>
            <th><?= __('Název') ?></th>
            <th><?= __('Max. počet bodů') ?></th>
            <th><?= __('Akce') ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($criterium->child_evaluation_criteria as $child): ?>
            <tr>
                <td><?= $this->Html->link($child->name, ['action' => 'criteriumAddModify', $child->id]) ?></td>
                <td><?= $child->max_points ?></td>
                <td>
                    <?= $this->Html->link(__('Otevřít'), ['action' => 'criteriumAddModify', $child->id]) ?>,
                    <?= $this->Html->link(__('Vytvořit kopii'), ['action' => 'criteriumCopy', $child->id], ['class' => 'text-success']) ?>
                    ,
                    <?= $this->Html->link(__('Smazat'), ['action' => 'criteriumDelete', $child->id], ['class' => 'text-danger']) ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
<?php endif; ?>
