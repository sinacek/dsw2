<?php

use App\Model\Entity\I18nMessage;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $message I18nMessage
 */

$this->assign('title', $message->id > 0 ? __('Úprava překladu') : __('Nový překlad'))
?>
    <h1><?= $this->fetch('title') ?></h1>

<?php
$noquilljs = mb_strlen(strip_tags($message->singular)) !== mb_strlen($message->singular) ? false : 'data-noquilljs';

echo $this->Form->create($message);
echo $this->Form->control('singular', ['disabled' => 'disabled', 'label' => __('Originál'), 'data-noquilljs' => $noquilljs]);
echo $this->Form->control('value_0', ['label' => 'Upravený překlad', 'data-noquilljs' => $noquilljs]);
echo $this->Form->submit(__('Uložit'), ['class' => 'btn btn-success']);
echo $this->Form->end();
?>
