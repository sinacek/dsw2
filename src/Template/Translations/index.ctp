<?php

use App\Model\Entity\I18nMessage;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $messages I18nMessage[]
 */

echo $this->element('simple_datatable');
?>

<table class="table" id="dtable">
    <thead>
    <tr>
        <th><?= __('Akce') ?></th>
        <th><?= __('Originální text') ?></th>
        <th><?= __('Upravený text') ?></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($messages as $message): ?>
        <tr>
            <td><?= $this->Html->link(__('Upravit'), ['action' => 'addModify', $message->id]) ?></td>
            <td>                <?= h($message->singular) ?>            </td>
            <td>                <?= h($message->value_0) ?? '' ?>            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
