<?php
/**
 * @var $this AppView
 * @var $fond Fond
 * @var $realm Realm
 * @var $program Program
 * @var $appeal Appeal
 * @var $form Form
 * @var $request Request
 * @var $organization Organization
 */

use App\Model\Entity\Appeal;
use App\Model\Entity\Fond;
use App\Model\Entity\Form;
use App\Model\Entity\Organization;
use App\Model\Entity\OrganizationSetting;
use App\Model\Entity\Program;
use App\Model\Entity\Realm;
use App\Model\Entity\Request;
use App\View\AppView;

$this->assign('title', 'Příručka správce dotačního portálu');
?>

<div class="card m-2">
    <h2 class="card-header">První kroky v novém dotačním portálu</h2>
    <div class="card-body">
        <p class="alert alert-info">
            Kroky zde uvedené je třeba provést v daném pořadí, jinak není možné některé kroky provést. <br/>
            Například lze vytvořit formulář bez dotačního programu, ale ne program bez oblasti podpory nebo dotačního
            fondu. <br/>
            Stejně tak nelze vytvořit žádost o podporu, bez vytvořeného programu, ale formuláře v programu vyžadované
            můžete upravit kdykoli
        </p>
        <ol>
            <li>
                <i class="fas <?= empty($this->getSiteSetting(OrganizationSetting::SITE_NAME)) ? "fa-times text-danger" : "fa-check text-success" ?>"></i>
                Nastavit základní informace o organizaci v
                <?= $this->Html->link('Administrátor > Správa organizací', ['controller' => 'Organizations', 'action' => 'edit'], ['target' => '_blank']) ?>
                <br/>Především pak
                <ul>
                    <li>v sekci "Základní nastavení organizace", kde lze vypnout registraci / přihlašování uživatel</li>
                    <li>v sekci "Nastavení dotačního procesu", která určuje základní předpoklady o průběhu procesu
                        podání a vyřízení žádostí
                    </li>
                    <li>v sekci "Technické údaje" a "Nastavení e-mailů" lze portál spojit s Datovou Schránkou, vlastním
                        e-mailovým serverem a přidat logo dotačního portálu
                    </li>
                </ul>
            </li>
            <li>
                <i class="fas <?= empty($fond) ? "fa-times text-danger" : "fa-check text-success" ?>"></i>
                <?= $this->Html->link('Vytvořit dotační fond',
                    ['controller' => 'GrantsManager'] + ($fond ? ['action' => 'indexFonds'] : ['action' => 'fondAddModify']),
                    ['target' => '_blank']) ?>
            </li>
            <li>
                <i class="fas <?= empty($realm) ? "fa-times text-danger" : "fa-check text-success" ?>"></i>
                <?= $this->Html->link('Vytvořit oblast(i) podpory',
                    ['controller' => 'GrantsManager'] + ($realm ? ['action' => 'indexRealms'] : ['action' => 'realmAddModify']),
                    ['target' => '_blank']) ?>
            </li>
            <li>
                <i class="fas <?= empty($program) ? "fa-times text-danger" : "fa-check text-success" ?>"></i>
                <?= $this->Html->link('Vytvořit alespoň 1 program, ve kterém poskytujete podporu',
                    ['controller' => 'GrantsManager'] + ($program ? ['action' => 'indexPrograms'] : ['action' => 'programAddModify']),
                    ['target' => '_blank']) ?>
            </li>
            <li>
                <i class="fas <?= empty($form) ? "fa-times text-danger" : "fa-check text-success" ?>"></i>
                <?= $this->Html->link('Vytvořit 1 formulář, který budete vyžadovat od žadatelů vyplnit, při podání žádosti',
                    ['controller' => 'Forms'] + ($form ? ['action' => 'index'] : ['action' => 'addModify']),
                    ['target' => '_blank']) ?>
            </li>
            <li>
                <i class="fas <?= empty($appeal) ? "fa-times text-danger" : "fa-check text-success" ?>"></i>
                <?= $this->Html->link('Vytvořit výzvu k podání žádostí o dotace, abyste si mohli proces otestovat',
                    ['controller' => 'GrantsManager'] + ($appeal ? ['action' => 'indexAppeals'] : ['action' => 'appealAddModify']),
                    ['target' => '_blank']) ?>
            </li>
            <li>
                <i class="fas <?= $this->isUser() ? (empty($request) ? "fa-times text-danger" : "fa-check text-success") : "fa-question text-warning" ?>"></i>
                <?= $this->Html->link('Vytvořit žádost ve vytvořeném programu, a otestovat jak probíhá proces vyplnění',
                    ['controller' => 'UserRequests'] + ($request ? ['action' => 'index'] : ['action' => 'addModify']),
                    ['target' => '_blank']) ?>
            </li>
        </ol>
    </div>
</div>

<div class="card m-2" id="users">
    <h2 class="card-header">Práce s uživateli</h2>
    <div class="card-body">
        <div>
            Dotační portál všem registrovaným uživatelům, dovoluje vytvářet žádosti o dotace a spravovat vlastní
            uživatelský účet, tj.
            <ul>
                <li>upravovat svoji identitu</li>
                <li>spravovat historii přijaté veřejné podpory</li>
                <li>sdílet svůj účet s dalšími uživateli</li>
            </ul>
        </div>
        <div>
            Nad rámec těchto, lze uživatelům přiřadit mimořádná oprávnění, a to ve dvou kategoriích, viz. níže
            <ol>
                <li><a href="#user-roles">Role: Oprávnění ke správě části nebo celého dotačního portálu</a></li>
                <li><a href="#user-teams">Týmy: Tedy příslušnost k jedné nebo více skupinám / organizačním jednotkám
                        dotačního úřadu</a></li>
            </ol>
        </div>
        <p>
            Pro správce portálu je pak důležité, že mohou vstoupit do jiného registrovaného uživatele, jako forma
            uživatelské podpory. Toto samozřejmě může provést jen oprávněná role.
        </p>
        <p>
            Žádná role nebo tým v systému, nemůže přímo upravovat podané žádosti, to může vždy jen uživatel. <br/>
            Vyjímku tvoří Správce dotačního portálu a Správce uživatel, který může upravit žádost před jejím podáním, ne
            však už ve chvíli, kdy je žádost odeslána (elektronicky nebo papírově).
        </p>
        <p class="font-weight-bold">
            Po úpravě rolí nebo týmů daného uživatele, se musí daný uživatel odhlásit a znovu přihlásit, aby se změny
            projevily.
        </p>
    </div>
</div>

<div class="card m-2" id="user-roles">
    <h2 class="card-header">Uživatelské role</h2>
    <div class="card-body">
        <p>
            Systém obsahuje několik málo rolí, pro základní rozdělení kompetencí.
        </p>
        <p>
            Pouze Správce dotačního portálu nebo Správce uživatel může jiným uživatelům udělovat/odebírat role nebo
            upravovat jejich členství v týmech.
        </p>
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>Název role</th>
                <th>Popis kompetencí</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="text-nowrap">Správce Dotačního Portálu</td>
                <td>
                    <ul>
                        <li>Má kompletní kontrolu nad dotačním portálem</li>
                        <li>Může provádět vše co dělají ostatní role</li>
                        <li>Pokud není členem konkrétního týmu, nevidí týmové rozhraní a nemůže zasahovat do zpracování
                            podaných žádostí (oprávnění si ale může sám přidat)
                        </li>
                    </ul>
                </td>
            </tr>
            <tr>
                <td class="text-nowrap">Správce Uživatel</td>
                <td>
                    <ul>
                        <li>Může zvát do portálu další uživatele</li>
                        <li>Může uživatelům přidělovat role, kromě role správce dotačního portálu</li>
                        <li>Může se přihlásit za jiného uživatele (z detailu uživatele funkcí "Vstoupit do uživatele")
                        </li>
                        <li>Může spravovat jen uživatele, kteří se někdy přihlásili v aktuálním dotačním portálu
                            (případně uživatele, které do portálu pozval)
                        </li>
                        <li>Může spravovat týmy a uživatele do týmů zařazovat nebo je z nich odebírat</li>
                    </ul>
                </td>
            </tr>
            <tr>
                <td class="text-nowrap">Správce dotací</td>
                <td>
                    <ul>
                        <li>Může spravovat Fondy, Oblasti podpory a Programy/Pod-programy</li>
                        <li>Může spravovat Hodnotící kritéria, a jejich zařazení</li>
                        <li>Může spravovat formuláře a jejich podobu/nastavení</li>
                        <li>Může spravovat výzvy k podání žádostí o dotaci</li>
                        <li>Může vytvořit dotační žádost ve výzvě, která je nastavena jako neaktivní (při testování)
                        </li>
                    </ul>
                </td>
            </tr>
            <tr>
                <td class="text-nowrap">Správce historie žadatelů</td>
                <td>
                    <ul>
                        <li>Spravuje jen sekci Historie žadatelů, tj. seznam identit historických žadatelů a jejich
                            žádostí
                        </li>
                        <li>Sekce "historie žadatelů" je nezávislá na historii žádostí podaných v dotačním software,
                            slouží k importu dat o žadatelích, které organizace shromáždila v předchozích letech, kdy
                            dotační proces prováděla papírově nebo v jiném elektronickém systému
                        </li>
                    </ul>
                </td>
            </tr>
            <tr>
                <td class="text-nowrap">Správce Wiki / Podpory</td>
                <td>
                    <ul>
                        <li>Může jen spravovat kategorie a jednotlivé stránky uživatelské podpory, které jsou dostupné
                            pod tlačítkem "Nápověda" vpravo v horním navigačním menu
                        </li>
                    </ul>
                </td>
            </tr>
            <tr>
                <td class="text-nowrap">Ekonom</td>
                <td>
                    <ul>
                        <li>Vykonává veřejnoprávní ekonomickou kontrolu nad dotačním portálem</li>
                        <li>V <a href="/about#process" target="_blank">Dotačním Procesu</a> od fáze "Zvěřejnění smlouvy
                            a vyplacení podpory" včetně, dohlíží nad plněním podmínek veřejnoprávní smlouvy
                        </li>
                        <li>
                            Eviduje že byla vyplacena podpora (a výše, tj. zda bylo vyplaceno 100% nebo méně, dle
                            příslušné smlouvy)
                        </li>
                        <li>
                            Kontroluje podaná vyúčtování udělené podpory a případně s žadateli elektronicky řeší nápravu
                        </li>
                        <li>V případě že je povoleno papírové podání žádosti, může pro účely evidence a zveřejnění
                            otevřených dat, manuálně evidovat fyzicky přijatá vyúčtování a závěrečnou zprávu
                        </li>
                    </ul>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="card m-2" id="user-teams">
    <h2 class="card-header">Týmy / Struktura dotačního úřadu</h2>
    <div class="card-body">
        <p>
            Týmy, také "organizační jednotky dotačního úřadu", jsou způsobem, jak rozdělit a organizovat práci
            zaměstnanců dotačního úřadu, při zpracování podaných žádostí
        </p>
        <p>
            Narozdíl od rolí, které jsou statické v rámci Dotačního Software, týmy jsou zcela v kompetenci dotačního
            úřadu. <br/>
            Typicky slouží k určení jednotek, jako jsou komise a výbory, příslušnost k odborům které poskytují dotace
            nebo nad dotačním procesem dohlíží.
        </p>
        <p>
            Členství v týmu uživateli samo o sobě nedává žádné pravomoci, je nutné, aby týmu byly přiřazeny kompetence v
            konkrétním dotačním programu nebo pod-programu. Vjímkou je náhled na historii žadatelů (výsledek správy v
            sekci "Historie žadatelů"), každý člen týmu, může vidět historii žadatelů a žádostí, které úřad zpracoval v
            předchozích letech.
        </p>
        <p>
            Tým může být zodpovědný za jednu nebo více činností, které jsou spojené s procesem zpracování podaných
            žádostí, činnosti (kompetence) musí být rozděleny v každém dotačním programu a jsou rozdělené následovně
        </p>
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>Kompetence týmu</th>
                <th>Povolené úkony</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="text-nowrap">Formální kontrola</td>
                <td>
                    <ul>
                        <li>Manuální označení žádosti úspěšně vyplněné, jako "odeslané" (pokud v nastavení organizace je
                            povoleno)
                        </li>
                        <li>Označení žádosti jako formálně správné, nebo zamínuté</li>
                        <li>Pokud je žádost zamítnuta, žadateli udělí termín do kterého musí náležitosti opravit a
                            datum, pokud žadatel do daného data žádost neupraví a znovu neodešlě (neprovede papírové
                            podání na úřadu), žádost je automaticky nastavena do stavu "zamítnuto formální kontrolou"
                        </li>
                        <li>
                            Formální kontrola nemůže žádosti upravovat, jen je prohlížet a případně posouvat do dalších
                            stavů v rámci <a href="/about#process" target="_blank">Dotačního procesu</a>
                        </li>
                    </ul>
                </td>
            </tr>
            <tr>
                <td class="text-nowrap">Návrh výše dotace</td>
                <td>
                    <ul>
                        <li>U žádosti může upravit návrh na výši poskytnuté podpory a popis účelu, na který je dotace
                            poskytnuta
                        </li>
                    </ul>
                </td>
            </tr>
            <tr>
                <td class="text-nowrap">Hodnocení dotace</td>
                <td>
                    <ul>
                        <li>
                            Pokud má daný dotační program určena hodnotící kritéria, může člen týmu provést své osobní
                            hodnocení, které se pak stane součástí průměru hodnocení
                        </li>
                        <li>
                            Člen týmu nevidí hodnocení ostatních hodnotitelů nebo výsledný průměr, dokud není žádost vč.
                            hodnocení a popisu účelu schválena
                        </li>
                        <li>
                            Součástí hodnocení je i textová poznámka, kterou si hodnotitel může vyplnit pro svoji
                            vlastní potřebu, a která není součástí dalšího dotačního procesu. To se může hodit, pokud
                            hodnocení uděluje dlouho před zasedáním kompetentního výboru/komise
                        </li>
                    </ul>
                </td>
            </tr>
            <tr>
                <td class="text-nowrap">Schválení výše dotace</td>
                <td>
                    <ul>
                        <li>U žádosti může upravit finální návrh na výši poskytnuté podpory a popis účelu a dosažené
                            hodnocení, na který je dotace poskytnuta
                        </li>
                        <li>
                            Pokud jsou v daném programu určena hodnotící kritéria, vidí výsledný průměr hodnocení
                            žádosti, ne však jednotlivá hodnocení nebo kdo hodnocení provedl, pouze počet započtených
                            hodnocení a průměr dosažený v jednotlivých kritériích
                        </li>
                        <li>
                            Schvalovatel, typicky předseda nebo tajemník komise/výboru, pak může určit hodnocení odlišné
                            od toho, které je výsledkem zpracování jednotlivých hodnotitelů, jelikož závazné pro další
                            proces je typicky usnesení takového výboru/komise, ne jednotlivá hodnocení, předcházející
                            jednání
                        </li>
                    </ul>
                </td>
            </tr>
            <tr>
                <td class="text-nowrap">Nastavení výsledného stavu žádosti</td>
                <td>
                    <ul>
                        <li>Poté co je žádost schválena a je pro ni schválena navržená podpora, slovní formulace účelu
                            (pokud byla určena) a bodové hodnocení, je žádost typicky schválena statutárními orgány
                            (rada, zastupitelstvo, ...), výsledek jednání odpovědného (statutárního) orgánu pak člen
                            tohoto týmu zanese
                            zpět do dotačního software
                        </li>
                        <li>
                            Může dotaci nastavit do stavu "Připraveno k podpisu smlouvy", případně žádost uzavřít, pokud
                            podpora nebyla schválena
                        </li>
                        <li>
                            Může dotaci nastavit do stavu "Smlouva podepsána, připraveno k vyplacení", případně žádost
                            uzavřít, pokud žadatel odmítl nebo nebyl schopen smlouvu o veřejné podpoře podepsat
                            (například pro nesplnění jiných formálních podmínek dotačního procesu)
                        </li>
                        <li>
                            Může ohlásit udělení podpory do Registru de minimis, tj. registru veřejné podpory malého
                            rozsahu, který provozuje Ministerstvo Zemědělství. Tento krok pak může udělat i uživatel s
                            rolí "Ekonom", čímž daná žádost přechází do kompetence Ekonomů, kteří dotační proces vedou
                            dále až k vypořádání, ukončení dané žádosti a zveřejnění v otevřených datech
                        </li>
                    </ul>
                </td>
            </tr>
            </tbody>
        </table>
        <p class="font-weight-bold">
            Stejně jako u Rolí, pokud se změní zařazení uživatele, musí se uživatel odhlásit a znovu přihlásit, aby se
            změny projevily
        </p>
    </div>
</div>
