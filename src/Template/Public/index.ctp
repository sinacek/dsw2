<?php
/**
 * @var $this AppView
 */

use App\Model\Entity\OrganizationSetting;
use App\View\AppView;

$this->assign('title', $this->getSiteName());

$cards = [];
if (!$this->isUser()) {
    $cards = [
        [
            'header' => __('Přihlášení'),
            'text' => __('Pokud již jste registrovaný uživatel, můžete se přihlásit'),
            'link' => ['_name' => 'login'],
            'action' => __('Přihlásit se'),
            'class' => 'btn-success'
        ],
        [
            'header' => __('Registrace'),
            'text' => __('Pokud jste Fyzická nebo Právnická osoba, která má zájem získat dotaci, registrujte se'),
            'link' => ['_name' => 'register'],
            'action' => __('Registrovat se')
        ],
        [
            'header' => __('Zapomenuté heslo'),
            'text' => __('Pokud máte účet, ale zapomněli jste heslo, můžete si jej obnovit'),
            'link' => ['_name' => 'password_recovery'],
            'action' => __('Obnovit heslo'),
            'class' => 'btn-warning'
        ]
    ];
} else {
    // ordinary user here
    $cards = [
        [
            'header' => __('Moje žádosti'),
            'text' => __('Aktuální i historické, rozpracované i odeslané žádosti o podporu'),
            'link' => ['_name' => 'user_requests'],
            'action' => __('Přejít k mým žádostem'),
            'class' => 'btn-danger'
        ],
        [
            'header' => __('Moje identita'),
            'text' => __('Upravit vlastní identifikaci, IČO, místo podnikání, kontaktní informace a bankovní spojení'),
            'link' => ['_name' => 'update_self_info'],
            'action' => __('Upravit informace')
        ],
    ];
}

// for all
$cards = array_merge($cards, [
    [
        'header' => __('Dotační Fondy'),
        'text' => __('Prozkoumat strukturu dotačních fondů'),
        'link' => ['_name' => 'public_fonds'],
        'action' => __('Prozkoumat'),
        'class' => 'btn-success'
    ],
    [
        'header' => __('Dotační Výzvy'),
        'text' => __('Prohlédněte si aktuální, historické i připravované dotační výzvy'),
        'link' => ['_name' => 'public_appeals'],
        'action' => __('Prohlédnout'),
        'class' => 'btn-success'
    ]
]);

?>

<?php if ($this->getSiteSetting(OrganizationSetting::INDEX_HERO)): ?>
    <div class="jumbotron">
        <h1 class="display-4"><?= $this->getSiteSetting(OrganizationSetting::INDEX_HERO_TITLE) ?></h1>
        <p class="lead"><?= $this->getSiteSetting(OrganizationSetting::INDEX_HERO_SUBTITLE) ?></p>
        <hr class="my-4">
        <p><?= $this->getSiteSetting(OrganizationSetting::INDEX_HERO_TEXT) ?></p>
    </div>
<?php endif; ?>

<?= $this->element('simple_cards', compact('cards')) ?>