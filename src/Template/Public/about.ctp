<div class="jumbotron jumbotron-fluid bg-opaque mt-3">
    <div class="container">
        <h1 class="display-4"><img src="/img/default64.png" alt="Dotační Software 2" class="mr-2"> Dotační Software&nbsp;2
        </h1>
        <p class="lead">Open-source nástroj pro obce, městské části a dotační úřady, umožňující příjem, zpracování a
            správu žádostí o dotace</p>
        <div class="">
            <ul class="list-unstyled">
                <li class="mt-1"><span class="text-success h4"><i class="fas fa-check-circle"></i></span> Odeslání
                    žádostí datovou
                    schránkou
                </li>
                <li class="mt-1"><span class="text-success h4"><i class="fas fa-check-circle"></i></span> Rychlejší
                    vypisování
                    dotačních výzev
                </li>
                <li class="mt-1"><span class="text-success h4"><i class="fas fa-check-circle"></i></span> Proces se
                    přizpůsobí vašim
                    požadavkům
                </li>
                <li class="mt-1"><span class="text-success h4"><i class="fas fa-check-circle"></i></span> Příjem a
                    kontrola
                    vyúčtování dotace
                </li>
                <li class="mt-1"><span class="text-success h4"><i class="fas fa-check-circle"></i></span> Automatická
                    publikace
                    otevřených dat
                </li>
                <li class="mt-1"><span class="text-success h4"><i class="fas fa-check-circle"></i></span> Sjednocení
                    formy přijatých
                    žádostí
                </li>
            </ul>
        </div>
    </div>
</div>

<div id="about" class="jumbotron jumbotron-fluid bg-opaque">
    <div class="container">
        <h2>O projektu</h2>
        <p>
            Cílem projektu je nabídnout <strong>zdarma všem obcím a městským částem v ČR</strong> pohodlný nástroj pro
            správu
            dotačních programů, příjem žádostí, jejich zpracování, kontrolu, hodnocení a následně i ohlášení akcí,
            vyúčtování a zveřejnění udělené podpory v otevřených datech
        </p>
        <p>
            Zásadní myšlenkou projektu je zbavit obec závislosti na IT poskytovatelích, tak aby si provoz i kompletní
            nastavení dotačního portálu mohla obec zařídit sama. To se týká především možnosti vytvořit si vlastní
            formuláře dotací (definovat jednotlivé položky formuláře) a spravovat si jak uživatelské účty zaměstnanců
            dotačního úřadu, tak účty žadatelů. Taktéž i vývoj na míru lze jednoduše poptat/soutěžit, <strong>nevnucujeme
                vám žádný vendor-lock</strong>
        </p>
        <p>
            Proto je projekt poskytnut ve formě <strong>open-source software vč. dokumentace pro instalaci a
                nastavení</strong>, tak aby
            jedinou otázkou k řešení, zůstalo zda si obec bude software provozovat sama, nebo zda bude soutěžit dodání
            provozní a uživatelské podpory
        </p>
        <p>
            Projekt je aktuálně využíván nebo testován v několika městských částech Prahy a dalších obcích v ČR a jeho
            vývoj a podporu zajišťují Otevřená Města. Jednotlivé funkce dotačního software jsou realizovány na základě
            požadavků a jejich implementace se typicky provádí tak, aby byla funkce poskytnuta všem obcím.
        </p>
        <p>
            Projekt je v aktivním vývoji a ne všechny funkce jsou již hotové, aktuální stav lze vždy zjistit na
            <a href="#features" class="text-decoration-none text-dark">této stránce</a>, nebo v
            <a class="text-decoration-none text-dark" href="https://gitlab.com/otevrenamesta/praha3/dsw2">úkolech u
                zdrojového kódu</a>
        </p>
    </div>
</div>

<div id="get-started" class="jumbotron jumbotron-fluid bg-opaque">
    <div class="container table-responsive">
        <h2>Jak si software vyzkoušet?</h2>
        <div class="row mb-1">
            <div class="col-md-1 text-center">
                <i class="text-light border-success bg-success circle">1</i>
            </div>
            <div class="col">
                Napište nám na e-mail <a href="mailto:dsw2@otevrenamesta.cz">dsw2@otevrenamesta.cz</a>
                požádejte o svůj vlastní portál ZDARMA
            </div>
        </div>
        <div class="row mb-1">
            <div class="col-md-1 text-center">
                <i class="text-light border-success bg-success circle">2</i>
            </div>
            <div class="col">
                Vyzkoušejte si administraci dotačního portálu, vytvoření formulářů, vyplnění a odeslání žádosti o dotaci
            </div>
        </div>
        <div class="row mb-1">
            <div class="col-md-1 text-center">
                <i class="text-light border-success bg-success circle">3</i>
            </div>
            <div class="col">
                Rozhodněte se dle <a href="#pricing">Ceníku</a> jak chcete software provozovat
            </div>
        </div>
        <span class="small">Při testování vám samozřejmě poskytneme pomoc s nastavením a zodpovíme všechny otázky, abyste mohli udělat informované rozhodnutí</span>
    </div>
</div>

<div id="process" class="jumbotron jumbotron-fluid bg-opaque">
    <div class="container table-responsive">
        <h2>Dotační proces</h2>
        <p>
            Abyste si mohli udělat představu, které všechny úkony s Dotačním Software lze provádět, zde je stručný popis
            dotačního procesu, jak jej implementujeme
        </p>
        <table class="table table-bordered table-striped">
            <colgroup>
                <col class="w-25">
                <col class="w-10">
                <col class="w-10">
                <col>
            </colgroup>
            <thead class="thead-dark">
            <tr>
                <th>Krok v dotačním procesu</th>
                <th>Povinný?</th>
                <th>Kompetence (Role / Tým)</th>
                <th>Popis</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $processes = [
                ['Přijetí žádosti', 'Povinný', 'Tým: Formální Kontrola', 'Přijetí může proběhnout úplně elektronicky nebo listinnou formou'],
                ['Formální kontrola', 'Povinný', 'Tým: Formální Kontrola', 'Každá elektronicky přijatá žádost musí být schválena formální kontrolou, případně může být žádost vrácena žadateli k opravě a v nejhorším případě vyřazena z dalšího zpracování. U listinných podání žádostí se předpokládá kontrola před elektronickou evidencí.'],
                ['Hodnocení žádosti', 'Nepovinný', 'Tým: Hodnocení Žádostí', 'Pokud chcete, můžete v jednotlivých programech žádosti hodnotit dle stanovených bodových kritérií, výsledek hodnocení pak může být podkladem pro další jednání dotačních orgánů'],
                ['Návrh změny výše nebo účelu dotace', 'Nepovinný', 'Tým: Návrh výše dotace', 'V procesu zpracování lze provést návrhy na změnu výše podpory nebo účelu'],
                ['Uzavření a finalizace žádostí', 'Nepovinný', 'Role: Správce dotací', 'Před jednáním statutárních orgánů (rada, zastupitelstvo, ...) je možné ukončit hodnocení a návrhy změn výše nebo účelu (typicky provádí komise/výbory)'],
                ['Nastavení výsledného stavu', 'Povinný', 'Tým: Schválení výše dotace', 'Po jednání statutárních orgánů je třeba provést evidenci výsledku jednání, zaslat vyrozumění o poskytnutí/neposkytnutí podpory a případně vyzvat žadatele k podpisu smlouvy'],
                ['Zvěřejnění smlouvy a vyplacení podpory', 'Povinný', 'Tým: Nastavení výsledného stavu žádosti / Role: Ekonom', 'Pro každou žádost je třeba s žadatelem uzavřít příslušnou smlouvu o veřejnoprávní podpoře a tuto zveřejnit v registru smluv. Portál umožňuje evidovat i situace kdy smlouva podepsána být nemůže, provést hlášení podpory de minimis a případně vyplatit i jen část schválené podpory (víceleté dotace)'],
                ['Ohlášení činnosti / akce / díla', 'Nepovinný', 'Žadatel', 'Pokud vyžadujete od úspěšných žadatelů, aby vás informovali o průběhu čerpání (uspořádání akce, dokočení díla, naplánované činnosti), toto mohou žadatelé provést přímo v dotačním portálu'],
                ['Vyúčtování dotace', 'Povinný', 'Žadatel / Role: Ekonom', 'Vyúčtování dotace je nezbytné, portál umožňuje žadateli podat vyúčtování (jednotlivé položky) a tyto doložit příslušnými doklady (výpis z BÚ, faktura, smlouva, pokladní doklad). Ekonom dotační organizace pak může vyúčtování akceptovat, nebo jej vrátit k doplnění/opravě žadateli.'],
                ['Veřejnoprávní kontrola', 'Nepovinný', 'Role: Ekonom', 'Skrze portál lze také evidovat situace kdy došlo k porušení veřejnoprávní smlouvy a musí být vrácena část nebo celá částka dotace a evidovat probíhající správní/trestní/soudní řízení.'],
                ['Zveřejnění udělené podpory', 'Povinný', 'Role: Ekonom', 'Dle zákonných povinností, portál automaticky zveřejní po ukončení procesu žádosti, identifikaci úspěšného žadatele, částku podpory, účel a hodnocení a část vyúčtování (závěrečnou zprávu a seznam uznaných nákladových položek) ve formátu otevřených dat']
            ];
            foreach ($processes as $process):
                ?>
                <tr>
                    <td class="font-weight-bold text-center"><?= $process[0] ?></td>
                    <td class="text-center <?= $process[1] === 'Povinný' ? 'bg-success text-light' : 'text-muted' ?>"><?= $process[1] ?></td>
                    <td class="text-center"><?= $process[2] ?></td>
                    <td><?= $process[3] ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<div id="features" class="jumbotron jumbotron-fluid bg-opaque">
    <div class="container table-responsive">
        <h2>Funkce</h2>
        <p class="lead">
            existující, vyvíjené i plánované
        </p>
        <table class="table-bordered table table-striped">
            <colgroup>
                <col class="w-25">
                <col class="w-25">
                <col class="w-50">
            </colgroup>
            <thead class="thead-dark">
            <tr>
                <th>Funkce</th>
                <th>Stav</th>
                <th>Další informace</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $features = [
                ['success', 'Správa struktury dotačního fondu', 'Dokončeno', 'Lze definovat jednotlivé dotační fondy, oblasti podpory, programy a pod-programy (účely)'],
                ['success', 'Tvorba vlastních formulářů', 'Dokončeno', 'Lze definovat neomezené množství formulářů, jejich položek, pořadí položek, konkrétní typ (číslo, datum, krátký/dlouhý text, výběr z možností,&nbsp;...)'],
                ['success', 'Správa uživatelské nápovědy', 'Dokončeno', 'Lze vytvořit vlastní kategorie a stránky nápovědy'],
                ['success', 'Sdílení uživatelských účtů', 'Dokončeno', 'Lze sdílet svůj účet s jinými uživateli, aby tito mohli pomoci s vyplněním žádostí. Žádost však stále může odeslat nebo smazat jen vlastník'],
                ['success', 'Odeslání žádosti datovou schránkou', 'Dokončeno', 'Uživatel může po úspěšném vyplnění žádosti, tuto odeslat datovou schránkou'],
                ['success', 'Písemné podání žádosti', 'Dokončeno', 'Pokud nechcete od žadatele vyžadovat vlastnictví datové schránky, lze žádost označit za přijatou, pokud vám ji dodá v papírové podobě'],
                ['success', 'Správa struktury dotačního úřadu', 'Dokončeno', 'Podle toho, jak vaše organizace zpracovává žádosti o dotace, lze definovat jednotlivé týmy (komise, výbory, pracovní skupiny, tajemníky,&nbsp;...) a jejich kompetence'],
                ['success', 'Kompletní správa textového obsahu', 'Dokončeno', 'V administračním rozhraní lze upravit jednotlivé textové položky (překlady) tak aby odpovídaly vašim požadavkům'],
                ['success', 'Hodnocení / Hodnotící kritéria', 'Dokončeno', 'Lze definovat jednotlivá hodnotící kritéria a umožnit hodnocení žádostí, bodovou nebo textovou formou, kritéria jsou pak aplikována na příslušné programy/pod-programy'],
                ['success', 'Uzávěrka hodnocení', 'Dokočeno', 'Pokud používáte hodnotící kritéria, můžete určit datum do kterého, v daném programu, mohou hodnotitelé udělovat svá hodnocení'],
                ['success', 'Vytváření výzev k podání žádostí', 'Dokončeno', 'Portál umožňuje vytvořit neomezené množství výzev k podání žádostí o dotaci, s určením období, kdy lze žádosti odeslat a v jakých oblastech/programech lze žádat'],
                ['success', 'Správa historie žadatelů', 'Dokončeno', 'Pokud máte již informace o poskytnuté podpoře v minulých letech, můžete ručně nebo hromadně importovat udělenou podporu, například z evidenčních Excel tabulek'],
                ['success', 'Nastavení vlastního e-mailového serveru', 'Dokončeno', 'Jak v režimu SaaS tak On-Premise, se vám může hodit konfigurace externího e-mailového serveru, abyste měli kontrolu nad e-mailovou komunikací s žadateli'],
                ['success', 'Nastavení Datové Schránky', 'Dokončeno', 'Pokud chcete umožnit nebo vyžadovat podání přes datovou schránku, jednoduše si v administračním rozhraní nastavíte certifikát pro Odesílací Bránu / Autentizační službu, systému ISDS'],
                ['success', 'Sociální služby - formuláře', 'Dokončeno', 'Pro financování sociálních služeb jsou vyžadovány složitější / strukturované formuláře, které jsou poskytnuty samostatně od základního typu formuláře s uživatelsky definovatelnými položkami. '
                    . '<br/>Aktuálně je hotovo 5 formulářů, konkrétně <ul><li>"Pesonální zajištění služby (informace o zaměstnancích)"</li><li>"Kapacita služby (pobytové a ostatní)"</li><li>"Finanční rozvaha k zajištění služby (projektu)"</li><li>"Rozpočet poskytované služby podle nákladových položek"</li>'
                    . '<li>Počet uživatelů sociální služby s bydlištěm ve správním obvodě</li></ul>Pokud byste potřebovali přidat další, stačí jen požádat, typicky je hotovo do 1 pracovního dne'],
                ['warning', 'Integrace se spisovou službou (NSESSS)', '10%', 'Aktuálně probíhá vývoj integrace se spisovými službami, pomocí rozhraní NSESSS (Národního Standardu pro Elektronické Systémy Spisové Služby)<br/>'
                    . 'Vývoj by měl být dokončen do konce 10/2020 a umožnit vkládání žádostí, příloh a vyúčtování přímo do vaší spisové služby. '
                    . 'viz. <a href="https://gitlab.com/otevrenamesta/praha3/dsw2/-/issues/3" target="_blank">Požadavek #3</a>'],
                ['warning', 'Postupné vyplácení víceletých dotací', '50%', 'Pokud poskytujete víceletou podporu, tak bude systém brzy podporovat možnost vyplatit jen část dotace vždy po akceptaci vyúčtování za předchozí období. '
                    . 'viz. <a href="https://gitlab.com/otevrenamesta/praha3/dsw2/-/issues/23" target="_blank">Požadavek #23</a>'],
                ['warning', 'Hlášení podpory de minimis', '25%', 'Pokud poskytujete podporu v režimu de minimis, po schválení žádosti a podpisu smlouvy, bude možné přímo z dotačního portálu provést ohlášení do RDM (Registr podpor de mimimis). '
                    . 'viz. <a href="https://gitlab.com/otevrenamesta/praha3/dsw2/-/issues/21" target="_blank">Požadavek #21</a>'],
                ['danger', 'Vyrozumění o (ne)poskytnutí dotace', '0%', 'Portál by měl umět podle uživatelsky definovaného vzoru generovat vyrozumění o (ne)poskytnutí veřejné podpory, z informací obsažených v portálu, vč. informací o dosaženém bodovém/slovním hodnocení'],
                ['danger', 'Příprava smlouvy o veřejnoprávní podpoře', '0%', 'Portál by měl umět taktéž před-připravit smlouvu o veřejnoprávní podpoře a tuto vložit do spisové služby nebo poskytnout ke stažení ve formátu Word (doc/docx/odt)'],
                ['danger', 'Rozdělení kompetencí ekonomického oddělení', '0%', 'Aktuálně existuje jen jedna role ekonoma, která může kontrolovat podaná vyúčtování a provádět veřejnoprávní kontrolu nad žadateli. V budoucnu by mělo být možné kompetenci určit jen nad jednotlivou oblastí podpory / dotačním programem'],
                ['success', 'Dokumentace instalační / provozní', '100%', 'Součástí zdrojového kódu je i dokumentace k instalaci, spuštění a nastavení dotačního portálu, takže by to měl zvládnout téměř kdokoli. '
                    . 'viz. <a href="https://gitlab.com/otevrenamesta/praha3/dsw2/-/tree/master/docs">složka docs/</a>'],
                ['success', 'Dokumentace uživatelská (pro žadatele)', '90%', 'Vzor obecného zacházení s dotačním portálem pro žadatele lze vidět <a href="https://dotace.praha3.cz/podpora" target="_blank">například zde, u Prahy 3</a>, pracujeme na tom aby byla obecná nápověda pro žadatele dostupná již při spuštění portálu, zatím si ji však musíte sami vytvořit při nastavení portálu.'],
                ['danger', 'Dokumentace administrátorská', '10%', 'Zatím není poskytnuta témeř žádná dokumentace k nastavení dotačních fondů, formulářů, výzev a dalších, toto bude brzy napraveno. Dokumentace je v portálu dostupná, i bez přihlášení, na adrese <a href="/admin/docs">/admin/docs</a>'],
            ];
            foreach ($features as $feature):
                ?>
                <tr>
                    <td class="font-weight-bold">
                        <?= $feature[1] ?>
                    </td>
                    <td class="text-center h5">
                        <span class="badge badge-<?= $feature[0] ?>"><?= $feature[2] ?></span>
                    </td>
                    <td>
                        <?= $feature[3] ?>
                    </td>
                </tr>
            <?php
            endforeach;
            ?>
            </tbody>
        </table>
    </div>
</div>

<div id="pricing" class="jumbotron jumbotron-fluid bg-opaque">
    <div class="container table-responsive">
        <h2>Ceník</h2>

        <p class="lead">Cena za používání / podporu při využívání projektu se odvíjí především od toho, zda je
            daná obec / městská část, členem spolku Otevřených Měst či nikoliv.</p>
        <p>Pokud vás zajímá členství, podmínky a
            případně další výhody, více informací naleznete zde:
            <a href="https://otevrenamesta.cz/clenstvi/"
               target="_blank" class="font-weight-bold">https://otevrenamesta.cz/clenstvi/</a>
        </p>

        <table class="table table-bordered table-striped table-dark">
            <colgroup>
                <col class="w-30">
                <col class="w-20">
                <col class="w-50">
            </colgroup>
            <thead class="thead-dark">
            <tr>
                <th>Nákladová položka</th>
                <th>Pro členy Otevřených Měst</th>
                <th>Ostatní obce a organizace</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>
                    <strong>Provoz v cloudu Otevřených Měst (SaaS)</strong>
                    <ul class="text-muted">
                        <li>provoz a podporu zajišťují Otevřená Města na vlastní IT infrastruktuře</li>
                        <li>Zahrnuje běžnou podporu v pracovní dny (8/5)</li>
                        <li>Zahrnuje školení pro správce dotačního fondu</li>
                        <li>Zahrnuje asistenci s prvním nastavením dotačního portálu a technických parametrů</li>
                        <li>Zdarma je samozřejmě oprava všech funkčních chyb i drobný vývoj</li>
                    </ul>
                </td>
                <td class="font-weight-bold text-center h4">
                    Zdarma
                </td>
                <td>
                    <table class="table table-light">
                        <thead class="thead-light">
                        <tr>
                            <th>Výše daňových příjmu obce (Kč/rok)</th>
                            <th>Cena za Dotační Software (rok)</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>do 200 mil.</td>
                            <td>12.600 Kč</td>
                        </tr>
                        <tr>
                            <td>do 400 mil.</td>
                            <td>25.200 Kč</td>
                        </tr>
                        <tr>
                            <td>do 600 mil.</td>
                            <td>37.800 Kč</td>
                        </tr>
                        <tr>
                            <td>do 800 mil.</td>
                            <td>50.400 Kč</td>
                        </tr>
                        <tr>
                            <td>do 1000 mil.</td>
                            <td>63.000 Kč</td>
                        </tr>
                        <tr>
                            <td>nad 1000 mil.</td>
                            <td>75.600 Kč</td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <strong>Provoz na vašem serveru (On-Premise)</strong>
                    <ul class="text-muted">
                        <li>Dotační software bude provozován na vaší IT infrastruktuře</li>
                        <li>Zahrnuje běžnou podporu v pracovní dny (8/5)</li>
                        <li>Zahrnuje školení pro správce dotačního fondu</li>
                        <li>Zahrnuje asistenci s prvním nastavením dotačního portálu a technických parametrů</li>
                        <li>Zdarma je samozřejmě oprava všech funkčních chyb i drobný vývoj</li>
                    </ul>
                </td>
                <td colspan="2" class="text-center">
                    Kompletní Dotační Software si můžete zdarma stáhnout na
                    <a href="https://gitlab.com/otevrenamesta/praha3/dsw2" target="_blank"
                       class="text-decoration-none text-light">stránkách Gitlab</a>
                    <br/><br/>
                    Pokud si software nedokážete spustit sami, nebo nemáte potřebné kapacity, můžete se obrátit na
                    Otevřená Města a požádat nás o zajištění provozu a/nebo uživatelské podpory.
                    <br/><br/>
                    Cenu bohužel nelze stanovit předem, kontaktujte nás prosím na e-mailu
                    <a class="text-decoration-none text-light"
                       href="mailto:dsw2@otevrenamesta.cz">dsw2@otevrenamesta.cz</a>
                    <hr class="bg-light"/>
                    Nebo můžete požádat svého stávajícího poskytovatele/dodavatele webových stránek,
                    provoz projektu je velmi jednoduchý a nevyžaduje žádné speciální počítače, servery nebo licence.
                </td>
            </tr>
            <tr>
                <td>
                    <strong>Vývoj na míru</strong>
                    <br/>
                    <p class="text-muted">
                        Například vývoj formulářů na míru, nebo integrace s produkty třetí strany, například
                        spisovou službou, ekonomickým software, IdM a další
                    </p>
                </td>
                <td colspan="2" class="font-weight-bold text-center">
                    Menší úpravy dle hodinové náročnosti s cenou 1000 Kč/hodina
                    <br/><br/>
                    Pro velké úpravy stanovíme, pokud to bude možné, fixní projektovou cenu
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<div id="gallery" class="jumbotron bg-opaque">
    <div class="container">
        <h2>Fotogalerie</h2>
        <div class="row">
            <?php
            $images = [
                ['hlavni_stranka_springfield.png', 'Ukázkový dotační portál města Springfield'],
                ['nastaveni_organizace_zakladni.png', 'Nastavení dotačního portálu: Základní informace'],
                ['nastaveni_organizace_technicke.png', 'Nastavení dotačního portálu: Technické detaily'],
                ['nastaveni_organizace_emaily.png', 'Nastavení dotačního portálu: E-mailový server'],
                ['sprava_struktury_dotacniho_uradu.png', 'Správa struktury dotačního úřadu'],
                ['detail_spravy_uzivatele.png', 'Detail správy uživatele / oprávnění'],
                ['sprava_napovedy.png', 'Správa nápovědy (sekcí a stránek uživatelské podpory)'],
                ['sprava_prekladu.png', 'Správa překladů (úprava statických textů)'],
                ['sprava_formularu.png', 'Správa formulářů k vyplnění'],
                ['sprava_formularu_detail.png', 'Detail úpravy formuláře'],
                ['sprava_dotacnich_vyzev.png', 'Správa dotačních výzev'],
                ['sprava_dotacnich_vyzev_detail.png', 'Detail úpravy výzvy k podání žádostí o dotaci'],
                ['hodnotici_kriteria.png', 'Správa skupin hodotících kritérií'],
                ['sprava_historie_zadatelu.png', 'Správa historie žadatelů / poskytnuté podpory'],
                ['me_zadosti.png', 'Žadatel: Přehled žádostí'],
                ['detail_zadosti.png', 'Žadatel: Detail žádosti'],
                ['rozpocet_projektu.png', 'Žadatel: Rozpočet projektu (náklady, výnosy, vlastní zdroje, dotace z jiných zdrojů na stejný projekt)'],
                ['moje_identita_1.png', 'Žadatel: Moje identita'],
                ['moje_identita_2_po_podily.png', 'Žadatel: Evidence vlastnické struktury a vlastněných podílů v právnických osobách'],
                ['moje_identita_3.png', 'Žadatel: Moje identita - bankovní účet a údaje ověřené přihlášením do Datové schránky'],
                ['socialni_personalni_zajisteni_sluzby.png', 'Sociální služby: Personální zajištění služby (informace o zaměstnancích dotované organizace)'],
                ['socialni_kapacita_sluzby.png', 'Sociální služby: Poskytovaná kapacita služby (s výhledem do dalšího období)'],
                ['socialni_financni_rozvaha.png', 'Sociální služby: Finanční rozvaha k zajištění služby podle zdroje financí'],
                ['socialni_rozpocet_podle_nakladove_polozky.png', 'Sociální služby: Rozpočet podle nákladových položek']
            ];
            foreach ($images as $image):
                ?>
                <div class="col-md-3 mt-2">
                    <a href="/about_assets/<?= $image[0] ?>" data-fancybox="previews"
                       data-caption="<?= $image[1] ?>" class="w-100">
                        <img src="/about_assets/<?= $image[0] ?>" class="w-100 border-dark border"
                             alt="<?= $image[1] ?>">
                    </a>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>

<div id="partners" class="jumbotron jumbotron-fluid bg-opaque">
    <div class="container">
        <h2>Partneři projektu</h2>
        <p class="lead">
            Uvedené jsou městské části a obce, které již využívají nebo aktuálně testují využití Dotačního Software,
            a organizace, které se podílejí na vývoji nebo propagaci projektu.
        </p>
        <div class="row">
            <?php
            $partners = [
                [
                    'logo' => 'https://www.praha3.cz/getFile/case:show/id:898290/2020-01-28%2011:27:41.000000/z%C3%A1kladn%C3%AD%20varianta%20PNG.png',
                    'name' => 'MČ Praha 3',
                    'link' => 'https://dotace.praha3.cz',
                    'linktext' => 'dotace.praha3.cz'
                ],
                [
                    'logo' => 'https://www.zvirevnouzi.cz/wp-content/uploads/sites/3/2019/08/Mestska-cast-Praha-4-znak.jpg',
                    'name' => 'MČ Praha 4',
                    'link' => 'https://www.praha4.cz/',
                    'linktext' => 'praha4.cz'
                ],
                [
                    'logo' => 'https://www.praha8.cz/image/Jns/logo-prvni-misto.jpg',
                    'name' => 'MČ Praha 8',
                    'link' => 'https://dotace.praha8.cz',
                    'linktext' => 'dotace.praha8.cz'
                ],
                [
                    'logo' => 'https://www.bezeckenadeje.cz/wp-content/uploads/2015/01/logo_barva_p12-1024x559.jpg',
                    'name' => 'MČ Praha 12',
                    'link' => 'https://dotace.praha12.cz',
                    'linktext' => 'dotace.praha12.cz'
                ],
                [
                    'logo' => 'https://www.top09.cz/files/photos/large/20120614182152.jpg',
                    'name' => 'MČ Praha 14',
                    'link' => 'https://praha14.dsw2.otevrenamesta.cz/',
                    'linktext' => 'dotace.praha14.cz'
                ],
                [
                    'logo' => 'https://upload.wikimedia.org/wikipedia/commons/thumb/6/68/Otev%C5%99en%C3%A1_m%C4%9Bsta_-_logo.svg/1200px-Otev%C5%99en%C3%A1_m%C4%9Bsta_-_logo.svg.png',
                    'name' => 'Otevřená Města',
                    'link' => 'https://otevrenamesta.cz/projekty/dsw',
                    'linktext' => 'otevrenamesta.cz'
                ],
                [
                    'logo' => 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSukHOIQjDfbZl0TIPDDT6tCI21uLElmhH2tA&usqp=CAU',
                    'name' => 'Operátor ICT',
                    'link' => 'https://operatorict.cz/',
                    'linktext' => 'operatorict.cz'
                ]
            ];

            foreach ($partners as $partner):
                ?>
                <div class="col-md-3 p-2 d-inline-block text-center">
                    <img src="<?= $partner['logo'] ?>"
                         alt="<?= $partner['name'] ?>"
                         class="w-100">
                    <a href="<?= $partner['link'] ?>" target="_blank"
                       class="text-dark stretched-link"><?= $partner['linktext'] ?></a>
                </div>
            <?php
            endforeach;
            ?>
        </div>
    </div>
</div>

<div id="links" class="jumbotron jumbotron-fluid bg-opaque">
    <div class="container">
        <h2>Další informace</h2>
        <div class="list-group">
            <a href="https://gitlab.com/otevrenamesta/praha3/dsw2" target="_blank"
               class="list-group-item list-group-item-action">Gitlab:
                Zdrojový kód</a>
            <a href="https://gitlab.com/otevrenamesta/praha3/dsw2/-/tree/master/docs" target="_blank"
               class="list-group-item list-group-item-action">
                Gitlab: Dokumentace (instalace serveru, nastavení dotačního portálu,&nbsp;...)
            </a>
            <a href="https://otevrenamesta.cz/projekty/dsw" target="_blank"
               class="list-group-item list-group-item-action">Otevřená
                Města: Projekt DSW</a>
        </div>

        <br/>

        <h2>Napsali o nás</h2>
        <div class="list-group">
            <a href="https://praha.pirati.cz/roboti-misto-uredniku-praha-3-nasazuje-otevreny-software.html"
               target="_blank" class="list-group-item list-group-item-action">
                pirati.cz: Piráti začali první úředníky nahrazovat robotem. Dotační software Prahy 3 nyní volně k
                dispozici všem
            </a>
            <a href="https://www.lupa.cz/aktuality/praha-3-podle-slibu-zverejnila-prvni-vlastni-software-jako-open-source/"
               target="_blank" class="list-group-item list-group-item-action">
                lupa.cz: Praha 3 podle slibů zveřejnila první vlastní software jako open source
            </a>

        </div>
    </div>
</div>

<div id="contacts" class="jumbotron bg-opaque">
    <div class="container">
        <h2>Kontakty</h2>
        <div class="list-group">
            <a href="mailto:dsw2@otevrenamesta.cz" target="_blank" class="list-group-item list-group-item-action">
                Pokud máte zájem si projekt vyzkoušet, nebo máte dotaz právního/obchodního charakteru, použijte prosím
                email:
                <strong>dsw2@otevrenamesta.cz</strong>
            </a>
            <a href="https://otevrenamesta.cz/kontakty/" target="_blank" class="list-group-item list-group-item-action">
                Kontakty na spolek Otevřená Města, <strong>https://otevrenamesta.cz/kontakty/</strong>
            </a>
            <a href="https://gitlab.com/otevrenamesta/praha3/dsw2/-/issues" target="_blank"
               class="list-group-item list-group-item-action">
                Pokud máte technický problém, nebo se chcete podílet na vývoji, <strong>Gitlab Issues</strong>
            </a>
        </div>
    </div>
</div>
