<?php

use App\View\AppView;

/**
 * @var $this AppView
 */

$this->assign('title', __('Nepodporovaný prohlížeč'));
?>

<div class="card">
    <div class="card-header">
        <h2><?= __('Bohužel používáte nepodporovaný prohlížeč') ?></h2>
    </div>
    <div class="card-body">
        <?= __('Pokud se vám zobrazila tato stránka, snažíte se používat dotační portál v prohlížeči, který je příliš starý nebo nepodporovaný.') ?>
        <br/>
        <?= __('Použijte prosím jeden z následujících prohlížečů:') ?>
        <hr/>
        <div class="row">
            <div class="col-md-2 text-center">
                <h4><?= __('Mozilla Firefox') ?></h4>
                <a href="https://www.firefox.cz/" class="stretched-link">
                    <img src="/browsers/firefox.svg" class="w-100" alt="<?= __('Mozilla Firefox') ?>">
                </a>
            </div>
            <div class="col-md-2 text-center">
                <h4><?= __('Google Chrome') ?></h4>
                <a href="https://www.google.com/intl/cs/chrome/" class="stretched-link">
                    <img src="/browsers/chrome.svg" class="w-100" alt="<?= __('Google Chrome') ?>">
                </a>
            </div>
            <div class="col-md-2 text-center">
                <h4><?= __('Microsoft Edge') ?></h4>
                <a href="https://www.microsoft.com/cs-cz/edge" class="stretched-link">
                    <img src="/browsers/microsoft-edge.svg" class="w-100" alt="<?= __('Microsoft Edge') ?>">
                </a>
            </div>
        </div>
    </div>
</div>
