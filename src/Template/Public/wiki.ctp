<?php

use App\Model\Entity\WikiCategory;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $wikis WikiCategory[]
 */

$this->assign('title', __('Nápověda'));

$hasAtLeastOneWiki = false;
foreach ($wikis as $category) {
    if (empty($category->wikis)) {
        continue;
    }
    $hasAtLeastOneWiki = true;
}

if (!$hasAtLeastOneWiki) {
    ?>
    <div class="alert alert-info">
        <?= __('Tento dotační portál zatím neobsahuje žádné stránky podpory') ?>
    </div>
    <?php
    return;
} ?>

<div class="row">
    <div class="col-lg-4">
        <div id="wiki-nav" class="list-group sticky-top">
            <?php
            foreach ($wikis as $category) {
                ?>
                <a class="list-group-item list-group-item-action"
                   href="#category-<?= $category->id ?>"><?= h($category->category_name) ?></a>
                <?php
                foreach ($category->wikis as $page) {
                    ?>
                    <a class="list-group-item list-group-item-action"
                       href="#wiki-<?= $page->id ?>">- <?= h($page->title) ?></a>
                    <?php
                }
            }
            ?>
        </div>
    </div>
    <div class="col-lg-8">
        <div data-spy="scroll" data-target="#wiki-nav" data-offset="0">
            <?php
            foreach ($wikis as $category) {
                ?>
                <div id="category-<?= $category->id ?>"
                        class="text-uppercase font-weight-bold mt-3"><?= h($category->category_name) ?></div>
                <?php
                foreach ($category->wikis as $page) {
                    ?>
                    <div class="card mt-1 mb-1 card-wiki" id="wiki-<?= $page->id ?>">
                        <h3 class="card-header d-flex">
                            <a class="flex-fill text-dark text-decoration-none"
                               href="#collapse-<?= $page->id ?>"
                               aria-controls="collapse-<?= $page->id ?>"
                               data-toggle="collapse"
                               aria-expanded="true"><?= $page->title ?>
                            </a>
                        </h3>
                        <div class="card-body collapse show" id="collapse-<?= $page->id ?>">
                            <div class="card-text">
                                <?= $page->contents ?>
                            </div>
                        </div>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function () {
        $('body').scrollspy({target: '#wiki-nav'});
    })
</script>
