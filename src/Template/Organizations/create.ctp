<?php

/**
 * @var $this AppView
 * @var $organization Organization
 */

use App\Model\Entity\Organization;
use App\View\AppView;

$this->assign('title', __('Vytvoření nové organizace'));

?>
    <h1>
        <?= $this->fetch('title') ?>
    </h1>
<?php
echo $this->Form->create($organization);
echo $this->Form->control('name', ['label' => __('Název Organizace'), 'required' => 'required']);
echo $this->Form->control('domains.1.domain', ['label' => __('Doménové jméno'), 'required' => 'required']);
echo $this->Form->hidden('domains.1.is_enabled', ['value' => true]);
echo $this->Form->submit(__('Uložit'), ['class' => 'btn btn-success']);
echo $this->Form->end();
