<?php

/**
 * @var $this AppView
 * @var $domain Domain
 */

use App\Model\Entity\Domain;
use App\View\AppView;

$this->assign('title', __('Přidání domény'));

?>
    <h1>
        <?= $this->fetch('title') ?>
    </h1>
<?php
echo $this->Form->create($domain);
echo $this->Form->control('domain', ['label' => __('Doménové jméno')]);
echo $this->Form->control('is_enabled', ['checked' => true, 'label' => __('Povolit?')]);
echo $this->Form->submit(__('Uložit'), ['class' => 'btn btn-success']);
echo $this->Form->end();
