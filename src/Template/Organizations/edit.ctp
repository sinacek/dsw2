<?php

/**
 * @var $this AppView
 * @var $organization Organization
 */

use App\Model\Entity\Organization;
use App\Model\Entity\OrganizationSetting;
use App\View\AppView;

$this->assign('title', __('Organizace') . ' - ' . h($organization->name));

echo $this->Form->create($organization);
?>
    <div class="card mt-2">
        <div class="card-header">
            <?php
            echo $this->Form->control('name', ['label' => __('Název Organizace')]);
            ?>
        </div>
    </div>
    <div class="card mt-2">
        <div class="card-header font-weight-bold">
            <a href="#basics" class="text-dark " role="button" data-toggle="collapse"
               aria-controls="basics" aria-expanded="false"><?= __('Základní nastavení Organizace') ?></a>
        </div>
        <div class="card-body collapse show" id="basics">
            <?php
            foreach (OrganizationSetting::ALL_BASIC as $field) {
                echo $this->Form->control('settings.' . $field, array_merge([
                    'label' => OrganizationSetting::getSettingLabel($field),
                    OrganizationSetting::getSettingFormOptionName($field) => $organization->getSettingValue($field, true),
                    'type' => OrganizationSetting::getSettingDataType($field)
                ], OrganizationSetting::getExtraFormControlOptions($field)));
            }
            ?>
        </div>
    </div>

    <div class="card mt-2">
        <div class="card-header font-weight-bold">
            <a href="#subsidy-process" class="text-dark " role="button" data-toggle="collapse"
               aria-controls="subsidy-process" aria-expanded="false"><?= __('Nastavení dotačního procesu') ?></a>
        </div>
        <div class="card-body collapse show" id="subsidy-process">
            <?php
            foreach (OrganizationSetting::ALL_SUBSIDY_PROCESS as $field) {
                echo $this->Form->control('settings.' . $field, array_merge([
                    'label' => OrganizationSetting::getSettingLabel($field),
                    OrganizationSetting::getSettingFormOptionName($field) => $organization->getSettingValue($field, true),
                    'type' => OrganizationSetting::getSettingDataType($field)
                ], OrganizationSetting::getExtraFormControlOptions($field)));
            }
            ?>
        </div>
    </div>

    <div class="card mt-2">
        <div class="card-header font-weight-bold">
            <a href="#mainpage" class="text-dark " role="button" data-toggle="collapse"
               aria-controls="basics" aria-expanded="false"><?= __('Hlavní stránka portálu') ?></a>
        </div>
        <div class="card-body collapse show" id="mainpage">
            <?php
            foreach (OrganizationSetting::ALL_INDEX_HERO as $field) {
                echo $this->Form->control('settings.' . $field, array_merge([
                    'label' => OrganizationSetting::getSettingLabel($field),
                    OrganizationSetting::getSettingFormOptionName($field) => $organization->getSettingValue($field, true),
                    'type' => OrganizationSetting::getSettingDataType($field)
                ], OrganizationSetting::getExtraFormControlOptions($field)));
            }
            ?>
        </div>
    </div>

    <div class="card mt-2 border-danger">
        <div class="card-header font-weight-bold">
            <a href="#technical" class="text-dark " role="button" data-toggle="collapse"
               aria-controls="basics" aria-expanded="false"><?= __('Technické údaje') ?></a>
        </div>
        <div class="card-body collapse" id="technical">
            <?php
            foreach (OrganizationSetting::ALL_TECHNICAL as $field) {
                echo $this->Form->control('settings.' . $field, array_merge([
                    'label' => OrganizationSetting::getSettingLabel($field),
                    OrganizationSetting::getSettingFormOptionName($field) => $organization->getSettingValue($field, true),
                    'type' => OrganizationSetting::getSettingDataType($field)
                ], OrganizationSetting::getExtraFormControlOptions($field)));
            }
            ?>
        </div>
    </div>

    <div class="card mt-2 border-danger">
        <div class="card-header font-weight-bold">
            <a href="#emails" class="text-dark " role="button" data-toggle="collapse"
               aria-controls="basics" aria-expanded="false"><?= __('Nastavení E-mailů') ?></a>
        </div>
        <div class="card-body collapse" id="emails">
            <?php
            foreach (OrganizationSetting::ALL_EMAIL as $field) {
                echo $this->Form->control('settings.' . $field, array_merge([
                    'label' => OrganizationSetting::getSettingLabel($field),
                    OrganizationSetting::getSettingFormOptionName($field) => $organization->getSettingValue($field, true),
                    'type' => OrganizationSetting::getSettingDataType($field)
                ], OrganizationSetting::getExtraFormControlOptions($field)));
            }
            ?>
        </div>
    </div>
<?php

echo $this->Form->submit(__('Uložit'), ['class' => 'btn btn-primary m-2']);
echo $this->Form->end();
