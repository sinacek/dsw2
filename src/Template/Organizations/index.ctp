<?php

/**
 * @var $this AppView
 * @var $organizations Organization[]
 */

use App\Model\Entity\Organization;
use App\View\AppView;

$this->assign('title', __('Správa Organizací'));

?>
<h1>
    <?= $this->fetch('title') ?>
</h1>
<?php if ($this->isSystemsManager()): ?>
    <?= $this->Html->link(__('Vytvořit novou organizaci'), ['action' => 'create']) ?>
<?php endif; ?>
<table class="table">
    <thead>
    <tr>
        <th><?= __('ID') ?></th>
        <th><?= __('Jméno') ?></th>
        <th><?= __('Domény') ?></th>
        <th><?= __('Akce') ?></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($organizations as $org): ?>
        <tr>
            <td><?= $org->id ?></td>
            <td><?= $this->Html->link(h($org->name), ['action' => 'edit', $org->id]) ?></td>
            <td>
                <?php
                foreach ($org->domains as $domain) {
                    echo(h($domain->domain));
                    echo $this->Html->link($domain->is_enabled ? sprintf(" (%s)", __('Zakázat')) : sprintf(" (%s)", __('Povolit')), ['action' => 'domainTrigger', $org->id, $domain->id], ['class' => $domain->is_enabled ? 'text-danger' : 'text-success']);
                    echo $this->Html->link(' (' . __('Smazat') . ')', ['action' => 'domainDelete', $org->id, $domain->id], ['class' => 'text-danger']);
                    echo '<br/>';
                }
                echo $this->Html->link('<i class="fas fa-plus-circle"></i> ' . __('Přidat doménové jméno'), ['action' => 'domainAdd', $org->id], ['class' => 'text-success', 'escape' => false])
                ?>
            </td>
            <td>
                <?= $this->Html->link(__('Upravit'), ['action' => 'edit', $org->id]) ?>,
                <?= $this->Html->link(__('Smazat'), ['action' => 'delete', $org->id], ['class' => 'text-danger']) ?>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
