<?php

use Cake\I18n\Number;

?>
<input type="hidden" name="MAX_FILE_SIZE" value="<?= getMaximumFileUploadSize() ?>">

<script type="text/javascript">
    $(document).ready(function () {
        $("input[type=hidden][name=MAX_FILE_SIZE]").each(function () {
            let filesize = $(this).val();
            $("input[type=file]").bind('change', function () {
                if (this.files.length > 0 && this.files[0].size > filesize) {
                    alert('<?= __('Soubor je větší než maximální povolená velikost') . ' ' . Number::toReadableSize(getMaximumFileUploadSize()) ?>');
                    $(this).val("");
                    // set original-filename as label of custom-file-field or empty if attribute not provided
                    $(this).siblings('label').empty().html($(this).attr('original-filename'));
                }
            });
        });
    });
</script>