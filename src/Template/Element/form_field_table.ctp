<?php

use App\Model\Entity\FormField;
use App\Model\Entity\FormFieldType;
use App\View\AppView;
use Cake\Utility\Hash;

/**
 * @var $this AppView
 * @var $displayOnly bool
 * @var $label string
 * @var $formField FormField
 * @var $currentValue mixed
 */

FormFieldType::extractTableSettings($formField, true);

if (!$displayOnly) {
    if (!empty($label)) {
        echo $label;
    } else {
        echo $formField->name;
    }
}

$labels = $formField->get('table_rows_labels');
$hasLabels = is_array($labels) && count($labels) > 0;
$columnDefinitions = $formField->get('columns') ?? [];
$showSums = boolval($formField->get('table_show_sums'));
$sumsRowLabel = $showSums ? $formField->get('table_show_sums_label') ?? __('Celkem') : null;
?>

    <table class="table table-bordered table-with-inputs">
        <thead>
        <tr>
            <?php if ($hasLabels): ?>
                <th><?= $formField->get('table_rows_header') ?></th>
            <?php endif; ?>
            <?php foreach ($columnDefinitions as $columnDefinition): ?>
                <th><?= $columnDefinition['title'] ?></th>
            <?php endforeach; ?>
        </tr>
        </thead>
        <tbody>
        <?php $rowCounter = 0;
        foreach ($hasLabels ? $labels : range(0, ($formField->get('table_rows_count') ?? 1) - 1) as $label): ?>
            <tr>
                <?php if ($hasLabels): ?>
                    <td class="rowlabel"><?= $label ?></td>
                <?php endif ?>
                <?php $columnCounter = 0;
                foreach ($columnDefinitions as $columnDefinition): ?>
                    <td>
                        <?php
                        $valueKey = sprintf("%s.%d.%d.val", $formField->getFormControlId(), $rowCounter, $columnCounter);
                        if ($displayOnly) {
                            echo Hash::get($currentValue, $valueKey);
                        } else {
                            echo $this->Form->control($valueKey, [
                                'type' => FormFieldType::TABLE_COLUMN_TYPES_FORM_CONTROL_TYPE[intval($columnDefinition['type'])],
                                'label' => false,
                                'step' => intval($columnDefinition['type']) === 2 ? 1 : .1
                            ]);
                        }
                        $columnCounter++;
                        ?>
                    </td>
                <?php endforeach; ?>
            </tr>
            <?php
            $rowCounter++;
        endforeach;
        ?>
        </tbody>
        <?php if ($showSums): ?>
            <tfoot>
            <tr class="font-weight-bold">
                <?php $sumLabelDone = false; ?>
                <?php if ($hasLabels): ?>
                    <td><?= $sumsRowLabel ?></td>
                    <?php $sumLabelDone = true; ?>
                <?php endif; ?>
                <?php $columnCounter = 0;
                foreach ($columnDefinitions as $columnDefinition): ?>
                    <td>
                        <?php
                        if (!$sumLabelDone) {
                            echo $sumsRowLabel . ': ';
                            $sumLabelDone = true;
                        }
                        $columnType = intval($columnDefinition['type']);
                        if (is_array($currentValue) && in_array($columnType, [2, 3], true)) {
                            echo Hash::reduce($currentValue[$formField->getFormControlId()] ?? $currentValue, '{n}.' . $columnCounter . '.val', function ($carry, $item) use ($columnType) {
                                return $carry + ($columnType === 2 ? intval($item) : floatval($item));
                            });
                        }
                        $columnCounter++;
                        ?>
                    </td>
                <?php endforeach; ?>
            </tr>
            </tfoot>
        <?php endif; ?>
    </table>
<?php if (!$displayOnly): ?>
    <style type="text/css">
        .table-with-inputs tbody tr td {
            padding: 0;
        }

        .table-with-inputs tbody tr td .form-group {
            margin-bottom: 0;
        }

        .table-with-inputs tbody tr td input {
            border-radius: 0;
            border: 0;
        }

        .table-with-inputs .rowlabel {
            padding: 0.75rem;
        }
    </style>
<?php endif; ?>
