<?php
/**
 * @var $this \App\View\AppView
 * @var $noscript bool
 * @var $disablePlaceholder bool
 */

if (!isset($disablePlaceholder) || !is_bool($disablePlaceholder)) {
    $disablePlaceholder = false;
}

$this->Html->script([
    'jquery.chained.selects-2.1.0.js',
], ['block' => true]);
?>
<?php if (!isset($noscript) || $noscript !== true): ?>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.chainedSelect').chainedSelects({
                data: <?= json_encode($data ?? []) ?>,
                selectCssClass: 'form-control',
                selectedKey: <?= (isset($selectedKey) && !empty($selectedKey)) ? is_string($selectedKey) ? sprintf('"%s"', $selectedKey) : intval($selectedKey) : 'false' ?>,
                placeholder: '<?= $disablePlaceholder ? '' : __('Vyberte si prosím z nabízených možností') ?>',
                autoSelectSingleOptions: true,
                onSelectedCallback: function (id) {
                    if (typeof chained_select_callback === 'function') {
                        chained_select_callback(id);
                    }
                }
            });
        });
    </script>
<?php endif; ?>
