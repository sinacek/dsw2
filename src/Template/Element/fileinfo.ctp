<?php
/**
 * @var $this AppView
 * @var $file \App\Model\Entity\File
 * @var $field FormField
 */

use App\Model\Entity\FormField;
use App\View\AppView;
use Cake\I18n\Number;

if (empty($file) || !($file instanceof \App\Model\Entity\File)) {
    return;
}
?>
<div class="alert alert-success">
    <strong><?= $field->name ?></strong> <br/>
    <?= __('Nahraný soubor') ?>: <?= h($file->original_filename) ?>, <?= __('velikost') ?>
    : <?= Number::toReadableSize($file->filesize) ?><br/>
    <strong><?= __('Pokud vyberete nový soubor, původní bude vymazán') ?></strong>
</div>
