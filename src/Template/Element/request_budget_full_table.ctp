<?php

use App\Model\Entity\Request;
use App\View\AppView;
use Cake\I18n\Number;

/**
 * @var $this AppView
 * @var $request Request
 */


if (empty($request) || !($request instanceof Request) || empty($request->request_budget)) {
    return;
}
?>

<table class="table table-striped">
    <tbody>
    <tr>
        <td><?= __('Požadovaná výše dotace') ?></td>
        <td class="text-right"><?= Number::currency($request->request_budget->requested_amount, 'CZK') ?></td>
    </tr>
    <?php if (!empty($request->request_budget->costs)): ?>
        <tr>
            <td><?= __('Celkové náklady na projekt') ?></td>
            <td class="text-right"><?= Number::currency($request->request_budget->total_costs, 'CZK') ?></td>
        </tr>
    <?php endif; ?>
    <?php if (!empty($request->request_budget->incomes)): ?>
        <tr>
            <td><?= __('Celkové výnosy projektu') ?></td>
            <td class="text-right"><?= Number::currency($request->request_budget->total_income, 'CZK') ?></td>
        </tr>
    <?php endif; ?>
    <?php if (!empty($request->request_budget->own_sources)): ?>
        <tr>
            <td><?= __('Vlastní zdroje celkem') ?></td>
            <td class="text-right"><?= Number::currency($request->request_budget->total_own_sources, 'CZK') ?></td>
        </tr>
    <?php endif; ?>
    <?php if (!empty($request->request_budget->other_subsidies)): ?>
    <tr>
        <td><?= __('Dotace z jiných zdrojů celkem') ?></td>
        <td class="text-right"><?= Number::currency($request->request_budget->total_other_subsidy, 'CZK') ?></td>
    </tr>
    </tbody>
    <?php endif; ?>
</table>


<?php if (!empty($request->request_budget->costs)): ?>
    <strong><?= __('Položkový rozpis plánovaných nákladů') ?></strong>
    <table class="table table-striped">
        <?php if ($request->program->requires_extended_budget): ?>
            <thead>
            <tr>
                <th><?= __('Náklad') ?></th>
                <th><?= __('Komentář') ?></th>
                <th class="text-right"><?= __('Náklad celkem') ?></th>
                <th class="text-right"><?= __('Částka požadovaná') ?></th>
            </tr>
            </thead>
        <?php endif; ?>
        <tbody>
        <?php foreach ($request->request_budget->costs as $cost): ?>
            <tr>
                <td><?= $cost->description ?></td>
                <?php if ($request->program->requires_extended_budget): ?>
                    <td><?= $cost->comment ?></td>
                    <td class="text-right"><?= Number::currency($cost->original_amount, 'CZK') ?></td>
                <?php endif; ?>
                <td class="text-right"><?= Number::currency($cost->amount, 'CZK') ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
<?php endif; ?>

<?php if (!empty($request->request_budget->incomes)): ?>
    <strong><?= __('Položkový rozpis plánovaných výnosů') ?></strong>
    <table class="table table-striped">
        <tbody>
        <?php foreach ($request->request_budget->incomes as $income): ?>
            <tr>
                <td><?= $income->description ?></td>
                <td class="text-right"><?= Number::currency($income->amount, 'CZK') ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
<?php endif; ?>

<?php if (!empty($request->request_budget->own_sources)): ?>
    <strong><?= __('Vlastní zdroje (položky)') ?></strong>
    <table class="table">
        <tbody>
        <?php foreach ($request->request_budget->own_sources as $own_source): ?>
            <tr>
                <td><?= $own_source->description ?></td>
                <td class="text-right"><?= Number::currency($own_source->amount, 'CZK') ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
<?php endif; ?>

<?php if (!empty($request->request_budget->other_subsidies)): ?>
    <strong><?= __('Dotace z jiných zdrojů (položky)') ?></strong>
    <table class="table">
        <tbody>
        <?php foreach ($request->request_budget->other_subsidies as $subsidy): ?>
            <tr>
                <td><?= $subsidy->description ?></td>
                <td class="text-right"><?= Number::currency($subsidy->amount, 'CZK') ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
<?php endif; ?>
