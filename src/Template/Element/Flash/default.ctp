<?php
/**
 * @var $message string
 * @var $params array
 */
$class = 'message';
if (!empty($params['class'])) {
    $class .= ' ' . $params['class'];
}
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<div class="alert <?= h($class) ?>" onclick="this.classList.add('d-none');"><?= $message ?></div>
