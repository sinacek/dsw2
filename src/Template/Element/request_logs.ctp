<?php

use App\Model\Entity\Request;
use App\Model\Entity\RequestState;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $request Request
 */

$request->loadLogs();
if (empty($request->request_logs)) {
    return;
}
?>

<div class="card mt-2">
    <h5 class="card-header">
        <?= __('Historie žádosti') ?>
    </h5>
    <div class="card-body table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th><?= __('ID') ?></th>
                <th><?= __('Datum / Čas') ?></th>
                <th><?= __('Stav žádosti') ?></th>
                <th><?= __('Kdo provedl změnu žádosti') ?></th>
                <th><?= __('Komentář formální kontroly') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($request->request_logs as $request_log): ?>
                <tr>
                    <td><?= $request_log->id ?></td>
                    <td><?= $request_log->created->nice() ?></td>
                    <td><?= RequestState::getLabelByStateId($request_log->request_state_id) ?></td>
                    <td><?= $request_log->user ? $request_log->user->email : ($request_log->executed_by_user_id === null ? __('Automaticky') : sprintf(__('Uživatel č. %d'), $request_log->executed_by_user_id)) ?></td>
                    <td><?= $request_log->lock_comment ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
