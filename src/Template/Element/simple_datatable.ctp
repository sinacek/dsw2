<?php
/**
 * @var $this \App\View\AppView)
 * @var $noscript bool
 */

$this->Html->css([
    'jquery.dataTables-1.10.20.min.css',
    'buttons.dataTables-1.6.2.min.css'
], ['block' => true]);
$this->Html->script([
    'jquery.dataTables-1.10.20.min.js',
    'dataTables.buttons-1.6.2.min.js',
    'buttons.html5-1.6.2.min.js',
    'buttons.flash-1.6.2.min.js',
    'buttons.colVis-1.6.2.min.js',
    'buttons.print-1.6.2.min.js',
    'jszip-3.1.3.min.js',
    'pdfmake-0.1.53.min.js',
    'vfs_fonts-0.1.53.js',
], ['block' => true]);
?>
<?php if (!isset($noscript) || $noscript !== true): ?>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#dtable").DataTable({
                dom: "<'border-top pb-2'><'row'<'col'l><'col'B><'col'f>>rtip",
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                pageLength: 200,
                lengthMenu: [20, 50, 100, 150, 200, 250, 300, 500, 1000],
                stateSave: true,
            });
        });
    </script>
<?php endif; ?>