<?php
/**
 * @var $fonds Fond[]
 */

use App\Model\Entity\Fond;

$fondInfo = [];
$realmInfo = [];
$programInfo = [];

if ($fonds->count() < 1) {
    return;
}

?>
<div id="public-fonds-detail" class="m-2 border p-2 rounded">
    <?= __('Vyberte si ze zobrazené struktury dotačních fondů') ?>
</div>
<div id="public-fonds" class="m-2">
</div>
<?php
$tree = [];
foreach ($fonds as $fond) {
    $realms = [];
    $fondInfo[$fond->id] = ['name' => $fond->name, 'description' => $fond->description];
    foreach ($fond->realms as $realm) {
        $programs = [];
        $realmInfo[$realm->id] = ['name' => $realm->name, 'description' => $realm->description, 'fond' => $realm->fond_id];
        foreach ($realm->programs as $program) {
            $programInfo[$program->id] = ['name' => $program->name, 'description' => $program->description, 'realm' => $program->realm_id, 'parent' => intval($program->parent_id)];
            if ($program->parent_id) {
                continue;
            }
            $nodes = [];
            foreach ($program->child_programs as $childProgram) {
                $programInfo[$childProgram->id] = ['name' => $childProgram->name, 'description' => $childProgram->description, 'realm' => $childProgram->realm_id ?? $program->realm_id, 'parent' => intval($childProgram->parent_id)];
                $nodes[] = [
                    'text' => $childProgram->name,
                    'icon' => 'fa-info fas mr-2',
                    'selectedIcon' => 'fa-info-circle fas mr-2',
                    'tags' => ['program', 'id-' . $childProgram->id]
                ];
            }
            $programs[] = [
                'text' => $program->name,
                'nodes' => empty($nodes) ? null : $nodes,
                'selectedIcon' => 'fa-info-circle fas ml-2 mr-2',
                'selectable' => empty($nodes),
                'icon' => empty($nodes) ? 'fa-info fas mr-2' : '',
                'tags' => ['program', 'id-' . $program->id]
            ];
        }
        if ($realm->is_hidden) {
            $realms = array_merge($realms, $programs);
        } else {
            $realms[] = [
                'text' => $realm->name,
                'nodes' => $programs,
                'selectedIcon' => 'fa-info-circle fas ml-2 mr-2',
                'selectable' => false,
                'tags' => ['realm']
            ];
        }
    }
    if ($fond->is_hidden) {
        $tree = array_merge($tree, $realms);
    } else {
        $tree[] = [
            'text' => $fond->name,
            'nodes' => $realms,
            'selectedIcon' => 'fa-info-circle fas ml-2 mr-2',
            'selectable' => false,
            'tags' => ['fond']
        ];
    }
}
?>
<script type="text/javascript">
    var treeData = <?= json_encode($tree) ?>;
    var fonds = <?= json_encode($fondInfo) ?>;
    var realms = <?= json_encode($realmInfo) ?>;
    var programs = <?= json_encode($programInfo) ?>;

    function showProgram(program_id) {
        var program = programs[program_id];
        var parent = programs[program.parent];
        var realm = realms[program.realm];
        var fond = fonds[realm.fond];
        var $target = $("#public-fonds-detail");
        $target.empty();
        $target.append(
            '<h2>' + program.name + "</h2>" +
            '<p>' + program.description + '</p>' + '<hr/>' +
            (parent ? '<h3>' + parent.name + '</h3>' + '<p>' + parent.description + '</p>' + '<hr/>' : '') +
            '<h3>' + realm.name + '</h3>' +
            '<p>' + realm.description + '</p>'
        );
        $(window).scrollTo($target, {duration: 800});
    }

    $(function () {
        var $target = $("#public-fonds");
        $target.treeview({
            data: treeData,
            selectable: false,
            onNodeSelected: function (event, data) {
                var tags = data.tags;
                if (tags.indexOf("program") >= 0) {
                    var id = tags[1].split('-')[1];
                    showProgram(id);
                }
            }
        });
        $target.treeview('expandAll');
    });
</script>
