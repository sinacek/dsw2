<?php

use App\Model\Entity\Identity;
use App\Model\Entity\Request;
use App\Model\Entity\User;
use App\View\AppView;
use Cake\I18n\FrozenDate;
use Cake\Log\Log;
use Cake\Utility\Hash;

/**
 * @var $this AppView
 * @var $user User
 * @var $version int
 * @var $request Request
 */

if (empty($user) || !($user instanceof User) || (empty($version) && empty($request->user_identity_version))) {
    return;
}

$request->loadIdentities();

$flattened = [];
foreach ($request->identities as $identity) {
    $flattened[$identity->name] = $identity->value;
}
$flattened = Hash::expand($flattened);

$isPo = boolval(Hash::get($flattened, Identity::IS_PO, 0));
$poRegistrationInformation = '';
if ($isPo) {
    $registeredWhen = Hash::get($flattened, Identity::PO_REGISTRATION_SINCE);
    try {
        $registeredDate = FrozenDate::parseDate($registeredWhen, 'Y-m-d');
        $registeredWhen = ($registeredDate ? ', ' . $registeredDate->nice() : $registeredWhen);
    } catch (Throwable $e) {
        Log::debug($e);
        $registeredWhen = empty($registeredWhen) ? '' : ', ' . $registeredWhen;
    }
    $registeredWhere = Hash::get($flattened, Identity::PO_REGISTRATION_DETAILS);
    $poRegistrationInformation = sprintf("%s%s", trim($registeredWhere), $registeredWhen);
}

$appView = ($this instanceof AppView) ? $this : new AppView($this->getRequest(), $this->getResponse(), $this->getEventManager());

$residenceCity = $appView->getCityName(intval(Hash::get($flattened, Identity::RESIDENCE_ADDRESS_CITY, null)));
$residenceCityPart = $appView->getCityPartName(intval(Hash::get($flattened, Identity::RESIDENCE_ADDRESS_CITY_PART, null)));
$legalFormName = $appView->getLegalFormName(intval(Hash::get($flattened, Identity::PO_CORPORATE_TYPE, null)));

$residenceAddress = sprintf("%s, %s %s, %s",
    Hash::get($flattened, Identity::RESIDENCE_ADDRESS_STREET),
    $residenceCity,
    $residenceCityPart,
    Hash::get($flattened, Identity::RESIDENCE_ADDRESS_ZIP)
);

$fullName = $fullName = $isPo ?
    Hash::get($flattened, Identity::PO_FULLNAME) :
    sprintf("%s %s %s %s",
        Hash::get($flattened, Identity::FO_DEGREE_BEFORE),
        Hash::get($flattened, Identity::FO_FIRSTNAME),
        Hash::get($flattened, Identity::FO_SURNAME),
        Hash::get($flattened, Identity::FO_DEGREE_AFTER)
    );
$fullNameVerified = strcmp($fullName, Hash::get($flattened, Identity::ISDS_VERIFIED_BUSINES_NAME)) === 0;

$ico = Hash::get($flattened, Identity::ISDS_VERIFIED_BUSINESS_ID) ?? Hash::get($flattened, Identity::PO_BUSINESS_ID);
$icoVerified = !empty($ico) && strcmp($ico, Hash::get($flattened, Identity::ISDS_VERIFIED_BUSINESS_ID)) === 0;

$dsId = Hash::get($flattened, Identity::CONTACT_DATABOX) ?? Hash::get($flattened, Identity::ISDS_VERIFIED_DATABOX_ID);
$dsIdVerified = !empty($dsId) && strcmp($dsId, Hash::get($flattened, Identity::ISDS_VERIFIED_DATABOX_ID)) === 0;

?>
<table class="table table-striped">
    <tbody>
    <tr>
        <td><?= __('Celé jméno') ?></td>
        <td>
            <?= $fullName ?>
            <?php if ($fullNameVerified) {
                echo '(ověřeno pomocí DS)';
            } ?>
        </td>
    </tr>
    <tr>
        <td><?= $isPo ? __('Sídlo právnické osoby') : __('Trvalá adresa') ?></td>
        <td><?= $residenceAddress ?></td>
    </tr>
    <tr>
        <td><?= __('Právní forma') ?></td>
        <td><?= $legalFormName ?></td>
    </tr>
    <?php if ($isPo): ?>
        <tr>
            <td><?= __('IČO') ?></td>
            <td>
                <?= $ico ?>
                <?php if ($icoVerified) {
                    echo '(ověřeno pomocí DS)';
                } ?>
            </td>
        </tr>
        <tr>
            <td><?= __('Registrace právnické osoby (kde, kdy)') ?></td>
            <td><?= $poRegistrationInformation ?></td>
        </tr>
    <?php endif; ?>
    <tr>
        <td><?= __('DIČ') ?></td>
        <td><?= Hash::get($flattened, $isPo ? Identity::PO_VAT_ID : Identity::FO_VAT_ID) ?></td>
    </tr>
    <?php if ($dsId): ?>
        <tr>
            <td><?= __('ID Datové Schránky') ?></td>
            <td>
                <?= $dsId ?>
                <?php if ($dsIdVerified) {
                    echo '(ověřeno pomocí DS)';
                } ?>
            </td>
        </tr>
    <?php endif; ?>
    <tr>
        <td><?= __('Kontaktní informace') ?></td>
        <td><?= Hash::get($flattened, Identity::CONTACT_EMAIL) ?>
            , <?= Hash::get($flattened, Identity::CONTACT_PHONE) ?></td>
    </tr>
    <?php if ($isPo && Identity::mustProvideStatutory(Hash::get($flattened, Identity::PO_CORPORATE_TYPE))): ?>
        <tr>
            <td><?= __('Statutární orgán zastupující žadatele') ?></td>
            <td>
                <?= sprintf("%s - %s, %s, %s",
                    Hash::get($flattened, Identity::STATUTORY_FULLNAME),
                    Hash::get($flattened, Identity::STATUTORY_REPRESENTATION_REASON),
                    Hash::get($flattened, Identity::STATUTORY_PHONE),
                    Hash::get($flattened, Identity::STATUTORY_EMAIL)
                ) ?>
            </td>
        </tr>
    <?php endif; ?>
    <tr>
        <td><?= __('Bankovní spojení') ?></td>
        <td>
            <?= sprintf("%s / %s (%s)",
                Hash::get($flattened, Identity::BANK_NUMBER),
                Hash::get($flattened, Identity::BANK_CODE),
                Hash::get($flattened, Identity::BANK_NAME)) ?>
        </td>
    </tr>
    <tr>
        <td>
            <?= __('Osoby s podílem v této právnické osobě') ?>
        </td>
        <td>
            <?php $po_interests = Identity::extractPOInterests($flattened); ?>
            <?php if (!empty($po_interests)): ?>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th><?= __('Velikost podílu') ?></th>
                        <th><?= __('Vlastník podílu') ?></th>
                        <th><?= __('IČO / Dat. narození') ?></th>
                    </tr>
                    </thead>
                    <?php foreach ($po_interests as $po_interest): ?>
                        <tr>
                            <td><?= Hash::get($po_interest, 'size') ?></td>
                            <td><?= Hash::get($po_interest, 'owner') ?></td>
                            <td><?= sprintf("%s / %s", Hash::get($po_interest, 'business_id'), Hash::get($po_interest, 'date_of_birth')) ?></td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            <?php endif; ?>
        </td>
    </tr>
    <tr>
        <td>
            <?= __('Právnické osoby v nichž má žadatel přímý podíl') ?>
        </td>
        <td>
            <?php $owned_interests = Identity::extractOwnedInterests($flattened); ?>
            <?php if (!empty($owned_interests)): ?>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th><?= __('Velikost podílu') ?></th>
                        <th><?= __('Název společnosti') ?></th>
                        <th><?= __('IČO') ?></th>
                    </tr>
                    </thead>
                    <?php foreach ($owned_interests as $owned_interest): ?>
                        <tr>
                            <td><?= Hash::get($owned_interest, 'size') ?></td>
                            <td><?= Hash::get($owned_interest, 'name') ?></td>
                            <td><?= Hash::get($owned_interest, 'business_id') ?></td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            <?php endif; ?>
        </td>
    </tr>
    </tbody>
</table>
