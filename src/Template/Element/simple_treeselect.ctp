<?php
/**
 * @var $this \App\View\AppView)
 * @var $noscript bool
 */

$this->Html->css([
    'jquery.tree-multiselect.2.6.1.min.css'
], ['block' => true]);
$this->Html->script([
    'jquery.tree-multiselect.2.6.1.min.js',
], ['block' => true]);
?>
<?php if (!isset($noscript) || $noscript !== true): ?>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.treeselect').each(function () {
                $(this).treeMultiselect({
                    sectionDelimiter: '||',
                    hideSidePanel: true,
                    searchable: true,
                    enableSelectAll: true,
                    selectAllText: '<?= __('Vybrat vše') ?>',
                    unselectAllText: '<?= __('Zrušit výběr') ?>',
                    startCollapsed: true
                });
            });
        });
    </script>
<?php endif; ?>
