<?php

use App\Model\Entity\Request;
use App\View\AppView;
use Cake\I18n\Number;

/**
 * @var $this AppView
 * @var $request Request
 */

if (empty($request) || !($request instanceof Request)) {
    return;
}

?>
    <style type="text/css">
        @media print {
            * {
                page-break-inside: avoid;
                page-break-after: avoid;
                page-break-before: avoid;
            }

            body > * {
                zoom: .75;
            }
        }

        table {
            page-break-inside: auto;
        }

        table tr {
            position: relative !important;
            float: none;
            page-break-inside: avoid;
            page-break-after: auto;
            page-break-before: auto;
        }

        thead {
            display: table-header-group
        }

        tfoot {
            display: table-footer-group
        }
    </style>

    <div class="card mt-2">
        <h5 class="card-header"><?= __('Žádost') ?></h5>
        <div class="card-body">
            <table class="table table-striped">
                <tr>
                    <td><strong><?= __('Název projektu') ?></strong></td>
                    <td><?= $request->name ?></td>
                    <td><strong><?= __('Identifikační číslo žádosti') ?></strong></td>
                    <td><?= $request->id ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Dotační program') ?></strong></td>
                    <td><?= $request->program->name ?></td>
                    <td><strong><?= __('Oblast podpory') ?></strong></td>
                    <td><?= ($request->program->realm ?? $request->program->parent_program->realm)->name ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Nadřízený dotační program') ?></strong></td>
                    <td><?= $request->program->parent_program ? $request->program->parent_program->name : '' ?></td>
                    <td><strong><?= __('Název dotační výzvy') ?></strong></td>
                    <td><?= $request->appeal->name ?></td>
                </tr>
            </table>
        </div>
    </div>

    <div class="card mt-2">
        <h5 class="card-header"><?= __('Žadatel') ?></h5>
        <div class="card-body">
            <?= $this->element('identity_full_table', ['user' => $request->user, 'request' => $request]) ?>
        </div>
    </div>

    <div class="card mt-2">
        <h5 class="card-header"><?= __('Rozpočet projektu') ?></h5>
        <div class="card-body">
            <?= $this->element('request_budget_full_table', compact('request')) ?>
        </div>
    </div>

    <div class="card mt-2">
        <h5 class="card-header"><?= __('Historie přijaté veřejné podpory') ?></h5>
        <div class="card-body">
            <?php $sorted = [];
            $startYear = intval(date('Y', strtotime('-2 years')));
            foreach ($request->user->public_income_histories as $history) {
                if ($history->fiscal_year >= $startYear) {
                    if (!isset($sorted[$history->fiscal_year])) {
                        $sorted[$history->fiscal_year] = [];
                    }
                    if (!isset($sorted[$history->fiscal_year][$history->public_income_source->source_name])) {
                        $sorted[$history->fiscal_year][$history->public_income_source->source_name] = [];
                    }
                    $sorted[$history->fiscal_year][$history->public_income_source->source_name][] = [$history->project_name, $history->amount_czk];
                }
            }
            krsort($sorted);
            ?>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th><?= __('Rok') ?></th>
                    <th><?= __('Poskytovatel') ?></th>
                    <th><?= __('Popis projektu') ?></th>
                    <th class="text-right"><?= __('Získaná podpora') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($sorted as $year => $sources): ?>
                    <?php foreach ($sources as $sourcename => $sourceitems): ?>
                        <?php foreach ($sourceitems as $data): ?>
                            <tr>
                                <td><?= $year ?></td>
                                <td><?= $sourcename ?></td>
                                <td><?= $data[0] ?></td>
                                <td class="text-right"><?= Number::currency($data[1], 'CZK') ?></td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endforeach; ?>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>

<?php foreach ($request->getForms() as $form):
    $requestForm = $form->getFormController($request);
    $requestForm->setUser($request->user);
    ?>
    <div class="card mt-2">
        <h4 class="card-header">
            <?= $requestForm->getFormName() ?>
        </h4>
        <div class="card-body">
            <?= $requestForm->render($this instanceof AppView ? $this : new AppView($this->getRequest(), $this->getResponse()), ['readOnly' => true]) ?>
        </div>
    </div>
<?php endforeach; ?>

<?php if (!empty($request->files) && $this instanceof AppView): ?>
    <div class="card mt-2 d-print-none">
        <h5 class="card-header"><?= __('Nepovinné přílohy') ?></h5>
        <div class="card-body">
            <ul class="list-group">
                <?php foreach ($request->files as $file): ?>
                    <li class="list-group-item">
                        <?php
                        $description = sprintf("%s: %s, %s: %s",
                            __('Název přílohy'),
                            h($file->original_filename),
                            __('Velikost'),
                            Number::toReadableSize($file->filesize)
                        );
                        echo $this->Html->link($description, ['action' => 'downloadAttachment', 'controller' => 'Teams', 'request_id' => $request->id, 'file_id' => $file->id], ['target' => '_blank']);
                        ?>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
<?php endif; ?>
