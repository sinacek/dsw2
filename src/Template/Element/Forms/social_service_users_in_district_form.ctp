<?php
/**
 * @var $this AppView
 * @var $form SocialServiceUsersInDistrictFormController
 * @var $request Request
 * @var $enableInput bool
 */

use App\Form\SocialServiceUsersInDistrictFormController;
use App\Model\Entity\Request;
use App\View\AppView;

$this->Form->setTemplates([
    'formGroup' => '{{input}}',
    'inputContainer' => '{{content}}'
]);
?>
<table class="table table-bordered table-with-inputs">
    <thead class="thead-dark">
    <tr>
        <th><?= __('Druh služby (vypište dle registrace)') ?></th>
        <th><?= __('Jednotka') ?></th>
        <th><?= sprintf("%s %s", __('Skutečnost'), $form->getYears()[0]) ?></th>
        <th><?= sprintf("%s %s", __('Předpoklad'), $form->getYears()[0]) ?></th>
    </tr>
    </thead>
    <tbody>
    <?php for ($i = 0; $i < 6; $i++): ?>
        <tr class="tr-inputs">
            <td class="rowspan" rowspan="2"><?= $form->fieldOrFormatted($this->Form, $i, 1) ?></td>
            <td class="text-left pl-2 pt-2"><?= __('Uživatel') ?></td>
            <td><?= $form->fieldOrFormatted($this->Form, $i, 2) ?></td>
            <td><?= $form->fieldOrFormatted($this->Form, $i, 3) ?></td>
        </tr>
        <tr class="tr-inputs">
            <td class="text-left pl-2 pt-2"><?= __('Počet hodin') ?></td>

            <td><?= $form->fieldOrFormatted($this->Form, $i, 4) ?></td>
            <td><?= $form->fieldOrFormatted($this->Form, $i, 5) ?></td>
        </tr>
    <?php endfor; ?>
    </tbody>
</table>

<style type="text/css">
    <?php if($enableInput): ?>

    .table-with-inputs * {
        border-radius: 0;
    }

    .tr-inputs td input {
        width: 100%;
        height: 100%;
        padding: .75rem;
        margin: 0;
        border-radius: 0;
        border: 0;
    }

    .tr-inputs td.rowspan input {
        padding: 2.1rem .75rem;
    }

    .tr-inputs .input-group-append, .tr-inputs .input-group-text {
        border-radius: 0;
        border: 0;
    }

    .tr-inputs td {
        padding: 0;
    }

    <?php endif; ?>

    .w-20 {
        width: 20% !important;
    }

    .w-30 {
        width: 30% !important;
    }

    .w-15 {
        width: 15% !important;
    }

    .invalid-feedback {
        text-align: center;
    }

    input.is-invalid {
        border: 1px solid red !important;
    }
</style>
