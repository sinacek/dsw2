<?php

use App\Form\SocialServiceBudgetFormController;
use App\Model\Entity\Request;
use App\View\AppView;
use Cake\I18n\Number;

/**
 * @var $this AppView
 * @var $request Request
 * @var $form SocialServiceBudgetFormController
 * @var $enableInput bool
 */

$this->Form->setTemplates([
    'formGroup' => '{{input}}',
    'inputContainer' => '{{content}}'
]);

?>

<?php if ($enableInput): ?>
    <div class="alert alert-info d-print-none">
        <?= __('Součty sekcí a procentní vyjádření se zobrazí až po uložení formuláře') ?>
    </div>
<?php endif; ?>

<table class="table table-bordered table-with-inputs">
    <colgroup>
        <col class="w-5">
        <col class="w-20">
        <col>
        <col class="w-10">
        <col>
        <col class="w-10">
        <col class="w-25">
    </colgroup>
    <thead class="thead-dark">
    <tr>
        <?php foreach ($form->getTables()['headers'] as $headerLabel => $colspan): ?>
            <th colspan="<?= $colspan ?>"><?= $headerLabel ?></th>
        <?php endforeach; ?>
    </tr>
    </thead>
    <tbody>
    <?= $form->renderSections($this->Form, $form->getTables()['sections']) ?>
    </tbody>
    <tfoot class="thead-dark">
    <tr class="sumall font-weight-bold">
        <th colspan="2"><?= __('Celkové náklady na realizaci služby (projektu)') ?></th>
        <th class="text-right"><?= $form->sumColumn(SocialServiceBudgetFormController::COLUMN_EXPECTED_EXPENSES) ?></th>
        <th class="text-center"><?= Number::toPercentage(100) ?></th>
        <th class="text-right"><?= $form->sumColumn(SocialServiceBudgetFormController::COLUMN_SUBSIDY_REQUEST) ?></th>
        <th class="text-center"><?= $form->getOverallRatio() ?></th>
        <th></th>
    </tr>
    </tfoot>
</table>

<script type="text/javascript">
    $(function () {

    });
</script>

<style type="text/css">

    .tr-sums, .sumall {
        background-color: #EDEDED;
    }

    <?php if($enableInput): ?>

    .table-with-inputs * {
        border-radius: 0;
    }

    .tr-inputs td input {
        width: 100%;
        height: 100%;
        font-size: 1.3rem;
        padding: .5rem;
        margin: 0;
        border-radius: 0;
        border: 0;
    }

    .tr-inputs .input-group-append, .tr-inputs .input-group-text {
        border-radius: 0;
        border: 0;
    }

    .tr-inputs td:nth-of-type(1), .tr-inputs td:nth-of-type(2), .tr-inputs td:nth-of-type(4) {
        padding: .5rem .75rem;
    }

    .tr-inputs td {
        padding: 0;
        margin: 0;
    }

    .text-right input {
        text-align: right;
    }

    <?php endif; ?>

    .w-5 {
        width: 5% !important;
    }

    .w-10 {
        width: 10% !important;
    }

    .w-20 {
        width: 20% !important;
    }

    .w-30 {
        width: 30% !important;
    }

    .w-15 {
        width: 15% !important;
    }
</style>
