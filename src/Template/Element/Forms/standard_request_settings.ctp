<?php
/**
 * @var $this AppView
 * @var $form AbstractFormController
 */

use App\Form\AbstractFormController;
use App\View\AppView;

$formID = $form->getFormDefinition()->id;

?>

<table class="table">
    <tbody>
    <?php foreach ($form->getFieldsInOrder() as $field): ?>
        <tr>
            <td>
                <?= $this->renderFormField($field); ?>
            </td>
            <td>
                <?= $this->Html->link(__('Upravit'), ['action' => 'fieldAddModify', $formID, $field->id]) ?>,
                <?= $this->Html->link(__('Smazat'), ['action' => 'fieldDelete', $formID, $field->id], ['class' => 'text-danger']) ?>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
