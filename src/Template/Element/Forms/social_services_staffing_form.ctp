<?php

use App\Form\SocialServicesStaffingFormController;
use App\Model\Entity\Request;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $form SocialServicesStaffingFormController
 * @var $request Request
 * @var $enableInput bool
 */

// for tables in this template disable wrapping <input> and displaying labels/helpers/...
$this->Form->setTemplates([
    'formGroup' => '{{input}}',
    'inputContainer' => '{{content}}'
]);

?>

<?php foreach ($form->getTables() as $tableName => $tableSettings): ?>

    <table class="table table-bordered table-with-inputs" id="sumtable-<?= $tableName ?>"
           data-table="<?= $tableName ?>">
        <colgroup>
            <?php foreach ($tableSettings['colgroup'] ?? [] as $colClass): ?>
                <col class="<?= $colClass ?>">
            <?php endforeach; ?>
        </colgroup>
        <thead class="thead-dark text-center">
        <tr>
            <?php foreach ($tableSettings['thead'] ?? [] as $index => $theadContent): ?>
                <th class="<?= $index === 0 ? 'h3' : '' ?>">
                    <?= $theadContent ?>
                </th>
            <?php endforeach; ?>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($tableSettings['groups'] ?? ['default'] as $groupName => $groupLabel): ?>
            <?php
            $onlyHeader = false;
            if (is_array($groupLabel)) {
                $onlyHeader = true;
                $groupLabel = $groupLabel['label'];
            }
            ?>
            <tr class="bg-light text-center font-weight-bold" data-group="<?= $groupName ?>" data-type="groupsum">
                <td class="text-nowrap text-left">
                    <?= $groupLabel ?>
                </td>
                <?php foreach (array_slice($tableSettings['columns'], 1) as $columnInputName => $columnInputOptions): ?>
                    <td data-column="<?= $columnInputName ?>"><?= $form->getSum($tableName, $columnInputName, $groupName) ?></td>
                <?php endforeach; ?>
            </tr>
            <?php if ($onlyHeader === true) {
                continue;
            } ?>
            <?php for ($i = 0; $i <= 100; $i++): ?>
                <tr class="<?= $enableInput ? 'tr-inputs' : '' ?> <?= $i >= $form->getTableFilledRowsCount($tableName, $groupName) ? 'd-none' : 'visible' ?>"
                    data-group="<?= $groupName ?>"
                    data-type="inputs">
                    <?php
                    foreach ($tableSettings['columns'] as $columnInputName => $columnInputOptions) {
                        ?>
                        <td>
                            <?php
                            $template = null;
                            if (isset($columnInputOptions['template'])) {
                                $template = $columnInputOptions['template'];
                                unset($columnInputOptions['template']);
                            }
                            $inputDataKey = sprintf("%s.%s.%d.%s", $tableName, $groupName, $i, $columnInputName);
                            if ($enableInput) {
                                $formControl = $this->Form->control($inputDataKey, $columnInputOptions + ['data-column' => $columnInputName]);
                            } else {
                                $formControl = $form->getFormattedData($inputDataKey, $tableName, $columnInputName);
                            }
                            if ($enableInput && !empty($template)) {
                                echo sprintf($template, $formControl);
                            } else {
                                echo $formControl;
                            }
                            ?>
                        </td>
                        <?php
                    }
                    ?>
                </tr>
            <?php endfor; ?>

            <?php if ($enableInput): ?>
                <tr class="tr-inputs" data-group="<?= $groupName ?>" data-type="addrow">
                    <td colspan="p-0 m-0">
                        <?= $this->Html->link('<i class="fas fa-plus-circle"></i>', '#', ['class' => 'btn btn-success border-0 w-50 float-left plus', 'escape' => false, 'data-group' => $groupName]) ?>
                        <?= $this->Html->link('<i class="fas fa-minus-circle"></i>', '#', ['class' => 'btn btn-danger border-0 w-50 float-right minus', 'escape' => false, 'data-group' => $groupName]) ?>
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            <?php endif; ?>
        <?php endforeach; ?>
        </tbody>
        <tfoot class="thead-dark">

        </tfoot>
    </table>
    <hr/>
<?php endforeach; ?>

<script type="text/javascript">
    const TABLES = <?= json_encode($form->getTables()) ?>;

    function formatFloat(value, sumFunction) {
        switch (sumFunction) {
            case 'currency':
                value = value.toFixed(2) + " Kč";
                break;
            case 'decimal':
                value = value.toFixed(2);
                break;
        }
        return value;
    }

    $(function () {
        $("tr[data-type=addrow] a.plus").click(function ($event) {
            $event.preventDefault();
            let $targetGroup = $(this).data('group');
            let $targetRow = $("tr[data-group=" + $targetGroup + "][data-type=inputs].d-none").first();
            $targetRow.toggleClass('d-none').toggleClass('visible');
            $("input[type=number]", $targetRow).trigger('change');
        });
        $("tr[data-type=addrow] a.minus").click(function ($event) {
            $event.preventDefault();
            let $targetGroup = $(this).data('group');
            let $targetRow = $("tr[data-group=" + $targetGroup + "][data-type=inputs].visible").last();
            $targetRow.toggleClass('d-none').toggleClass('visible');
            $("input", $targetRow).not("[type=number]").val("");
            $("input[type=number]", $targetRow).trigger('change');
        });

        $("input[type=number]").change(function () {
            let $targetRow = $(this).closest('tr[data-type=inputs]')
            let $targetGroup = $targetRow.data('group');
            let $targetCol = $(this).data('column');
            let $targetProvides = $(this).data('fraction-provider');
            let $currentValue = Number.parseFloat($(this).val());
            let $targetTable = $(this).closest('table').data('table');
            let $targetSum = $('tr[data-type=groupsum][data-group=' + $targetGroup + '] > td[data-column=' + $targetCol + ']').first();
            let $sumFunction = TABLES[$targetTable]['sums'][$targetCol];
            if ($sumFunction === false) {
                return;
            }
            let $sum = 0;
            $('tr[data-group=' + $targetGroup + '].visible input[data-column=' + $targetCol + ']').each(function () {
                let $parsed = parseFloat($(this).val());
                $sum += isNaN($parsed) ? 0 : $parsed;
            });
            $targetSum.text(formatFloat($sum, $sumFunction));

            if ($targetProvides) {
                $("input[data-fraction-source=" + $targetProvides + "]", $targetRow).each(function () {
                    let $fractionOf = Number.parseFloat($(this).data('fraction-of'));
                    $(this).val(Number.parseFloat(($currentValue / $fractionOf)).toFixed(2));
                });
            }

            // update all parent groups if any
            $.each(TABLES[$targetTable]['groups'], function (index, value) {
                if (typeof value !== 'string') {
                    let $isParent = false;
                    $.each(value['children'], function (ch_index, ch_value) {
                        if ("" + ch_value === "" + $targetGroup) {
                            $isParent = true;
                        }
                    });
                    if ($isParent) {
                        let $parentSum = 0;
                        $.each(value['children'], function (ch_index, ch_value) {
                            $('table[data-table=' + $targetTable + '] tr[data-group=' + ch_value + '].visible input[data-column=' + $targetCol + ']').each(function () {
                                let $parsed = parseFloat($(this).val());
                                $parentSum += isNaN($parsed) ? 0 : $parsed;
                            });
                        });
                        let $targetParentSum = $('tr[data-type=groupsum][data-group=' + index + '] > td[data-column=' + $targetCol + ']');
                        $targetParentSum.text(formatFloat($parentSum, $sumFunction));
                    }
                }
            });
        });
    });
</script>


<style type="text/css">
    <?php if($enableInput): ?>

    .table-with-inputs * {
        border-radius: 0;
    }

    .tr-inputs td input {
        width: 100%;
        height: 100%;
        padding: 0.75rem;
        margin: 0;
        border-radius: 0;
        border: 0;
    }

    .tr-inputs .input-group-append, .tr-inputs .input-group-text {
        border-radius: 0;
        border: 0;
    }

    .tr-inputs td {
        padding: 0;
    }

    <?php endif; ?>

    .w-20 {
        width: 20% !important;
    }

    .w-30 {
        width: 30% !important;
    }

    .w-15 {
        width: 15% !important;
    }
</style>
