<?php

use App\Form\SocialServiceBalanceSheetFormController;
use App\Model\Entity\Request;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $form SocialServiceBalanceSheetFormController
 * @var $request Request
 * @var $enableInput bool
 */

// for tables in this template disable wrapping <input> and displaying labels/helpers/...
$this->Form->setTemplates([
    'formGroup' => '{{input}}',
    'inputContainer' => '{{content}}'
]);
$columnCount = count($form->getTable()['columns']) - 1;
$rowCounter = 0;
?>

<?php if ($enableInput): ?>
    <div class="alert alert-info d-print-none">
        <?= __('Součty sloupců se objeví až po uložení formuláře') ?>
    </div>
<?php endif; ?>

<table class="table table-bordered table-with-inputs">
    <colgroup>
        <col class="w-20">
        <col class="w-15">
        <col class="w-15">
        <col class="w-15">
        <col class="w-15">
        <col class="w-15">
    </colgroup>
    <thead class="thead-dark text-center">
    <tr>
        <?php foreach ($form->getTable()['columns'] as $columnLabel): ?>
            <th><?= $columnLabel ?></th>
        <?php endforeach; ?>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($form->getTable()['rows'] as $rowLabel): ?>
        <tr class="tr-inputs">
            <td class="font-weight-bold p-3"><?= $rowLabel ?></td>
            <?php for ($i = 0; $i < $columnCount; $i++): ?>
                <td class="text-right"><?= $form->fieldOrFormatted($this->Form, $rowCounter, $i) ?></td>
            <?php endfor; ?>
        </tr>
        <?php $rowCounter++; ?>
    <?php endforeach; ?>
    </tbody>
    <tfoot class="thead-dark">
    <tr class="">
        <th><?= $form->getTable()['sumrow'] ?></th>
        <?php for ($i = 0; $i < $columnCount; $i++): ?>
            <td class="text-right colsum-<?= $i ?>"><?= $form->getColumnSum($i) ?></td>
        <?php endfor; ?>
    </tr>
    </tfoot>
</table>

<style type="text/css">
    <?php if($enableInput): ?>

    .table-with-inputs * {
        border-radius: 0;
    }

    .tr-inputs td input {
        width: 100%;
        height: 100%;
        padding: 1.75rem;
        margin: 0;
        border-radius: 0;
        border: 0;
    }

    .tr-inputs .input-group-append, .tr-inputs .input-group-text {
        border-radius: 0;
        border: 0;
    }

    .tr-inputs td {
        padding: 0;
    }

    <?php endif; ?>

    .w-20 {
        width: 20% !important;
    }

    .w-30 {
        width: 30% !important;
    }

    .w-15 {
        width: 15% !important;
    }
</style>
