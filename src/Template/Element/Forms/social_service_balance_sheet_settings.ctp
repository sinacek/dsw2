<?php
/**
 * @var $this AppView
 * @var $form SocialServiceBalanceSheetFormController
 */

use App\Form\SocialServiceBalanceSheetFormController;
use App\View\AppView;

?>

<?= $this->asHtmlListRecursive([
    'Rozpočet služby (projektu) podle jednotlivých zdrojů financování',
]); ?>

    <hr/>

<?php
echo $this->Form->create($form->getFormDefinition());
?>
    <h2><?= __('Nastavení sloupců tabulky') ?></h2>
<?php
echo $this->Form->control('form_settings.0.id', ['value' => $form->getSetting(SocialServiceBalanceSheetFormController::COLUMN_1_YEAR)->id]);
echo $this->Form->control('form_settings.0.value', ['type' => 'number', 'label' => __('Rok pro první sloupec tabulky')]);
echo $this->Form->control('form_settings.1.id', ['value' => $form->getSetting(SocialServiceBalanceSheetFormController::COLUMN_2_YEAR)->id]);
echo $this->Form->control('form_settings.1.value', ['type' => 'number', 'label' => __('Rok pro druhý sloupec tabulky')]);
echo $this->Form->control('form_settings.2.id', ['value' => $form->getSetting(SocialServiceBalanceSheetFormController::COLUMN_3_YEAR)->id]);
echo $this->Form->control('form_settings.2.value', ['type' => 'number', 'label' => __('Rok pro třetí až poslední sloupce tabulky')]);
?>
    <h2><?= __('Nastavení řádků tabulky') ?></h2>
    <span class="text-danger">
    <?= __('Pozor, pokud již byl formulář vyplněn ( v rámci podané žádosti ), a změníte zde uvedené řádky, může dojít ke ztrátě nebo poškození dat vyplněných formulářů') ?>
</span>
<?php
echo $this->Form->control('form_settings.3.id', ['value' => $form->getSetting(SocialServiceBalanceSheetFormController::SETTING_ROWS)->id]);
echo $this->Form->control('form_settings.3.value', ['value' => str_replace(';', PHP_EOL, $form->getSettingValue(SocialServiceBalanceSheetFormController::SETTING_ROWS)), 'label' => __('Jednotlivé řádky tabulky ve správném pořadí'), 'maxlength' => false, 'type' => 'textarea', 'data-noquilljs' => 'data-noquilljs']);
echo $this->Form->submit(__('Uložit nastavení'), ['class' => 'btn btn-success']);
echo $this->Form->end();

?>
