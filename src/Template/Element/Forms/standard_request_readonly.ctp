<?php
/**
 * @var $this AppView
 * @var $requestForm StandardRequestFormController
 */

use App\Form\StandardRequestFormController;
use App\Model\Entity\FormFieldType;
use App\View\AppView;
use Cake\I18n\Number;

?>
<table class="table">
    <tbody>
    <?php
    foreach ($requestForm->getFieldsInOrder() as $field) {

        $field_description = $field->name;
        $field_value = '';

        $parsedValue = $requestForm->getData($field->getFormControlId());
        $descriptionSubtitle = '';
        $descriptionSubtitleWithSeparator = '';

        if (!empty(trim(strip_tags($field->description)))) {
            $descriptionSubtitle = sprintf('<span class="small">%s</span>', $field->description);
            $descriptionSubtitleWithSeparator = '<hr/>' . $descriptionSubtitle;
        }

        switch ($field->form_field_type_id) {
            default:
                $field_description = 'NOT IMPLEMENTED ' . $field->form_field_type_id;
                $field_value = json_encode($field) . json_encode($parsedValue);
                break;
            case FormFieldType::FIELD_VARCHAR:
            case FormFieldType::FIELD_TEXT:
            case FormFieldType::FIELD_INTEGER:
                $field_value = $parsedValue . $descriptionSubtitleWithSeparator;
                break;
            case FormFieldType::FIELD_DATE:
                if ($parsedValue instanceof DateTimeInterface) {
                    $field_value = $parsedValue->format('d. m. Y') . $descriptionSubtitleWithSeparator;
                }
                break;
            case FormFieldType::FIELD_DATETIME:
                if ($parsedValue instanceof DateTimeInterface) {
                    $field_value = $parsedValue->format('d. m. Y H:i:s') . $descriptionSubtitleWithSeparator;
                }
                break;
            case FormFieldType::FIELD_DOUBLE:
                $field_value = Number::format($parsedValue, ['places' => 2]) . $descriptionSubtitleWithSeparator;
                break;
            case FormFieldType::FIELD_YES_NO:
                $field_value = $parsedValue === 'no' ? 'NE' : 'ANO';
                $field_value .= $descriptionSubtitleWithSeparator;
                break;
            case FormFieldType::FIELD_CHECKBOX:
                $field_value = boolval($parsedValue) ? 'ANO' : 'NE';
                $field_value .= $descriptionSubtitleWithSeparator;
                break;
            case FormFieldType::FIELD_CHOICES:
                $field_value = $requestForm->getRawValue($field->getFormControlId()) . $descriptionSubtitleWithSeparator;
                break;
            case FormFieldType::FIELD_FILE:
                $file = $requestForm->getCurrentlyAssignedFileOrNull($field);
                if (!empty($file)) {
                    $field_value = sprintf("%s: %s, %s: %s",
                        __('Název přílohy'),
                        h($file->original_filename),
                        __('Velikost'),
                        Number::toReadableSize($file->filesize)
                    );
                }
                break;
            case FormFieldType::FIELD_TITLE_NOT_INTERACTIVE:
            case FormFieldType::FIELD_TEXT_NOT_INTERACTIVE:
                $field_value .= $descriptionSubtitle;
                break;
        }

        // output

        switch ($field->form_field_type_id) {
            default:
                echo '<tr><td class="w-30">' . $field_description . '</td><td class="w-70">' . $field_value . '</td></tr>';
                break;
            case FormFieldType::FIELD_TITLE_NOT_INTERACTIVE:
            case FormFieldType::FIELD_TEXT_NOT_INTERACTIVE:
                echo '<tr><td colspan="2">' . $field_description . $field_value . '</td></tr>';
                break;
            case FormFieldType::FIELD_TABLE:
                echo '<tr><td class="w-30">' . $field->name . '</td><td>';
                echo $this->element('form_field_table', ['formField' => $field, 'currentValue' => [$field->getFormControlId() => $parsedValue], 'displayOnly' => true]);
                echo '</td></tr>';
        }
    }
    ?>
    </tbody>
</table>
<style type="text/css">
    .w-30 {
        width: 30% !important;
    }

    .w-70 {
        width: 70% !important;
    }
</style>
