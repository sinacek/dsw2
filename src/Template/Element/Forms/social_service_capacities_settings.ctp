<?php
/**
 * @var $this AppView
 * @var $form SocialServiceCapacitiesFormController
 */

use App\Form\SocialServiceCapacitiesFormController;
use App\View\AppView;

?>

<?= $this->asHtmlListRecursive([
    'Kapacita (pobytové služby)',
    'Kapacita (ostatní služby)',
    'Vždy s výhledem 1 rok zpět, aktuální rok a plán na dotované období',
]); ?>

<hr/>

<?php
echo $this->Form->create($form->getFormDefinition());
echo $this->Form->control('form_settings.0.id', ['value' => $form->getSetting(SocialServiceCapacitiesFormController::SETTING_COLUMN_1_YEAR)->id]);
echo $this->Form->control('form_settings.0.value', ['type' => 'number', 'label' => __('Rok pro první sloupec tabulek')]);
echo $this->Form->control('form_settings.1.id', ['value' => $form->getSetting(SocialServiceCapacitiesFormController::SETTING_COLUMN_2_YEAR)->id]);
echo $this->Form->control('form_settings.1.value', ['type' => 'number', 'label' => __('Rok pro druhý sloupec tabulek')]);
echo $this->Form->control('form_settings.2.id', ['value' => $form->getSetting(SocialServiceCapacitiesFormController::SETTING_COLUMN_3_YEAR)->id]);
echo $this->Form->control('form_settings.2.value', ['type' => 'number', 'label' => __('Rok pro třetí sloupec tabulek')]);
echo $this->Form->control('form_settings.3.id', ['value' => $form->getSetting(SocialServiceCapacitiesFormController::SETTING_COLUMN_4_YEAR)->id]);
echo $this->Form->control('form_settings.3.value', ['type' => 'number', 'label' => __('Rok pro čtvrtý sloupec tabulek')]);
echo $this->Form->control('form_settings.4.id', ['value' => $form->getSetting(SocialServiceCapacitiesFormController::SETTING_SHOW_BED_DAYS)->id]);
echo $this->Form->control('form_settings.4.value', ['checked' => $form->getSettingValue(SocialServiceCapacitiesFormController::SETTING_SHOW_BED_DAYS),
    'label' => __('V tabulce pobytové kapacity služby zobrazit řádek Lůžkodny'), 'type' => 'checkbox']);
echo $this->Form->control('form_settings.5.id', ['value' => $form->getSetting(SocialServiceCapacitiesFormController::SETTING_SHOW_FIELD_PROGRAMS_ROW)->id]);
echo $this->Form->control('form_settings.5.value', ['checked' => $form->getSettingValue(SocialServiceCapacitiesFormController::SETTING_SHOW_FIELD_PROGRAMS_ROW),
    'label' => __('V tabulce kapacity ostatních služeb zobrazit řádek Počet zájemců o terénní programy'), 'type' => 'checkbox']);
echo $this->Form->control('form_settings.6.id', ['value' => $form->getSetting(SocialServiceCapacitiesFormController::SETTING_SHOW_COLUMN_4)->id]);
echo $this->Form->control('form_settings.6.value', ['checked' => $form->getSettingValue(SocialServiceCapacitiesFormController::SETTING_SHOW_COLUMN_4),
    'label' => __('V obou tabulkách zobrazit čtvrtý sloupec (Plán na přespříští rok)'), 'type' => 'checkbox']);
echo $this->Form->submit(__('Uložit nastavení'), ['class' => 'btn btn-success']);
echo $this->Form->end();

?>
