<?php
/**
 * @var $this AppView
 * @var $bool bool
 */

use App\View\AppView;

?>
<span class="d-none"><?= empty($bool) ? '1' : '0' ?></span>
<button type="button"
        class="btn pr-2 <?= empty($bool) ? 'btn-danger' : 'btn-success' ?>">
    &nbsp;
</button>
