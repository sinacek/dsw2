<?php
/**
 * @var $this \App\View\AppView)
 * @var $noscript bool
 */

$this->Html->css([
    'select2-4.0.13.min.css'
], ['block' => true]);
$this->Html->script([
    'select2-4.0.13.full.min.js',
], ['block' => true]);
?>
<?php if (!isset($noscript) || $noscript !== true): ?>
    <script type="text/javascript">
        const lang_settings = {
            inputTooShort: function () {
                return "<?= __('Zadejte alespoň 2 znaky pro vyhledání') ?>";
            }
        }
        $(document).ready(function () {
            $('.select2').select2({
                language: lang_settings
            });
            $('.select2tags').select2({
                tags: true,
                language: lang_settings
            });
            $('.select2tagsWithSeparators').select2({
                tags: true,
                language: lang_settings,
                tokenSeparators: [',', ' ']
            });
            $('.select2search').select2({
                minimumInputLength: 2,
                language: lang_settings
            });
        });
    </script>
<?php endif; ?>
