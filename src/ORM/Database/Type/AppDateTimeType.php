<?php

namespace App\ORM\Database\Type;

use Cake\Database\Type\DateTimeType;

class AppDateTimeType extends DateTimeType
{
    protected $_format = [
        'Y-m-d H:i:s',
        'Y-m-d\TH:i:sP',
        'Y-m-d\TH:i:s',
    ];
}
