<?php
declare(strict_types=1);

namespace App\Form;

use App\Model\Entity\Form;
use App\Model\Entity\FormField;
use App\Model\Entity\FormFieldType;
use App\Model\Entity\FormSetting;
use App\Model\Entity\Request;
use App\Model\Table\FormFieldsTable;
use App\Model\Table\RequestFilledFieldsTable;
use App\View\AppView;
use Cake\Event\EventManager;
use Cake\I18n\Number;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\View\Helper\FormHelper;

class SocialServiceBalanceSheetFormController extends AbstractFormController
{

    private bool $_readOnly = false;
    private array $_tableFields = [];
    const TABLENAME = 'social_service_balance_sheet';

    const COLUMN_1_YEAR = self::TABLENAME . '.year.1';
    const COLUMN_2_YEAR = self::TABLENAME . '.year.2';
    const COLUMN_3_YEAR = self::TABLENAME . '.year.3';
    const SETTING_ROWS = self::TABLENAME . '.rows';
    static array $SETTINGS = [];

    public function __construct(Form $formDefinition, Request $userRequest = null, EventManager $eventManager = null)
    {
        self::$SETTINGS = [
            self::COLUMN_1_YEAR => date('Y', strtotime('-1 year')),
            self::COLUMN_2_YEAR => date('Y'),
            self::COLUMN_3_YEAR => date('Y', strtotime('+1 year')),
        ];
        parent::__construct($formDefinition, $userRequest, $eventManager);
    }

    public function setup(): self
    {
        /** @var FormFieldsTable $formFieldsTable */
        $formFieldsTable = $this->getTableLocator()->get('FormFields');

        /** @var FormField[] $queryExisting */
        $queryExisting = $formFieldsTable->find('all', [
            'conditions' => [
                'form_id' => $this->getFormDefinition()->id,
                'is_required' => true,
                'form_field_type_id' => FormFieldType::FIELD_TEXT,
            ],
        ]);

        $knownTables = [self::TABLENAME];
        $maxOrder = 1;

        foreach ($queryExisting as $existingField) {
            if ($existingField->name === self::TABLENAME) {
                $this->_tableFields[$existingField->name] = $existingField;
                $maxOrder = max($existingField->field_order, $maxOrder);
            } else {
                $formFieldsTable->delete($existingField);
            }
        }

        foreach ($knownTables as $tableName) {
            if (!isset($this->_tableFields[$tableName])) {
                $newField = $formFieldsTable->newEntity([
                    'form_id' => $this->getFormDefinition()->id,
                    'name' => $tableName,
                    'is_required' => true,
                    'form_field_type_id' => FormFieldType::FIELD_TEXT,
                    'field_order' => $maxOrder++,
                ]);
                if ($formFieldsTable->save($newField)) {
                    $this->_tableFields[$tableName] = $newField;
                }
            }
        }

        $formSettingsTable = $this->getTableLocator()->get('FormSettings');

        foreach (self::$SETTINGS as $settingName => $settingDefaultValue) {
            $this->_settings[$settingName] = $formSettingsTable->findOrCreate([
                'form_id' => $this->getFormDefinition()->id,
                'name' => $settingName,
            ], function (FormSetting $entity) use ($settingDefaultValue) {
                $entity->value = $settingDefaultValue;
            });
        }
        $this->_settings[self::SETTING_ROWS] = $formSettingsTable->findOrCreate([
            'form_id' => $this->getFormDefinition()->id,
            'name' => self::SETTING_ROWS,
        ], function (FormSetting $entity) {
            $entity->value = join(';', $this->getDefaultRows());
        });

        return $this;
    }

    public function prefill(Request $request): self
    {
        $this->setUserRequest($request);

        foreach ($this->getFilledFields($request->id) as $field) {
            $definitionField = $this->getFieldById($field->form_field_id);
            if (!empty($definitionField) && $definitionField->name === self::TABLENAME) {
                $this->_rawValues[$definitionField->name] = $field->value;
                $this->_data = json_decode($field->value, true);
            }
        }

        return $this;
    }

    protected function _execute(array $data)
    {
        /** @var RequestFilledFieldsTable $filledFieldsTable */
        $filledFieldsTable = TableRegistry::getTableLocator()->get('RequestFilledFields');

        foreach ([self::TABLENAME] as $tableName) {
            $filledTable = $filledFieldsTable->findOrCreate([
                'form_id' => $this->getFormDefinition()->id,
                'request_id' => $this->getUserRequest()->id,
                'form_field_id' => $this->_tableFields[$tableName]->id,
            ]);
            $filledTable->value = json_encode($data ?? []);

            if (!$filledFieldsTable->save($filledTable)) {
                return false;
            }
        }

        return true;
    }

    public function render(AppView $appView, array $options = []): ?string
    {
        $returnAsString = isset($options['returnAsString']) ? $options['returnAsString'] === true : false;
        $this->_readOnly = Hash::get($options, 'readOnly') === true;

        $return = $appView->element('Forms/social_service_balance_sheet_form', [
            'form' => $this,
            'request' => $this->getUserRequest(),
            'enableInput' => !$this->_readOnly,
        ]);

        if ($returnAsString) {
            return $return;
        }

        echo $return;

        return null;
    }

    public function fieldOrFormatted(FormHelper $formHelper, int $rowNumber, int $colNumber): string
    {
        $dataKey = sprintf("r%d.c%d", $rowNumber, $colNumber);
        if ($this->_readOnly) {
            return Number::currency($this->getData($dataKey) ?? 0, 'CZK');
        } else {
            return sprintf(
                '<div class="input-group">%s<div class="input-group-append"><span class="input-group-text">Kč</span></div></div>',
                $formHelper->control($dataKey, ['type' => 'number', 'step' => .01, 'default' => 0])
            );
        }
    }

    public function getColumnSum(int $colNumber): string
    {
        $sum = 0;
        for ($rowCounter = 0; $rowCounter < count($this->getTable()['rows']); $rowCounter++) {
            $sum += floatval($this->getData(sprintf("r%d.c%d", $rowCounter, $colNumber)));
        }

        return Number::currency($sum, 'CZK');
    }

    public function getDefaultRows()
    {
        return [
            __('Dotace MČ Praha 3 z Fondu sociálního a zdravotního'),
            __('MČ Praha 3 – ostatní zdroje'),
            __('HMP'),
            __('MČ Praha 1, 2, 8, 9, 10'),
            __('Ostatní MČ'),
            __('Příspěvek zřizovatele'),
            __('Úhrady od uživatele/ klientů - příspěvek na péči'),
            __('Ostatní úhrady od uživatelů (strava, ubyt., apod.)'),
            __('Úhrady za fakultativní služby'),
            __('Ostatní kraje'),
            __('Dotace MPSV'),
            __('Ministerstvo zdravotnictví'),
            __('Ostatní resorty státní správy'),
            __('Meziresortní rady vlády (komise a výbory)'),
            __('Úřady práce'),
            __('Fondy zdrav. pojišťoven'),
            __('Nadace zahraniční i tuzemské'),
            __('Sbírky'),
            __('Sponzorské dary'),
            __('Prostředky strukturálních fondů EU'),
            __('Ostatní'),
        ];
    }

    public function getTable()
    {
        $defaultColumns = [
            __('Rozpočet služby (projektu) podle jednotlivých zdrojů financování'),
            sprintf("%s %s", __('Skutečnost'), $this->getSettingValue(self::COLUMN_1_YEAR, self::$SETTINGS[self::COLUMN_1_YEAR])),
            sprintf("%s %s", __('Skutečnost'), $this->getSettingValue(self::COLUMN_2_YEAR, self::$SETTINGS[self::COLUMN_2_YEAR])),
            sprintf("%s %s", __('Rozpočet'), $this->getSettingValue(self::COLUMN_3_YEAR, self::$SETTINGS[self::COLUMN_3_YEAR])),
            sprintf("%s %s", __('Schválené zdroje pro rok'), $this->getSettingValue(self::COLUMN_3_YEAR, self::$SETTINGS[self::COLUMN_3_YEAR])),
            sprintf(__('Zdroje, které bude organizace pro rok %s požadovat / nárokovat'), $this->getSettingValue(self::COLUMN_3_YEAR, self::$SETTINGS[self::COLUMN_3_YEAR])),
        ];

        return [
            'columns' => $defaultColumns,
            'rows' => explode(';', $this->getSettingValue(self::SETTING_ROWS)),
            'sumrow' => __('Celkem'),
        ];
    }

    public function saveSetting(array $data = []): Form
    {
        $rtn = parent::saveSetting($data);
        foreach ($rtn->form_settings as $setting) {
            if ($setting->name === self::SETTING_ROWS) {
                $raw = strip_tags($setting->value);
                $array = [];
                foreach (preg_split("/((\r?\n)|(\r\n?))/", $raw) as $line) {
                    $line = str_replace(';', '', trim($line));
                    if (!empty($line)) {
                        $array[] = $line;
                    }
                }
                $setting->value = join(';', $array);
            }
        }

        return $rtn;
    }

    public function renderFormSettings(AppView $appView): string
    {
        return $appView->element('Forms/social_service_balance_sheet_settings', [
            'form' => $this,
        ]);
    }

    public function isFormFilledCompletely(?Request $request = null): bool
    {
        for ($i = 0; $i < count($this->getTable()['columns']); $i++) {
            if ($this->getColumnSum($i) > 0) {
                return true;
            }
        }

        return false;
    }
}
