<?php
declare(strict_types=1);

namespace App\Form;

use App\Model\Entity\Form;
use App\Model\Entity\FormField;
use App\Model\Entity\FormFieldType;
use App\Model\Entity\FormSetting;
use App\Model\Entity\Request;
use App\Model\Table\FormFieldsTable;
use App\Model\Table\RequestFilledFieldsTable;
use App\View\AppView;
use Cake\Event\EventManager;
use Cake\I18n\Number;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\View\Helper\FormHelper;

class SocialServiceCapacitiesFormController extends AbstractFormController
{
    const SETTING_COLUMN_1_YEAR = 'year.1';
    const SETTING_COLUMN_2_YEAR = 'year.2';
    const SETTING_COLUMN_3_YEAR = 'year.3';
    const SETTING_COLUMN_4_YEAR = 'year.4';
    static array $YEAR_DEFAULTS = [];
    const SETTING_SHOW_BED_DAYS = 'show_bed_days';
    const SETTING_SHOW_COLUMN_4 = 'show_column_4';
    const SETTING_SHOW_FIELD_PROGRAMS_ROW = 'show_field_programs';

    private bool $_readOnly = false;
    private array $_tableFields = [];

    public function __construct(Form $formDefinition, Request $userRequest = null, EventManager $eventManager = null)
    {
        self::$YEAR_DEFAULTS = [
            self::SETTING_COLUMN_1_YEAR => date('Y', strtotime('-1 year')),
            self::SETTING_COLUMN_2_YEAR => date('Y'),
            self::SETTING_COLUMN_3_YEAR => date('Y', strtotime('+1 year')),
            self::SETTING_COLUMN_4_YEAR => date('Y', strtotime('+2 years')),
        ];
        parent::__construct($formDefinition, $userRequest, $eventManager);
    }

    public function setup(): self
    {
        /** @var FormFieldsTable $formFieldsTable */
        $formFieldsTable = $this->getTableLocator()->get('FormFields');

        /** @var FormField[] $queryExisting */
        $queryExisting = $formFieldsTable->find('all', [
            'conditions' => [
                'form_id' => $this->getFormDefinition()->id,
                'is_required' => true,
                'form_field_type_id' => FormFieldType::FIELD_TEXT,
            ],
        ]);

        $tableDefinitions = $this->getTables();
        $knownTables = array_keys($tableDefinitions);
        $maxOrder = 1;

        foreach ($queryExisting as $existingField) {
            if (in_array($existingField->name, $knownTables, true)) {
                $this->_tableFields[$existingField->name] = $existingField;
                $maxOrder = max($existingField->field_order, $maxOrder);
            } else {
                $formFieldsTable->delete($existingField);
            }
        }

        foreach ($this->getTables() as $tableName => $tableSettings) {
            if (!isset($this->_tableFields[$tableName])) {
                $newField = $formFieldsTable->newEntity([
                    'form_id' => $this->getFormDefinition()->id,
                    'name' => $tableName,
                    'is_required' => true,
                    'form_field_type_id' => FormFieldType::FIELD_TEXT,
                    'field_order' => $maxOrder++,
                ]);
                if ($formFieldsTable->save($newField)) {
                    $this->_tableFields[$tableName] = $newField;
                }
            }
        }

        $formSettingsTable = $this->getTableLocator()->get('FormSettings');
        foreach (self::$YEAR_DEFAULTS as $setting_year_name => $setting_year_default_value) {
            $this->_settings[$setting_year_name] = $formSettingsTable->findOrCreate([
                'form_id' => $this->getFormDefinition()->id,
                'name' => $setting_year_name
            ], function (FormSetting $yearSetting) use ($setting_year_default_value) {
                $yearSetting->value = $setting_year_default_value;
            });
        }
        $this->_settings[self::SETTING_SHOW_BED_DAYS] = $formSettingsTable->findOrCreate([
            'form_id' => $this->getFormDefinition()->id,
            'name' => self::SETTING_SHOW_BED_DAYS
        ], function (FormSetting $bedDays) {
            $bedDays->value = false;
        });
        $this->_settings[self::SETTING_SHOW_FIELD_PROGRAMS_ROW] = $formSettingsTable->findOrCreate([
            'form_id' => $this->getFormDefinition()->id,
            'name' => self::SETTING_SHOW_FIELD_PROGRAMS_ROW
        ], function (FormSetting $bedDays) {
            $bedDays->value = 0;
        });
        $this->_settings[self::SETTING_SHOW_COLUMN_4] = $formSettingsTable->findOrCreate([
            'form_id' => $this->getFormDefinition()->id,
            'name' => self::SETTING_SHOW_COLUMN_4
        ], function (FormSetting $bedDays) {
            $bedDays->value = 0;
        });

        return $this;
    }

    protected function _execute(array $data)
    {
        /** @var RequestFilledFieldsTable $filledFieldsTable */
        $filledFieldsTable = TableRegistry::getTableLocator()->get('RequestFilledFields');

        foreach ($this->getTables() as $tableName => $tableSettings) {
            $filledTable = $filledFieldsTable->findOrCreate([
                'form_id' => $this->getFormDefinition()->id,
                'request_id' => $this->getUserRequest()->id,
                'form_field_id' => $this->_tableFields[$tableName]->id,
            ]);
            $filledTable->value = json_encode($data[$tableName] ?? []);

            if (!$filledFieldsTable->save($filledTable)) {
                return false;
            }
        }

        return true;
    }

    /**
     * @inheritDoc
     */
    public function prefill(Request $request): self
    {
        $this->setUserRequest($request);

        foreach ($this->getFilledFields($request->id) as $field) {
            $definitionField = $this->getFieldById($field->form_field_id);
            if (!empty($definitionField)) {
                $this->_rawValues[$definitionField->name] = $field->value;
                $this->_data[$definitionField->name] = json_decode($field->value, true);
            }
        }

        return $this;
    }

    public function render(AppView $appView, array $options = []): ?string
    {
        $returnAsString = isset($options['returnAsString']) ? $options['returnAsString'] === true : false;
        $this->_readOnly = Hash::get($options, 'readOnly') === true;

        $return = $appView->element('Forms/social_service_capacities_form', [
            'form' => $this,
            'request' => $this->getUserRequest(),
            'enableInput' => !$this->_readOnly,
        ]);

        if ($returnAsString) {
            return $return;
        }

        echo $return;

        return null;
    }

    public function getYears()
    {
        $years = [
            $this->getSettingValue(self::SETTING_COLUMN_1_YEAR, self::$YEAR_DEFAULTS[self::SETTING_COLUMN_1_YEAR]),
            $this->getSettingValue(self::SETTING_COLUMN_2_YEAR, self::$YEAR_DEFAULTS[self::SETTING_COLUMN_2_YEAR]),
            $this->getSettingValue(self::SETTING_COLUMN_3_YEAR, self::$YEAR_DEFAULTS[self::SETTING_COLUMN_3_YEAR]),
        ];
        if ($this->getSettingValue(self::SETTING_SHOW_COLUMN_4, false)) {
            $years[] = $this->getSettingValue(self::SETTING_COLUMN_4_YEAR, self::$YEAR_DEFAULTS[self::SETTING_COLUMN_4_YEAR]);
        }
        return $years;
    }

    public function getTables()
    {
        $tables = [
            'residency' => [
                'title' => __('Kapacita (pobytové služby)'),
                'rows' => [
                    'r1' => __('Počet uživatelů/rok'),
                    'r2' => __('Počet lůžek'),
                ],
            ],
            'others' => [
                'title' => __('Kapacita (ostatní služby)'),
                'rows' => [
                    'o1' => __('Počet uživatelů/rok'),
                    'o2' => __('Počet hodin přímé péče'),
                    'o3' => __('Maximální okamžitá kapacita služby (ambulantní služba)'),
                ],
            ],
        ];
        if ($this->getSettingValue(self::SETTING_SHOW_BED_DAYS, false)) {
            $tables['residency']['rows'] = ['r0' => __('Počet lůžkodnů')] + $tables['residency']['rows'];
        }
        if ($this->getSettingValue(self::SETTING_SHOW_FIELD_PROGRAMS_ROW, false)) {
            $tables['others']['rows']['o4'] = __('Počet zájemců o službu (terénní programy)');
        }
        return $tables;
    }

    public function renderFormSettings(AppView $appView): string
    {
        return $appView->element('Forms/social_service_capacities_settings', [
            'form' => $this
        ]);
    }

    public function fieldOrFormatted(FormHelper $formHelper, string $tableName, string $row, int $counter): string
    {
        $dataKey = sprintf("%s.%s.%d", $tableName, $row, $counter);
        if ($this->_readOnly) {
            return Number::format($this->getData($dataKey) ?? 0);
        } else {
            return $formHelper->control($dataKey, ['type' => 'number', 'step' => 1, 'default' => 0]);
        }
    }

    public function isFormFilledCompletely(?Request $request = null): bool
    {
        foreach ($this->getTables() as $tableName => $tableSettings) {
            foreach ($tableSettings['rows'] as $rowId => $rowLabel) {
                for ($counter = 0; $counter < 6; $counter++) {
                    if (!empty($this->getData(sprintf("%s.%s.%d", $tableName, $rowId, $counter)))) {
                        return true;
                    }
                }
            }
        }

        return false;
    }
}
