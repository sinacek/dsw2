<?php
declare(strict_types=1);

namespace App\Form;

use App\Model\Entity\FormField;
use App\Model\Entity\FormFieldType;
use App\Model\Entity\Request;
use App\Model\Entity\RequestFilledField;
use App\Model\Table\FormFieldsTable;
use App\Model\Table\RequestFilledFieldsTable;
use App\View\AppView;
use Cake\I18n\Number;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\View\Helper\FormHelper;

class SocialServiceBudgetFormController extends AbstractFormController
{

    private bool $_readOnly = false;
    private array $_sections = [];
    private ?FormField $_formField = null;
    private array $_colSums = [];
    const FORM_FIELD_NAME = 'SocialServiceBudget';

    public const COLUMN_EXPECTED_EXPENSES = 1,
        COLUMN_EXPENSES_TO_SECTION_RATIO = 5,
        COLUMN_SUBSIDY_REQUEST = 3,
        COLUMN_COMMENT = 4,
        COLUMN_SUBSIDY_TO_EXPENSES_RATIO = 2;

    public function setup(): self
    {

        /** @var FormFieldsTable $formFieldsTable */
        $formFieldsTable = $this->getTableLocator()->get('FormFields');

        $this->_formField = $formFieldsTable->findOrCreate([
            'form_id' => $this->getFormDefinition()->id,
            'is_required' => true,
            'form_field_type_id' => FormFieldType::FIELD_TEXT,
            'field_order' => 1,
        ], function ($entity) {
            $entity->set('name', self::FORM_FIELD_NAME);
        });

        if ($this->_formField->name !== self::FORM_FIELD_NAME) {
            $this->_formField->name = self::FORM_FIELD_NAME;
            $this->_formField = $formFieldsTable->save($this->_formField);
        }

        return $this;
    }

    public function render(AppView $appView, array $options = []): ?string
    {
        $returnAsString = isset($options['returnAsString']) ? $options['returnAsString'] === true : false;
        $this->_readOnly = Hash::get($options, 'readOnly') === true;

        $return = $appView->element('Forms/social_service_budget_form', [
            'form' => $this,
            'request' => $this->getUserRequest(),
            'enableInput' => !$this->_readOnly,
        ]);

        if ($returnAsString) {
            return $return;
        }

        echo $return;

        return null;
    }

    public function renderFormSettings(AppView $appView): string
    {
        return $appView->asHtmlListRecursive([
            __('Rozpočet poskytované služby (projektu) a požadavek od MČ Praha podle nákladových položek'),
        ]);
    }

    public function isFormFilledCompletely(?Request $request = null): bool
    {
        $this->sumColumn(self::COLUMN_EXPECTED_EXPENSES);
        $this->sumColumn(self::COLUMN_SUBSIDY_REQUEST);

        return !empty($this->_colSums[self::COLUMN_EXPECTED_EXPENSES]) && !empty($this->_colSums[self::COLUMN_SUBSIDY_REQUEST]);
    }

    public function getTables()
    {
        return [
            'headers' => [
                __('Nákladová položka') => '2',
                __('Plánované náklady (rozpočet služby)') => '',
                __('Procentní vyjádření podílu položky v celkovém rozpočtu') => '',
                __('Požadavek na dotaci') => '',
                __('Procentní vyjádření nákladů k celkovému rozpočtu služby') => '',
                __('Poznámka / slovní komentář') => '',
            ],
            'sections' => [
                __('Provozní náklady celkem') => [
                    __('Materiální náklady celkem') => [
                        __('Potraviny'),
                        __('Kancelářské potřeby'),
                        __('Vybavení (DDHIM do 40 tis. Kč)'),
                        __('Pohonné hmoty'),
                        __('Jiné (popište do poznámky)'),
                    ],
                    __('Nemateriálové náklady') => [
                        __('Energie') => [
                            __('Elektřina'),
                            __('Plyn'),
                            __('Vodné a stočné'),
                            __('Jiné (popište do poznámky)'),
                        ],
                        __('Opravy a udržování') => [
                            __('Opravy a udržování budov'),
                            __('Opravy a udržování vozidel'),
                            __('Jiné (popište do poznámky)'),
                        ],
                        __('Cestovné zaměstnanců'),
                        __('Ostatní služby') => [
                            __('Telefony'),
                            __('Poštovné'),
                            __('Ostatní spoje'),
                            __('Nájemné'),
                            __('Stravovací služby'),
                            __('Právní a ekonomické služby'),
                            __('Školení a kurzy'),
                            __('Pořízení (DNIM do 60 tis. Kč)'),
                            __('Jiné (popište do poznámky)'),
                        ],
                    ],
                    __('Jiné provozní náklady') => [
                        __('Odpisy'),
                        __('Jiné (popište do poznámky)'),
                    ],
                    __('Finanční náklady') => [
                        __('Daně a poplatky'),
                        __('Jiné (popište do poznámky)'),
                    ],
                ],
                __('Osobní náklady celkem') => [
                    __('Mzdové náklady') => [
                        __('Hrubé mzdy'),
                        __('OON na DPČ'),
                        __('OON na DPP'),
                        __('Ostatní mzdové náklady'),
                    ],
                    __('Odvody na sociální a zdravotní pojištění') => [
                        __('Pojistné ke mzdám'),
                        __('Pojistné k DPČ'),
                        __('Ostatní pojistné'),
                    ],
                    __('Ostatní sociální náklady'),
                ],
            ],
        ];
    }

    /**
     * @inheritDoc
     */
    public function prefill(Request $request): self
    {
        $this->setUserRequest($request);

        foreach ($this->getFilledFields($request->id) as $field) {
            $definitionField = $this->getFieldById($field->form_field_id);
            if (!empty($definitionField)) {
                $this->_rawValues[$field->id] = $field->value;
                $this->_data = unserialize($field->value);
            }
        }

        $this->markSections($this->getTables()['sections']);

        return $this;
    }

    public function getData($field = null)
    {
        return $this->_data[$field] ?? parent::getData($field);
    }

    public function renderSections(FormHelper $formHelper, array $sectionData = [], string $prefix = '')
    {
        $counter = 0;
        foreach ($sectionData as $key => $value) {
            $counter++;
            $isHeader = false;
            $title = null;
            $section = sprintf("%s%d", $prefix, $counter);
            if (is_string($value)) {
                $title = $value;
            } elseif (is_array($value)) {
                $title = $key;
                $isHeader = true;
            }
            ?>
            <tr class="<?= $isHeader ? 'tr-sums' : 'tr-inputs' ?>" data-sum-prefix="<?= $section ?>">
                <td class="text-left <?= $isHeader ? 'font-weight-bold' : '' ?>"><?= $section ?></td>
                <td class="font-weight-bold text-left"><?= $title ?></td>
                <td class="text-right"><?= $this->fieldOrFormatted($formHelper, $section, self::COLUMN_EXPECTED_EXPENSES, $isHeader) ?></td>
                <td class="text-center"><?= $this->fieldOrFormatted($formHelper, $section, self::COLUMN_EXPENSES_TO_SECTION_RATIO, $isHeader) ?></td>
                <td class="text-right"><?= $this->fieldOrFormatted($formHelper, $section, self::COLUMN_SUBSIDY_REQUEST, $isHeader) ?></td>
                <td class="text-center"><?= $this->fieldOrFormatted($formHelper, $section, self::COLUMN_SUBSIDY_TO_EXPENSES_RATIO, $isHeader) ?></td>
                <td><?= $this->fieldOrFormatted($formHelper, $section, self::COLUMN_COMMENT, $isHeader) ?></td>
            </tr>
            <?php

            if ($isHeader) {
                $this->renderSections($formHelper, $value, sprintf("%s%d.", $prefix, $counter));
            }
        }
    }

    public function getSectionRatio(string $section)
    {
        $row_value = intval($this->getData(sprintf("%s.%d", $section, self::COLUMN_EXPECTED_EXPENSES)));
        $this->sumColumn(self::COLUMN_EXPECTED_EXPENSES);
        $parent_section_sum = intval($this->_colSums[self::COLUMN_EXPECTED_EXPENSES]);

        if (empty($row_value) || empty($parent_section_sum)) {
            return Number::toPercentage(0);
        }

        return Number::toPercentage(($row_value / $parent_section_sum) * 100);
    }

    public function getRatio(string $section)
    {
        $data_a = intval($this->getData(sprintf("%s.%d", $section, self::COLUMN_EXPECTED_EXPENSES)));
        $data_b = intval($this->getData(sprintf("%s.%d", $section, self::COLUMN_SUBSIDY_REQUEST)));

        if (empty($data_a) || empty($data_b)) {
            return Number::toPercentage(0);
        }

        return Number::toPercentage(($data_b / $data_a) * 100);
    }

    public function fieldOrFormatted(FormHelper $formHelper, string $section, int $column, bool $isHeader = false): string
    {
        $dataKey = sprintf("%s.%d", $section, $column);
        if ($this->_readOnly || $isHeader) {
            switch ($column) {
                case self::COLUMN_EXPECTED_EXPENSES:
                case self::COLUMN_SUBSIDY_REQUEST:
                    return Number::currency($this->getData($dataKey) ?? 0, 'CZK');
                case self::COLUMN_SUBSIDY_TO_EXPENSES_RATIO:
                    return $this->getRatio($section);
                case self::COLUMN_EXPENSES_TO_SECTION_RATIO:
                    return $this->getSectionRatio($section);
                case self::COLUMN_COMMENT:
                    return h(strval($this->getData($dataKey) ?? ''));
            }
        } else {
            switch ($column) {
                case self::COLUMN_EXPECTED_EXPENSES:
                case self::COLUMN_SUBSIDY_REQUEST:
                    return sprintf(
                        '<div class="input-group">%s<div class="input-group-append"><span class="input-group-text">Kč</span></div></div>',
                        $formHelper->control($dataKey, ['type' => 'number', 'step' => .01, 'default' => 0])
                    );
                case self::COLUMN_SUBSIDY_TO_EXPENSES_RATIO:
                    return $this->getRatio($section);
                case self::COLUMN_EXPENSES_TO_SECTION_RATIO:
                    return $this->getSectionRatio($section);
                case self::COLUMN_COMMENT:
                    return $formHelper->control($dataKey, ['type' => 'string', 'maxlength' => 255]);
            }
        }

        return 'invalid';
    }

    public function sumColumn(int $column): string
    {
        if (!empty($this->_colSums[$column])) {
            return Number::currency($this->_colSums[$column], 'CZK');
        }
        $this->markSections($this->getTables()['sections']);
        $sum = 0;
        $colString = strval($column);
        foreach ($this->_data as $key => $value) {
            if (strrpos($key, $colString) === strlen($key) - 1) {
                if (!in_array(substr($key, 0, strrpos($key, '.')), $this->_sections, true)) {
                    $sum += $value;
                }
            }
        }
        $this->_colSums[$column] = $sum;

        return Number::currency($this->_colSums[$column], 'CZK');
    }

    public function getOverallRatio(): string
    {
        $this->sumColumn(3);
        $this->sumColumn(1);

        $colsum3 = $this->_colSums[3];
        $colsum1 = $this->_colSums[1];

        if (empty($colsum3) || empty($colsum1)) {
            return Number::toPercentage(0);
        }

        return Number::toPercentage(($colsum3 / $colsum1) * 100);
    }

    public function markSections(array $sectionData = [], string $prefix = '', bool $recurse = false): array
    {
        if ($recurse || empty($this->_sections)) {
            $counter = 0;
            foreach ($sectionData as $key => $value) {
                $counter++;
                $section = sprintf("%s%d", $prefix, $counter);
                if (is_array($value)) {
                    $this->_sections[] = $section;
                    $this->markSections($value, sprintf("%s%d.", $prefix, $counter), true);
                }
            }
        }

        return $this->_sections;
    }

    public function addToSectionSums(array $existingSums, string $valueAddress, float $value)
    {
        $targetColumn = mb_substr($valueAddress, -1);
        $section = $valueAddress;
        while ($section !== null) {
            if (in_array($section, $this->_sections, true)) {
                $existingAddress = sprintf("%s.%s", $section, $targetColumn);
                $existingSums[$existingAddress] = ($existingSums[$existingAddress] ?? 0) + $value;
            }
            $dotPosition = strrpos($section, '.');
            if ($dotPosition === false) {
                $section = null;
            } else {
                $section = mb_substr($section, 0, $dotPosition);
            }
            if (empty($section)) {
                $section = null;
            }
        }

        return $existingSums;
    }

    protected function _execute(array $data)
    {
        /** @var RequestFilledFieldsTable $filledFieldsTable */
        $filledFieldsTable = TableRegistry::getTableLocator()->get('RequestFilledFields');

        $flat = Hash::flatten($data);
        $sectionSums = [];

        foreach ($flat as $rowColumn => $value) {
            $column = intval(mb_substr($rowColumn, -1));
            switch ($column) {
                case self::COLUMN_EXPECTED_EXPENSES:
                case self::COLUMN_SUBSIDY_REQUEST:
                    $flat[$rowColumn] = round(floatval($value), 2);
                    $sectionSums = $this->addToSectionSums($sectionSums, $rowColumn, $flat[$rowColumn]);
                    break;
                case self::COLUMN_COMMENT:
                    $flat[$rowColumn] = mb_substr($value, 0, 254);
                    break;
            }
        }

        $flat = array_merge($flat, $sectionSums);

        $filledField = $filledFieldsTable->findOrCreate([
            'request_id' => $this->getUserRequest()->id,
            'form_id' => $this->getFormDefinition()->id,
            'form_field_id' => $this->_formField->id,
        ]);
        $filledField->value = serialize($flat);

        return $filledFieldsTable->save($filledField) instanceof RequestFilledField;
    }
}
