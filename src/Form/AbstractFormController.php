<?php
declare(strict_types=1);

namespace App\Form;

use App\Model\Entity\File;
use App\Model\Entity\Form;
use App\Model\Entity\FormField;
use App\Model\Entity\FormFieldType;
use App\Model\Entity\FormSetting;
use App\Model\Entity\FormType;
use App\Model\Entity\Request;
use App\Model\Entity\RequestFilledField;
use App\Model\Entity\User;
use App\Model\Table\FilesTable;
use App\Model\Table\FormFieldsTable;
use App\Model\Table\FormsTable;
use App\Model\Table\RequestFilledFieldsTable;
use App\View\AppView;
use Cake\Datasource\EntityInterface;
use Cake\Event\EventManager;
use Cake\Log\Log;
use Cake\ORM\Exception\PersistenceFailedException;
use Cake\ORM\Locator\LocatorAwareTrait;
use Cake\ORM\TableRegistry;

/**
 * Base class of FormControllers, subclass MUST implement methods
 * - render -> to provide user-facing form UI
 * - _execute -> to handle submitted form data and propagate to DB
 * - isFormFilledCompletely - to validate conditions, possibly based on form or organization settings
 * - renderFormSettings - to render admin-facing form settings UI
 */
abstract class AbstractFormController extends \Cake\Form\Form implements FormControllerInterface
{
    use LocatorAwareTrait;

    protected ?Form $_formDefinition = null;
    protected ?iterable $_fields = null;
    protected ?iterable $_fieldsInOrder = null;
    protected ?iterable $_rawValues = [];
    protected ?Request $_userRequest = null;
    protected ?User $_user = null;
    /**
     * @var FormSetting[] array in form of [SETTING-NAME => SETTING-VALUE]
     */
    protected array $_settings = [];

    /**
     * @inheritDoc
     */
    public function __construct(Form $formDefinition, Request $userRequest = null, EventManager $eventManager = null)
    {
        parent::__construct($eventManager);
        $this->setFormDefinition($formDefinition);
        $this->setUserRequest($userRequest);
        $this->setup();
        if ($userRequest instanceof Request) {
            $this->prefill($userRequest);
        }
    }

    public function setup(): self
    {
        // to be overriden in subclasses
        return $this;
    }

    /**
     * @inheritDoc
     */
    final public function setFormDefinition(?Form $formDefinition): self
    {
        $this->_formDefinition = $formDefinition;

        foreach ($formDefinition->form_settings ?? [] as $formSetting) {
            $this->_settings[$formSetting->name] = $formSetting;
        }

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getFormDefinition(): Form
    {
        return $this->_formDefinition;
    }

    /**
     * @inheritDoc
     */
    public function getOriginalFields(): iterable
    {
        if ($this->_fields === null) {
            if (is_iterable($this->_formDefinition->form_fields)) {
                $this->_fields = $this->_formDefinition->form_fields;
            } else {
                /** @var FormFieldsTable $fieldsTable */
                $fieldsTable = $this->getTableLocator()->get('FormFields');
                $this->_fields = $fieldsTable->find('all', [
                    'conditions' => [
                        'FormFields.form_id' => $this->_formDefinition->id,
                    ],
                ])->toArray();
            }
        }

        return $this->_fields;
    }

    /**
     * @inheritDoc
     */
    public function getFieldsInOrder(): iterable
    {
        if ($this->_fieldsInOrder === null) {
            $fields = [];
            foreach ($this->getOriginalFields() as $field) {
                $fields[$field->field_order] = $field;
            }
            ksort($fields);
            $this->_fieldsInOrder = $fields;
        }

        return $this->_fieldsInOrder;
    }

    /**
     * @inheritDoc
     */
    public function getHtmlHelperFormType(): string
    {
        foreach ($this->getFieldsInOrder() as $field) {
            if ($field->form_field_type_id === FormFieldType::FIELD_FILE) {
                return 'file';
            }
        }

        return 'post';
    }

    /**
     * @inheritDoc
     */
    public function getFormName(): string
    {
        return h($this->getFormDefinition()->name);
    }

    /**
     * @inheritDoc
     */
    public function setUserRequest(?Request $request): FormControllerInterface
    {
        $this->_userRequest = $request;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getUserRequest(): ?Request
    {
        return $this->_userRequest;
    }

    /**
     * @inheritDoc
     */
    public function hasUserDefinedFields(): bool
    {
        return $this->getFormDefinition()->form_type_id === FormType::FORM_TYPE_STANDARD;
    }

    /**
     * @inheritDoc
     */
    public function getFieldById(int $formFieldId): ?FormField
    {
        foreach ($this->getFieldsInOrder() as $field) {
            if ($field->id === $formFieldId) {
                return $field;
            }
        }

        return null;
    }

    /**
     * @inheritDoc
     */
    public function getFilledFields(int $userRequestId): iterable
    {
        /**
         * @var RequestFilledFieldsTable $filledFieldsTable
         */
        $filledFieldsTable = $this->getTableLocator()->get('RequestFilledFields');

        /**
         * @var RequestFilledField[] $filledFields
         */
        return $filledFieldsTable->find(
            'all',
            [
                'conditions' => [
                    'form_id' => $this->getFormDefinition()->id,
                    'request_id' => $userRequestId,
                ],
            ]
        )->toArray();
    }

    public function deleteSavedData(): bool
    {
        /**
         * @var RequestFilledFieldsTable $filledFieldsTable
         */
        $filledFieldsTable = $this->getTableLocator()->get('RequestFilledFields');

        if (!($this->getUserRequest() instanceof Request)) {
            return false;
        }

        try {
            $filledFieldsTable->deleteAll([
                'form_id' => $this->getFormDefinition()->id,
                'request_id' => $this->getUserRequest()->id,
            ]);

            return true;
        } catch (\Throwable $t) {
            Log::error($t);
        }

        return false;
    }

    /**
     * @inheritDoc
     */
    public function setUser(User $user): FormControllerInterface
    {
        $this->_user = $user;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getUser(): ?User
    {
        return $this->_user;
    }

    /**
     * @inheritDoc
     */
    public function prefill(Request $request): self
    {
        $this->setUserRequest($request);

        foreach ($this->getFilledFields($request->id) as $field) {
            $definitionField = $this->getFieldById($field->form_field_id);
            if (!empty($definitionField)) {
                $this->_rawValues[$definitionField->getFormControlId()] = $field->value;
                $this->_data[$definitionField->getFormControlId()] = $definitionField->parseFieldValue($field->value);
            }
        }

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getRawValue(string $formControlId): ?string
    {
        return $this->_rawValues[$formControlId] ?? null;
    }

    /**
     * @inheritDoc
     */
    public function getCurrentlyAssignedFileOrNull(FormField $field): ?File
    {
        if ($field->form_field_type_id === FormFieldType::FIELD_FILE) {
            $currentValue = $this->getData($field->getFormControlId());
            if (intval($currentValue) > 0) {
                return $this->retrieveFile(intval($currentValue), $this->getUserRequest() ? $this->getUserRequest()->user_id : null);
            }
        }

        return null;
    }

    /**
     * @inheritDoc
     */
    public function retrieveFile(int $file_id, int $user_id = null): ?File
    {
        /** @var FilesTable $filesTable */
        $filesTable = TableRegistry::getTableLocator()->get('Files');
        /** @noinspection PhpIncompatibleReturnTypeInspection */
        return $filesTable->find(
            'all',
            [
                'conditions' => [
                    'Files.user_id' => $user_id ?? $this->getUser()->id,
                    'Files.id' => $file_id,
                ],
            ]
        )->first();
    }

    /**
     * @inheritDoc
     */
    public function renderAdditionalFieldInfo(FormField $field, AppView $view, bool $returnAsString = false): ?string
    {
        $return = null;

        if ($field->form_field_type_id === FormFieldType::FIELD_FILE) {
            $storedFieldValue = $this->getData($field->getFormControlId());
            if (!empty($storedFieldValue) && intval($storedFieldValue) > 0) {
                $fileEntity = $this->retrieveFile($storedFieldValue, $this->getUserRequest() ? $this->getUserRequest()->user_id : null);
                if (!empty($fileEntity)) {
                    $return .= $view->element('fileinfo', ['file' => $fileEntity, 'field' => $field]);
                }
            }
        }

        if (!$returnAsString) {
            echo $return;

            return null;
        }

        return $return;
    }

    /**
     * @inheritDoc
     */
    public function duplicate(FormsTable $formsTable): Form
    {
        $form = $formsTable->get($this->getFormDefinition()->id, [
            'contain' => [
                'FormFields',
            ],
        ]);

        $form->unsetProperty('created')->unsetProperty('modified')->unsetProperty('id');
        $form->name .= sprintf(" - %s %s", __('Kopie'), date('Y-m-d H:i:s'));
        $form->isNew(true);

        foreach ($form->form_fields ?? [] as $formField) {
            $formField->unsetProperty('id')->unsetProperty('form_id');
            $formField->isNew(true);
        }

        foreach ($form->form_settings ?? [] as $formSetting) {
            $formSetting->unsetProperty('id')->unsetProperty('form_id')
                ->unsetProperty('created')->unsetProperty('modified');
            $formSetting->isNew(true);
        }

        if ($formsTable->save($form)) {
            return $form;
        }

        Log::error('RequestForm duplicate failed');
        Log::error(json_encode($form->getErrors()));
        throw new PersistenceFailedException($form, $form->getFirstError());
    }

    /**
     * @param array $data post data
     * @return Form|EntityInterface
     */
    public function saveSetting(array $data = []): Form
    {
        $formsTable = $this->getTableLocator()->get('Forms');
        // default handler to be overriden in sub-classes
        return $formsTable->patchEntity($this->getFormDefinition(), $data);
    }

    /**
     * Tries to get setting (possibly modified) from cache
     *
     * @param string $settingName
     * @param null $defaultValue
     * @return mixed|string|null
     */
    public function getSettingValue(string $settingName, $defaultValue = null)
    {
        if (isset($this->_settings[$settingName])) {
            return $this->_settings[$settingName]->value;
        }

        return $defaultValue;
    }

    /**
     * Retrieves the raw setting by name
     *
     * @param string $settingName
     * @return FormSetting|null
     */
    public function getSetting(string $settingName): ?FormSetting
    {
        return $this->_settings[$settingName] ?? null;
    }
}
