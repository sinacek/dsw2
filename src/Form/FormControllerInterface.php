<?php
declare(strict_types=1);

namespace App\Form;

use App\Model\Entity\File;
use App\Model\Entity\Form;
use App\Model\Entity\FormField;
use App\Model\Entity\Request;
use App\Model\Entity\RequestFilledField;
use App\Model\Entity\User;
use App\Model\Table\FormsTable;
use App\View\AppView;
use Cake\Event\EventManager;
use Throwable;

interface FormControllerInterface
{

    /**
     * FormControllerInterface constructor.
     * @param Form $formDefinition form DB representation
     * @param Request|null $request user request DB representation
     * @param EventManager|null $eventManager
     */
    public function __construct(Form $formDefinition, Request $request = null, EventManager $eventManager = null);

    /**
     * Fills the internal values for FormHelper from DB
     *
     * @param Request $request
     * @return $this
     */
    public function prefill(Request $request): self;

    /**
     * Create copy of uderlying Form along with all fields, and return it's copy
     * or throw if the copy could not be created / persisted
     *
     * @param FormsTable $formsTable
     * @return Form
     * @throws Throwable
     */
    public function duplicate(FormsTable $formsTable): Form;

    /**
     * Get FormFields in correct order
     *
     * @return FormField[]
     */
    public function getFieldsInOrder(): iterable;

    /**
     * Get FormFields in order as saved in DB
     *
     * @return FormField[]
     */
    public function getOriginalFields(): iterable;

    /**
     * Set Form definition, might not include fields
     *
     * @param Form $formDefinition underlying DB Form representation
     * @return $this
     */
    public function setFormDefinition(Form $formDefinition): self;

    /**
     * Returns underlying DB form representation
     *
     * @return Form
     */
    public function getFormDefinition(): Form;

    /**
     * Returns FormField by ID or null if not found in this controller
     *
     * @param int $formFieldId id of form field
     * @return FormField|null
     */
    public function getFieldById(int $formFieldId): ?FormField;

    /**
     * Returns user-facing form name
     *
     * @return string
     */
    public function getFormName(): string;

    /**
     * Returns 'post' for forms without file field(s), 'file' for those that require file upload
     *
     * @return string
     */
    public function getHtmlHelperFormType(): string;

    /**
     * Contain related user request DB representation
     *
     * @param Request|null $request user request, if provided
     * @return $this
     */
    public function setUserRequest(?Request $request): self;

    /**
     * Allow the form to be set-up, ie. pre-creating FormField definition
     * or default FormSettings
     *
     * @return $this
     */
    public function setup(): self;

    /**
     * Returns Request instance if set
     *
     * @return Request|null
     */
    public function getUserRequest(): ?Request;

    /**
     * Contain related user DB representation
     *
     * @param User $user
     * @return $this
     */
    public function setUser(User $user): self;

    /**
     * Returns User if contained
     *
     * @return User|null
     */
    public function getUser(): ?User;

    /**
     * Renders contained form's fields through FormHelper within AppView,
     * if $options['returnAsString'] is set to `true`, it will return the rendered controls instead
     * of outputting them, else it returns `null`
     * if $options['readOnly'] is set to `true`, it will return values rendered as table/list,
     * instead of form / inputs
     *
     * options
     * - `returnAsString` -> defaults to `false`
     * - `readOnly` -> defaults to `false`
     *
     * @param AppView $appView appview necessary to access FormHelper instance
     * @param array $options see function docs for known options
     * @return string|null
     */
    public function render(AppView $appView, array $options = []): ?string;

    /**
     * Returns form settings
     *
     * @param AppView $appView appview necessary to access FormHelper / UrlHelper
     * @return string
     */
    public function renderFormSettings(AppView $appView): string;

    /**
     * Whether this controller allows user to define all/additional fields
     *
     * @return bool
     */
    public function hasUserDefinedFields(): bool;

    /**
     * Checks, whether all fields of this form-controller has been correctly
     * filled-in by user or not
     *
     * @param Request|null $request optionally provide request
     * @return bool
     */
    public function isFormFilledCompletely(?Request $request = null): bool;

    /**
     * Gets filled-fields (DB instances) by user-request's id
     *
     * @param int $userRequestId
     * @return RequestFilledField[]
     */
    public function getFilledFields(int $userRequestId): iterable;

    /**
     * @param int $file_id Files.id
     * @return File|null
     */
    public function retrieveFile(int $file_id): ?File;

    /**
     * @param FormField $field relevant form field
     * @return File|null
     */
    public function getCurrentlyAssignedFileOrNull(FormField $field): ?File;

    /**
     * @param FormField $field relevant field
     * @param AppView $view appview instance to access elements rendering
     * @param bool $returnAsString
     * @return string|null
     */
    public function renderAdditionalFieldInfo(FormField $field, AppView $view, bool $returnAsString = false): ?string;

    /**
     * @param string $formControlId get raw DB stored value for form control by it's identifier
     * @return string|null
     */
    public function getRawValue(string $formControlId): ?string;

    /**
     * Used to validate & save settings of given form instance
     * Returns modified Form entity (with modified settings) meant to be persisted
     * Entity might contain errors to indicate in admin-facing form
     *
     * @param array $data
     * @return Form
     */
    public function saveSetting(array $data = []): Form;

    /**
     * Deletes persisted values of this form, returns boolean result of ORM delete
     *
     * @return bool
     */
    public function deleteSavedData(): bool;
}
