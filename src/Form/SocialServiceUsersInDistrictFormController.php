<?php
declare(strict_types=1);

namespace App\Form;

use App\Model\Entity\FormField;
use App\Model\Entity\FormFieldType;
use App\Model\Entity\Request;
use App\Model\Table\FormFieldsTable;
use App\Model\Table\RequestFilledFieldsTable;
use App\View\AppView;
use Cake\I18n\Number;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\View\Helper\FormHelper;

class SocialServiceUsersInDistrictFormController extends AbstractFormController
{

    private bool $_readOnly = false;
    private ?FormField $_formField = null;

    const COLUMN_SERVICE_TYPE = 1,
        COLUMN_PEOPLE_THIS_YEAR = 2,
        COLUMN_PEOPLE_NEXT_YEAR = 3,
        COLUMN_HOURS_THIS_YEAR = 4,
        COLUMN_HOURS_NEXT_YEAR = 5;

    const TABLENAME = 'SocialServiceUsersInDistrict';

    public function render(AppView $appView, array $options = []): ?string
    {
        $returnAsString = isset($options['returnAsString']) ? $options['returnAsString'] === true : false;
        $this->_readOnly = Hash::get($options, 'readOnly') === true;

        $return = $appView->element('Forms/social_service_users_in_district_form', [
            'form' => $this,
            'request' => $this->getUserRequest(),
            'enableInput' => !$this->_readOnly,
        ]);

        if ($returnAsString) {
            return $return;
        }

        echo $return;

        return null;
    }

    public function renderFormSettings(AppView $appView): string
    {
        return $appView->asHtmlListRecursive([
            'Druh služby vs. počet uživatel / hodin dané služby za letošní rok a období žádosti',
        ]);
    }

    public function getYears()
    {
        return [
            date('Y'),
            date('Y', strtotime('+1 year')),
        ];
    }

    public function isFormFilledCompletely(?Request $request = null): bool
    {
        foreach ($this->_data as $row => $items) {
            $noTitle = empty($items[1] ?? null);
            $noItems = empty(join(array_slice($items, 1)));
            if (!$noTitle && !$noItems) {
                return true;
            }
        }

        return false;
    }

    public function fieldOrFormatted(FormHelper $formHelper, int $row, int $column): string
    {
        $dataKey = sprintf("%d.%d", $row, $column);
        $data = $this->getData($dataKey);
        switch ($column) {
            default:
                return 'INVALID';
            case self::COLUMN_SERVICE_TYPE:
                return $this->_readOnly ? $data : $formHelper->control($dataKey, ['type' => 'string', 'data-noquilljs' => 'data-noquilljs', 'maxlength' => 255]);
            case self::COLUMN_HOURS_NEXT_YEAR:
            case self::COLUMN_HOURS_THIS_YEAR:
            case self::COLUMN_PEOPLE_NEXT_YEAR:
            case self::COLUMN_PEOPLE_THIS_YEAR:
                return $this->_readOnly ? Number::format($data, ['places' => 0]) : $formHelper->control($dataKey, ['type' => 'number', 'step' => 1, 'min' => 0]);
        }
    }

    /**
     * @inheritDoc
     */
    public function prefill(Request $request): self
    {
        $this->setUserRequest($request);

        foreach ($this->getFilledFields($request->id) as $field) {
            $definitionField = $this->getFieldById($field->form_field_id);
            if (!empty($definitionField)) {
                $this->_rawValues[$field->id] = $field->value;
                $this->_data = json_decode($field->value, true);
            }
        }

        return $this;
    }

    protected function _execute(array $data)
    {
        $errors = [];
        foreach ($data as $row => $items) {
            $oneEmpty = empty($items[1] ?? null);
            foreach ($items as $index => $value) {
                if (empty($value)) {
                    $oneEmpty = !$oneEmpty && true;
                }
            }
            if ($oneEmpty) {
                foreach ($items as $index => $value) {
                    if (empty($value)) {
                        $errors[sprintf("%d.%d", $row, $index)] = __('Musí být vyplněno');
                    }
                }
            }
        }
        $this->setErrors(Hash::expand($errors));
        if (!empty($this->getErrors())) {
            return false;
        }

        /** @var RequestFilledFieldsTable $filledFieldsTable */
        $filledFieldsTable = TableRegistry::getTableLocator()->get('RequestFilledFields');

        $filledRow = $filledFieldsTable->findOrCreate([
            'form_id' => $this->getFormDefinition()->id,
            'request_id' => $this->getUserRequest()->id,
            'form_field_id' => $this->_formField->id,
        ]);

        $filledRow->value = json_encode($data ?? []);

        if (!$filledFieldsTable->save($filledRow)) {
            return false;
        }

        return true;
    }

    public function setup(): self
    {
        /** @var FormFieldsTable $formFieldsTable */
        $formFieldsTable = $this->getTableLocator()->get('FormFields');

        $this->_formField = $formFieldsTable->findOrCreate([
            'form_id' => $this->getFormDefinition()->id,
            'is_required' => true,
            'form_field_type_id' => FormFieldType::FIELD_TEXT,
            'field_order' => 1,
        ], function ($entity) {
            $entity->set('name', self::TABLENAME);
        });

        if ($this->_formField->name !== self::TABLENAME) {
            $this->_formField->name = self::TABLENAME;
            $this->_formField = $formFieldsTable->save($this->_formField);
        }

        return $this;
    }
}
