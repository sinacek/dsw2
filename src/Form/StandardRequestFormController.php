<?php
declare(strict_types=1);

namespace App\Form;

use App\Model\Entity\FormFieldType;
use App\Model\Entity\Request;
use App\Model\Table\FilesTable;
use App\Model\Table\RequestFilledFieldsTable;
use App\View\AppView;
use Cake\Log\Log;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use InvalidArgumentException;
use Throwable;

class StandardRequestFormController extends AbstractFormController
{

    /**
     * @inheritDoc
     */
    protected function _execute(array $data): bool
    {
        /** @var RequestFilledFieldsTable $filledFieldsTable */
        $filledFieldsTable = TableRegistry::getTableLocator()->get('RequestFilledFields');
        /** @var FilesTable $filesTable */
        $filesTable = TableRegistry::getTableLocator()->get('Files');

        $toSave = [];

        foreach ($this->getFieldsInOrder() as $definitionField) {
            $definitionFieldKey = $definitionField->getFormControlId();
            if (isset($data[$definitionFieldKey])) {
                $value = $data[$definitionFieldKey];

                // upload file as part of form processing
                if ($definitionField->form_field_type_id === FormFieldType::FIELD_FILE) {
                    if (is_array($value) && $value['error'] !== UPLOAD_ERR_NO_FILE) {
                        // array can be only if new upload is processed
                        if ($value['error'] !== 0 || $value['size'] <= 1 || $value['size'] > getMaximumFileUploadSize()) {
                            continue;
                        }
                        $fileResult = $filesTable->newEntity();
                        $fileMoveResult = $fileResult->fileUploadMove($value, $this->getUser()->id);
                        if (is_string($fileMoveResult)) {
                            $fileResult->filepath = $fileMoveResult;
                            $fileResult->original_filename = $value['name'];
                            $fileResult->filesize = $value['size'];
                            $fileResult->user_id = $this->getUser()->id;
                            if ($filesTable->save($fileResult)) {
                                // if this does not get hit, serialization of FormFieldType::File will result in null
                                $value = $fileResult->id;
                            }
                        }
                    } elseif (is_array($value) && $value['error'] === UPLOAD_ERR_NO_FILE) {
                        // file was not provided in form
                        $value = $this->getData($definitionFieldKey);
                    }
                    if (intval($this->getData($definitionFieldKey)) > 0 && $value !== $this->getData($definitionFieldKey)) {
                        // file was changed, remove the old one virtually and physically
                        $previousFileId = $this->getData($definitionFieldKey);
                        $previousFile = $filesTable->get(
                            $previousFileId,
                            [
                                'conditions' => [
                                    'Files.user_id' => $this->getUser()->id,
                                ],
                            ]
                        );
                        if ($filesTable->delete($previousFile)) {
                            $previousFile->deletePhysically();
                        }
                    }
                }

                $filledField = $filledFieldsTable->findOrCreate(
                    [
                        'form_id' => $this->getFormDefinition()->id,
                        'request_id' => $this->getUserRequest()->id,
                        'form_field_id' => $definitionField->id,
                    ]
                );

                $filledField->value = $definitionField->serializeValue($value);
                $this->_data[$definitionFieldKey] = $filledField->value;
                $toSave[] = $filledField;
            }
        }
        try {
            if ($filledFieldsTable->saveMany($toSave)) {
                return true;
            }
        } catch (Throwable $t) {
            Log::error('StandardRequestFormController::_execute saveMany throwed: ' . $t->getMessage());
            Log::error($t->getTraceAsString());
        }

        return false;
    }

    /**
     * @inheritDoc
     */
    public function render(AppView $appView, array $options = []): ?string
    {
        $return = null;
        $returnAsString = isset($options['returnAsString']) ? $options['returnAsString'] === true : false;
        $readOnly = Hash::get($options, 'readOnly') === true;

        if ($readOnly) {
            $return = $appView->element('Forms/standard_request_readonly', ['requestForm' => $this]);
        } else {
            foreach ($this->getFieldsInOrder() as $field) {
                $return .= $this->renderAdditionalFieldInfo($field, $appView, true);
                $return .= $appView->renderFormField($field, ['file' => $this->getCurrentlyAssignedFileOrNull($field)]);
            }
        }

        if ($returnAsString) {
            return $return;
        }

        echo $return;

        return null;
    }

    /**
     * @inheritDoc
     */
    public function isFormFilledCompletely(?Request $userRequest = null): bool
    {
        $request = $userRequest ?? $this->getUserRequest();
        if ($request === null) {
            throw new InvalidArgumentException('UserRequest is not present, cannot proceed');
        }

        if (empty($this->getFieldsInOrder())) {
            // table without fields is never filled, to prevent invalid forms accross the system
            return false;
        }

        $filledFields = $this->getFilledFields($request->id);

        $completed = true;
        foreach ($this->getFieldsInOrder() as $formField) {
            if (!$formField->canBeFilled()) {
                continue;
            }

            foreach ($filledFields as $filledField) {
                if ($filledField->form_field_id === $formField->id) {
                    if ($formField->isFilled($filledField) === true) {
                        continue 2;
                    } else {
                        return false;
                    }
                }
            }

            $completed = false;
        }

        return $completed;
    }

    /**
     * @inheritDoc
     */
    public function renderFormSettings(AppView $appView): string
    {
        return $appView->element('Forms/standard_request_settings', [
            'form' => $this,
        ]);
    }
}
