<?php
declare(strict_types=1);

namespace App\Form;

use App\Model\Entity\FormField;
use App\Model\Entity\FormFieldType;
use App\Model\Entity\FormSetting;
use App\Model\Entity\Request;
use App\Model\Table\FormFieldsTable;
use App\Model\Table\RequestFilledFieldsTable;
use App\View\AppView;
use Cake\I18n\Number;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;

class SocialServicesStaffingFormController extends AbstractFormController
{
    private const SETTING_WORKLOAD_MAX = 'social_staffing.workload.max';
    /**
     * @var array [tableName => FormField]
     */
    private array $_tableFields = [];

    /**
     * @inheritDoc
     */
    public function render(AppView $appView, array $options = []): ?string
    {
        $returnAsString = isset($options['returnAsString']) ? $options['returnAsString'] === true : false;
        $readOnly = Hash::get($options, 'readOnly') === true;

        $return = $appView->element('Forms/social_services_staffing_form', [
            'form' => $this,
            'request' => $this->getUserRequest(),
            'enableInput' => !$readOnly,
        ]);

        if ($returnAsString) {
            return $return;
        }

        echo $return;

        return null;
    }

    public function setup(): self
    {
        /** @var FormFieldsTable $formFieldsTable */
        $formFieldsTable = $this->getTableLocator()->get('FormFields');

        /** @var FormField[] $queryExisting */
        $queryExisting = $formFieldsTable->find('all', [
            'conditions' => [
                'form_id' => $this->getFormDefinition()->id,
                'is_required' => true,
                'form_field_type_id' => FormFieldType::FIELD_TEXT,
            ],
        ]);

        $tableDefinitions = $this->getTables();
        $knownTables = array_keys($tableDefinitions);
        $maxOrder = 1;

        foreach ($queryExisting as $existingField) {
            if (in_array($existingField->name, $knownTables, true)) {
                $this->_tableFields[$existingField->name] = $existingField;
                $maxOrder = max($existingField->field_order, $maxOrder);
            } else {
                $formFieldsTable->delete($existingField);
            }
        }

        foreach ($this->getTables() as $tableName => $tableSettings) {
            if (!isset($this->_tableFields[$tableName])) {
                $newField = $formFieldsTable->newEntity([
                    'form_id' => $this->getFormDefinition()->id,
                    'name' => $tableName,
                    'is_required' => true,
                    'form_field_type_id' => FormFieldType::FIELD_TEXT,
                    'field_order' => $maxOrder++,
                ]);
                if ($formFieldsTable->save($newField)) {
                    $this->_tableFields[$tableName] = $newField;
                }
            }
        }

        $formSettingsTable = $this->getTableLocator()->get('FormSettings');
        $this->_settings[self::SETTING_WORKLOAD_MAX] = $formSettingsTable->findOrCreate([
            'form_id' => $this->getFormDefinition()->id,
            'name' => self::SETTING_WORKLOAD_MAX,
        ], function (FormSetting $setting) {
            // 2021 => maximum of 2016 worked hours
            $setting->value = 2016;
        });

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function isFormFilledCompletely(?Request $request = null): bool
    {
        foreach ($this->getTables() as $table => $tableSettings) {
            foreach ($tableSettings['groups'] as $groupName => $groupLabel) {
                if (!is_array($groupLabel)) {
                    if ($this->getTableFilledRowsCount($table, strval($groupName)) > 0) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    public function renderFormSettings(AppView $appView): string
    {
        $description = $appView->asHtmlListRecursive([
            'Přehled personálního zajištění',
            'Pokrývá DPČ, DPP, HPP a dobrovolníky',
            'Je vyžadováno vyplnit alespoň jednoho zaměstnance v celém formuláři',
        ]);

        $form = $appView->Form->create($this->getFormDefinition())
            . $appView->Form->control('form_settings.0.id', ['value' => $this->_settings[self::SETTING_WORKLOAD_MAX]->id])
            . $appView->Form->control('form_settings.0.value', ['type' => 'number', 'label' => __('Počet pracovních hodin pro přepočet DPP hodin na úvazky')])
            . $appView->Form->submit(__('Uložit nastavení'), ['class' => 'btn btn-success'])
            . $appView->Form->end();

        return $description . '<hr/>' . $form;
    }

    public function getTables(): array
    {
        return [
            'dpc' => [
                'colgroup' => ['w-30', '', '', '', 'w-20', 'w-20'],
                'thead' => [
                    __('Dohoda o pracovní činnosti'),
                    __('Počet osob<br/><small>v dané pracovní pozici</small>'),
                    __('Úvazek<br/><small>celkem za řádek</small>'),
                    __('Počet měsíců<br/><small>celkem za řádek</small>'),
                    __('Hrubá odměna<br/><small>v Kč celkem za řádek</small>'),
                    __('Z toho požadavek o dotaci od MČ'),
                ],
                'sums' => [
                    'name-or-function' => false,
                    'people' => 'number',
                    'worload' => 'decimal',
                    'months' => false,
                    'cost' => 'currency',
                    'subsidy' => 'currency',
                ],
                'columns' => [
                    'name-or-function' => [
                        'type' => 'text',
                        'placeholder' => __('Jméno a/nebo pozice'),
                    ],
                    'people' => [
                        'type' => 'number',
                        'step' => 1,
                        'default' => 1,
                    ],
                    'workload' => [
                        'type' => 'number',
                        'step' => .01,
                        'default' => 1,
                    ],
                    'months' => [
                        'type' => 'number',
                        'default' => 12,
                        'step' => 1,
                    ],
                    'cost' => [
                        'default' => 0,
                        'step' => .01,
                        'type' => 'number',
                        'template' => '<div class="input-group">%s<div class="input-group-append"><span class="input-group-text">Kč</span></div></div>',
                    ],
                    'subsidy' => [
                        'default' => 0,
                        'step' => .01,
                        'type' => 'number',
                        'template' => '<div class="input-group">%s<div class="input-group-append"><span class="input-group-text">Kč</span></div></div>',
                    ],
                ],
                'groups' => [
                    'dpc-a' => __('a. Pracovníci v přímé péči celkem'),
                    'dpc-b' => __('b. Ostatní pracovníci celkem'),
                ],
            ],
            'dpp' => [
                'colgroup' => ['w-30', '', '', 'w-15', 'w-15', 'w-15'],
                'thead' => [
                    __('Dohoda o provedení práce'),
                    __('Počet osob<br/><small>v dané pracovní pozici</small>'),
                    __('Počet odpracovaných hodin<br/><small>celkem za řádek</small>'),
                    __('Přepočteno na celé úvazky<br/><small>celkem za řádek</small>'),
                    __('Sjednaná odměna<br/><small>v Kč za hodinu</small>'),
                    __('Z toho požadavek o dotaci od MČ'),
                ],
                'sums' => [
                    'function' => false,
                    'people' => 'number',
                    'hours' => 'number',
                    'workload' => 'decimal',
                    'hour-price' => 'currency',
                    'subsidy' => 'currency',
                ],
                'columns' => [
                    'function' => [
                        'type' => 'text',
                        'placeholder' => __('Pracovní zařazení'),
                    ],
                    'people' => [
                        'type' => 'number',
                        'step' => 1,
                        'default' => 1,
                    ],
                    'hours' => [
                        'type' => 'number',
                        'step' => 1,
                        'default' => 1,
                        'data-fraction-provider' => 'hours',
                    ],
                    'workload' => [
                        'type' => 'number',
                        'step' => .01,
                        'default' => 1,
                        'data-fraction-of' => intval($this->getSettingValue(self::SETTING_WORKLOAD_MAX, 2016)),
                        'data-fraction-source' => 'hours',
                    ],
                    'hour-price' => [
                        'type' => 'number',
                        'step' => 1,
                        'default' => 0,
                    ],
                    'subsidy' => [
                        'type' => 'number',
                        'step' => 1,
                        'default' => 0,
                    ],
                ],
                'groups' => [
                    'dpp-a' => __('a. Pracovníci v přímé péči celkem'),
                    'dpp-b' => __('b. Ostatní pracovníci celkem'),
                ],
            ],
            'employees' => [
                'columns' => [
                    'function ' => [
                        'type' => 'text',
                        'placeholder' => __('Pracovní náplň'),
                    ],
                    'people' => [
                        'type' => 'number',
                        'step' => 1,
                        'default' => 1,
                    ],
                    'workload' => [
                        'type' => 'number',
                        'step' => .01,
                        'default' => 1,
                    ],
                    'year-salary' => [
                        'type' => 'number',
                        'step' => 1,
                        'default' => 0,
                    ],
                    'subsidy' => [
                        'type' => 'number',
                        'step' => 1,
                        'default' => 0,
                    ],
                ],
                'thead' => [
                    __('Přehled zaměstnanců'),
                    __('Počet osob'),
                    __('Přepočteno na celé úvazky'),
                    __('Hrubá mzda<br/><small>v Kč za rok celkem</small>'),
                    __('Z toho požadavek o dotaci od MČ'),
                ],
                'sums' => [
                    'function' => false,
                    'people' => 'number',
                    'workload' => 'decimal',
                    'year-salary' => 'decimal',
                    'subsidy' => 'decimal',
                ],
                'groups' => [
                    '1' => [
                        'label' => __('1. Pracovníci v přímé péči celkem'),
                        'children' => ['11', '12', '13', '14'],
                    ],
                    '11' => __('1.1 Sociální pracovník'),
                    '12' => __('1.2 Pracovník v sociálních službách'),
                    '13' => __('1.3 Zdravotničtí pracovníci'),
                    '14' => __('1.4 Pedagogičtí pracovníci'),
                    '2' => [
                        'label' => __('Administrativní a ostatní pracovníci celkem'),
                        'children' => ['21', '22'],
                    ],
                    '21' => __('2.1 Vedoucí a administrativní pracovníci'),
                    '22' => __('2.2 Ostatní pracovníci'),
                ],
            ],
            'volunteers' => [
                'thead' => [
                    __('Dobrovolníci'),
                    __('Počet osob'),
                    __('Počet hodin za měsíc'),
                ],
                'sums' => [
                    'function' => false,
                    'people' => 'number',
                    'hours-per-month' => 'number',
                ],
                'columns' => [
                    'function' => [
                        'type' => 'text',
                        'placeholder' => __('Pracovní náplň'),
                    ],
                    'people' => [
                        'type' => 'number',
                        'step' => 1,
                        'default' => 1,
                    ],
                    'hours-per-month' => [
                        'type' => 'number',
                        'step' => 1,
                        'default' => 1,
                    ],
                ],
                'groups' => [
                    'default' => __('Dobrovolníci celkem'),
                ],
            ],
        ];
    }

    public function getFormattedData(string $dataKey, string $tableName, string $columnName): ?string
    {
        $data = $this->getData($dataKey);
        $format = $this->getTables()[$tableName]['sums'][$columnName] ?? null;

        if ($data === null) {
            return null;
        }

        switch ($format) {
            default:
                return $data;
            case 'currency':
                return Number::currency($data, 'CZK');
            case 'number':
                return Number::format($data, ['precision' => 0]);
            case 'decimal':
                return Number::format($data, ['precision' => 2, 'places' => 2]);
        }
    }

    public function getSum(string $tableName, string $columnName, string $groupName): string
    {
        $sumFunction = $this->getTables()[$tableName]['sums'][$columnName] ?? false;

        $groupLabel = $this->getTables()[$tableName]['groups'][$groupName];
        $groups = is_array($groupLabel) ? $groupLabel['children'] : [$groupName];

        $sum = 0;

        foreach ($groups as $groupName) {
            $sum += Hash::reduce($this->getData(), sprintf("%s.%s", $tableName, $groupName), function ($acc, $row) use ($columnName) {
                return $acc + floatval($row[$columnName]);
            });
        }

        switch ($sumFunction) {
            default:
                return '';
            case 'number':
                return Number::format($sum, ['precision' => 0]);
            case 'decimal':
                return Number::format($sum, ['places' => 2, 'precision' => 2]);
            case 'currency':
                return Number::currency($sum, 'CZK');
        }
    }

    /**
     * @inheritDoc
     */
    public function prefill(Request $request): self
    {
        $this->setUserRequest($request);

        foreach ($this->getFilledFields($request->id) as $field) {
            $definitionField = $this->getFieldById($field->form_field_id);
            if (!empty($definitionField)) {
                $this->_rawValues[$definitionField->name] = $field->value;
                $this->_data[$definitionField->name] = json_decode($field->value, true);
            }
        }

        return $this;
    }

    public function getTableFilledRowsCount(string $tableName, string $groupName = null): int
    {
        $key = $tableName;
        if (!empty($groupName)) {
            $key = sprintf("%s.%s", $tableName, $groupName);
        }

        return count($this->getData($key) ?? []);
    }

    protected function _execute(array $data)
    {
        /** @var RequestFilledFieldsTable $filledFieldsTable */
        $filledFieldsTable = TableRegistry::getTableLocator()->get('RequestFilledFields');

        foreach ($this->getTables() as $tableName => $tableSettings) {
            $filledTable = $filledFieldsTable->findOrCreate([
                'form_id' => $this->getFormDefinition()->id,
                'request_id' => $this->getUserRequest()->id,
                'form_field_id' => $this->getTableFormField($tableName)->id,
            ]);
            $filledTable->value = json_encode($this->filterEmptyRows($tableName, $data[$tableName] ?? []));

            if (!$filledFieldsTable->save($filledTable)) {
                return false;
            }
        }

        return true;
    }

    private function getTableFormField(string $tableName): FormField
    {
        return $this->_tableFields[$tableName];
    }

    private function filterEmptyRows(string $tableName, array $data): array
    {
        $firstKey = array_key_first($this->getTables()[$tableName]['columns']);
        $filteredRows = [];
        foreach ($this->getTables()[$tableName]['groups'] as $groupName => $label) {
            $filteredGroup = array_filter(Hash::extract($data, $groupName), function ($row) use ($firstKey) {
                // first column is always name of person / work function / etc..
                return !empty($row[$firstKey]);
            });
            $filteredRows[$groupName] = array_values($filteredGroup);
        }

        return $filteredRows;
    }
}
