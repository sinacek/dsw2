<?php

namespace App\Mailer;

use App\Model\Entity\OrganizationSetting;
use App\Model\Entity\Request;
use App\Model\Entity\User;
use Cake\Mailer\Mailer;
use InvalidArgumentException;

class UserMailer extends Mailer
{

    /**
     * @param User $user
     * @return Mailer
     */
    public function verifyEmail(User $user)
    {
        $this->viewBuilder()->setTemplate('verify_email');
        $siteName = h(OrganizationSetting::getSetting(OrganizationSetting::SITE_NAME));
        $title = sprintf("%s - %s", __d('email', 'Potvrzení registrace'), $siteName);
        if (empty($user->mail_verification_code)) {
            throw new InvalidArgumentException('mail_verification_code is empty, cannot send verifyEmail');
        }

        return $this->setTo($user->email)
            ->setSubject($title)
            ->setEmailFormat('both')
            ->setViewVars(
                [
                    'code' => $user->mail_verification_code,
                    'user_id' => $user->id,
                    'orgName' => $siteName,
                    'title' => $title,
                ]
            );
    }

    public function forgotPassword(User $user)
    {
        $this->viewBuilder()->setTemplate('lost_password');
        $siteName = h(OrganizationSetting::getSetting(OrganizationSetting::SITE_NAME));
        $title = sprintf("%s - %s", __d('email', 'Obnovení hesla'), $siteName);
        if (empty($user->mail_verification_code)) {
            throw new InvalidArgumentException('mail_verification_code is empty, cannot send forgotPassword');
        }

        return $this->setTo($user->email)
            ->setSubject($title)
            ->setEmailFormat('both')
            ->setViewVars(
                [
                    'code' => $user->mail_verification_code,
                    'user_id' => $user->id,
                    'orgName' => $siteName,
                    'title' => $title,
                ]
            );
    }

    public function invite(User $user)
    {
        $this->viewBuilder()->setTemplate('invite');
        $siteName = h(OrganizationSetting::getSetting(OrganizationSetting::SITE_NAME));
        $title = sprintf("%s - %s", __d('email', 'Pozvánka'), $siteName);
        if (empty($user->mail_verification_code)) {
            throw new InvalidArgumentException('mail_verification_code is empty, cannot send invite');
        }

        return $this->setTo($user->email)
            ->setSubject($title)
            ->setEmailFormat('both')
            ->setViewVars(
                [
                    'code' => $user->mail_verification_code,
                    'user_id' => $user->id,
                    'orgName' => $siteName,
                    'title' => $title,
                ]
            );
    }

    public function requestNotice(User $user, Request $request)
    {
        $this->viewBuilder()->setTemplate('request_notice');
        $siteName = h(OrganizationSetting::getSetting(OrganizationSetting::SITE_NAME));
        $title = sprintf("%s - %s", __d('email', 'Upozornění na změnu stavu žádosti'), $siteName);

        return $this->setTo($user->email)
            ->setSubject($title)
            ->setEmailFormat('both')
            ->setViewVars([
                'orgName' => $siteName,
                'title' => $title,
                'request' => $request,
            ]);
    }

    public function requestFormalControlReturned(User $user, Request $request)
    {
        $this->viewBuilder()->setTemplate('request_formal_control_returned');
        $siteName = h(OrganizationSetting::getSetting(OrganizationSetting::SITE_NAME));
        $title = sprintf("%s - %s", __d('email', 'Vaše žádost byla vrácena k opravě chyb'), $siteName);

        return $this->setTo($user->email)
            ->setSubject($title)
            ->setEmailFormat('both')
            ->setViewVars([
                'orgName' => $siteName,
                'title' => $title,
                'request' => $request,
            ]);
    }
}
