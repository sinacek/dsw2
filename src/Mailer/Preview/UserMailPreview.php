<?php

namespace App\Mailer\Preview;

use App\Middleware\OrgDomainsMiddleware;
use App\Model\Entity\User;
use App\Model\Table\UsersTable;
use DebugKit\Mailer\MailPreview;

/**
 * @property UsersTable $Users
 */
class UserMailPreview extends MailPreview
{
    /**
     * @return mixed
     */
    public function verifyEmail()
    {
        $this->loadModel('Users');
        /** @var User $user */
        $user = $this->Users->find('all')->firstOrFail();
        if (empty($user->mail_verification_code)) {
            $user->regenerateEmailToken();
        }

        return $this->getMailer('User')->verifyEmail(
            $user
        );
    }

    /**
     * @return mixed
     */
    public function forgotPassword()
    {
        $this->loadModel('Users');
        /** @var User $user */
        $user = $this->Users->find('all')->firstOrFail();
        if (empty($user->mail_verification_code)) {
            $user->regenerateEmailToken();
        }

        return $this->getMailer('User')->forgotPassword(
            $user
        );
    }
}
