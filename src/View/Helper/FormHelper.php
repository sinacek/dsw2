<?php

namespace App\View\Helper;

class FormHelper extends \Cake\View\Helper\FormHelper
{
    public function control($fieldName, array $options = [])
    {
        $class = isset($options['class']) ? $options['class'] : '';
        if ($this->isFieldError($fieldName)) {
            $class .= ' is-invalid';
        } else {
            //$class .= ' is-valid';
        }
        if (endsWith($fieldName, '._ids') && mb_strpos($class, 'treeselect') === false) {
            $class .= ' select2';
        }
        $options['templateVars']['class'] = $class;

        if ((isset($options['type']) && $options['type'] === 'checkbox') || isset($options['checked']) || (isset($options['value']) && is_bool($options['value']))) {
            $options['templateVars']['customType'] = 'form-check';
        }
        $options['nestedInput'] = false;

        if (isset($options['type']) && $options['type'] === 'file') {
            if (!isset($options['templates']) || !is_array($options['templates'])) {
                $options['templates'] = [];
            }
            $options['templates']['formgroup'] = '{{label}}{{input}}';
            $options['templates']['label'] = '<label class="custom-file-label" {{attrs}}>{{text}}{{tooltip}}</label>';
            $options['templateVars']['customType'] = 'custom-file mb-2';
        }

        return parent::control($fieldName, $options);
    }
}
