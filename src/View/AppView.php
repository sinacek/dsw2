<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     3.0.0
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */

namespace App\View;

use App\Model\Entity\BudgetItemType;
use App\Model\Entity\CsuLegalForm;
use App\Model\Entity\CsuMunicipality;
use App\Model\Entity\CsuMunicipalityPart;
use App\Model\Entity\File;
use App\Model\Entity\FormField;
use App\Model\Entity\FormFieldType;
use App\Model\Entity\Identity;
use App\Model\Entity\OrganizationSetting;
use App\Model\Entity\Request;
use App\Model\Entity\RequestBudget;
use App\Model\Entity\User;
use App\Model\Table\CsuLegalFormsTable;
use App\Model\Table\CsuMunicipalitiesTable;
use App\Model\Table\CsuMunicipalityPartsTable;
use App\View\Helper\FormHelper;
use App\View\Widget\BasicWidget;
use App\View\Widget\CustomDateWidget;
use Cake\ORM\Locator\LocatorAwareTrait;
use Cake\View\View;

/**
 * Application View
 *
 * Your application's default view class
 *
 * @link     https://book.cakephp.org/3.0/en/views.html#the-app-view
 * @property FormHelper $Form
 */
class AppView extends View
{
    use LocatorAwareTrait;

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading helpers.
     *
     * e.g. `$this->loadHelper('Html');`
     *
     * @return void
     */
    public function initialize()
    {
        $this->loadHelper(
            'Form',
            [
                'templates' => 'bootstrap_form',
                'className' => 'App\View\Helper\FormHelper',
            ]
        );

        $customDateWidget = new CustomDateWidget($this->Form->templater());
        $this->Form->addWidget('datetime', $customDateWidget);
        $this->Form->addWidget('date', $customDateWidget);
        $this->Form->addWidget('bank_code', new BasicWidget($this->Form->templater()));
    }

    public function isRequest()
    {
        if (!$this->getRequest()) {
            return false;
        }

        return true;
    }

    public function isUser()
    {
        if (!$this->getRequest()) {
            return false;
        }
        if (!$this->getRequest()->getSession()) {
            return false;
        }
        if (!($this->getRequest()->getSession()->read('Auth.User') instanceof User)) {
            return false;
        }

        return true;
    }

    public function getUser($field = false)
    {
        if (!$this->isUser()) {
            return null;
        }

        return $this->getRequest()->getSession()->read('Auth.User' . ($field === false ? '' : '.' . $field));
    }

    public function getUserEmail()
    {
        return h($this->getUser('email'));
    }

    public function getSiteName()
    {
        return h($this->getSiteSetting(OrganizationSetting::SITE_NAME));
    }

    public function getSiteLogo()
    {
        return h($this->getSiteSetting(OrganizationSetting::SITE_LOGO));
    }

    public function getSiteSetting(string $settingName, bool $returnDefaultIfNotSet = false)
    {
        $value = OrganizationSetting::getSetting($settingName);

        return $value === null ? ($returnDefaultIfNotSet ? OrganizationSetting::getDefaultValue($settingName) : null) : $value;
    }

    public function isGrantsManager()
    {
        return $this->getCurrentUser()->isGrantsManager();
    }

    public function isUsersManager()
    {
        return $this->getCurrentUser()->isUsersManager();
    }

    public function isSystemsManager()
    {
        return $this->getCurrentUser()->isSystemsManager();
    }

    public function isPortalsManager()
    {
        return $this->getCurrentUser()->isPortalsManager();
    }

    public function isManager()
    {
        return $this->getCurrentUser()->isManager();
    }

    public function isTeamMember()
    {
        return $this->getCurrentUser()->hasAtLeastOneTeam();
    }

    public function isTeamFormalControlor()
    {
        return $this->getCurrentUser()->hasFormalControlTeam();
    }

    public function isTeamEvaluator()
    {
        return $this->getCurrentUser()->hasCommentsTeam();
    }

    public function isTeamProposals()
    {
        return $this->getCurrentUser()->hasProposalsTeam();
    }

    public function isTeamApprovals()
    {
        return $this->getCurrentUser()->hasApprovalsTeam();
    }

    public function isTeamManagers()
    {
        return $this->getCurrentUser()->hasManagersTeam();
    }

    public function isWikiManager()
    {
        return $this->getCurrentUser()->isWikiManager();
    }

    public function isEconomicsManager()
    {
        return $this->getCurrentUser()->isEconomicsManager();
    }

    public function getCurrentUser(): User
    {
        // empty user will fail all auth checks
        $user = $this->getUser();
        if (empty($user) || !($user instanceof User)) {
            $user = new User();
        }

        return $user;
    }

    public function isUserIdentityCompleted(?int $identity_version = null): bool
    {
        return $this->getCurrentUser()->isIdentityCompleted($identity_version);
    }

    /**
     * @param int $user_id
     * @param int $user_identity_version
     * @param string|null $formatString if null, return identity array, else return formatted string
     * @param bool $forceReload
     * @return array|string
     */
    public function getUserIdentity(int $user_id, int $user_identity_version, string $formatString = null, bool $forceReload = false)
    {
        $identitiesTable = $this->getTableLocator()->get('Identities');
        $identityCacheKey = sprintf("identity.%d.%d", $user_id, $user_identity_version);
        $data = $this->getRequest()->getSession()->read($identityCacheKey);
        if (!is_array($data) || $forceReload === true) {
            $data = [];
            $query = $identitiesTable->find('all', [
                'conditions' => [
                    'Identities.user_id' => $user_id,
                    'Identities.version' => $user_identity_version,
                ],
            ]);
            foreach ($query as $identityRow) {
                $data[$identityRow->name] = $identityRow->value;
            }
            $this->getRequest()->getSession()->write($identityCacheKey, $data);
        }

        if (!empty($formatString)) {
            switch ($formatString) {
                default:
                case 'default':
                    return Identity::format($data);
            }
        }

        return $data;
    }

    /**
     * @param array $data
     * @param string $idField
     * @param string $nameField
     * @param string $childrenField
     * @param array $whitelist_ids
     * @param array $blacklist_ids
     * @return array
     */
    public function formatThreaded(iterable $data, $idField = 'id', $nameField = 'name', $childrenField = 'children', $whitelist_ids = [], $blacklist_ids = [])
    {
        $rtn = [];
        foreach ($data as $d) {
            if (in_array($d->{$idField}, $blacklist_ids) || (!empty($whitelist_ids) && in_array($d->{$idField}, $whitelist_ids))) {
                continue;
            }
            if (is_array($d->{$childrenField}) && !empty($d->{$childrenField})) {
                $formattedChildren = $this->formatThreaded($d->{$childrenField}, $idField, $nameField, $childrenField, $whitelist_ids, $blacklist_ids);
                if (!empty($formattedChildren)) {
                    $rtn[trim($d->{$nameField})] = $formattedChildren;
                }
            } else {
                $rtn[trim($d->{$idField})] = $d->{$nameField};
            }
        }

        return $rtn;
    }

    public function indexArrayById(array $data, string $idField = 'id')
    {
        $rtn = [];
        foreach ($data as $item) {
            $rtn[$item->{$idField}] = $item;
        }

        return $rtn;
    }

    public function asHtmlListRecursive(array $data)
    {
        $rtn = '<ul>';
        foreach ($data as $key => $val) {
            $rtn .= '<li>';
            if (!is_array($val)) {
                $rtn .= $val;
            } else {
                $rtn .= $key;
                $rtn .= $this->asHtmlListRecursive($val);
            }
            $rtn .= '</li>';
        }
        $rtn .= '</ul>';

        return $rtn;
    }

    public function asHtmlList(array $data, string $fieldname = 'name')
    {
        $rtn = '<ul>';
        foreach ($data as $key => $val) {
            $rtn .= '<li>';
            if (is_string($val)) {
                $rtn .= $val;
            } elseif (is_object($val) && isset($val->{$fieldname})) {
                $rtn .= $val->{$fieldname};
            } else {
                // fallback to toString method
                $rtn .= $val;
            }
            $rtn .= '</li>';
        }
        $rtn .= '</ul>';

        return $rtn;
    }

    public function threadedAsHtmlList($data, $whitelist_ids = [], $blacklist_ids = [], $idField = 'id', $nameField = 'name', $childrenField = 'children')
    {
        $formatted = $this->formatThreaded($data, $idField, $nameField, $childrenField, $whitelist_ids, $blacklist_ids);

        return $this->asHtmlListRecursive($formatted);
    }

    public function renderFormField(FormField $field, array $options = []): string
    {
        $rtn = "";
        $label = "";
        $currentFieldValue = $this->Form->getSourceValue($field->getFormControlId());
        $displayOnly = $options['displayOnly'] ?? false === true;

        if (!empty(trim(strip_tags($field->description)))) {
            $label = sprintf('<legend class="small">%s</legend>', $field->description);
        }

        switch ($field->form_field_type_id) {
            default:
                return sprintf("non-rendered-field:%d title:%s", $field->form_field_type_id, $field->name);
            case FormFieldType::FIELD_TITLE_NOT_INTERACTIVE:
                $rtn .= sprintf("<h2>%s</h2>", $field->name) . $label;
                break;
            case FormFieldType::FIELD_TEXT_NOT_INTERACTIVE:
                $rtn .= sprintf('<p>%s</p>', $field->name);
                $rtn .= sprintf("<div style='line-height: normal;' class='field-text-not-interactive'>%s</div>", $field->description);
                $rtn .= '<hr/>';
                break;
            case FormFieldType::FIELD_DOUBLE:
                if ($displayOnly) {
                    return $this->Number->format($currentFieldValue, ['places' => 2]);
                }
                $rtn .= $this->Form->control($field->getFormControlId(), ['type' => FormFieldType::getFormHelperType($field->form_field_type_id), 'step' => 0.01, 'label' => $field->name . $label, 'data-noquilljs' => 'data-noquilljs', 'required' => $field->is_required, 'escape' => false]);
                break;
            case FormFieldType::FIELD_TEXT:
            case FormFieldType::FIELD_VARCHAR:
            case FormFieldType::FIELD_INTEGER:
            case FormFieldType::FIELD_CHECKBOX:
            case FormFieldType::FIELD_DATE:
            case FormFieldType::FIELD_DATETIME:
                $rtn .= $this->Form->control($field->getFormControlId(), ['type' => FormFieldType::getFormHelperType($field->form_field_type_id), 'label' => $field->name . $label, 'data-noquilljs' => 'data-noquilljs', 'required' => $field->is_required, 'escape' => false]);
                break;
            case FormFieldType::FIELD_YES_NO:
                if ($displayOnly) {
                    return var_export($currentFieldValue, true);
                }
                $rtn .= $this->Form->control($field->getFormControlId(), ['type' => 'radio', 'options' => ['yes' => 'ANO', 'no' => 'NE'], 'required' => $field->is_required, 'label' => $field->name . $label, 'escape' => false]);
                break;
            case FormFieldType::FIELD_FILE:
                $filename = (isset($options['file']) && $options['file'] instanceof File) ? $options['file']->original_filename : '';
                $rtn .= $this->Form->control($field->getFormControlId(), ['type' => 'file', 'required' => $field->is_required && empty($currentFieldValue), 'label' => empty($currentFieldValue) ? ($field->name . $label) : $filename, 'original-filename' => $filename, 'escape' => false]);
                break;
            case FormFieldType::FIELD_CHOICES:
                $rtn .= $this->Form->control($field->getFormControlId(), ['type' => FormFieldType::getFormHelperType($field->form_field_type_id), 'label' => $field->name . $label, 'required' => $field->is_required, 'options' => $field->getChoices(), 'escape' => false]);
                break;
            case FormFieldType::FIELD_TABLE:
                $rtn .= $this->element('form_field_table', ['formField' => $field, 'label' => $label, 'displayOnly' => $displayOnly, 'currentValue' => $currentFieldValue]);
                break;
        }

        return $rtn;
    }

    private array $budget_rows_rendered = [];

    public function budgetRows(int $budgetItemType, RequestBudget $budget, int $extraRowsToRender = 10, array $options = [])
    {
        $prefix = null;
        $existingCount = 0;
        $col1_class = 'col-md-8';
        $col2_class = 'col-md-4';
        $col3_class = '';
        $col4_class = '';
        $extendedRow = false;
        switch ($budgetItemType) {
            case BudgetItemType::TYPE_INCOME:
                $prefix = 'incomes';
                break;
            default:
            case BudgetItemType::TYPE_COST:
                $extendedRow = boolval($options['extended']) ?? false;
                if ($extendedRow) {
                    $col1_class = 'col-md-4';
                    $col2_class = 'col-md-2';
                    $col3_class = 'col-md-2';
                    $col4_class = 'col-md-4';
                }
                $prefix = 'costs';
                break;
            case BudgetItemType::TYPE_OWN_SOURCE:
                $prefix = 'own_sources';
                break;
            case BudgetItemType::TYPE_SUBSIDY:
                $prefix = 'other_subsidies';
                break;
        }
        if (isset($budget->{$prefix}) && !empty($budget->{$prefix})) {
            $existingCount = is_countable($budget->{$prefix}) ? count($budget->{$prefix}) : 0;
        }
        for ($counter = 0; $counter < ($existingCount + $extraRowsToRender); $counter++) {
            if (!isset($this->budget_rows_rendered[$prefix])) {
                $this->budget_rows_rendered[$prefix] = 0;
            }
            ?>
            <div class="row">
                <?php
                echo $this->Form->control(sprintf('%s.%d.budget_item_type_id', $prefix, $this->budget_rows_rendered[$prefix]), ['type' => 'hidden', 'value' => $budgetItemType]);
                echo $this->Form->control(sprintf('%s.%d.id', $prefix, $this->budget_rows_rendered[$prefix]), ['type' => 'hidden']); ?>
                <div class="<?= $col1_class ?>">
                    <?php echo $this->Form->control(sprintf('%s.%d.description', $prefix, $this->budget_rows_rendered[$prefix]), ['label' => __('Popis položky'), 'required' => false]) ?>
                </div>
                <?php if ($extendedRow): ?>
                    <div class="<?= $col3_class ?>">
                        <?php echo $this->Form->control(sprintf('%s.%d.original_amount', $prefix, $this->budget_rows_rendered[$prefix]), ['label' => __('Celková výše nákladu (Kč)'), 'required' => false, 'data-source-type' => $budgetItemType]) ?>
                    </div>
                    <div class="<?= $col2_class ?>">
                        <?php echo $this->Form->control(sprintf('%s.%d.amount', $prefix, $this->budget_rows_rendered[$prefix]), ['label' => __('Požadovaná částka (Kč)'), 'required' => false, 'data-source-type' => '0']) ?>
                    </div>
                    <div class="<?= $col4_class ?>">
                        <?php echo $this->Form->control(sprintf('%s.%d.comment', $prefix, $this->budget_rows_rendered[$prefix]), ['label' => __('Komentář (nepovinný)'), 'required' => false]) ?>
                    </div>
                <?php else: ?>
                    <div class="<?= $col2_class ?>">
                        <?php echo $this->Form->control(sprintf('%s.%d.amount', $prefix, $this->budget_rows_rendered[$prefix]), ['label' => __('Částka (Kč)'), 'required' => false, 'data-source-type' => $budgetItemType]) ?>
                    </div>
                <?php endif; ?>
            </div>
            <?php
            $this->budget_rows_rendered[$prefix]++;
        }
    }

    public function canSwitchUsers(): bool
    {
        return !empty($this->getCurrentUser()->getOriginIdentity($this->getRequest())->allowed_users);
    }

    /**
     * @return User[]
     */
    public function getAllowedUsersForSwitch(): iterable
    {
        $origin = $this->getCurrentUser()->getOriginIdentity($this->getRequest());
        $allowed_users = [];

        if ($this->getCurrentUser()->id !== $origin->id) {
            $allowed_users[] = $origin;
        }

        foreach ($origin->allowed_users ?? [] as $allowed_link) {
            if ($allowed_link->origin_user instanceof User && $allowed_link->origin_user->id !== $this->getCurrentUser()->id) {
                $allowed_users[] = $allowed_link->origin_user;
            }
        }

        return $allowed_users;
    }

    public function isCurrentlyOriginUser(): bool
    {
        $origin = $this->getCurrentUser()->getOriginIdentity($this->getRequest());

        return $origin->id === $this->getCurrentUser()->id;
    }

    public function isRequestOwner(Request $request): bool
    {
        $origin = $this->getCurrentUser()->getOriginIdentity($this->getRequest());

        return $request->user_id === $origin->id;
    }

    public function getLegalFormName(?int $legalFormNumber): ?string
    {
        if (empty($legalFormNumber)) {
            return null;
        }

        /** @var CsuLegalFormsTable $csuLegalForms */
        $csuLegalForms = $this->getTableLocator()->get('CsuLegalForms');

        /** @var CsuLegalForm|null $formByNumber */
        $formByNumber = $csuLegalForms->find('all', [
            'conditions' => [
                'number' => $legalFormNumber,
            ]])->first();

        return $formByNumber ? $formByNumber->name : null;
    }

    public function getCityName(?int $municipalityNumber): ?string
    {
        if (empty($municipalityNumber)) {
            return null;
        }

        /** @var CsuMunicipalitiesTable $csuMunicipalities */
        $csuMunicipalities = $this->getTableLocator()->get('CsuMunicipalities');

        /** @var CsuMunicipality $municipalityByNumber */
        $municipalityByNumber = $csuMunicipalities->find('all', [
            'conditions' => [
                'number' => $municipalityNumber,
            ],
        ])->first();

        return $municipalityByNumber ? $municipalityByNumber->name : null;
    }

    public function getCityPartName(?int $municipalityPartNumber): ?string
    {
        if (empty($municipalityPartNumber)) {
            return null;
        }

        /** @var CsuMunicipalityPartsTable $csuMunicipalityParts */
        $csuMunicipalityParts = $this->getTableLocator()->get('CsuMunicipalityParts');

        /** @var CsuMunicipalityPart $municipalityPartByNumber */
        $municipalityPartByNumber = $csuMunicipalityParts->find('all', [
            'conditions' => [
                'number' => $municipalityPartNumber,
            ],
        ])->first();

        return $municipalityPartByNumber ? $municipalityPartByNumber->name : null;
    }
}
