<?php

namespace App\Middleware;

use Cake\Http\Exception\ForbiddenException;

class DomainNotConfiguredException extends ForbiddenException
{
}
