<?php

namespace App\Middleware;

use App\Model\Entity\Domain;
use App\Model\Entity\Organization;
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Core\HttpApplicationInterface;
use Cake\Http\Response;
use Cake\Http\ServerRequest;
use Cake\ORM\TableRegistry;

class OrgDomainsMiddleware
{
    public const CACHING_KEY = 'org_domains_middleware_organizations';
    private static ?string $cacheConfig = null;
    /**
     * @var $domains Organization[]
     */
    private static ?array $organizations = null;

    protected ?HttpApplicationInterface $app = null;

    private static ?Organization $currentOrganization = null;
    private static ?Domain $currentDomain = null;

    public function __construct(HttpApplicationInterface $app = null, $cacheConfig = 'default')
    {
        self::$cacheConfig = $cacheConfig;
        $this->app = $app;
    }

    private function loadOrganization(ServerRequest $request)
    {
        if (!empty(self::$organizations)) {
            foreach (self::$organizations as $organization) {
                foreach ($organization->domains as $domain) {
                    // domain name is unique in database column, so this is safe
                    if ($domain->domain === $request->host()) {
                        self::$currentDomain = $domain;
                        self::$currentOrganization = $organization;
                        break 2;
                    }
                }
            }
        }

        if (self::$currentOrganization === null) {
            $domainsTable = TableRegistry::getTableLocator()->get('Domains');
            self::$currentDomain = $domainsTable->find(
                'all',
                [
                    'contain' => [
                        'Organizations',
                        'Organizations.OrganizationSettings',
                    ],
                    'conditions' => [
                        'Domains.domain' => $request->host(),
                    ],
                ]
            )->first();
            if (self::$currentDomain) {
                self::$currentOrganization = self::$currentDomain->organization;
            }
        }
    }

    private function makeCache()
    {
        if (Cache::enabled() && Cache::getConfig(self::$cacheConfig)) {
            // read cache if possible
            if (self::$organizations === null) {
                self::$organizations = Cache::read(self::CACHING_KEY, self::$cacheConfig) ?: null;
            }
        }

        if (self::$organizations === null) {
            $organizationsTable = TableRegistry::getTableLocator()->get('Organizations');
            self::$organizations = $organizationsTable->find(
                'all',
                [
                    'contain' => [
                        'Domains',
                        'OrganizationSettings',
                    ],
                ]
            )->all()->toArray();

            if (Cache::enabled() && Cache::getConfig(self::$cacheConfig)) {
                // save cache if possible
                Cache::write(self::CACHING_KEY, self::$organizations, self::$cacheConfig);
            }
        }
    }

    /**
     * Checks if the domain / organization is currently enabled, and allowed to be accessed
     *
     * @param ServerRequest $request The request.
     * @param Response $response The response.
     * @param callable $next Callback to invoke the next middleware.
     * @return Response A response
     */
    public function __invoke(ServerRequest $request, Response $response, $next)
    {
        $this->makeCache();
        $this->loadOrganization($request);
        $domain = $_SERVER['HTTP_HOST'] ?? '';

        if (empty(self::$currentOrganization)) {
            // disable debug in case it's enabled
            Configure::write('debug', false);
            throw new DomainNotConfiguredException(sprintf('Doména %s není nastavena', $domain));
        }

        if (!self::$currentDomain->is_enabled) {
            // disable debug in case it's enabled
            Configure::write('debug', false);
            throw new DomainNotConfiguredException(sprintf('Doména %s je aktuálně zakázána', $domain));
        }

        Configure::write('App.fullBaseUrl', 'https://' . self::$currentDomain->domain);
        self::$currentOrganization->storeConfiguration();

        return $next($request, $response);
    }

    public static function flushCache()
    {
        Cache::delete(self::CACHING_KEY, self::$cacheConfig ?: 'default');
    }

    public static function getOrganizationById(int $organization_id): ?Organization
    {
        foreach (self::$organizations as $organization) {
            if ($organization->id === $organization_id) {
                return $organization;
            }
        }

        return null;
    }

    public static function getCurrentOrganizationId(): int
    {
        return self::$currentOrganization->id;
    }

    public static function getCurrentOrganization(): Organization
    {
        return self::$currentOrganization;
    }

    public static function getCurrentDomain(): Domain
    {
        return self::$currentDomain;
    }
}
