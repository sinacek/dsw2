<?php

namespace App\Middleware;

use App\Traits\EmailThrowableTrait;

class ErrorHandlerMiddleware extends \ErrorEmail\Middleware\ErrorHandlerMiddleware
{
    use EmailThrowableTrait;
}
