<?php


namespace App\Command;

use App\Model\Entity\Request;
use App\Model\Entity\RequestState;
use App\Model\Table\IdentitiesTable;
use App\Model\Table\RequestsTable;
use Cake\Console\Arguments;
use Cake\Console\Command;
use Cake\Console\ConsoleIo;
use Throwable;

/**
 * @property-read RequestsTable $Requests
 * @property-read IdentitiesTable $Identities
 */
class CronCommand extends Command
{
    public function execute(Arguments $args, ConsoleIo $io): ?int
    {
        $finishedOK = true;

        $io->success('Running LockRequests');
        $result = $this->lockRequests($io);
        if ($result === null) {
            $io->success('LockRequests OK');
        } else {
            $finishedOK = false;
            $io->error('LockRequests error code ' . $result);
        }

        $io->success('Running LockIdentities');
        $result = $this->lockIdentities($io);
        if ($result === null) {
            $io->success('LockIdentities OK');
        } else {
            $finishedOK = false;
            $io->error('LockIdentities error code ' . $result);
        }

        return $finishedOK ? null : 1;
    }

    private function lockIdentities(ConsoleIo $io)
    {
        $this->loadModel('Requests');
        $this->loadModel('Identities');

        $requestsWithLockedIdentities = $this->Requests->find('all', [
            'conditions' => [
                'Requests.request_state_id NOT IN' => [
                    RequestState::STATE_NEW,
                    RequestState::STATE_READY_TO_SUBMIT,
                ],
            ],
        ]);

        /** @var Request $request */
        foreach ($requestsWithLockedIdentities as $request) {
            $incorrectUnlockedIdentities = $this->Identities->find('all', [
                'conditions' => [
                    'Identities.version' => $request->user_identity_version,
                    'Identities.user_id' => $request->user_id,
                    'Identities.is_locked' => false,
                ],
            ]);
            $count = $incorrectUnlockedIdentities->count();
            if ($count > 0) {
                $io->warning(sprintf('USER %d VERSION %d UNLOCKED %d', $request->user_id, $request->user_identity_version, $count));
            }

            if ($count > 0) {
                $io->success(sprintf("LOCKED ROWS %d", $this->Identities->updateAll([
                    'is_locked' => true,
                ], [
                    'Identities.version' => $request->user_identity_version,
                    'Identities.user_id' => $request->user_id,
                ])));
            }
        }

        // null return is success
        return null;
    }

    private function lockRequests(ConsoleIo $io): ?int
    {
        $this->loadModel('Requests');

        $requestsNotLocked = $this->Requests->find('all', [
            'conditions' => [
                'request_state_id >=' => RequestState::STATE_READY_TO_SUBMIT,
                'is_locked' => false,
            ],
        ]);

        /** @var Request $request */
        foreach ($requestsNotLocked as $request) {
            $shouldBeLocked = !RequestState::canUserEditRequest($request->request_state_id);
            if ($request->is_locked !== $shouldBeLocked) {
                $request->set('is_locked', $shouldBeLocked);
                $io->info(sprintf('Request %d is unlocked in state %d, correcting', $request->id, $request->request_state_id));
            }
            if (!$this->Requests->save($request)) {
                $io->error(sprintf('Could not correct request %d reason %s', $request->id, json_encode($request->getErrors())));
            }
        }

        $requestsToBeLockedAutomatically = $this->Requests->find(
            'all',
            [
                'conditions' => [
                    'request_state_id IN' => [RequestState::STATE_NEW, RequestState::STATE_READY_TO_SUBMIT],
                    'lock_when <' => date('Y-m-d'),
                ],
            ]
        );

        $io->info(sprintf('Requests to be locked %d', $requestsToBeLockedAutomatically->count()));

        foreach ($requestsToBeLockedAutomatically as $request) {
            /**
             * @var Request $request
             */
            $request->setCurrentStatus(RequestState::STATE_FORMAL_CHECK_REJECTED);

            try {
                if ($this->Requests->save($request) === false) {
                    $io->error(sprintf('Request %d cannot be modified, reasons: ', $request->id) . json_encode($request->getErrors()));

                    return 2;
                }
            } catch (Throwable $t) {
                $io->error($t->getMessage());
                $io->error($t->getTraceAsString());

                return 1;
            }
            $io->success(sprintf('Request ID:%d automatically transitioned to state REJECTED', $request->id));
        }

        $io->success('All done successfully');

        return null;
    }
}
