<?php

namespace App\Auth;

use App\Model\Entity\User;
use Cake\Auth\FormAuthenticate;
use Cake\Http\Response;
use Cake\Http\ServerRequest;

class FormSaltAuthenticate extends FormAuthenticate
{
    public function authenticate(ServerRequest $request, Response $response)
    {
        $fields = $this->_config['fields'];
        if (!$this->_checkFields($request, $fields)) {
            return false;
        }

        /**
 * @var User $user_by_email
*/
        $user_by_email = $this->_query($request->getData($fields['username']))->first();

        if (empty($user_by_email)) {
            return false;
        }

        return password_verify($request->getData($fields['password']) . $user_by_email->salt, $user_by_email->password) ?
            $user_by_email : [];
    }
}
