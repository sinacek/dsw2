<?php
namespace App\Model\Table;

use App\Model\Entity\AppealsToProgram;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * AppealsToPrograms Model
 *
 * @property AppealsTable&BelongsTo $Appeals
 * @property ProgramsTable&BelongsTo $Programs
 *
 * @method AppealsToProgram get($primaryKey, $options = [])
 * @method AppealsToProgram newEntity($data = null, array $options = [])
 * @method AppealsToProgram[] newEntities(array $data, array $options = [])
 * @method AppealsToProgram|false save(EntityInterface $entity, $options = [])
 * @method AppealsToProgram saveOrFail(EntityInterface $entity, $options = [])
 * @method AppealsToProgram patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method AppealsToProgram[] patchEntities($entities, array $data, array $options = [])
 * @method AppealsToProgram findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class AppealsToProgramsTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('appeals_to_programs');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Appeals', [
            'foreignKey' => 'appeal_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Programs', [
            'foreignKey' => 'program_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['appeal_id'], 'Appeals'));
        $rules->add($rules->existsIn(['program_id'], 'Programs'));

        return $rules;
    }
}
