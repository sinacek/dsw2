<?php

namespace App\Model\Table;

use App\Model\Entity\AppEntity;
use App\Model\Entity\Realm;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * Realms Model
 *
 * @property FondsTable&BelongsTo $Fonds
 * @property ProgramsTable&HasMany $Programs
 *
 * @method Realm get($primaryKey, $options = [])
 * @method Realm newEntity($data = null, array $options = [])
 * @method Realm[] newEntities(array $data, array $options = [])
 * @method Realm|false save(EntityInterface $entity, $options = [])
 * @method Realm saveOrFail(EntityInterface $entity, $options = [])
 * @method Realm patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method Realm[] patchEntities($entities, array $data, array $options = [])
 * @method Realm findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class RealmsTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('realms');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo(
            'Fonds',
            [
                'foreignKey' => 'fond_id',
                'joinType' => 'INNER',
            ]
        );
        $this->hasMany(
            'Programs',
            [
                'foreignKey' => 'realm_id',
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('description')
            ->allowEmptyString('description');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['fond_id'], 'Fonds'));

        $rules->addDelete(
            function ($entity, $options) {
                return $this->fkCheck($entity, $options);
            },
            'fkCheck',
            [
                'errorField' => 'fks',
                'message' => __('Nelze smazat kvůli existujícím vazbám'),
            ]
        );

        return $rules;
    }

    /**
     * @inheritDoc
     */
    public function fkCheck(AppEntity $entity, $options)
    {
        if ($entity->isNew()) {
            return true;
        }

        $withAssociations = $this->get(
            $entity->id,
            [
                'contain' => [
                    'Programs.Appeals.Requests',
                    'Programs.Requests',
                ],
            ]
        );

        foreach ($withAssociations->programs as $program) {
            if (!empty($program->appeals)) {
                return __('Nelze smazat oblast podpory s programy aktivními ve výzvách');
            }
            if (!empty($program->requests)) {
                return __('Nelze smazat oblast podpory s programy které obsahují rozpracované žádosti');
            }
        }

        return true;
    }
}
