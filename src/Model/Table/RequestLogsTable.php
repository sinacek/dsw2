<?php

namespace App\Model\Table;

use App\Model\Entity\RequestLog;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * RequestLogs Model
 *
 * @property RequestsTable&BelongsTo $Requests
 * @property RequestStatesTable&BelongsTo $RequestStates
 * @property UsersTable&BelongsTo $Users
 *
 * @method RequestLog get($primaryKey, $options = [])
 * @method RequestLog newEntity($data = null, array $options = [])
 * @method RequestLog[] newEntities(array $data, array $options = [])
 * @method RequestLog|false save(EntityInterface $entity, $options = [])
 * @method RequestLog saveOrFail(EntityInterface $entity, $options = [])
 * @method RequestLog patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method RequestLog[] patchEntities($entities, array $data, array $options = [])
 * @method RequestLog findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class RequestLogsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('request_logs');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Requests', [
            'foreignKey' => 'request_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('RequestStates', [
            'foreignKey' => 'request_state_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'executed_by_user_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('lock_comment')
            ->allowEmptyString('lock_comment');

        $validator
            ->boolean('is_locked')
            ->requirePresence('is_locked', 'create')
            ->notEmptyString('is_locked');

        $validator
            ->scalar('comment')
            ->allowEmptyString('comment');

        $validator
            ->scalar('purpose')
            ->allowEmptyString('purpose');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['request_id'], 'Requests'));
        $rules->add($rules->existsIn(['request_state_id'], 'RequestStates'));
        $rules->add($rules->existsIn(['executed_by_user_id'], 'Users'));

        return $rules;
    }
}
