<?php

namespace App\Model\Table;

use App\Model\Entity\Identity;
use ArrayObject;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\Log\Log;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * Identities Model
 *
 * @property UsersTable&BelongsTo $Users
 * @property IdentitiesTable&BelongsTo $ParentIdentities
 * @property IdentitiesTable&HasMany $ChildIdentities
 *
 * @method Identity get($primaryKey, $options = [])
 * @method Identity newEntity($data = null, array $options = [])
 * @method Identity[] newEntities(array $data, array $options = [])
 * @method Identity|false save(EntityInterface $entity, $options = [])
 * @method Identity saveOrFail(EntityInterface $entity, $options = [])
 * @method Identity patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method Identity[] patchEntities($entities, array $data, array $options = [])
 * @method Identity findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class IdentitiesTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('identities');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('value')
            ->requirePresence('value', 'create')
            ->notEmptyString('value');

        $validator
            ->boolean('is_locked')
            ->notEmptyString('is_locked');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->isUnique(['user_id', 'version', 'name']));

        return $rules;
    }

    public function beforeSave(Event $event, EntityInterface $entity, ArrayObject $options)
    {
        if ($entity instanceof Identity) {
            if ($entity->is_locked === true && $entity->isDirty('value')) {
                Log::debug('prevent updating value in Identity which is locked');
                $event->stopPropagation();
                $event->setResult(false);
            }

            return;
        }
    }
}
