<?php

namespace App\Model\Table;

use App\Model\Entity\UsersToRole;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UsersToRoles Model
 *
 * @property UsersTable&BelongsTo $Users
 * @property UserRolesTable&BelongsTo $UserRoles
 * @property OrganizationsTable&BelongsTo $Organizations
 *
 * @method UsersToRole get($primaryKey, $options = [])
 * @method UsersToRole newEntity($data = null, array $options = [])
 * @method UsersToRole[] newEntities(array $data, array $options = [])
 * @method UsersToRole|false save(EntityInterface $entity, $options = [])
 * @method UsersToRole saveOrFail(EntityInterface $entity, $options = [])
 * @method UsersToRole patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method UsersToRole[] patchEntities($entities, array $data, array $options = [])
 * @method UsersToRole findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class UsersToRolesTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param  array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('users_to_roles');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo(
            'Users',
            [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
            ]
        );
        $this->belongsTo(
            'UserRoles',
            [
            'foreignKey' => 'user_role_id',
            'joinType' => 'INNER',
            ]
        );
        $this->belongsTo(
            'Organizations',
            [
            'foreignKey' => 'organization_id',
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param  Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param  RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['user_role_id'], 'UserRoles'));

        return $rules;
    }
}
