<?php

namespace App\Model\Table;

use App\Model\Entity\CsuMunicipalitiesToCsuMunicipalityPart;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CsuMunicipalitiesToCsuMunicipalityParts Model
 *
 * @method CsuMunicipalitiesToCsuMunicipalityPart get($primaryKey, $options = [])
 * @method CsuMunicipalitiesToCsuMunicipalityPart newEntity($data = null, array $options = [])
 * @method CsuMunicipalitiesToCsuMunicipalityPart[] newEntities(array $data, array $options = [])
 * @method CsuMunicipalitiesToCsuMunicipalityPart|false save(EntityInterface $entity, $options = [])
 * @method CsuMunicipalitiesToCsuMunicipalityPart saveOrFail(EntityInterface $entity, $options = [])
 * @method CsuMunicipalitiesToCsuMunicipalityPart patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method CsuMunicipalitiesToCsuMunicipalityPart[] patchEntities($entities, array $data, array $options = [])
 * @method CsuMunicipalitiesToCsuMunicipalityPart findOrCreate($search, callable $callback = null, $options = [])
 */
class CsuMunicipalitiesToCsuMunicipalityPartsTable extends Table
{
    /**
     * Initialize method
     *
     * @param  array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('csu_municipalities_to_csu_municipality_parts');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo(
            'CsuMunicipalities',
            [
            'foreignKey' => 'csu_municipalities_number',
            'bindingKey' => 'number',
            ]
        );

        $this->belongsTo(
            'CsuMunicipalityParts',
            [
            'foreignKey' => 'csu_municipality_parts_number',
            'bindingKey' => 'number',
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param  Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->integer('csu_municipalities_number')
            ->requirePresence('csu_municipalities_number', 'create')
            ->notEmptyString('csu_municipalities_number');

        $validator
            ->integer('csu_municipality_parts_number')
            ->requirePresence('csu_municipality_parts_number', 'create')
            ->notEmptyString('csu_municipality_parts_number');

        return $validator;
    }
}
