<?php

namespace App\Model\Table;

use App\Model\Entity\BudgetItemType;
use App\Model\Entity\RequestBudget;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * RequestBudgets Model
 *
 * @property RequestsTable&BelongsTo $Requests
 * @property BudgetItemsTable&HasMany $Incomes
 * @property BudgetItemsTable&HasMany $Costs
 * @property BudgetItemsTable&HasMany $OtherSubsidies
 * @property BudgetItemsTable&HasMany $OwnSources
 *
 * @method RequestBudget get($primaryKey, $options = [])
 * @method RequestBudget newEntity($data = null, array $options = [])
 * @method RequestBudget[] newEntities(array $data, array $options = [])
 * @method RequestBudget|false save(EntityInterface $entity, $options = [])
 * @method RequestBudget saveOrFail(EntityInterface $entity, $options = [])
 * @method RequestBudget patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method RequestBudget[] patchEntities($entities, array $data, array $options = [])
 * @method RequestBudget findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class RequestBudgetsTable extends Table
{
    /**
     * Initialize method
     *
     * @param  array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('request_budgets');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo(
            'Requests',
            [
            'foreignKey' => 'request_id',
            'joinType' => 'INNER',
            ]
        );
        $this->hasMany(
            'Incomes',
            [
            'className' => 'BudgetItems',
            'conditions' => [
                'budget_item_type_id' => BudgetItemType::TYPE_INCOME,
            ],
            'foreignKey' => 'request_budget_id',
            ]
        );
        $this->hasMany(
            'Costs',
            [
            'className' => 'BudgetItems',
            'conditions' => [
                'budget_item_type_id' => BudgetItemType::TYPE_COST,
            ],
            'foreignKey' => 'request_budget_id',
            ]
        );
        $this->hasMany(
            'OwnSources',
            [
            'className' => 'BudgetItems',
            'conditions' => [
                'budget_item_type_id' => BudgetItemType::TYPE_OWN_SOURCE,
            ],
            'foreignKey' => 'request_budget_id',
            ]
        );
        $this->hasMany(
            'OtherSubsidies',
            [
            'className' => 'BudgetItems',
            'conditions' => [
                'budget_item_type_id' => BudgetItemType::TYPE_SUBSIDY,
            ],
            'foreignKey' => 'request_budget_id',
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param  Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->decimal('total_income')
            ->notEmptyString('total_income');

        $validator
            ->decimal('total_costs')
            ->notEmptyString('total_costs');

        $validator
            ->decimal('total_own_sources')
            ->notEmptyString('total_own_sources');

        $validator
            ->decimal('total_other_subsidy')
            ->notEmptyString('total_other_subsidy');

        $validator
            ->decimal('requested_amount')
            ->notEmptyString('requested_amount');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param  RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['request_id'], 'Requests'));

        return $rules;
    }
}
