<?php

namespace App\Model\Table;

use App\Model\Entity\OrganizationSetting;
use App\Model\Entity\User;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsToMany;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;
use OldDsw\Model\Table\UctyTable;

/**
 * Users Model
 *
 * @property UserRolesTable&BelongsToMany $UserRoles
 * @property UsersToUsersTable&HasMany $AllowedUsers
 * @property UsersToUsersTable&HasMany $AllowedToUsers
 * @property TeamsTable&BelongsToMany $Teams
 * @property UsersToRolesTable&HasMany $UsersToRoles
 * @property UctyTable&BelongsToMany $StareUcty
 * @property PublicIncomeHistoriesTable&HasMany $PublicIncomeHistories
 * @property RequestsTable&HasMany $Requests
 *
 * @method User get($primaryKey, $options = [])
 * @method User newEntity($data = null, array $options = [])
 * @method User[] newEntities(array $data, array $options = [])
 * @method User|false save(EntityInterface $entity, $options = [])
 * @method User saveOrFail(EntityInterface $entity, $options = [])
 * @method User patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method User[] patchEntities($entities, array $data, array $options = [])
 * @method User findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class UsersTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('email');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsToMany(
            'UserRoles',
            [
                'through' => 'UsersToRoles',
            ]
        );

        $this->belongsToMany(
            'Teams',
            [
                'through' => 'TeamsToUsers',
            ]
        );

        $this->hasMany('UsersToRoles');
        $this->hasMany('PublicIncomeHistories');
        $this->hasMany('Requests');

        $this->hasMany('AllowedUsers', [
            'className' => 'UsersToUsers',
            'foreignKey' => 'allowed_user_id',
        ]);

        $this->hasMany('AllowedToUsers', [
            'className' => 'UsersToUsers',
            'foreignKey' => 'user_id',
        ]);

        $this->belongsToMany(
            'StareUcty',
            [
                'through' => 'UsersToOlddswAccounts',
                'targetForeignKey' => 'ucet_id',
                'propertyName' => 'stare_ucty',
                'className' => 'OldDsw.Ucty',
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->email('email', false, __('E-mail není ve správném formátu'))
            ->requirePresence('email', 'create')
            ->notEmptyString('email', __('E-mail nesmí být prázdný'))
            ->add('email', 'unique', ['rule' => 'validateUnique', 'provider' => 'table', 'message' => __('E-mail již je zaregistrován')]);

        $validator
            ->scalar('password')
            ->maxLength('password', 64)
            ->requirePresence('password', 'create')
            ->notEmptyString('password');

        $validator
            ->scalar('salt')
            ->maxLength('salt', 64)
            ->requirePresence('salt', 'create')
            ->notEmptyString('salt');

        $validator
            ->boolean('is_enabled')
            ->notEmptyString('is_enabled');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));

        return $rules;
    }

    /**
     * @param Query $query query to get modified
     * @param array $options query options
     * @return Query
     */
    public function findWithRoles(Query $query, array $options)
    {
        $query->contain(['UsersToRoles', 'AllowedUsers', 'AllowedUsers.OriginUser', 'Teams' => TeamsTable::ALL_TEAMS]);

        if (OrganizationSetting::getSetting(OrganizationSetting::HAS_DSW) === true) {
            $query->contain(['StareUcty']);
        }

        return $query;
    }

    public function findForSwitch(Query $query, array $options)
    {
        $query->contain(['UsersToRoles']);

        if (OrganizationSetting::getSetting(OrganizationSetting::HAS_DSW) === true) {
            $query->contain(['StareUcty']);
        }

        return $query;
    }
}
