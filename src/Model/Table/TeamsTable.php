<?php

namespace App\Model\Table;

use App\Model\Entity\Team;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Association\BelongsToMany;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Teams Model
 *
 * @property OrganizationsTable&BelongsTo $Organizations
 * @property UsersTable&BelongsToMany $Users
 * @property ProgramsTable&HasMany $FormalCheckPrograms
 * @property ProgramsTable&HasMany $PriceProposalPrograms
 * @property ProgramsTable&HasMany $PriceApprovalPrograms
 * @property ProgramsTable&HasMany $CommentsPrograms
 * @property ProgramsTable&HasMany $RequestManagerPrograms
 *
 * @method Team get($primaryKey, $options = [])
 * @method Team newEntity($data = null, array $options = [])
 * @method Team[] newEntities(array $data, array $options = [])
 * @method Team|false save(EntityInterface $entity, $options = [])
 * @method Team saveOrFail(EntityInterface $entity, $options = [])
 * @method Team patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method Team[] patchEntities($entities, array $data, array $options = [])
 * @method Team findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class TeamsTable extends AppTable
{
    public const ALL_TEAMS = [
        'FormalCheckPrograms', // formální kontrola
        'PriceProposalPrograms', // navrhovatelé
        'PriceApprovalPrograms', // schvalovatelé
        'CommentsPrograms', // hodnotitelé
        'RequestManagerPrograms', // finalizace
    ];

    /**
     * Initialize method
     *
     * @param  array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('teams');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo(
            'Organizations',
            [
            'foreignKey' => 'organization_id',
            'joinType' => 'INNER',
            ]
        );
        $this->belongsToMany(
            'Users',
            [
            'through' => 'TeamsToUsers',
            ]
        );
        $this->hasMany(
            'FormalCheckPrograms',
            [
            'className' => 'Programs',
            'foreignKey' => 'formal_check_team_id',
            ]
        );
        $this->hasMany(
            'PriceProposalPrograms',
            [
            'className' => 'Programs',
            'foreignKey' => 'price_proposal_team_id',
            ]
        );
        $this->hasMany(
            'PriceApprovalPrograms',
            [
            'className' => 'Programs',
            'foreignKey' => 'price_approval_team_id',
            ]
        );
        $this->hasMany(
            'CommentsPrograms',
            [
            'className' => 'Programs',
            'foreignKey' => 'comments_team_id',
            ]
        );
        $this->hasMany(
            'RequestManagerPrograms',
            [
            'className' => 'Programs',
            'foreignKey' => 'request_manager_team_id',
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param  Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param  RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['organization_id'], 'Organizations'));

        return $rules;
    }
}
