<?php

namespace App\Model\Table;

use App\Model\Entity\RequestState;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * RequestStates Model
 *
 * @method RequestState get($primaryKey, $options = [])
 * @method RequestState newEntity($data = null, array $options = [])
 * @method RequestState[] newEntities(array $data, array $options = [])
 * @method RequestState|false save(EntityInterface $entity, $options = [])
 * @method RequestState saveOrFail(EntityInterface $entity, $options = [])
 * @method RequestState patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method RequestState[] patchEntities($entities, array $data, array $options = [])
 * @method RequestState findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class RequestStatesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('request_states');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        return $validator;
    }
}
