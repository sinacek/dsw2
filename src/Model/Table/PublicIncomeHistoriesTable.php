<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PublicIncomeHistories Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\PublicIncomeSourcesTable&\Cake\ORM\Association\BelongsTo $PublicIncomeSources
 *
 * @method \App\Model\Entity\PublicIncomeHistory get($primaryKey, $options = [])
 * @method \App\Model\Entity\PublicIncomeHistory newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PublicIncomeHistory[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PublicIncomeHistory|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PublicIncomeHistory saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PublicIncomeHistory patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PublicIncomeHistory[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PublicIncomeHistory findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PublicIncomeHistoriesTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('public_income_histories');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('PublicIncomeSources', [
            'foreignKey' => 'public_income_source_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->integer('amount_czk')
            ->requirePresence('amount_czk', 'create')
            ->notEmptyString('amount_czk');

        $validator
            ->scalar('fiscal_year')
            ->requirePresence('fiscal_year', 'create')
            ->notEmptyString('fiscal_year');

        $validator
            ->scalar('project_name')
            ->maxLength('project_name', 255)
            ->requirePresence('project_name', 'create')
            ->notEmptyString('project_name');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['public_income_source_id'], 'PublicIncomeSources'));

        return $rules;
    }
}
