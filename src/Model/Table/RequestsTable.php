<?php

namespace App\Model\Table;

use App\Model\Entity\Request;
use App\Model\Entity\RequestState;
use Cake\Datasource\EntityInterface;
use Cake\Log\Log;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Association\BelongsToMany;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Association\HasOne;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * Requests Model
 *
 * @property OrganizationsTable&BelongsTo $Organizations
 * @property UsersTable&BelongsTo $Users
 * @property ProgramsTable&BelongsTo $Programs
 * @property AppealsTable&BelongsTo $Appeals
 * @property EvaluationsTable&HasMany $Evaluations
 * @property AttachmentsTable&HasMany $Attachments
 * @property FilesTable&BelongsToMany $Files
 * @property RequestBudgetsTable&HasOne $RequestBudgets
 * @property RequestLogsTable&HasMany $RequestLogs
 * @property IdentitiesTable&HasMany $Identities
 *
 * @method Request get($primaryKey, $options = [])
 * @method Request newEntity($data = null, array $options = [])
 * @method Request[] newEntities(array $data, array $options = [])
 * @method Request|false save(EntityInterface $entity, $options = [])
 * @method Request saveOrFail(EntityInterface $entity, $options = [])
 * @method Request patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method Request[] patchEntities($entities, array $data, array $options = [])
 * @method Request findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class RequestsTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('requests');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Organizations', [
            'foreignKey' => 'organization_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Programs', [
            'foreignKey' => 'program_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Appeals', [
            'foreignKey' => 'appeal_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('Evaluations', [
            'foreignKey' => 'request_id',
        ]);
        $this->belongsToMany('Files', [
            'className' => 'Files',
            'through' => 'Attachments',
        ]);
        $this->hasOne('RequestBudgets');
        $this->hasMany('Evaluations');
        $this->hasMany('RequestLogs');
        $this->hasMany('Identities', [
            'foreignKey' => [
                'user_id',
                'version',
            ],
            'bindingKey' => [
                'user_id',
                'user_identity_version',
            ],
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->boolean('is_locked')
            ->notEmptyString('is_locked');

        $validator
            ->date('lock_when')
            ->allowEmptyDate('lock_when');

        $validator
            ->boolean('is_reported')
            ->notEmptyString('is_reported');

        $validator
            ->boolean('is_settled')
            ->notEmptyString('is_settled');

        $validator
            ->integer('final_subsidy_amount')
            ->allowEmptyString('final_subsidy_amount');

        $validator
            ->integer('user_identity_version')
            ->requirePresence('user_identity_version', 'create')
            ->notEquals('user_identity_version', 0, null, function ($context) {
                return !RequestState::canUserEditRequest(intval($context['data']['request_state_id'] ?? RequestState::STATE_NEW));
            });

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['organization_id'], 'Organizations'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['program_id'], 'Programs'));
        $rules->add($rules->existsIn(['appeal_id'], 'Appeals'));

        return $rules;
    }
}
