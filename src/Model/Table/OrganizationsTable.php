<?php

namespace App\Model\Table;

use App\Model\Entity\Organization;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Organizations Model
 *
 * @property DomainsTable&HasMany $Domains
 * @property OrganizationSettingsTable&HasMany $OrganizationSettings
 * @property FondsTable&HasMany $Fonds
 *
 * @method Organization get($primaryKey, $options = [])
 * @method Organization newEntity($data = null, array $options = [])
 * @method Organization[] newEntities(array $data, array $options = [])
 * @method Organization|false save(EntityInterface $entity, $options = [])
 * @method Organization saveOrFail(EntityInterface $entity, $options = [])
 * @method Organization patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method Organization[] patchEntities($entities, array $data, array $options = [])
 * @method Organization findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class OrganizationsTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param  array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('organizations');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany(
            'Domains',
            [
            'foreignKey' => 'organization_id',
            ]
        );

        $this->hasMany(
            'OrganizationSettings',
            [
            'foreignKey' => 'organization_id',
            ]
        );

        $this->hasMany(
            'UsersToRoles',
            [
            'foreignKey' => 'organization_id',
            ]
        );

        $this->hasMany(
            'Fonds',
            [
            'foreignKey' => 'organization_id',
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param  Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        return $validator;
    }
}
