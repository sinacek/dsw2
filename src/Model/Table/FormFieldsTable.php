<?php

namespace App\Model\Table;

use App\Model\Entity\AppEntity;
use App\Model\Entity\FormField;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Association\HasMany;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * FormFields Model
 *
 * @property FormsTable&BelongsTo $Forms
 * @property FormFieldTypesTable&BelongsTo $FormFieldTypes
 * @property RequestFilledFieldsTable&HasMany $RequestFilledFields
 *
 * @method FormField get($primaryKey, $options = [])
 * @method FormField newEntity($data = null, array $options = [])
 * @method FormField[] newEntities(array $data, array $options = [])
 * @method FormField|false save(EntityInterface $entity, $options = [])
 * @method FormField saveOrFail(EntityInterface $entity, $options = [])
 * @method FormField patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method FormField[] patchEntities($entities, array $data, array $options = [])
 * @method FormField findOrCreate($search, callable $callback = null, $options = [])
 */
class FormFieldsTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('form_fields');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->belongsTo('Forms', [
            'foreignKey' => 'form_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('FormFieldTypes', [
            'foreignKey' => 'form_field_type_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('RequestFilledFields');
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('description')
            ->allowEmptyString('description');

        $validator
            ->boolean('is_required')
            ->notEmptyString('is_required');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['form_id'], 'Forms'));
        $rules->add($rules->existsIn(['form_field_type_id'], 'FormFieldTypes'));
        $rules->addDelete(
            function ($entity, $options) {
                return $this->fkCheck($entity, $options);
            },
            'fkCheck',
            [
                'errorField' => 'fks',
                'message' => __('Nelze smazat kvůli existujícím vazbám'),
            ]
        );

        return $rules;
    }

    public function fkCheck(AppEntity $entity, array $options)
    {
        if ($entity->isNew()) {
            return true;
        }

        $withAssociations = $this->get($entity->id, [
            'contain' => [
                'RequestFilledFields',
            ],
        ]);

        if (!empty($withAssociations->request_filled_fields)) {
            return __('Nelze smazat formulář, který byl již vyplněn');
        }

        return true;
    }
}
