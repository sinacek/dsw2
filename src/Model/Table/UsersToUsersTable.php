<?php

namespace App\Model\Table;

use App\Model\Entity\UsersToUser;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UsersToUsers Model
 *
 * @property UsersTable&BelongsTo $OriginUser
 * @property UsersTable&BelongsTo $AllowedUser
 *
 * @method UsersToUser get($primaryKey, $options = [])
 * @method UsersToUser newEntity($data = null, array $options = [])
 * @method UsersToUser[] newEntities(array $data, array $options = [])
 * @method UsersToUser|false save(EntityInterface $entity, $options = [])
 * @method UsersToUser saveOrFail(EntityInterface $entity, $options = [])
 * @method UsersToUser patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method UsersToUser[] patchEntities($entities, array $data, array $options = [])
 * @method UsersToUser findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class UsersToUsersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('users_to_users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('OriginUser', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
            'className' => 'Users',
        ]);
        $this->belongsTo('AllowedUser', [
            'foreignKey' => 'allowed_user_id',
            'joinType' => 'INNER',
            'className' => 'Users',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->notEqualToField('user_id', 'allowed_user_id', null, 'update')
            ->notEqualToField('allowed_user_id', 'user_id', null, 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'OriginUser'));
        $rules->add($rules->existsIn(['allowed_user_id'], 'AllowedUser'));

        return $rules;
    }
}
