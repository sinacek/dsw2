<?php

namespace App\Model\Table;

use App\Model\Entity\EvaluationCriterium;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * EvaluationCriteria Model
 *
 * @property EvaluationCriteriaTable&BelongsTo $ParentEvaluationCriteria
 * @property OrganizationsTable&BelongsTo $Organizations
 * @property EvaluationCriteriaTable&HasMany $ChildEvaluationCriteria
 *
 * @method EvaluationCriterium get($primaryKey, $options = [])
 * @method EvaluationCriterium newEntity($data = null, array $options = [])
 * @method EvaluationCriterium[] newEntities(array $data, array $options = [])
 * @method EvaluationCriterium|false save(EntityInterface $entity, $options = [])
 * @method EvaluationCriterium saveOrFail(EntityInterface $entity, $options = [])
 * @method EvaluationCriterium patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method EvaluationCriterium[] patchEntities($entities, array $data, array $options = [])
 * @method EvaluationCriterium findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class EvaluationCriteriaTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('evaluation_criteria');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo(
            'ParentEvaluationCriteria',
            [
                'className' => 'EvaluationCriteria',
                'foreignKey' => 'parent_id',
            ]
        );
        $this->addBehavior(
            'CounterCache',
            [
                'ParentEvaluationCriteria' => [
                    'max_points' => function ($event, $entity, Table $table, $original) {
                        return $table->find(
                            'all',
                            [
                                'conditions' => [
                                    'parent_id' => $entity->parent_id,
                                ],
                                'fields' => [
                                    'sum' => 'SUM(max_points)',
                                ],
                            ]
                        );
                    },
                ],
            ]
        );

        $this->belongsTo(
            'Organizations',
            [
                'foreignKey' => 'organization_id',
                'joinType' => 'INNER',
            ]
        );
        $this->hasMany(
            'ChildEvaluationCriteria',
            [
                'className' => 'EvaluationCriteria',
                'foreignKey' => 'parent_id',
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('parent_id')
            ->notEqualToField('parent_id', 'id', null, 'update')
            ->notEqualToField('id', 'parent_id', null, 'create')
            ->allowEmptyString('parent_id');

        $validator
            ->scalar('description')
            ->allowEmptyString('description');

        $validator
            ->integer('max_points')
            ->requirePresence('max_points', 'create')
            ->notEmptyString('max_points');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['parent_id'], 'ParentEvaluationCriteria'));
        $rules->add($rules->existsIn(['organization_id'], 'Organizations'));

        return $rules;
    }
}
