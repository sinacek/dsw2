<?php

namespace App\Model\Table;

use App\Model\Entity\AppEntity;
use App\ORM\Rule\LinkConstraint;
use Cake\ORM\Association;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Utility\Inflector;
use InvalidArgumentException;

class AppTable extends Table
{

    // can validate deeper than _addDeleteLinkConstraintRule (eg. 1-association-level)

    /**
     * @param AppEntity $entity entity on which the checks should run
     * @param array $options options
     * @return bool
     */
    public function fkCheck(AppEntity $entity, array $options)
    {
        return true;
    }

    // in CakePHP 4.x this can be replaced by $rules->isNotLinkedTo

    /**
     * @param RulesChecker $rules rules
     * @param Association|string $association association
     * @param string|null $errorField error field
     * @param string|null $message message
     * @param string $linkStatus link status
     * @param string $ruleName rule name
     * @return void
     */
    public function addDeleteLinkConstraintRule(
        RulesChecker $rules,
        $association,
        ?string $errorField,
        ?string $message,
        string $linkStatus = LinkConstraint::STATUS_NOT_LINKED,
        string $ruleName = '_isLinkedTo'
    ): void {
        if ($association instanceof Association) {
            $associationAlias = $association->getName();

            if ($errorField === null) {
                $errorField = $association->getProperty();
            }
        } elseif (is_string($association)) {
            $associationAlias = $association;

            if ($errorField === null) {
                $repository = $this->_options['repository'] ?? null;
                if ($repository instanceof Table) {
                    $association = $repository->getAssociation($association);
                    $errorField = $association->getProperty();
                } else {
                    $errorField = Inflector::underscore($association);
                }
            }
        } else {
            throw new InvalidArgumentException(
                sprintf(
                    'Argument 1 is expected to be of type `\Cake\ORM\Association|string`, `%s` given.',
                    getTypeName($association)
                )
            );
        }

        if (!$message) {
            $message = sprintf("%s %s", _('Nelze smazat kvůli závislým datům v'), $associationAlias);
        }

        $rule = new LinkConstraint(
            $association,
            $linkStatus
        );

        $rules->addDelete($rule, $ruleName, compact('errorField', 'message'));
    }
}
