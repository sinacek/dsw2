<?php

namespace App\Model\Table;

use App\Model\Entity\Domain;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * Domains Model
 *
 * @property OrganizationsTable&BelongsTo $Organizations
 *
 * @method Domain get($primaryKey, $options = [])
 * @method Domain newEntity($data = null, array $options = [])
 * @method Domain[] newEntities(array $data, array $options = [])
 * @method Domain|false save(EntityInterface $entity, $options = [])
 * @method Domain saveOrFail(EntityInterface $entity, $options = [])
 * @method Domain patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method Domain[] patchEntities($entities, array $data, array $options = [])
 * @method Domain findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class DomainsTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('domains');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Organizations', [
            'foreignKey' => 'organization_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('domain')
            ->maxLength('domain', 255)
            ->requirePresence('domain', 'create')
            ->notEmptyString('domain')
            ->add('domain', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['domain']));
        $rules->add($rules->existsIn(['organization_id'], 'Organizations'));

        return $rules;
    }

    /**
     * @param Query $query query to get modified
     * @param array $options query options
     * @return Query
     */
    public function findByOrganization(Query $query, array $options)
    {
        return $query->where(['id' => $options['id'], 'organization_id' => $options['organization_id']]);
    }
}
