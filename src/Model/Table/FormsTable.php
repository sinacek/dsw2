<?php

namespace App\Model\Table;

use App\Model\Entity\AppEntity;
use App\Model\Entity\Form;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Association\BelongsToMany;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * Forms Model
 *
 * @property OrganizationsTable&BelongsTo $Organizations
 * @property FormFieldsTable&HasMany $FormFields
 * @property ProgramsTable&BelongsToMany $Programs
 * @property RequestFilledFieldsTable&HasMany $RequestFilledFields
 * @property FormTypesTable&BelongsTo $FormTypes
 * @property FormSettingsTable&HasMany $FormSettings
 *
 * @method Form get($primaryKey, $options = [])
 * @method Form newEntity($data = null, array $options = [])
 * @method Form[] newEntities(array $data, array $options = [])
 * @method Form|false save(EntityInterface $entity, $options = [])
 * @method Form saveOrFail(EntityInterface $entity, $options = [])
 * @method Form patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method Form[] patchEntities($entities, array $data, array $options = [])
 * @method Form findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class FormsTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('forms');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo(
            'Organizations',
            [
                'foreignKey' => 'organization_id',
                'joinType' => 'INNER',
            ]
        );
        $this->belongsTo(
            'FormTypes',
            [
                'foreignKey' => 'form_type_id',
                'joinType' => 'INNER',
            ]
        );
        $this->hasMany(
            'FormFields',
            [
                'foreignKey' => 'form_id',
            ]
        );
        $this->belongsToMany(
            'Programs',
            [
                'through' => 'programs_to_forms',
            ]
        );
        $this->hasMany('FormSettings', [
            'foreignKey' => 'form_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['organization_id'], 'Organizations'));
        $rules->addDelete(
            function ($entity, $options) {
                return $this->fkCheck($entity, $options);
            },
            'fkCheck',
            [
                'errorField' => 'fks',
                'message' => __('Nelze smazat kvůli existujícím vazbám'),
            ]
        );

        return $rules;
    }

    public function fkCheck(AppEntity $entity, array $options)
    {
        if ($entity->isNew()) {
            return true;
        }

        $withAssociations = $this->get($entity->id, [
            'contain' => [
                'FormFields',
                'FormFields.RequestFilledFields',
            ],
        ]);

        foreach ($withAssociations->form_fields as $form_field) {
            if (!empty($form_field->request_filled_fields)) {
                return __('Nelze smazat formulář, který byl již vyplněn');
            }
        }

        return true;
    }
}
