<?php
namespace App\Model\Table;

use App\Model\Entity\OrganizationSetting;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OrganizationSettings Model
 *
 * @property OrganizationsTable&BelongsTo $Organizations
 *
 * @method OrganizationSetting get($primaryKey, $options = [])
 * @method OrganizationSetting newEntity($data = null, array $options = [])
 * @method OrganizationSetting[] newEntities(array $data, array $options = [])
 * @method OrganizationSetting|false save(EntityInterface $entity, $options = [])
 * @method OrganizationSetting saveOrFail(EntityInterface $entity, $options = [])
 * @method OrganizationSetting patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method OrganizationSetting[] patchEntities($entities, array $data, array $options = [])
 * @method OrganizationSetting findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class OrganizationSettingsTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('organization_settings');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Organizations', [
            'foreignKey' => 'organization_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('value')
            ->requirePresence('value', 'create')
            ->notEmptyString('value');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['organization_id'], 'Organizations'));

        return $rules;
    }
}
