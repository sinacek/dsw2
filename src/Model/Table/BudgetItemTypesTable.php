<?php

namespace App\Model\Table;

use App\Model\Entity\BudgetItemType;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * BudgetItemTypes Model
 *
 * @property BudgetItemsTable&HasMany $BudgetItems
 *
 * @method BudgetItemType get($primaryKey, $options = [])
 * @method BudgetItemType newEntity($data = null, array $options = [])
 * @method BudgetItemType[] newEntities(array $data, array $options = [])
 * @method BudgetItemType|false save(EntityInterface $entity, $options = [])
 * @method BudgetItemType saveOrFail(EntityInterface $entity, $options = [])
 * @method BudgetItemType patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method BudgetItemType[] patchEntities($entities, array $data, array $options = [])
 * @method BudgetItemType findOrCreate($search, callable $callback = null, $options = [])
 */
class BudgetItemTypesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('budget_item_types');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->hasMany('BudgetItems', [
            'foreignKey' => 'budget_item_type_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('type_name')
            ->maxLength('type_name', 255)
            ->requirePresence('type_name', 'create')
            ->notEmptyString('type_name');

        return $validator;
    }
}
