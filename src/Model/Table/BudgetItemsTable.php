<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * BudgetItems Model
 *
 * @property \App\Model\Table\RequestBudgetsTable&\Cake\ORM\Association\BelongsTo $RequestBudgets
 *
 * @method \App\Model\Entity\BudgetItem get($primaryKey, $options = [])
 * @method \App\Model\Entity\BudgetItem newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\BudgetItem[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\BudgetItem|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\BudgetItem saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\BudgetItem patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\BudgetItem[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\BudgetItem findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class BudgetItemsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('budget_items');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('RequestBudgets', [
            'foreignKey' => 'request_budget_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('BudgetItemTypes');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->decimal('amount')
            ->notEmptyString('amount');

        $validator
            ->scalar('description')
            ->maxLength('description', 255)
            ->requirePresence('description', 'create')
            ->notEmptyString('description');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['request_budget_id'], 'RequestBudgets'));
        $rules->add($rules->existsIn(['budget_item_type_id'], 'BudgetItemTypes'));

        return $rules;
    }
}
