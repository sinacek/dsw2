<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * I18nMessages Model
 *
 * @method \App\Model\Entity\I18nMessage get($primaryKey, $options = [])
 * @method \App\Model\Entity\I18nMessage newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\I18nMessage[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\I18nMessage|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\I18nMessage saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\I18nMessage patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\I18nMessage[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\I18nMessage findOrCreate($search, callable $callback = null, $options = [])
 */
class I18nMessagesTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('i18n_messages');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('domain')
            ->maxLength('domain', 100)
            ->requirePresence('domain', 'create')
            ->notEmptyString('domain');

        $validator
            ->scalar('locale')
            ->maxLength('locale', 5)
            ->requirePresence('locale', 'create')
            ->notEmptyString('locale');

        $validator
            ->scalar('singular')
            ->maxLength('singular', 255)
            ->requirePresence('singular', 'create')
            ->notEmptyString('singular');

        $validator
            ->scalar('plural')
            ->maxLength('plural', 255)
            ->allowEmptyString('plural');

        $validator
            ->scalar('context')
            ->maxLength('context', 50)
            ->allowEmptyString('context');

        $validator
            ->scalar('value_0')
            ->maxLength('value_0', 255)
            ->allowEmptyString('value_0');

        $validator
            ->scalar('value_1')
            ->maxLength('value_1', 255)
            ->allowEmptyString('value_1');

        $validator
            ->scalar('value_2')
            ->maxLength('value_2', 255)
            ->allowEmptyString('value_2');

        return $validator;
    }
}
