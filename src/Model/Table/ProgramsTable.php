<?php

namespace App\Model\Table;

use App\Model\Entity\AppEntity;
use App\Model\Entity\Program;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Association\BelongsToMany;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * Programs Model
 *
 * @property ProgramsTable&BelongsTo $ParentPrograms
 * @property RealmsTable&BelongsTo $Realms
 * @property AppealsTable&BelongsToMany $Appeals
 * @property ProgramsTable&HasMany $ChildPrograms
 * @property EvaluationCriteriaTable&BelongsTo $EvaluationCriteria
 * @property FormsTable&BelongsToMany $Forms
 * @property TeamsTable&BelongsTo $FormalCheckTeams
 * @property RequestsTable&HasMany $Requests
 *
 * @method Program get($primaryKey, $options = [])
 * @method Program newEntity($data = null, array $options = [])
 * @method Program[] newEntities(array $data, array $options = [])
 * @method Program|false save(EntityInterface $entity, $options = [])
 * @method Program saveOrFail(EntityInterface $entity, $options = [])
 * @method Program patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method Program[] patchEntities($entities, array $data, array $options = [])
 * @method Program findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class ProgramsTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('programs');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo(
            'ParentPrograms',
            [
                'className' => 'Programs',
                'foreignKey' => 'parent_id',
            ]
        );
        $this->belongsTo(
            'Realms',
            [
                'foreignKey' => 'realm_id',
                'joinType' => 'INNER',
            ]
        );
        $this->belongsToMany(
            'Appeals',
            [
                'through' => 'AppealsToPrograms',
            ]
        );
        $this->hasMany(
            'ChildPrograms',
            [
                'className' => 'Programs',
                'foreignKey' => 'parent_id',
            ]
        );
        $this->belongsTo(
            'EvaluationCriteria',
            [
                'foreignKey' => 'evaluation_criteria_id',
            ]
        );
        $this->belongsToMany(
            'Forms',
            [
                'through' => 'programs_to_forms',
            ]
        );
        $this->belongsTo(
            'FormalCheckTeams',
            [
                'className' => 'Teams',
                'foreignKey' => 'formal_check_team_id',
            ]
        );
        $this->belongsTo(
            'PriceProposalTeams',
            [
                'className' => 'Teams',
                'foreignKey' => 'price_proposal_team_id',
            ]
        );
        $this->belongsTo(
            'PriceApprovalTeams',
            [
                'className' => 'Teams',
                'foreignKey' => 'price_approval_team_id',
            ]
        );
        $this->belongsTo(
            'CommentsTeams',
            [
                'className' => 'Teams',
                'foreignKey' => 'comments_team_id',
            ]
        );
        $this->belongsTo(
            'RequestManagerTeams',
            [
                'className' => 'Teams',
                'foreignKey' => 'request_manager_team_id',
            ]
        );
        $this->hasMany('Requests', [
            'foreignKey' => 'program_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('description')
            ->allowEmptyString('description');

        $validator
            ->scalar('evaluation_criteria_id')
            ->allowEmptyString('evaluation_criteria_id');

        $validator
            ->scalar('parent_id')
            ->allowEmptyString('parent_id')
            ->notEqualToField('parent_id', 'id', null, 'update')
            ->notEqualToField('id', 'parent_id', null, 'create');

        $allowIfChildProgram = function ($context) {
            return empty($context['parent_id']);
        };

        $validator
            ->allowEmptyString('realm_id', null, $allowIfChildProgram);

        $validator
            ->allowEmptyString('formal_check_team_id');

        $validator
            ->allowEmptyString('price_proposal_team_id');
        $validator
            ->allowEmptyString('price_approval_team_id');
        $validator
            ->allowEmptyString('comments_team_id');
        $validator
            ->allowEmptyString('request_manager_team_id');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['parent_id'], 'ParentPrograms'));
        $rules->add($rules->existsIn(['realm_id'], 'Realms'));
        $rules->add($rules->existsIn(['evaluation_criteria_id'], 'EvaluationCriteria'));
        $rules->add($rules->existsIn(['formal_check_team_id'], 'FormalCheckTeams'));
        $rules->add($rules->existsIn(['price_proposal_team_id'], 'PriceProposalTeams'));
        $rules->add($rules->existsIn(['price_approval_team_id'], 'PriceApprovalTeams'));
        $rules->add($rules->existsIn(['comments_team_id'], 'CommentsTeams'));
        $rules->add($rules->existsIn(['request_manager_team_id'], 'RequestManagerTeams'));

        $this->addDeleteLinkConstraintRule($rules, 'Appeals', null, __('Nelze smazat Program, protože je součástí výzvy'));
        $this->addDeleteLinkConstraintRule($rules, 'Requests', null, __('Nelze smazat Program, ve kterém již jsou vytvořené žádosti o dotaci'));

        return $rules;
    }

    /**
     * @inheritDoc
     */
    public function fkCheck(AppEntity $entity, $options)
    {
        if ($entity->isNew()) {
            return true;
        }

        $withAssociations = $this->get(
            $entity->id,
            ['contains' => [
                'Appeals',
                'Requests',
            ],
            ]
        );

        if (!empty($withAssociations->appeals)) {
            return __('Nelze smazat program, který je součástí existující výzvy');
        }

        if (!empty($withAssociations->requests)) {
            return __('Nelze smazat program, ve kterém již jsou rozpracované žádosti o dotaci');
        }

        return true;
    }
}
