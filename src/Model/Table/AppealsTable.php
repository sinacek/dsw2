<?php

namespace App\Model\Table;

use App\Model\Entity\Appeal;
use App\ORM\Rule\LinkConstraint;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsToMany;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Appeals Model
 *
 * @property ProgramsTable&BelongsToMany $Programs
 * @property AppealsToProgramsTable&HasMany $AppealsToPrograms
 *
 * @method Appeal get($primaryKey, $options = [])
 * @method Appeal newEntity($data = null, array $options = [])
 * @method Appeal[] newEntities(array $data, array $options = [])
 * @method Appeal|false save(EntityInterface $entity, $options = [])
 * @method Appeal saveOrFail(EntityInterface $entity, $options = [])
 * @method Appeal patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method Appeal[] patchEntities($entities, array $data, array $options = [])
 * @method Appeal findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class AppealsTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param  array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('appeals');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsToMany(
            'Programs',
            [
            'through' => 'AppealsToPrograms',
            ]
        );

        $this->hasMany('AppealsToPrograms');

        $this->hasMany('Requests');
    }

    /**
     * Default validation rules.
     *
     * @param  Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('description')
            ->allowEmptyString('description');

        $validator
            ->scalar('link')
            ->allowEmptyString('link');

        $validator
            ->boolean('is_active')
            ->notEmptyString('is_active');

        $validator
            ->date('open_from')
            ->requirePresence('open_from', 'create')
            ->notEmptyDate('open_from');

        $validator
            ->date('open_to')
            ->requirePresence('open_to', 'create')
            ->notEmptyDate('open_to');

        return $validator;
    }

    /**
     * @inheritDoc
     */
    public function buildRules(RulesChecker $rules)
    {
        $this->addDeleteLinkConstraintRule($rules, 'Requests', null, __('Nelze smazat Výzvu jelikož obsahuje žádosti o podporu'));

        return $rules;
    }
}
