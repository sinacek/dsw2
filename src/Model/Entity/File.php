<?php

namespace App\Model\Entity;

use Cake\I18n\FrozenTime;
use InvalidArgumentException;

/**
 * File Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $filepath
 * @property int $filesize
 * @property string $original_filename
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 *
 * @property User $user
 * @property Attachment[] $attachments
 */
class File extends AppEntity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'filepath' => true,
        'filesize' => true,
        'original_filename' => true,
        'modified' => true,
        'created' => true,
        'user' => true,
        'attachments' => true,
    ];

    /**
     * Moves file into /var/www/html/files/$user_id/`year`/`month`/`timestamp`
     *
     * @param array|null $filedata filedata as provided in Request->getData()
     * @param int $user_id id of user uploading the file
     * @return bool|string false if operation failed, filepath relative to folder files otherwise
     */
    public function fileUploadMove(?array $filedata, int $user_id)
    {
        if (empty($filedata) || $filedata['error'] !== UPLOAD_ERR_OK || $filedata['size'] <= 1 || $filedata['size'] > getMaximumFileUploadSize()) {
            throw new InvalidArgumentException();
        }
        $year = date('Y');
        $month = date('n');
        $timestamp = time();
        $targetDirectory = ROOT . DS . 'files' . DS . $user_id . DS . $year . DS . $month . DS;
        if (!file_exists($targetDirectory)) {
            mkdir($targetDirectory, 0770, true);
        }
        if (move_uploaded_file($filedata['tmp_name'], $targetDirectory . $timestamp)) {
            return $user_id . DS . $year . DS . $month . DS . $timestamp;
        }

        return false;
    }

    /**
     * @return bool
     */
    public function deletePhysically(): bool
    {
        if (!empty($this->filepath)) {
            $filename = ROOT . DS . 'files' . DS . $this->filepath;
            if (is_file($filename)) {
                return unlink($filename);
            }
        }

        return false;
    }
}
