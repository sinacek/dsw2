<?php

namespace App\Model\Entity;

use Cake\I18n\FrozenTime;

/**
 * Realm Entity
 *
 * @property int $id
 * @property int $fond_id
 * @property boolean $is_enabled
 * @property boolean $is_hidden
 * @property string $name
 * @property string|null $description
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 *
 * @property Fond $fond
 * @property Program[] $programs
 */
class Realm extends AppEntity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'fond_id' => true,
        'name' => true,
        'description' => true,
        'modified' => true,
        'created' => true,
        'fond' => true,
        'programs' => true,
        'is_enabled' => true,
        'is_hidden' => true,
    ];
}
