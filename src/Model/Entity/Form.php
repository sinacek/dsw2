<?php

namespace App\Model\Entity;

use App\Form\AbstractFormController;
use App\Form\SocialServiceBalanceSheetFormController;
use App\Form\SocialServiceBudgetFormController;
use App\Form\SocialServiceCapacitiesFormController;
use App\Form\SocialServicesStaffingFormController;
use App\Form\SocialServiceUsersInDistrictFormController;
use App\Form\StandardRequestFormController;
use App\Model\Table\FormsTable;
use Cake\I18n\FrozenTime;
use Cake\Log\Log;
use Cake\ORM\Locator\LocatorAwareTrait;

/**
 * Form Entity
 *
 * @property int $id
 * @property string $name
 * @property int $form_type_id
 * @property int $organization_id
 * @property FrozenTime|null $modified
 *
 * @property Organization $organization
 * @property FormField[] $form_fields
 * @property Program[] $programs
 * @property FormSetting[] $form_settings
 */
class Form extends AppEntity
{
    use LocatorAwareTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'organization_id' => true,
        'modified' => true,
        'organization' => true,
        'form_fields' => true,
        'form_type_id' => true,
        'programs' => true,
        'form_settings' => true,
    ];

    public function getFormController(Request $userRequest = null): AbstractFormController
    {
        if ($this->form_settings === null) {
            // ensure settings are loaded
            /** @var FormsTable $formsTable */
            $formsTable = $this->getTableLocator()->get('Forms');
            $formsTable->loadInto($this, ['FormSettings']);
        }
        switch ($this->form_type_id) {
            default:
            case FormType::FORM_TYPE_STANDARD:
                return new StandardRequestFormController($this, $userRequest);
            case FormType::FORM_TYPE_SOCIAL_SERVICE_EMPLOYEES:
                return new SocialServicesStaffingFormController($this, $userRequest);
            case FormType::FORM_TYPE_SOCIAL_SERVICE_CAPACITIES:
                return new SocialServiceCapacitiesFormController($this, $userRequest);
            case FormType::FORM_TYPE_SOCIAL_BALANCE_SHEET:
                return new SocialServiceBalanceSheetFormController($this, $userRequest);
            case FormType::FORM_TYPE_SOCIAL_BUDGET:
                return new SocialServiceBudgetFormController($this, $userRequest);
            case FormType::FORM_TYPE_SOCIAL_USERS_COUNT:
                return new SocialServiceUsersInDistrictFormController($this, $userRequest);
        }
    }
}
