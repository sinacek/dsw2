<?php

namespace App\Model\Entity;

use Cake\I18n\FrozenTime;

/**
 * Program Entity
 *
 * @property int $id
 * @property int|null $parent_id
 * @property int $realm_id
 * @property int $evaluation_criteria_id
 * @property int $formal_check_team_id
 * @property bool $requires_budget
 * @property bool $requires_balance_sheet
 * @property bool $requires_extended_budget
 * @property string $name
 * @property string|null $description
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 *
 * @property Program $parent_program
 * @property Realm $realm
 * @property Appeal[] $appeals
 * @property Program[] $child_programs
 * @property EvaluationCriterium $evaluation_criterium
 * @property Form[] $forms
 * @property Request[] $requests
 * @property Team $formal_check_team
 * @property Team $price_proposal_team
 * @property Team $price_approval_team
 * @property Team $comments_team
 * @property Team $request_manager_team
 */
class Program extends AppEntity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'parent_id' => true,
        'realm_id' => true,
        'name' => true,
        'description' => true,
        'modified' => true,
        'created' => true,
        'parent_program' => true,
        'realm' => true,
        'appeals' => true,
        'child_programs' => true,
        'evaluation_criteria_id' => true,
        'forms' => true,
        'formal_check_team_id' => true,
        'formal_check_team' => true,
        'price_proposal_team_id' => true,
        'price_approval_team_id' => true,
        'comments_team_id' => true,
        'request_manager_team_id' => true,
        'price_proposal_team' => true,
        'price_approval_team' => true,
        'comments_team' => true,
        'request_manager_team' => true,
        'requires_budget' => true,
        'requires_balance_sheet' => true,
        'requires_extended_budget' => true,
    ];

    public function getDataSection($separator = '||')
    {
        $realm = $this->realm ? $this->realm->name : null;
        $fond = null;
        if ($realm) {
            $fond = $this->realm->fond ? $this->realm->fond->name . $separator : null;
        }
        $parent = $this->parent_program ? $this->parent_program->name : null;

        return trim($fond) . trim($realm) . ($parent ? $separator . trim($parent) : null);
    }
}
