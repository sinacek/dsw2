<?php
namespace App\Model\Entity;

use Cake\I18n\FrozenTime;
use Cake\ORM\Entity;

/**
 * EvaluationCriterium Entity
 *
 * @property int $id
 * @property int|null $parent_id
 * @property int $organization_id
 * @property string $name
 * @property string|null $description
 * @property int $max_points
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 *
 * @property EvaluationCriterium $parent_evaluation_criterium
 * @property Organization $organization
 * @property EvaluationCriterium[] $child_evaluation_criteria
 */
class EvaluationCriterium extends AppEntity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'parent_id' => true,
        'organization_id' => true,
        'name' => true,
        'description' => true,
        'max_points' => true,
        'modified' => true,
        'created' => true,
        'parent_evaluation_criterium' => true,
        'organization' => true,
        'child_evaluation_criteria' => true,
    ];
}
