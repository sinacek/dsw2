<?php

namespace App\Model\Entity;

use Cake\I18n\FrozenTime;
use Cake\ORM\Entity;

/**
 * FormType Entity
 *
 * @property int $id
 * @property string $type_name
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 */
class FormType extends Entity
{
    public const FORM_TYPE_STANDARD = 1,
        FORM_TYPE_SOCIAL_SERVICE_EMPLOYEES = 2,
        FORM_TYPE_SOCIAL_SERVICE_CAPACITIES = 3,
        FORM_TYPE_SOCIAL_BALANCE_SHEET = 4,
        FORM_TYPE_SOCIAL_BUDGET = 5,
    FORM_TYPE_SOCIAL_USERS_COUNT = 6;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'type_name' => true,
        'modified' => true,
        'created' => true,
    ];
}
