<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Evaluation Entity
 *
 * @property int $id
 * @property int $request_id
 * @property int $evaluation_criteria_id
 * @property int $user_id
 * @property string $responses
 * @property float $points
 * @property string|null $comment
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property \Cake\I18n\FrozenTime|null $created
 *
 * @property \App\Model\Entity\Request $request
 * @property \App\Model\Entity\EvaluationCriterium $evaluation_criterium
 * @property \App\Model\Entity\User $user
 */
class Evaluation extends AppEntity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'request_id' => true,
        'evaluation_criteria_id' => true,
        'user_id' => true,
        'responses' => true,
        'points' => true,
        'comment' => true,
        'modified' => true,
        'created' => true,
        'request' => true,
        'evaluation_criterium' => true,
        'user' => true,
    ];
}
