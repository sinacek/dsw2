<?php

namespace App\Model\Entity;

use Cake\I18n\FrozenTime;
use Cake\ORM\Entity;
use Prophecy\Exception\InvalidArgumentException;

/**
 * Team Entity
 *
 * @property int $id
 * @property string $name
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 * @property int $organization_id
 *
 * @property Organization $organization
 * @property User[] $users
 * @property Program[] $formal_check_programs
 * @property Program[] $price_proposal_programs
 * @property Program[] $price_approval_programs
 * @property Program[] $comments_programs
 * @property Program[] $request_manager_programs
 */
class Team extends AppEntity
{
    public const ALL_TEAMS = [
        'formal_check_programs',
        'price_proposal_programs',
        'price_approval_programs',
        'comments_programs',
        'request_manager_programs',
    ];

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'modified' => true,
        'created' => true,
        'organization_id' => true,
        'organization' => true,
        'users' => true,
        'formal_check_programs' => true,
        'price_proposal_programs' => true,
        'price_approval_programs' => true,
        'comments_programs' => true,
        'request_manager_programs' => true,
    ];
}
