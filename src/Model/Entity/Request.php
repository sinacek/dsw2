<?php

namespace App\Model\Entity;

use Cake\I18n\FrozenDate;
use Cake\I18n\FrozenTime;
use Cake\ORM\Locator\LocatorAwareTrait;

/**
 * Request Entity
 *
 * @property int $id
 * @property int $organization_id
 *
 * @property int $request_state_id
 *
 * @property string|null $reference_number spisová značka
 * @property int $user_id
 * @property int $user_identity_version
 * @property int $program_id
 * @property string $name
 * @property bool $is_locked
 * @property FrozenDate|null $lock_when
 * @property string|null $lock_comment
 * @property bool $is_reported
 * @property bool $is_settled
 * @property int|null $final_subsidy_amount
 * @property float $subsidy_paid
 * @property string|null $comment
 * @property string|null $purpose
 * @property string|null $final_evaluation
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 * @property int $appeal_id
 *
 * @property Organization $organization
 * @property User $user
 * @property Program $program
 * @property Appeal $appeal
 * @property Evaluation[] $evaluations
 * @property File[] $files
 * @property RequestBudget $request_budget
 * @property RequestLog[] $request_logs
 * @property Identity[] $identities
 */
class Request extends AppEntity
{
    use LocatorAwareTrait;

    public const FIELD_REQUEST_STATE_ID = 'request_state_id';

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'comment' => true,
        'purpose' => true,
        'organization_id' => true,
        'user_id' => true,
        'program_id' => true,
        'name' => true,
        'is_locked' => true,
        'lock_when' => true,
        'is_reported' => true,
        'is_settled' => true,
        'final_subsidy_amount' => true,
        'modified' => true,
        'created' => true,
        'appeal_id' => true,
        'organization' => true,
        'user' => true,
        'program' => true,
        'appeal' => true,
        'evaluations' => true,
        'files' => true,
        'request_budget' => true,
        'request_state_id' => true,
        'reference_number' => true,
        'lock_comment' => true,
        'user_identity_version' => true,
        'request_logs' => true,
        'identities' => true,
    ];

    public function canTransitionTo(int $new_request_state_id): bool
    {
        if ($new_request_state_id === $this->getOriginal(self::FIELD_REQUEST_STATE_ID)) {
            // no transition here, allow updating other fields if request state does not change
            return true;
        }

        return in_array($new_request_state_id, RequestState::ALLOWED_TRANSITIONS[$this->getOriginal(self::FIELD_REQUEST_STATE_ID)] ?? [], true);
    }

    public function setCurrentStatus(int $new_request_state_id, ?int $currentUserId = null, bool $setError = true): bool
    {
        // check if new state is known
        if (!in_array($new_request_state_id, RequestState::KNOWN_STATUSES, true)) {
            $this->addError(self::FIELD_REQUEST_STATE_ID, 'Nový stav žádosti není znám', $setError);

            return false;
        }

        // check if the transition is allowed from original persisted state
        if (!$this->canTransitionTo($new_request_state_id)) {
            $this->addError(self::FIELD_REQUEST_STATE_ID, 'Toto není povolený stav, do kterého žádost může aktuálně přejít', $setError);

            return false;
        }

        // make transition
        $this->request_state_id = $new_request_state_id;
        $this->is_locked = !RequestState::canUserEditRequest($new_request_state_id);
        if ($this->is_locked === true) {
            // reset the lock timeout if already locked
            $this->lock_when = null;
            $this->lock_comment = null;
        }

        if ($new_request_state_id === RequestState::STATE_SUBMITTED) {
            $this->loadUser();
            $this->set('user_identity_version', $this->user->getLastIdentityVersion(false));
            $this->setCurrentIdentityLocked(true);
        } elseif (RequestState::canUserEditRequest($new_request_state_id)) {
            $requestsTable = $this->getTableLocator()->get('Requests');
            $requestsWithSameIdentity = $requestsTable->find('all', [
                'conditions' => [
                    'user_id' => $this->user_id,
                    'user_identity_version' => $this->user_identity_version,
                ],
            ]);
            // check if there is any request with same identity level
            // if there is none, unlock the identity as well
            $canUnlockIdentity = true;
            /** @var Request $request */
            foreach ($requestsWithSameIdentity as $request) {
                if ($request->id !== $this->id) {
                    $canUnlockIdentity = false;
                }
            }
            if ($canUnlockIdentity) {
                $this->setCurrentIdentityLocked(false);
            }
        }

        // record the transition consequences in logs
        $this->addLog($currentUserId);

        return true;
    }

    public function setCurrentIdentityLocked(bool $isLocked): self
    {
        $this->loadIdentities();
        $dirty = false;
        foreach ($this->identities as $identity) {
            if ($identity->is_locked !== $isLocked) {
                $identity->is_locked = $isLocked;
                $dirty = true;
            }
        }
        $this->setDirty('identities', $dirty);

        return $this;
    }

    public function canUserSubmitRequest(): bool
    {
        if ($this->appeal instanceof Appeal) {
            return $this->appeal->canUserSubmitRequest($this);
        }

        return false;
    }

    public function loadUser(): self
    {
        if ($this->user === null) {
            $requestsTable = $this->getTableLocator()->get('Requests');
            $requestsTable->loadInto($this, ['Users']);
        }

        return $this;
    }

    public function loadIdentities(): self
    {
        if ($this->identities === null) {
            $requestsTable = $this->getTableLocator()->get('Requests');
            $requestsTable->loadInto($this, ['Identities']);
        }

        return $this;
    }

    public function loadLogs(): self
    {
        // null => not loaded, [] => loaded and empty
        if ($this->request_logs === null) {
            $requestsTable = $this->getTableLocator()->get('Requests');
            $requestsTable->loadInto($this, ['RequestLogs']);
        }

        usort($this->request_logs, function (RequestLog $a, RequestLog $b) {
            return $b->created->timestamp - $a->created->timestamp;
        });

        return $this;
    }

    private function addLog(?int $executed_by_user_id = null, bool $force = false): bool
    {
        if (!$force && !$this->isDirty() && $this->request_state_id === $this->getOriginal(self::FIELD_REQUEST_STATE_ID)) {
            // do not add log if nothing changed or it's not forced by function call
            return false;
        }
        $this->loadLogs();
        /** @var RequestLog $newLog */
        $newLog = $this->getTableLocator()->get('RequestLogs')->newEntity();
        // record current state in the log
        $newLog->request_state_id = $this->request_state_id;
        $newLog->request_id = $this->id;
        $newLog->lock_comment = $this->lock_comment;
        $newLog->is_locked = $this->is_locked;
        $newLog->comment = $this->comment;
        $newLog->purpose = $this->purpose;
        $newLog->executed_by_user_id = $executed_by_user_id;
        // add and enforce association to be persisted
        $this->request_logs[] = $newLog;
        $this->setDirty('request_logs');

        return true;
    }

    private function addError(string $field, string $error, bool $reallySetError = true)
    {
        if ($reallySetError) {
            $this->setError($field, $error);
        }
    }

    /**
     * @param int $form_id Forms.id of form associated with request
     * @return bool
     */
    public function isFormCompleted(int $form_id): bool
    {
        $form = $this->getFormById($form_id);
        if (empty($form)) {
            return false;
        }

        return $form->getFormController($this)->isFormFilledCompletely();
    }

    /**
     * @return Form[]
     */
    public function getForms(): iterable
    {
        $rtn = [];
        foreach ((!empty($this->program) && !empty($this->program->forms)) ? $this->program->forms : [] as $form) {
            $rtn[$form->id] = $form;
        }
        if (!empty($this->program->parent_program)) {
            foreach ($this->program->parent_program->forms as $form) {
                $rtn[$form->id] = $form;
            }
        }

        return $rtn;
    }

    /**
     * @param int $attachment_id find attachment by it's DB id, matching the user-owner of this request
     * @return File|null
     */
    public function findAttachment(int $attachment_id): ?File
    {
        foreach ($this->files as $attachment) {
            if ($attachment->id === $attachment_id && $this->user_id === $attachment->user_id) {
                return $attachment;
            }
        }

        return null;
    }

    /**
     * @param int $form_id Forms.id of form associated with this request
     * @return Form|null
     */
    public function getFormById(int $form_id): ?Form
    {
        foreach ($this->getForms() as $form) {
            if ($form->id === $form_id) {
                return $form;
            }
        }

        return null;
    }

    /**
     * @return string
     */
    public function getCurrentStateLabel(): string
    {
        return RequestState::getLabelByStateId($this->request_state_id);
    }

    /**
     * Returns budget validation code from @RequestBudget constants
     *
     * @return int
     */
    public function validateBudgetRequirements(): int
    {
        if ($this->request_budget instanceof RequestBudget) {
            return $this->request_budget->validateBudgetRequirements($this->appeal, $this->program);
        }

        return RequestBudget::BUDGET_MISSING;
    }
}
