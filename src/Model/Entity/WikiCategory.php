<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * WikiCategory Entity
 *
 * @property int $id
 * @property string $category_name
 * @property int $organization_id
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Organization $organization
 * @property \App\Model\Entity\Wiki[] $wikis
 */
class WikiCategory extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'category_name' => true,
        'organization_id' => true,
        'modified' => true,
        'organization' => true,
        'wikis' => true,
    ];
}
