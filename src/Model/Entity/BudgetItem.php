<?php

namespace App\Model\Entity;

use Cake\I18n\FrozenTime;
use Cake\ORM\Entity;

/**
 * BudgetItem Entity
 *
 * @property int $id
 * @property float $amount per-item requested subsidy
 * @property float $original_amount per-item original amount (cost) for extended budget
 * @property string $description required item description
 * @property null|string $comment optional comment for item
 * @property FrozenTime|null $modified auto, should be non-null always
 * @property int $request_budget_id
 * @property int $budget_item_type_id
 *
 * @property RequestBudget $request_budget
 */
class BudgetItem extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'amount' => true,
        'original_amount' => true,
        'description' => true,
        'comment' => true,
        'modified' => true,
        'request_budget_id' => true,
        'request_budget' => true,
        'budget_item_type_id' => true,

    ];
}
