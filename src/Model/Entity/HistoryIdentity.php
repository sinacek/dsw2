<?php

namespace App\Model\Entity;

use Cake\I18n\FrozenDate;
use Cake\I18n\FrozenTime;

/**
 * HistoryIdentity Entity
 *
 * @property int $id
 * @property int $organization_id
 * @property string $name
 * @property string|null $first_name
 * @property string|null $last_name
 * @property FrozenDate|null $date_of_birth
 * @property string|null $ico
 * @property string|null $dic
 * @property string|null $databox_id
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 *
 * @property History[] $histories
 * @property Organization $organization
 */
class HistoryIdentity extends AppEntity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'first_name' => true,
        'last_name' => true,
        'date_of_birth' => true,
        'ico' => true,
        'dic' => true,
        'databox_id' => true,
        'modified' => true,
        'created' => true,
        'databox' => true,
        'histories' => true,
        'organization' => true,
        'organization_id' => true,
    ];

    /**
     * Generate identity as string with necessary attributes, including BID/VAT
     * @return string
     */
    public function formatIdentity(): string
    {
        $ic = empty($this->ico) ? '' : sprintf("%s %s, ", __('IČO: '), $this->ico);
        $dic = empty($this->dic) ? '' : sprintf("%s %s, ", __('DIČ: '), $this->dic);
        $fo = empty($this->date_of_birth) || empty($this->first_name) || empty($this->last_name) ? '' : sprintf("%s %s %s", $this->first_name, $this->last_name, $this->date_of_birth->format('d.m.Y'));

        return sprintf("%s (%s%s%s)", $this->name, $ic, $dic, $fo);
    }

    public function formatIcoDic(): string
    {
        return h($this->ico) . (empty($this->dic) ? '' : ', ' . h($this->dic));
    }

    public function formatFirstLastName(bool $wrapInBrackets = false): ?string
    {
        if (empty($this->first_name) && empty($this->last_name)) {
            return null;
        }
        $ret = sprintf("%s %s", $this->first_name, $this->last_name);

        return $wrapInBrackets ? sprintf(" (%s)", $ret) : $ret;
    }
}
