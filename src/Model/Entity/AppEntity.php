<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

class AppEntity extends Entity
{
    /**
     * Helper method to get first error from self or associated entities
     * returns null of no error found
     *
     * @return string|null
     */
    public function getFirstError(): ?string
    {
        if (empty($this->getErrors())) {
            return null;
        }
        foreach ($this->getErrors() as $field => $errors) {
            if (is_string($errors)) {
                return $errors;
            }
            if (is_array($errors)) {
                foreach ($errors as $rule => $message) {
                    if (is_string($message)) {
                        return $message;
                    }
                }
            }
        }

        return null;
    }
}
