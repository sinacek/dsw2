<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * BudgetItemType Entity
 *
 * @property int $id
 * @property string $type_name
 *
 * @property \App\Model\Entity\BudgetItem[] $budget_items
 */
class BudgetItemType extends Entity
{
    public const TYPE_INCOME = 1, TYPE_COST = 2, TYPE_OWN_SOURCE = 3, TYPE_SUBSIDY = 4;
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'type_name' => true,
        'budget_items' => true,
    ];
}
