<?php
namespace App\Model\Entity;

use Cake\I18n\FrozenTime;
use Cake\ORM\Entity;

/**
 * History Entity
 *
 * @property int $id
 * @property int $history_identity_id
 * @property int $czk_amount
 * @property int $year
 * @property string $project_name
 * @property string $funding_source
 * @property string|null $notes
 * @property FrozenTime|null $modified
 *
 * @property HistoryIdentity $history_identity
 */
class History extends AppEntity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'history_identity_id' => true,
        'czk_amount' => true,
        'project_name' => true,
        'funding_source' => true,
        'notes' => true,
        'modified' => true,
        'history_identity' => true,
        'year' => true,
    ];
}
