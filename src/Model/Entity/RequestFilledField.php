<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * RequestFilledField Entity
 *
 * @property int $id
 * @property int $request_id
 * @property int $form_id
 * @property int $form_field_id
 * @property string|null $value
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property \Cake\I18n\FrozenTime|null $created
 *
 * @property \App\Model\Entity\Request $request
 * @property \App\Model\Entity\Form $form
 * @property \App\Model\Entity\FormField $form_field
 */
class RequestFilledField extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'request_id' => true,
        'form_id' => true,
        'form_field_id' => true,
        'value' => true,
        'modified' => true,
        'created' => true,
        'request' => true,
        'form' => true,
        'form_field' => true,
    ];
}
