<?php
namespace App\Model\Entity;

use Cake\I18n\FrozenTime;
use Cake\ORM\Entity;

/**
 * UsersToRole Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $user_role_id
 * @property int|null $organization_id
 * @property FrozenTime|null $modified
 *
 * @property User $user
 * @property UserRole $user_role
 * @property Organization $organization
 */
class UsersToRole extends AppEntity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'user_role_id' => true,
        'modified' => true,
        'user' => true,
        'user_role' => true,
        'organization_id' => true,
        'organization' => true,
    ];
}
