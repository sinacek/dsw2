<?php

namespace App\Model\Entity;

use Cake\I18n\FrozenDate;
use Cake\I18n\FrozenTime;

/**
 * AppealsToProgram Entity
 *
 * @property int $id
 * @property int $appeal_id
 * @property int $program_id
 * @property int $budget_size
 * @property int $max_request_budget
 * @property FrozenDate|null $evaluation_deadline
 * @property FrozenTime|null $modified
 *
 * @property Appeal $appeal
 * @property Program $program
 */
class AppealsToProgram extends AppEntity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'appeal_id' => true,
        'program_id' => true,
        'modified' => true,
        'appeal' => true,
        'program' => true,
        'budget_size' => true,
        'max_request_budget' => true,
        'evaluation_deadline' => true,
    ];
}
