<?php

namespace App\Model\Entity;

use Cake\I18n\FrozenTime;
use Cake\ORM\Entity;

/**
 * FormSetting Entity
 *
 * @property int $id
 * @property int $form_id
 * @property string $name
 * @property string $value
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 *
 * @property Form $form
 */
class FormSetting extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'form_id' => true,
        'name' => true,
        'value' => true,
        'modified' => true,
        'created' => true,
        'form' => true,
    ];
}
