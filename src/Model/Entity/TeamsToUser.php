<?php

namespace App\Model\Entity;

use Cake\I18n\FrozenTime;
use Cake\ORM\Entity;

/**
 * TeamsToUser Entity
 *
 * @property int $id
 * @property int $team_id
 * @property int $user_id
 * @property FrozenTime|null $modified
 *
 * @property Team $team
 * @property User $user
 */
class TeamsToUser extends AppEntity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'team_id' => true,
        'user_id' => true,
        'modified' => true,
        'team' => true,
        'user' => true,
    ];
}
