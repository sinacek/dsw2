<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PublicIncomeSource Entity
 *
 * @property int $id
 * @property int|null $organization_id
 * @property string $source_name
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Organization $organization
 * @property \App\Model\Entity\PublicIncomeHistory[] $public_income_histories
 */
class PublicIncomeSource extends AppEntity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'organization_id' => true,
        'source_name' => true,
        'modified' => true,
        'organization' => true,
        'public_income_histories' => true,
    ];
}
