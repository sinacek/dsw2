<?php

namespace App\Model\Entity;

use App\Form\IdentityForm;
use Cake\I18n\FrozenTime;
use Cake\Utility\Hash;
use InvalidArgumentException;

/**
 * Identity Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property string $value
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 * @property int $version
 * @property bool $is_locked
 *
 * @property User $user
 */
class Identity extends AppEntity
{
    // indicate that identity is Corporate / Pravnicka Osoba
    public const IS_PO = 'is.po';

    public const FO_FIRSTNAME = 'fo.firstname',
        FO_SURNAME = 'fo.surname',
        FO_DEGREE_AFTER = 'fo.degree.after',
        FO_DEGREE_BEFORE = 'fo.degree.before',
        FO_DATE_OF_BIRTH = 'fo.date_of_birth',
        FO_VAT_ID = 'fo.vat_id';

    public const PO_FULLNAME = 'po.fullname',
        PO_BUSINESS_ID = 'po.business_id',
        PO_CORPORATE_TYPE = 'po.corporate_type',
        PO_VAT_ID = 'po.vat_id',
        PO_REGISTRATION_DETAILS = 'po.registration.details',
        PO_REGISTRATION_SINCE = 'po.registration.since';

    public const RESIDENCE_ADDRESS_CITY = 'address.residence.city',
        RESIDENCE_ADDRESS_CITY_PART = 'address.residence.city_part',
        RESIDENCE_ADDRESS_STREET = 'address.residence.street',
        RESIDENCE_ADDRESS_ZIP = 'address.residence.zip';

    public const POSTAL_ADDRESS_CITY = 'address.postal.city',
        POSTAL_ADDRESS_CITY_PART = 'address.postal.city_part',
        POSTAL_ADDRESS_STREET = 'address.postal.street',
        POSTAL_ADDRESS_ZIP = 'address.postal.zip';

    public const BANK_NAME = 'bank.name',
        BANK_NUMBER = 'bank.account_number',
        BANK_CODE = 'bank.code';

    public const NOTE = 'note';

    public const CONTACT_PHONE = 'contact.phone',
        CONTACT_EMAIL = 'contact.email',
        CONTACT_DATABOX = 'contact.databox';

    public const ISDS_VERIFIED_DATABOX_ID = 'isds.verified.databox.id',
        ISDS_VERIFIED_DATABOX_TYPE = 'isds.verified.databox.type',
        ISDS_VERIFIED_BUSINESS_ID = 'isds.verified.business_id',
        ISDS_VERIFIED_BUSINES_NAME = 'isds.verified.business_name';

    public const ALL_VERIFIED_INFO = [
        self::ISDS_VERIFIED_DATABOX_ID,
        self::ISDS_VERIFIED_DATABOX_TYPE,
        self::ISDS_VERIFIED_BUSINES_NAME,
        self::ISDS_VERIFIED_BUSINESS_ID,
    ];

    public const STATUTORY_FULLNAME = 'statutory.fullname',
        STATUTORY_REPRESENTATION_REASON = 'statutory.representation_reason',
        STATUTORY_PHONE = 'statutory.phone',
        STATUTORY_EMAIL = 'statutory.email';

    public const ALL_STATUTORY = [
        self::STATUTORY_FULLNAME,
        self::STATUTORY_REPRESENTATION_REASON,
        self::STATUTORY_PHONE,
        self::STATUTORY_EMAIL,
    ];

    public const INTEREST_X_SIZE = 'interest.%d.size',
        INTEREST_X_OWNER = 'interest.%d.owner',
        INTEREST_X_BUSINESS_ID = 'interest.%d.business_id',
        INTEREST_X_DATE_OF_BIRTH = 'interest.%d.date_of_birth',
        INTEREST_X_COUNTRY = 'interest.%d.country';

    public const OWNED_X_SIZE = 'owned.%d.size',
        OWNED_X_BUSINESS_ID = 'owned.%d.business_id',
        OWNED_X_NAME = 'owned.%d.name',
        OWNED_X_COUNTRY = 'owned.%d.country';

    public const FULL_INTEREST_TEMPLATE = [
        self::INTEREST_X_SIZE,
        self::INTEREST_X_OWNER,
        self::INTEREST_X_BUSINESS_ID,
        self::INTEREST_X_DATE_OF_BIRTH,
        self::INTEREST_X_COUNTRY,
    ];

    public const FULL_OWNED_INTERESTS_TEMPLATE = [
        self::OWNED_X_SIZE,
        self::OWNED_X_BUSINESS_ID,
        self::OWNED_X_NAME,
        self::OWNED_X_COUNTRY,
    ];

    public const BOOLEANS = [
        self::IS_PO,
    ];

    public const INTEGERS = [
    ];

    public const DOUBLES = [
    ];

    public const EMAILS = [
        self::STATUTORY_EMAIL,
        self::CONTACT_EMAIL,
    ];

    public const REQUIRED_FIELDS = [
        self::BANK_NUMBER,
        self::BANK_NAME,
        self::BANK_CODE,
        self::CONTACT_PHONE,
        self::CONTACT_EMAIL,
        self::RESIDENCE_ADDRESS_CITY,
        self::RESIDENCE_ADDRESS_ZIP,
        self::RESIDENCE_ADDRESS_STREET,
    ];

    public const PO_REQUIRED_FIELDS = [
        self::PO_BUSINESS_ID,
        self::PO_CORPORATE_TYPE,
        self::PO_FULLNAME,
    ];

    public const FO_REQUIRED_FIELDS = [
        self::FO_DATE_OF_BIRTH,
        self::FO_SURNAME,
        self::FO_FIRSTNAME,
    ];

    public const TEXTEAREAS = [
        self::NOTE,
    ];

    public const SELECTS = [
        self::INTEREST_X_COUNTRY,
        self::OWNED_X_COUNTRY,
        self::POSTAL_ADDRESS_CITY,
        self::POSTAL_ADDRESS_CITY_PART,
        self::RESIDENCE_ADDRESS_CITY_PART,
        self::RESIDENCE_ADDRESS_CITY,
        self::PO_CORPORATE_TYPE,
    ];

    public const DATES = [
        self::FO_DATE_OF_BIRTH,
        self::INTEREST_X_DATE_OF_BIRTH,
        self::PO_REGISTRATION_SINCE,
    ];

    /**
     * @return array
     */
    public static function getLabels(): array
    {
        return [
            self::IS_PO => __('Mám IČO, jsem podnikatel nebo obchodní korporace'),

            self::FO_FIRSTNAME => __('Křestní jméno/jména'),
            self::FO_SURNAME => __('Příjmení'),
            self::FO_DEGREE_AFTER => __('Tituly za jménem'),
            self::FO_DEGREE_BEFORE => __('Tituly před jménem'),
            self::FO_DATE_OF_BIRTH => __('Datum narození'),
            self::FO_VAT_ID => __('DIČ'),

            self::PO_VAT_ID => __('DIČ'),
            self::PO_FULLNAME => __('Název společnosti'),
            self::PO_BUSINESS_ID => __('IČO'),
            self::PO_CORPORATE_TYPE => __('Právní forma'),

            self::CONTACT_EMAIL => __('E-mailová adresa'),
            self::CONTACT_PHONE => __('Telefonní číslo'),
            self::CONTACT_DATABOX => __('ID Datové Schránky'),

            self::ISDS_VERIFIED_DATABOX_ID => __('Ověřené ID Datové Schránky'),
            self::ISDS_VERIFIED_DATABOX_TYPE => __('Ověřený Typ Datové Schránky'),
            self::ISDS_VERIFIED_BUSINES_NAME => __('Ověřený Název subjektu'),
            self::ISDS_VERIFIED_BUSINESS_ID => __('Ověřené IČO subjektu'),

            self::NOTE => false,

            self::BANK_NAME => __('Název banky'),
            self::BANK_CODE => __('Kód banky'),
            self::BANK_NUMBER => __('Číslo účtu'),

            self::RESIDENCE_ADDRESS_CITY => __('Obec / Město'),
            self::RESIDENCE_ADDRESS_CITY_PART => __('Část obce / Městská část'),
            self::RESIDENCE_ADDRESS_STREET => __('Ulice vč. čísla popisného a orientačního'),
            self::RESIDENCE_ADDRESS_ZIP => __('PSČ'),

            self::POSTAL_ADDRESS_CITY => __('Obec / Město'),
            self::POSTAL_ADDRESS_CITY_PART => __('Část obce / Městská část'),
            self::POSTAL_ADDRESS_STREET => __('Ulice vč. čísla popisného a orientačního'),
            self::POSTAL_ADDRESS_ZIP => __('PSČ'),

            self::STATUTORY_EMAIL => __('E-mailová adresa'),
            self::STATUTORY_FULLNAME => __('Celé jméno statutárního zástupce'),
            self::STATUTORY_PHONE => __('Telefonní číslo'),
            self::STATUTORY_REPRESENTATION_REASON => __('Právní důvod zastoupení'),

            self::INTEREST_X_SIZE => __('Velikost podílu (procenta nebo zlomek)'),
            self::INTEREST_X_OWNER => __('Plné jméno vlastníka podílu'),
            self::INTEREST_X_BUSINESS_ID => __('IČO'),
            self::INTEREST_X_DATE_OF_BIRTH => __('Datum narození vlastníka podílu'),
            self::INTEREST_X_COUNTRY => __('Státní příslušnost'),

            self::OWNED_X_SIZE => __('Velikost podílu (procenta nebo zlomek)'),
            self::OWNED_X_BUSINESS_ID => __('IČO'),
            self::OWNED_X_NAME => __('Název právnické osoby'),

            self::PO_REGISTRATION_SINCE => __('Datum registrace / vzniku právnické osoby'),
            self::PO_REGISTRATION_DETAILS => __('Spisová značka / Živnostenský úřad'),
        ];
    }

    /**
     * @param string $fieldId id of identity field, from Identity constants
     * @return string|null
     */
    public static function getFieldLabel(string $fieldId): ?string
    {
        $labels = self::getLabels();

        return isset($labels[$fieldId]) ? $labels[$fieldId] : null;
    }

    /**
     * @param string $fieldId id of identity field, from Identity constants
     * @return string
     */
    public static function getFieldFormControlType(string $fieldId): string
    {
        if (in_array($fieldId, self::EMAILS)) {
            return 'email';
        }
        if (in_array($fieldId, self::SELECTS)) {
            return 'select';
        }
        if (in_array($fieldId, self::INTEGERS)) {
            return 'number';
        }
        if (in_array($fieldId, self::DOUBLES)) {
            return 'number';
        }
        if (in_array($fieldId, self::DATES)) {
            return 'date';
        }
        if (in_array($fieldId, self::TEXTEAREAS)) {
            return 'textarea';
        }
        if (in_array($fieldId, self::BOOLEANS)) {
            return 'checkbox';
        }

        return 'text';
    }

    /**
     * @return array
     */
    public static function getPlaceholders(): array
    {
        return [
            self::FO_DATE_OF_BIRTH => __('Nepovinné pro právnickou osobu'),
            self::FO_VAT_ID => __('Vyplňte DIČ, pokud vám bylo přiděleno'),
            self::FO_SURNAME => __('Příjmení'),
            self::FO_FIRSTNAME => __('Křestí jméno/jména'),

            self::RESIDENCE_ADDRESS_STREET => __('například Koněvova 1853/129'),
            self::RESIDENCE_ADDRESS_ZIP => __('například 293 01'),
            self::RESIDENCE_ADDRESS_CITY_PART => __('například Mladá Boleslav II'),
            self::RESIDENCE_ADDRESS_CITY => __('například Mladá Boleslav'),

            self::POSTAL_ADDRESS_STREET => __('například Koněvova 1853/129'),
            self::POSTAL_ADDRESS_ZIP => __('například 293 01'),
            self::POSTAL_ADDRESS_CITY_PART => __('například Mladá Boleslav II'),
            self::POSTAL_ADDRESS_CITY => __('například Mladá Boleslav'),

            self::CONTACT_PHONE => __('například +420 608 123 456'),
            self::STATUTORY_PHONE => __('například +420 608 123 456'),

            self::BANK_CODE => __('například 0100'),
            self::BANK_NAME => __('například Komerční Banka'),
            self::BANK_NUMBER => __('například 86-199488014'),

            self::NOTE => __('Nepovinné, vyplňte jen pokud je třeba něco dodat k vyplněné identifikaci / identitě žadatele'),

            self::PO_REGISTRATION_DETAILS => __('Např. Městský soud v Praze, C 123456'),
        ];
    }

    /**
     * @param string $fieldId id of identity field, from Identity constants
     * @return string|null
     */
    public static function getFieldFormPlaceholder(string $fieldId): ?string
    {
        $placeholders = self::getPlaceholders();

        return isset($placeholders[$fieldId]) ? $placeholders[$fieldId] : null;
    }

    /**
     * @return null[]
     */
    public static function getDefaultValues(): array
    {
        return [
            self::FO_DATE_OF_BIRTH => null,
            self::INTEREST_X_DATE_OF_BIRTH => null,
        ];
    }

    /**
     * @param string $fieldId id of identity field, from Identity constants
     * @return mixed|null
     */
    public static function getFieldFormDefaultValue(string $fieldId)
    {
        return self::getDefaultValues()[$fieldId] ?? null;
    }

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'name' => true,
        'value' => true,
        'modified' => true,
        'created' => true,
        'version' => true,
        'is_locked' => true,
        'user' => true,
    ];

    /**
     * @param string $fieldId id of identity field, from Identity constants
     * @return bool|string|null
     */
    public static function getFieldFormEmpty(string $fieldId)
    {
        if (in_array($fieldId, self::SELECTS)) {
            return __('Prosím vyberte si z možností');
        }
        switch ($fieldId) {
            default:
                return false;
            case self::INTEREST_X_DATE_OF_BIRTH:
            case self::FO_DATE_OF_BIRTH:
                return true;
        }
    }

    /**
     * @param string $fieldId id of identity field, from Identity constants
     * @param mixed $data user-provided
     * @return string
     */
    public static function serialize(string $fieldId, $data): string
    {
        if (in_array($fieldId, self::BOOLEANS)) {
            return boolval($data) ? '1' : '0';
        }

        return $data;
    }

    /**
     * @param string $fieldId id of identity field, from Identity constants
     * @param bool $isPO whether current identity is business corporation
     * @param int|null $poFormKod business corporation form id from ARES/CSU
     * @return bool
     */
    public static function getFieldFormIsRequired(string $fieldId, bool $isPO, ?int $poFormKod): bool
    {
        if (in_array($fieldId, self::getRequiredFieldIds($isPO, $poFormKod), true)) {
            return true;
        }

        return false;
    }

    public static function mustProvideStatutory(?int $poFormType = null): bool
    {
        return empty($poFormType) || $poFormType < 100 || $poFormType > 110;
    }

    /**
     * @param bool $isPO whether current identity is business corporation
     * @param int|null $poFormaKod business corporation form id from ARES/CSU
     * @return array of identity fields, from Identity constants
     */
    public static function getRequiredFieldIds(bool $isPO, ?int $poFormaKod): array
    {
        $required = self::REQUIRED_FIELDS;
        $required = array_merge($required, $isPO ? self::PO_REQUIRED_FIELDS : self::FO_REQUIRED_FIELDS);
        if ($isPO && self::mustProvideStatutory($poFormaKod)) {
            $required = array_merge(self::ALL_STATUTORY);
            $required[] = sprintf(self::INTEREST_X_SIZE, 0);
        }

        return $required;
    }

    public static function getIdentityFullName(array $data): string
    {
        $isPO = boolval($data[self::IS_PO] ?? 0);
        if ($isPO) {
            return sprintf("%s (%s)", $data[self::PO_FULLNAME] ?? 'N/A', $data[self::PO_BUSINESS_ID] ?? 'N/A');
        }

        return sprintf(
            "%s %s %s %s",
            $data[self::FO_DEGREE_BEFORE] ?? '',
            $data[self::FO_FIRSTNAME] ?? '',
            $data[self::FO_SURNAME] ?? '',
            $data[self::FO_DEGREE_AFTER] ?? ''
        );
    }

    public static function format(array $identity_data, string $format = 'default'): string
    {
        switch ($format) {
            case 'default':
                return self::getIdentityFullName($identity_data);
        }
        throw new InvalidArgumentException('incorrect identity format provided');
    }

    public static function getFormattedKey(string $raw_key, int $counter)
    {
        return sprintf($raw_key, $counter);
    }

    public static function extractPOInterests(array $flattened): array
    {
        $flattened = Hash::flatten($flattened);
        $non_empty_interests = [];
        $requiredInterestFields = [
            Identity::INTEREST_X_SIZE,
            Identity::INTEREST_X_OWNER,
        ];
        for ($i = 0; $i < IdentityForm::MAX_NUMBER_OF_OWNED_INTERESTS; $i++) {
            $identity = [];
            foreach (Identity::FULL_INTEREST_TEMPLATE as $interestField) {
                $flatKey = sprintf($interestField, $i);
                $value = $flattened[$flatKey] ?? null;
                $part = explode('.', $flatKey);
                $part = end($part);
                if (in_array($interestField, $requiredInterestFields, true) && empty($value)) {
                    continue 2;
                }
                $identity[$part] = $value;
            }
            $non_empty_interests[] = $identity;
        }

        return $non_empty_interests;
    }

    public static function extractOwnedInterests(array $flattened): array
    {
        $flattened = Hash::flatten($flattened);
        $non_empty_interests = [];

        for ($i = 0; $i < IdentityForm::MAX_NUMBER_OF_OWNED_INTERESTS; $i++) {
            $identity = [];
            foreach (Identity::FULL_OWNED_INTERESTS_TEMPLATE as $interestField) {
                $flatKey = sprintf($interestField, $i);
                $value = $flattened[$flatKey] ?? null;
                $part = explode('.', $flatKey);
                $part = end($part);
                if (empty($value)) {
                    continue 2;
                }
                $identity[$part] = $value;
            }
            $non_empty_interests[] = $identity;
        }

        return $non_empty_interests;
    }
}
