<?php

namespace App\Model\Entity;

use Cake\Core\Configure;
use Cake\I18n\FrozenTime;
use Cake\View\Helper\UrlHelper;
use Cake\View\View;

/**
 * OrganizationSetting Entity
 *
 * @property int $id
 * @property int $organization_id
 * @property string $name
 * @property string $value
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 *
 * @property Organization $organization
 */
class OrganizationSetting extends AppEntity
{
    public const SITE_NAME = 'site.name', SITE_LOGO = 'site.logo',
        SITE_FAVICON = 'site.favicon';

    public const DS_ATS_ID = 'ds.ats_id',
        DS_ID = 'ds.id',
        DS_CERTIFICATE = 'ds.cert', DS_CERTKEY = 'ds.certkey',
        DS_TEST_ENVIRONMENT = 'ds.test';

    public const HAS_DSW = 'p3.dsw.enable';

    public const INDEX_HERO_TITLE = 'index.hero.title',
        INDEX_HERO_SUBTITLE = 'index.hero.subtitle',
        INDEX_HERO_TEXT = 'index.hero.text',
        INDEX_HERO = 'index.hero';

    public const EMAIL_SERVER = 'EmailTransport.default.host',
        EMAIL_PORT = 'EmailTransport.default.port',
        EMAIL_TLS = 'EmailTransport.default.tls',
        EMAIL_SERVER_USERNAME = 'EmailTransport.default.username',
        EMAIL_SERVER_PASSWORD = 'EmailTransport.default.password',
        EMAIL_FROM = 'Email.default.from',
        EMAIL_REPLYTO = 'Email.default.replyTo';

    public const CONTACT_EMAIL = 'site.contact.email',
        CONTACT_PHONE = 'site.contact.phone',
        CONTACT_ADDRESS = 'site.contact.address';

    public const ENABLE_REGISTRATION = 'site.registration.enable',
        ENABLE_LOGIN_USERS = 'site.login.users.enable';

    public const GA_TRACKING_ID = 'site.ga.id';

    public const CUSTOM_CSS = 'site.custom.css',
        CUSTOM_JS = 'site.custom.js';

    public const IDENTITY_REQUIRE_DS = 'identity.require.ds',
        IDENTITY_REQUIRE_PO_REGISTRATION_DETAILS = 'identity.require.registration_info';

    public const SUBSIDY_HISTORY_YEARS = 'identity.history.years';

    public const ALLOW_MANUAL_SUBMIT_OF_REQUESTS = 'formal_control.allow_manual';

    public const BOOLEANS = [
        self::EMAIL_TLS,
        self::HAS_DSW,
        self::ENABLE_REGISTRATION,
        self::ENABLE_LOGIN_USERS,
        self::DS_TEST_ENVIRONMENT,
        self::IDENTITY_REQUIRE_DS,
        self::ALLOW_MANUAL_SUBMIT_OF_REQUESTS,
        self::IDENTITY_REQUIRE_PO_REGISTRATION_DETAILS,
    ];

    public const INTEGERS = [
        self::EMAIL_PORT,
        self::SUBSIDY_HISTORY_YEARS,
    ];

    public const DOUBLES = [

    ];

    public const EMAILS = [
        self::EMAIL_FROM,
        self::EMAIL_SERVER_USERNAME,
        self::CONTACT_EMAIL,
        self::EMAIL_REPLYTO,
    ];

    public const TEXTEAREAS = [
        self::DS_CERTIFICATE,
        self::DS_CERTKEY,
        self::INDEX_HERO_TEXT,
        self::CONTACT_ADDRESS,
        self::CUSTOM_JS,
        self::CUSTOM_CSS,
    ];

    public const ALL_TECHNICAL = [
        self::GA_TRACKING_ID,
        self::SITE_LOGO,
        self::SITE_FAVICON,
        self::DS_TEST_ENVIRONMENT,
        self::DS_ATS_ID,
        self::DS_ID,
        self::DS_CERTIFICATE,
        self::DS_CERTKEY,
        self::CUSTOM_CSS,
        self::CUSTOM_JS,
    ];

    public const ALL_BASIC = [
        self::SITE_NAME,
        self::CONTACT_EMAIL,
        self::CONTACT_PHONE,
        self::CONTACT_ADDRESS,
        self::ENABLE_REGISTRATION,
        self::ENABLE_LOGIN_USERS,
    ];

    public const ALL_SUBSIDY_PROCESS = [
        self::IDENTITY_REQUIRE_DS,
        self::IDENTITY_REQUIRE_PO_REGISTRATION_DETAILS,
        self::ALLOW_MANUAL_SUBMIT_OF_REQUESTS,
        self::SUBSIDY_HISTORY_YEARS,
    ];

    public const ALL_INDEX_HERO = [
        self::INDEX_HERO_TITLE,
        self::INDEX_HERO_SUBTITLE,
        self::INDEX_HERO_TEXT,
    ];
    public const ALL_EMAIL = [
        self::EMAIL_SERVER,
        self::EMAIL_PORT,
        self::EMAIL_TLS,
        self::EMAIL_SERVER_USERNAME,
        self::EMAIL_SERVER_PASSWORD,
        self::EMAIL_FROM,
        self::EMAIL_REPLYTO,
    ];
    public const APP_GLOBAL_SETTINGS_GROUPS = [
        self::ALL_EMAIL,
    ];
    public const DEFAULT_VALUES = [
        self::EMAIL_TLS => false,
        self::ENABLE_LOGIN_USERS => true,
        self::ENABLE_REGISTRATION => true,
        self::IDENTITY_REQUIRE_DS => true,
        self::ALLOW_MANUAL_SUBMIT_OF_REQUESTS => true,
        self::SUBSIDY_HISTORY_YEARS => 2,
        self::IDENTITY_REQUIRE_PO_REGISTRATION_DETAILS => false,
    ];

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'organization_id' => true,
        'name' => true,
        'value' => true,
        'modified' => true,
        'created' => true,
        'organization' => true,
    ];

    /**
     * Necessary for labels i18n translation
     *
     * @return array
     */
    public static function getLabels(): array
    {
        return [
            self::SITE_LOGO => __('Logo (název souboru nebo URL adresa)'),
            self::SITE_FAVICON => __('Favicon (název souboru nebo URL adresa)'),
            self::DS_ATS_ID => __('Datová Schránka ID Odesílací Brány / Autentizační Služby'),
            self::DS_ID => __('Datová Schránka ID'),
            self::DS_CERTIFICATE => __('Datová Schránka - Certifikát pro Odesílací Bránů'),
            self::DS_CERTKEY => __('Datová Schránka - Privátní klíč k certifikátu'),
            self::DS_TEST_ENVIRONMENT => __('Použít testovací prostředí Datových Schránek (czebox.cz) ?'),

            self::SITE_NAME => __('Název Portálu'),
            self::ENABLE_LOGIN_USERS => __('Povolit přihlašování uživatel (občanů, netýká se administrátorů)'),
            self::ENABLE_REGISTRATION => __('Povolit registraci uživatel (netýká se uživatel, pozvaných skrze sekci Správa Uživatelů'),
            self::IDENTITY_REQUIRE_DS => __('Vyžadovat vyplnění ID Datové Schránky v identitě žadatele'),
            self::IDENTITY_REQUIRE_PO_REGISTRATION_DETAILS => __('Vyžadovat vyplnění informací o registraci právnické osoby v identitě žadatele'),

            self::INDEX_HERO_TITLE => __('Nadpis'),
            self::INDEX_HERO_SUBTITLE => __('Podnadpis'),
            self::INDEX_HERO_TEXT => __('Text'),

            self::EMAIL_SERVER => __('E-mailový Server'),
            self::EMAIL_PORT => __('Port'),
            self::EMAIL_TLS => __('TLS Zabezpečení zapnuto?'),
            self::EMAIL_SERVER_USERNAME => __('Uživatelské jméno'),
            self::EMAIL_SERVER_PASSWORD => __('Heslo'),
            self::EMAIL_FROM => __('Odesílatel'),
            self::EMAIL_REPLYTO => __('Adresa, kam se mají doručovat odpovědi na odeslané e-maily'),

            self::CONTACT_EMAIL => __('Kontaktní email (veřejný)'),
            self::CONTACT_PHONE => __('Kontaktní telefon (veřejný)'),
            self::CONTACT_ADDRESS => __('Kontaktní adresa (veřejná)'),

            self::GA_TRACKING_ID => __('Google Analytics trackovací kód (typicky začíná "UA-")'),
            self::CUSTOM_JS => __('Vlastní Javascript kód, který bude aplikován na každou stránku dotačního portálu'),
            self::CUSTOM_CSS => __('Vlastní CSS styl, který bude aplikován na každou stránku dotačního portálu'),

            self::ALLOW_MANUAL_SUBMIT_OF_REQUESTS => __('Povolit týmu formální kontroly manuální označení žádostí jako došlých'),
            self::SUBSIDY_HISTORY_YEARS => __('Počet let, za které by měl žadatel uvést historii své přijaté veřejné podpory'),
        ];
    }

    /**
     * @param string $field whether the setting is app global, or portal specific
     * @return bool
     */
    public static function isAppGlobalSetting(string $field): bool
    {
        foreach (self::APP_GLOBAL_SETTINGS_GROUPS as $group) {
            if (in_array($field, $group)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param string $field string id of OrganizationSetting, see constants
     * @return array|false[]|mixed|null
     */
    public static function getDefaultValue(string $field)
    {
        if (self::isAppGlobalSetting($field)) {
            return Configure::read($field);
        }
        if (in_array($field, array_keys(self::DEFAULT_VALUES))) {
            return self::DEFAULT_VALUES[$field];
        }

        return null;
    }

    /**
     * @param string $separator separator of contact fields
     * @param bool $links make links active?
     * @param bool $address_plain strip html tags from address (value is provided from WYSIWYG editor)
     * @return string
     */
    public static function getContactInfo(string $separator = " - ", bool $links = false, bool $address_plain = false): string
    {
        $mail = self::getSetting(self::CONTACT_EMAIL);
        $phone = self::getSetting(self::CONTACT_PHONE);
        $address = self::getSetting(self::CONTACT_ADDRESS);
        if (empty($mail) && empty($phone) && empty($address)) {
            return "";
        }
        if ($links) {
            $phone = '<a href="tel:' . str_replace(' ', '', $phone) . '">' . $phone . '</a>';
            $mail = '<a href="mailto:' . $mail . '">' . $mail . '</a>';
        }

        return sprintf("%s%s%s%s%s", $mail, $separator, $phone, $separator, $address_plain ? strip_tags($address, '<a>') : $address);
    }

    /**
     * @return void
     */
    public function storeConfiguration(): void
    {
        Configure::write(sprintf('%s%s', self::isAppGlobalSetting($this->name) ? '' : 'Organization.', $this->name), $this->deserialize());
    }

    /**
     * get current setting value
     *
     * @param string $name setting id, see constants
     * @return array|false[]|mixed
     */
    public static function getSetting(string $name)
    {
        return Configure::read(sprintf('%s%s', self::isAppGlobalSetting($name) ? '' : 'Organization.', $name));
    }

    /**
     * @param string $field setting id, see constants
     * @return string|null
     */
    public static function getSettingLabel(string $field): ?string
    {
        if (in_array($field, array_keys(self::getLabels()))) {
            return self::getLabels()[$field];
        }

        return null;
    }

    /**
     * @return bool|float|int|string
     */
    public function deserialize()
    {
        return self::deserializeValue($this->name, $this->value);
    }

    /**
     * @param string $field setting id
     * @param string $value serialized, string, value
     * @return bool|float|int|string
     */
    public static function deserializeValue(string $field, string $value)
    {
        if (in_array($field, self::BOOLEANS)) {
            return boolval($value);
        }

        if (in_array($field, self::INTEGERS)) {
            return intval($value);
        }

        if (in_array($field, self::DOUBLES)) {
            return floatval($value);
        }

        return $value;
    }

    /**
     * @param string $field setting id, see constants
     * @return string
     */
    public static function getSettingDataType(string $field): string
    {
        if (in_array($field, self::BOOLEANS)) {
            return 'checkbox';
        }

        if (in_array($field, self::INTEGERS)) {
            return 'integer';
        }

        if (in_array($field, self::DOUBLES)) {
            return 'decimal';
        }

        if (in_array($field, self::EMAILS)) {
            return 'email';
        }

        if (in_array($field, self::TEXTEAREAS)) {
            return 'textarea';
        }

        return 'string';
    }

    /**
     * @param string $field setting id, see constants
     * @return array|string[]
     */
    public static function getExtraFormControlOptions(string $field): array
    {
        if (in_array($field, [self::DS_CERTIFICATE, self::DS_CERTKEY, self::CUSTOM_CSS, self::CUSTOM_JS])) {
            return ['data-noquilljs' => 'data-noquilljs'];
        }

        return [];
    }

    /**
     * @param string $field setting id, see constants
     * @return string
     */
    public static function getSettingFormOptionName(string $field): string
    {
        if (in_array($field, self::BOOLEANS)) {
            return 'checked';
        }

        return 'value';
    }

    /**
     * @return string
     */
    public static function getFaviconFullPath(): string
    {
        $favicon_path = self::getSetting(self::SITE_FAVICON) ?: 'default.ico';
        $is_full_url = isset(parse_url($favicon_path)['scheme']);

        return $is_full_url ? $favicon_path : (new UrlHelper(new View()))->image($favicon_path, ['fullBase' => true]);
    }
}
