<?php

namespace App\Model\Entity;

use Cake\I18n\FrozenDate;
use Cake\I18n\FrozenTime;

/**
 * Appeal Entity
 *
 * @property int $id
 * @property string $name
 * @property string|null $description
 * @property string|null $link
 * @property bool $is_active
 * @property int $organization_id
 * @property FrozenDate $open_from
 * @property FrozenDate $open_to
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 *
 * @property Program[] $programs
 * @property Appeal[] $appeals
 * @property AppealsToProgram[] $appeals_to_programs
 */
class Appeal extends AppEntity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'description' => true,
        'link' => true,
        'is_active' => true,
        'open_from' => true,
        'open_to' => true,
        'modified' => true,
        'created' => true,
        'programs' => true,
        'organization_id' => true,
        'appeals' => true,
        'appeals_to_programs' => true,
    ];

    public function canUserSubmitRequest(Request $request): bool
    {
        if (empty($this->open_from) || empty($this->open_to)) {
            return false;
        }

        $now = FrozenDate::now();

        return $this->open_from->lessThanOrEquals($now) && ($this->open_to->greaterThanOrEquals($now) || ($request->lock_when instanceof FrozenDate && $request->lock_when->greaterThanOrEquals($now)));
    }

    public function getMaxRequestBudget(int $program_id): ?float
    {

        foreach ($this->appeals_to_programs ?? [] as $program_settings) {
            if ($program_settings->program_id === $program_id) {
                return $program_settings->max_request_budget;
            }
        }

        return null;
    }

    public function checkRequestLimit(float $requested_amount, int $program_id): bool
    {
        $max_program_budget = $this->getMaxRequestBudget($program_id);
        if ($max_program_budget < 1) {
            return true;
        }

        return $requested_amount <= $max_program_budget;
    }
}
