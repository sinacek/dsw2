<?php

namespace App\Model\Entity;

use Cake\I18n\FrozenTime;
use Cake\ORM\Entity;

/**
 * RequestLog Entity
 *
 * @property int $id
 * @property int $request_id
 * @property int $request_state_id
 * @property string|null $lock_comment
 * @property bool $is_locked
 * @property string|null $comment
 * @property string|null $purpose
 * @property int|null $executed_by_user_id
 * @property FrozenTime|null $created
 *
 * @property Request $request
 * @property RequestState $request_state
 * @property User $user
 */
class RequestLog extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'request_id' => true,
        'request_state_id' => true,
        'lock_comment' => true,
        'is_locked' => true,
        'comment' => true,
        'purpose' => true,
        'executed_by_user_id' => true,
        'created' => true,
        'request' => true,
        'request_state' => true,
        'user' => true,
    ];
}
