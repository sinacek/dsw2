<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Wiki Entity
 *
 * @property int $id
 * @property int $wiki_category_id
 * @property string $title
 * @property string $contents
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property \Cake\I18n\FrozenTime|null $created
 *
 * @property \App\Model\Entity\WikiCategory $wiki_category
 */
class Wiki extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'wiki_category_id' => true,
        'title' => true,
        'contents' => true,
        'modified' => true,
        'created' => true,
        'wiki_category' => true,
    ];
}
