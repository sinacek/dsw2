<?php

namespace App\Model\Entity;

use Cake\I18n\FrozenTime;
use Cake\ORM\Entity;

/**
 * RequestState Entity
 *
 * @property int $id
 * @property string $name
 * @property FrozenTime|null $modified
 */
class RequestState extends Entity
{
    public const STATE_NEW = 1,
        STATE_READY_TO_SUBMIT = 2,
        STATE_SUBMITTED = 3,
        STATE_FORMAL_CHECK_APPROVED = 4,
        STATE_FORMAL_CHECK_REJECTED = 13,
        STATE_REQUEST_APPROVED = 5,
        STATE_READY_TO_SIGN_CONTRACT = 6,
        STATE_CONTRACT_SIGNED_READY_TO_PAYOUT = 7,
        STATE_PAID_READY_FOR_SETTLEMENT = 10,
        STATE_CONTRACT_BREACH_CRIMINAL_PROCEEDINGS = 11,
        STATE_SETTLEMENT_SUBMITTED = 8,
        STATE_WAITING_FOR_SETTLEMENT_TO_BE_PAID = 12,
        STATE_CLOSED_FINISHED = 9;

    public const KNOWN_STATUSES = [
        self::STATE_NEW,
        self::STATE_READY_TO_SUBMIT,
        self::STATE_SUBMITTED,
        self::STATE_FORMAL_CHECK_APPROVED,
        self::STATE_FORMAL_CHECK_REJECTED,
        self::STATE_REQUEST_APPROVED,
        self::STATE_READY_TO_SIGN_CONTRACT,
        self::STATE_CONTRACT_SIGNED_READY_TO_PAYOUT,
        self::STATE_PAID_READY_FOR_SETTLEMENT,
        self::STATE_CONTRACT_BREACH_CRIMINAL_PROCEEDINGS,
        self::STATE_SETTLEMENT_SUBMITTED,
        self::STATE_WAITING_FOR_SETTLEMENT_TO_BE_PAID,
        self::STATE_CLOSED_FINISHED,
    ];

    public const ALLOWED_TRANSITIONS = [
        // allow newly created request to be transitioned to STATE_NEW
        null => [
            self::STATE_NEW,
        ],
        0 => [
            self::STATE_NEW,
        ],
        self::STATE_NEW => [
            // new request must be first filled successfully
            self::STATE_READY_TO_SUBMIT,
            // for automatic locking of requests returned by formal control
            self::STATE_FORMAL_CHECK_REJECTED,
        ],
        self::STATE_READY_TO_SUBMIT => [
            // when requirements of request change (eg. changing target program / having form definitions changed)
            self::STATE_NEW,
            // allowed is also deleting the request, which is not state transition
            self::STATE_SUBMITTED,
            // for automatic locking of requests returned by formal control
            self::STATE_FORMAL_CHECK_REJECTED,
        ],
        self::STATE_SUBMITTED => [
            // formal check control must be done
            self::STATE_FORMAL_CHECK_REJECTED,
            self::STATE_FORMAL_CHECK_APPROVED,
            // formal control sending request back to user to fix issues
            self::STATE_NEW,
        ],
        self::STATE_FORMAL_CHECK_APPROVED => [
            // formal check reverting the previous decision
            self::STATE_SUBMITTED,
            // request is evaluated, purpose/wording updates approved as well
            self::STATE_REQUEST_APPROVED,
        ],
        self::STATE_FORMAL_CHECK_REJECTED => [
            // formal check reverting the previous decision
            self::STATE_SUBMITTED,
            // finalizing the state
            self::STATE_CLOSED_FINISHED,
        ],
        self::STATE_CLOSED_FINISHED => [
            // STATE_CLOSED_FINISHED has no further transitions
        ],
        self::STATE_REQUEST_APPROVED => [
            // statutory did not approve the request, or approved with zero subsidy amount
            self::STATE_CLOSED_FINISHED,
            // statutory approved the request, awaits contract to be signed
            self::STATE_READY_TO_SIGN_CONTRACT,
        ],
        self::STATE_READY_TO_SIGN_CONTRACT => [
            // contract signed and published, ready to payout
            self::STATE_CONTRACT_SIGNED_READY_TO_PAYOUT,
            // contract was not signed, or further checks prevented the contract from being signed
            self::STATE_CLOSED_FINISHED,
        ],
        self::STATE_CONTRACT_SIGNED_READY_TO_PAYOUT => [
            // subsidy has been paid out (partially or 100%)
            self::STATE_PAID_READY_FOR_SETTLEMENT,
        ],
        self::STATE_PAID_READY_FOR_SETTLEMENT => [
            // user correctly provides settlement
            self::STATE_SETTLEMENT_SUBMITTED,
            // user did not submit settlement, or settlement cannot be fixed
            self::STATE_CONTRACT_BREACH_CRIMINAL_PROCEEDINGS,
        ],
        self::STATE_SETTLEMENT_SUBMITTED => [
            // settlement was not accepted, return the request to user to fix settlement issues
            self::STATE_PAID_READY_FOR_SETTLEMENT,
            // settlement was accepted, and the subsidy was not paid 100%, ready to pay out next part (multi-year subsidy)
            self::STATE_CONTRACT_SIGNED_READY_TO_PAYOUT,
            // there is amount of subsidy requester has to pay back to the office
            self::STATE_WAITING_FOR_SETTLEMENT_TO_BE_PAID,
        ],
        self::STATE_WAITING_FOR_SETTLEMENT_TO_BE_PAID => [
            // settlement paid and the request process is closed as finished
            self::STATE_CLOSED_FINISHED,
            // requester did not paid back what was expected, or different kind of breach of subsidy conditions was found out
            self::STATE_CONTRACT_BREACH_CRIMINAL_PROCEEDINGS,
        ],
        self::STATE_CONTRACT_BREACH_CRIMINAL_PROCEEDINGS => [
            // requester was ordered to pay back part/full subsidy for breaching the conditions
            // settlement here can be either requester paid or seizure of property/money through executor
            self::STATE_WAITING_FOR_SETTLEMENT_TO_BE_PAID,
        ],
    ];

    /**
     * Get array of State.id => State.label, translated by db-gettext
     * @return array
     */
    public static function getLabels(): array
    {
        return [
            self::STATE_NEW => __('Žádost vytvořena'),
            self::STATE_READY_TO_SUBMIT => __('Žádost úspěšně vyplněna'),
            self::STATE_SUBMITTED => __('Odesláno dotačnímu úřadu'),
            self::STATE_FORMAL_CHECK_APPROVED => __('Schváleno formální kontrolou'),
            self::STATE_FORMAL_CHECK_REJECTED => __('Žádost byla vyřazena pro nesplnění podmínek'),
            self::STATE_REQUEST_APPROVED => __('Výše dotace schválena spolu s hodnocením (pokud bylo poskytnuto) a slovním hodnocením (pokud bylo uděleno)'),
            self::STATE_READY_TO_SIGN_CONTRACT => __('Připraveno k podpisu smlouvy (schváleno radou / zastupitelstvem / vrcholným orgánem dotačního úřadu)'),
            self::STATE_CONTRACT_SIGNED_READY_TO_PAYOUT => __('Smlouva podepsána a dotace poskytnuta'),
            self::STATE_PAID_READY_FOR_SETTLEMENT => __('Dotace vyplacena, čekání na vyúčtování'),
            self::STATE_SETTLEMENT_SUBMITTED => __('Vyúčtování podáno'),
            self::STATE_CONTRACT_BREACH_CRIMINAL_PROCEEDINGS => __('Porušení veřejnoprávní smlouvy / pravidel rozpočtové kázně'),
            self::STATE_WAITING_FOR_SETTLEMENT_TO_BE_PAID => __('Čekání na vypořádání / vrácení nevyúčtované části dotace'),
            self::STATE_CLOSED_FINISHED => __('Uzavření / ukončení procesu žádosti'),
        ];
    }

    /**
     * @param int $state_id get label for specific state, or null if state has no label defined
     * @return string|null
     */
    public static function getLabelByStateId(int $state_id): ?string
    {
        return self::getLabels()[$state_id] ?? null;
    }

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'modified' => true,
    ];

    public static function canBeDeleted(int $request_state_id): bool
    {
        return in_array($request_state_id, [self::STATE_NEW, self::STATE_READY_TO_SUBMIT], true);
    }

    public static function canUserEditRequest(int $request_state_id): bool
    {
        return in_array($request_state_id, [self::STATE_NEW, self::STATE_READY_TO_SUBMIT], true);
    }
}
