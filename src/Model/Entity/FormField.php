<?php

namespace App\Model\Entity;

use Cake\I18n\FrozenDate;
use Cake\I18n\FrozenTime;

/**
 * FormField Entity
 *
 * @property int $id
 * @property int $form_id
 * @property string $name
 * @property string|null $description
 * @property bool $is_required
 * @property int $form_field_type_id
 * @property int $field_order
 * @property string|null $choices
 * @property string|null $table_settings
 *
 * @property Form $form
 * @property FormFieldType $form_field_type
 * @property RequestFilledField[] $request_filled_fields
 */
class FormField extends AppEntity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'form_id' => true,
        'name' => true,
        'description' => true,
        'is_required' => true,
        'form_field_type_id' => true,
        'form' => true,
        'form_field_type' => true,
        'field_order' => true,
        'choices' => true,
        'table_settings' => true,
    ];

    /**
     * @return string
     */
    public function getFormControlId(): string
    {
        return sprintf('form-%d-field-%d', $this->form_id, $this->id);
    }

    /**
     * @param string|null $value return deserialized value by specific form field type
     * @return bool|FrozenDate|FrozenTime|float|int|string|null
     */
    public function parseFieldValue(?string $value)
    {
        return FormFieldType::parseValue($this->form_field_type_id, $value, $this);
    }

    /**
     * @param mixed $value user-provided, filtered, value
     * @return string|null
     */
    public function serializeValue($value): ?string
    {
        return FormFieldType::serializeValue($this->form_field_type_id, $value, $this);
    }

    /**
     * @param RequestFilledField $filledField DB provided filled-field instance to be checked
     * @return bool
     */
    public function isFilled(RequestFilledField $filledField): bool
    {
        return !$this->is_required || FormFieldType::isFilled($this->form_field_type_id, $filledField->value, $this);
    }

    /**
     * @return bool
     */
    public function canBeFilled(): bool
    {
        return FormFieldType::canBeFilled($this->form_field_type_id);
    }

    /**
     * @return array|mixed
     */
    public function getChoices(): array
    {
        return empty($this->choices) ? [] : unserialize($this->choices);
    }
}
