<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CsuMunicipalitiesToCsuMunicipalityPart Entity
 *
 * @property int $id
 * @property int $csu_municipalities_number
 * @property int $csu_municipality_parts_number
 */
class CsuMunicipalitiesToCsuMunicipalityPart extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'csu_municipalities_number' => true,
        'csu_municipality_parts_number' => true,
    ];
}
