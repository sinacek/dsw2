<?php

namespace App\Model\Entity;

use Cake\I18n\FrozenTime;
use Cake\ORM\Entity;

/**
 * UserRole Entity
 *
 * @property int $id
 * @property string $role_name
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 *
 * @property User[] $users
 */
class UserRole extends AppEntity
{
    public const MANAGER_SYSTEMS = 1, MANAGER_PORTALS = 2,
        MANAGER_USERS = 3, MANAGER_GRANTS = 5, MANAGER_HISTORIES = 6,
        MANAGER_WIKIS = 7, MANAGER_ECONOMICS = 8;
    public const USER_PORTAL_MEMBER = 4;
    // aggregates
    public const ALL_MANAGER = [
        self::MANAGER_SYSTEMS, self::MANAGER_PORTALS,
        self::MANAGER_USERS, self::MANAGER_GRANTS,
        self::MANAGER_HISTORIES,
    ];

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'role_name' => true,
        'modified' => true,
        'created' => true,
        'users_to_roles' => true,
    ];
}
