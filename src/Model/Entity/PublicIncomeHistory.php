<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PublicIncomeHistory Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $amount_czk
 * @property string $fiscal_year
 * @property int $public_income_source_id
 * @property string $project_name
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property \Cake\I18n\FrozenTime|null $created
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\PublicIncomeSource $public_income_source
 */
class PublicIncomeHistory extends AppEntity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'amount_czk' => true,
        'fiscal_year' => true,
        'public_income_source_id' => true,
        'project_name' => true,
        'modified' => true,
        'created' => true,
        'user' => true,
        'public_income_source' => true,
    ];
}
