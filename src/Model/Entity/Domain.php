<?php
namespace App\Model\Entity;

use Cake\I18n\FrozenTime;
use Cake\ORM\Entity;

/**
 * Domain Entity
 *
 * @property int $id
 * @property int $organization_id
 * @property boolean $is_enabled
 * @property string $domain
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 *
 * @property Organization $organization
 */
class Domain extends AppEntity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'organization_id' => true,
        'domain' => true,
        'modified' => true,
        'created' => true,
        'organization' => true,
        'is_enabled' => true,
    ];
}
