<?php

namespace App\Model\Entity;

use App\Middleware\OrgDomainsMiddleware;
use App\Model\Table\IdentitiesTable;
use App\Model\Table\UsersTable;
use Cake\Datasource\EntityInterface;
use Cake\Http\ServerRequest;
use Cake\I18n\FrozenTime;
use Cake\ORM\Locator\LocatorAwareTrait;
use Cake\ORM\Query;
use Cake\ORM\TableRegistry;
use InvalidArgumentException;
use OldDsw\Model\Entity\Ucet;

/**
 * User Entity
 *
 * @property int $id
 * @property string $email
 * @property string $password
 * @property string $salt
 * @property string $mail_verification_code
 * @property bool $is_enabled
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 *
 * @property UserRole[] $user_roles
 * @property UsersToRole[] $users_to_roles
 * @property Team[] $teams
 * @property Ucet[] $stare_ucty
 * @property PublicIncomeHistory[] $public_income_histories
 * @property UsersToUser[] $allowed_users
 * @property UsersToUser[] $allowed_to_users
 * @property Request[] $requests
 */
class User extends AppEntity
{
    use LocatorAwareTrait;

    public const SESSION_ORIGIN_IDENTITY = 'origin_identity';

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'email' => true,
        'password' => true,
        'salt' => true,
        'mail_verification_code' => true,
        'is_enabled' => true,
        'modified' => true,
        'created' => true,
        'teams' => true,
        'users_to_roles' => true,
        'stare_ucty' => true,
        'allowed_users' => true,
        'allowed_to_users' => true,
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password',
    ];

    /**
     * @param bool $setEmpty set null instead of generating random new one?
     * @return void
     */
    public function regenerateEmailToken(bool $setEmpty = false): void
    {
        $this->mail_verification_code = $setEmpty ? null : random_str('alphanum', 32);
        $this->setDirty('mail_verification_code');
    }

    /**
     * @return void
     */
    public function regenerateSalt(): void
    {
        $this->salt = random_str('alphanum', 64);
        $this->setDirty('salt');
    }

    /**
     * @return bool
     */
    public function isSystemsManager(): bool
    {
        return $this->hasRole(UserRole::MANAGER_SYSTEMS);
    }

    /**
     * @return bool
     */
    public function isPortalsManager(): bool
    {
        return $this->hasRole(UserRole::MANAGER_PORTALS, OrgDomainsMiddleware::getCurrentOrganizationId());
    }

    /**
     * @return bool
     */
    public function isUsersManager(): bool
    {
        return $this->hasRole(UserRole::MANAGER_USERS, OrgDomainsMiddleware::getCurrentOrganizationId()) || $this->isPortalsManager() || $this->isSystemsManager();
    }

    /**
     * @return bool
     */
    public function isGrantsManager(): bool
    {
        return $this->hasRole(UserRole::MANAGER_GRANTS, OrgDomainsMiddleware::getCurrentOrganizationId()) || $this->isPortalsManager() || $this->isSystemsManager();
    }

    /**
     * @return bool
     */
    public function isHistoryManager(): bool
    {
        return $this->hasRole(UserRole::MANAGER_HISTORIES, OrgDomainsMiddleware::getCurrentOrganizationId()) || $this->isPortalsManager() || $this->isSystemsManager();
    }

    /**
     * @return bool
     */
    public function isWikiManager(): bool
    {
        return $this->hasRole(UserRole::MANAGER_WIKIS, OrgDomainsMiddleware::getCurrentOrganizationId()) || $this->isPortalsManager() || $this->isSystemsManager();
    }

    /**
     * @return bool
     */
    public function isEconomicsManager(): bool
    {
        return $this->hasRole(UserRole::MANAGER_ECONOMICS, OrgDomainsMiddleware::getCurrentOrganizationId()) || $this->isPortalsManager() || $this->isSystemsManager();
    }

    /**
     * @return bool
     */
    public function isManager(): bool
    {
        return $this->hasRoles(UserRole::ALL_MANAGER, OrgDomainsMiddleware::getCurrentOrganizationId(), true) || $this->isSystemsManager();
    }

    /**
     * @param int $requested_role_id role id
     * @param int|null $organization_context id of organization, if the role must not be global
     * @return bool
     */
    public function hasRole(int $requested_role_id, ?int $organization_context = null): bool
    {
        if (!is_array($this->users_to_roles)) {
            return false;
        }
        foreach ($this->users_to_roles as $role_map) {
            if ($role_map->user_role_id != $requested_role_id) {
                continue;
            }
            if (empty($organization_context)) {
                return true;
            }
            if ($role_map->organization_id === $organization_context) {
                return true;
            }
            if ($requested_role_id === UserRole::MANAGER_SYSTEMS && empty($role_map->organization_id)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param array $roles_ids id of roles that must be present
     * @param int|null $organization_context id of organization, if the roles must not be global
     * @param bool $atLeastOneRolePresent presence one of the requested roles suffices to return true
     * @return bool
     */
    public function hasRoles(array $roles_ids, ?int $organization_context = null, bool $atLeastOneRolePresent = false): bool
    {
        $rtn = true;
        foreach ($roles_ids as $roles_id) {
            $hasThisRole = $this->hasRole($roles_id, $organization_context);
            if ($atLeastOneRolePresent && $hasThisRole) {
                return true;
            }
            $rtn = $rtn && $hasThisRole;
        }

        return $rtn;
    }

    /**
     * @param int $requested_role_id id of requested role
     * @param bool $includeGlobal whether to include global roles (organization_id is null in such case)
     * @return array
     */
    public function getOrganizationIdsWhereUserHasRole(int $requested_role_id = UserRole::MANAGER_PORTALS, bool $includeGlobal = false): array
    {
        $orgs = [];
        foreach ($this->users_to_roles as $role_map) {
            if ($role_map->user_role_id === $requested_role_id && ($includeGlobal || !empty($role_map->organization_id))) {
                $orgs[] = $role_map->organization_id;
            }
        }

        return $orgs;
    }

    /**
     * @param int $requested_role_id id of requested role
     * @param bool $includeEmpty whether to include global roles (organization_id is null in such case)
     * @param array $fallbackRoles if requested role is not found, search for organizations with one of the fallback roles
     * @return array
     */
    public function getOrganizationIdsWhereUserHasRoleWithFallbacks(int $requested_role_id = UserRole::MANAGER_PORTALS, bool $includeEmpty = false, array $fallbackRoles = []): array
    {
        $orgs = $this->getOrganizationIdsWhereUserHasRole($requested_role_id, $includeEmpty);
        foreach ($fallbackRoles as $fallbackRole) {
            if (!empty($orgs)) {
                return $orgs;
            }
            $orgs = $this->getOrganizationIdsWhereUserHasRole($fallbackRole, $includeEmpty);
        }

        return $orgs;
    }

    /**
     * @param int $organization_id within which organization the portal-member role should be created
     * @return void
     */
    public function createMemberRoleIfNotExists(int $organization_id): void
    {
        if (!$this->hasRole(UserRole::USER_PORTAL_MEMBER, $organization_id)) {
            /** @var UsersToRole $role */
            $role = TableRegistry::getTableLocator()->get('UsersToRoles')->newEntity();
            if (!$this->isNew()) {
                $role->user_id = $this->id;
            }
            $role->organization_id = $organization_id;
            $role->user_role_id = UserRole::USER_PORTAL_MEMBER;
            if (!is_array($this->users_to_roles)) {
                $this->users_to_roles = [];
            }
            $this->users_to_roles[] = $role;
            $this->setDirty('users_to_roles');
        }
    }

    /**
     * @param string $newPassword plaintext password
     * @return void
     */
    public function setNewPassword(string $newPassword): void
    {
        while (empty($this->salt)) {
            $this->regenerateSalt();
        }
        $this->password = password_hash($newPassword . $this->salt, PASSWORD_ARGON2ID);
        $this->setDirty('password');
    }

    /**
     * @return int[]
     */
    public function getStareUctyIds(): array
    {
        $rtn = [];
        if (!empty($this->stare_ucty)) {
            foreach ($this->stare_ucty as $ucet) {
                $rtn[] = $ucet->id;
            }
        }

        return $rtn;
    }

    /**
     * Checks if in the current portal user has at least one team role assigned
     *
     * @return bool
     */
    public function hasAtLeastOneTeam(): bool
    {
        foreach ($this->teams ?? [] as $team) {
            if ($team->organization_id === OrgDomainsMiddleware::getCurrentOrganizationId()) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param string $team_field name of team role, such as formal_check_programs, see Entity\Team properties
     * @return array
     */
    public function findTeamsWithProgramsIn(string $team_field)
    {
        $teams = [];
        foreach ($this->teams as $team) {
            if (OrgDomainsMiddleware::getCurrentOrganizationId() !== $team->organization_id) {
                continue;
            }
            if (isset($team->{$team_field}) && !empty($team->{$team_field})) {
                $teams[] = $team;
            }
        }

        return $teams;
    }

    /**
     * @return bool
     */
    public function hasFormalControlTeam(): bool
    {
        return !empty($this->findTeamsWithProgramsIn('formal_check_programs'));
    }

    /**
     * @param string $field get ids of programs by team role name, such as formal_check_programs, see Entity\Teams properties
     * @return array
     */
    public function getProgramIdsForTeam(string $field)
    {
        $rtn = [];
        $teams = $this->findTeamsWithProgramsIn($field);
        foreach ($teams as $team) {
            foreach ($team->{$field} as $program) {
                $rtn[] = $program->id;
            }
        }

        return $rtn;
    }

    /**
     * @return array
     */
    public function getAllTeamProgramIds()
    {
        $rtn = [];
        foreach (Team::ALL_TEAMS as $team_field) {
            $rtn = array_merge($rtn, $this->getProgramIdsForTeam($team_field));
        }

        return array_unique($rtn);
    }

    /**
     * @return array
     */
    public function getProgramIdsForFormalControl(): array
    {
        return $this->getProgramIdsForTeam('formal_check_programs');
    }

    /**
     * @return array
     */
    public function getProgramIdsForComments(): array
    {
        return $this->getProgramIdsForTeam('comments_programs');
    }

    /**
     * @return bool
     */
    public function hasCommentsTeam(): bool
    {
        return !empty($this->findTeamsWithProgramsIn('comments_programs'));
    }

    /**
     * @return bool
     */
    public function hasProposalsTeam(): bool
    {
        return !empty($this->findTeamsWithProgramsIn('price_proposal_programs'));
    }

    /**
     * @return bool
     */
    public function hasApprovalsTeam(): bool
    {
        return !empty($this->findTeamsWithProgramsIn('price_approval_programs'));
    }

    /**
     * @return bool
     */
    public function hasManagersTeam(): bool
    {
        return !empty($this->findTeamsWithProgramsIn('request_manager_programs'));
    }

    /**
     * @param bool $unlockedOnly return max version of unlocked identities only
     * @return int
     */
    public function getLastIdentityVersion(bool $unlockedOnly = true): int
    {
        if ($unlockedOnly === true) {
            $allLast = $this->getAllIdentity(null, true);
        } else {
            /** @var IdentitiesTable $identitiesTable */
            $identitiesTable = TableRegistry::getTableLocator()->get('Identities');
            $allLast = $identitiesTable->find('all', [
                'conditions' => [
                    'Identities.user_id' => $this->id,
                ],
            ]);
        }

        $maxVersion = 0;
        foreach ($allLast as $identity) {
            if ($identity->version > $maxVersion) {
                $maxVersion = $identity->version;
            }
        }

        return $maxVersion;
    }

    /**
     * @param int|null $version version of identity, as listed in appropriate link
     * @param bool $lastUnlockedVersion if provided $version is null or zero, this must be set to true
     * @return Identity[]|Query
     */
    public function getAllIdentity(?int $version = null, bool $lastUnlockedVersion = false): Query
    {
        if (empty($version) && $lastUnlockedVersion === false) {
            throw new InvalidArgumentException('if no version provided, you must request last unlocked version');
        }

        /** @var IdentitiesTable $identitiesTable */
        $identitiesTable = TableRegistry::getTableLocator()->get('Identities');

        $conditions = [
            'Identities.user_id' => $this->id,
        ];

        if ($lastUnlockedVersion === true) {
            $conditions['Identities.is_locked'] = false;
        } else {
            $conditions['Identities.version'] = $version;
        }

        /** @var Identity[] $identities */
        return $identitiesTable->find(
            'all',
            [
                'conditions' => $conditions,
            ]
        );
    }

    public function getIdentityMissingFields(?int $identity_version = null): ?array
    {
        $identities = $identity_version === 0 ? [] : $this->getAllIdentity($identity_version ?? $this->getLastIdentityVersion(false));
        $flattened = [];
        $missing_fields = [];
        foreach ($identities as $identity) {
            $flattened[$identity->name] = $identity->value;
        }

        $requiredFields = Identity::getRequiredFieldIds($flattened[Identity::IS_PO] ?? false, $flattened[Identity::PO_CORPORATE_TYPE] ?? 0);
        foreach ($requiredFields as $requiredFieldId) {
            if (!isset($flattened[$requiredFieldId]) || empty(trim($flattened[$requiredFieldId]))) {
                $missing_fields[] = $requiredFieldId;
            }
        }

        if (OrganizationSetting::getSetting(OrganizationSetting::IDENTITY_REQUIRE_DS)) {
            if (empty($flattened[Identity::CONTACT_DATABOX])) {
                $missing_fields[] = Identity::CONTACT_DATABOX;
            }
        }

        if ($flattened[Identity::IS_PO] ?? false && OrganizationSetting::getSetting(OrganizationSetting::IDENTITY_REQUIRE_PO_REGISTRATION_DETAILS)) {
            foreach ([Identity::PO_REGISTRATION_SINCE, Identity::PO_REGISTRATION_DETAILS] as $poRegistrationDetailField) {
                if (empty($flattened[$poRegistrationDetailField])) {
                    $missing_fields[] = $poRegistrationDetailField;
                }
            }
        }

        return $missing_fields;
    }

    /**
     * @param int|null $identity_version
     * @return bool
     */
    public function isIdentityCompleted(?int $identity_version = null): bool
    {
        return empty($this->getIdentityMissingFields($identity_version));
    }

    public function getOriginIdentity(ServerRequest $request): User
    {
        $origin_data = $request->getSession()->read(self::SESSION_ORIGIN_IDENTITY);

        return $origin_data instanceof User ? $origin_data : $this;
    }

    /**
     * returns false, if there is already origin identity set
     * returns true, if no origin identity found or if it matches the one passed in arguments
     *
     * @param ServerRequest $request
     * @param User|EntityInterface $originIdentity
     * @return bool
     */
    public function setOriginIdentity(ServerRequest $request, User $originIdentity): bool
    {
        $origin_data = $request->getSession()->read(self::SESSION_ORIGIN_IDENTITY);
        if (($origin_data instanceof User && $origin_data->id === $originIdentity->id) || empty($origin_data)) {
            $request->getSession()->write(self::SESSION_ORIGIN_IDENTITY, $originIdentity);

            return true;
        }

        return false;
    }

    /**
     * Checks whether
     *
     * @param ServerRequest $request
     * @param int $user_id
     * @return bool
     */
    public function canSwitchToUser(ServerRequest $request, int $user_id): bool
    {
        $identity = $request->getSession()->read(self::SESSION_ORIGIN_IDENTITY);
        if (!($identity instanceof User)) {
            // if no origin identity set, retrieve the session one
            $identity = $request->getSession()->read('Auth.User');
        } elseif ($identity->id === $user_id) {
            // if origin identity matches the requested one
            return true;
        }
        foreach ($identity->allowed_users ?? [] as $allowed_user) {
            if ($allowed_user->user_id === $user_id) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param ServerRequest $request
     * @param User|EntityInterface $target_user
     * @return bool
     */
    public function switchToUser(ServerRequest $request, User $target_user): bool
    {
        $origin = $this->getOriginIdentity($request);
        $hasAccess = $origin->canSwitchToUser($request, $target_user->id);
        if (!$hasAccess) {
            return false;
        }

        if ($this->setOriginIdentity($request, $origin)) {
            $request->getSession()->write('Auth.User', $target_user);

            return true;
        }

        return false;
    }

    /**
     * Patches allowed_to_users list for saving with association,
     * returns array of UsersToUser entities, meant to be removed if patch saving proceeds
     *
     * @param array $newEmails
     * @param UsersTable $usersTable
     * @param string $formField
     * @return array
     */
    public function patchAllowedToUsers(array $newEmails, UsersTable $usersTable, string $formField = 'allowed_users_emails'): array
    {
        $this->setErrors([$formField => []], true);
        $userEntities = [];
        $links = [];
        $toRemove = [];
        // filter and push validation errors on each invalid newEmails value
        foreach ($newEmails as $email) {
            $email = trim($email);
            if (empty(filter_var($email, FILTER_VALIDATE_EMAIL, FILTER_NULL_ON_FAILURE))) {
                $this->setErrors([$formField => sprintf(__('"%s" Není platná e-mailová adresa'), h($email))]);
                break;
            } elseif ($email === $this->email) {
                $this->setErrors([$formField => __('Nemůžete sdílet účet sami se sebou')]);
                break;
            }
            $user_by_email = $usersTable->find('all', ['conditions' => ['Users.email' => $email]])->first();
            if (empty($user_by_email)) {
                $this->setErrors([$formField => sprintf(__('Uživatel s e-mailem %s nebyl nalezen'), $email)]);
                break;
            }
            $userEntities[$user_by_email->id] = $user_by_email;
            $links[$user_by_email->id] = false;
        }
        // check if the current links are still valid or meant to be removed
        foreach ($this->allowed_to_users ?? [] as $index => $existing_link) {
            if ($existing_link->user_id !== $this->id) {
                $links[$existing_link->allowed_user_id] = true;
            } elseif (in_array($existing_link->allowed_user_id, array_keys($links), true)) {
                $links[$existing_link->allowed_user_id] = true;
            } else {
                $toRemove[] = $existing_link;
                unset($this->allowed_to_users[$index]);
            }
        }
        // patch local allowed_to_users field to contain new allowed values
        foreach ($links as $allowed_user_id => $isAlreadySet) {
            if (!$isAlreadySet) {
                $newLink = $usersTable->AllowedUsers->newEntity([
                    'user_id' => $this->id,
                    'allowed_user_id' => $allowed_user_id,
                ]);
                $newLink->allowed_user = $userEntities[$allowed_user_id];
                $this->allowed_to_users[] = $newLink;
            }
        }
        $this->setDirty('allowed_to_users');

        return $toRemove;
    }

    public function loadRequests(): self
    {
        if (!is_array($this->requests)) {
            $usersTable = $this->getTableLocator()->get('Users');
            $usersTable->loadInto($this, ['Requests']);
        }

        return $this;
    }
}
