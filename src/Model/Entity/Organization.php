<?php

namespace App\Model\Entity;

use Cake\Core\Configure;
use Cake\I18n\FrozenTime;
use Cake\Mailer\Email;
use Cake\Mailer\TransportFactory;
use Cake\Mailer\Transport\MailTransport;

/**
 * Organization Entity
 *
 * @property int $id
 * @property string $name
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 *
 * @property Domain[] $domains
 * @property OrganizationSetting[] $organization_settings
 */
class Organization extends AppEntity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'modified' => true,
        'created' => true,
        'domains' => true,
        'organization_settings' => true,
    ];

    /**
     * @return void
     */
    public function storeConfiguration(): void
    {
        foreach ($this->organization_settings as $organization_setting) {
            $organization_setting->storeConfiguration();
        }
        $this->reconfigureEmails();
    }

    /**
     * @return void
     */
    public function reconfigureEmails(): void
    {
        foreach (Configure::read('EmailTransport') as $configName => $config) {
            if (!in_array($configName, TransportFactory::configured())) {
                if (empty($config['host'])) {
                    $config['className'] = MailTransport::class;
                }
                TransportFactory::setConfig($configName, $config);
            }
        }
        foreach (Configure::read('Email') as $configName => $config) {
            if (!in_array($configName, Email::configured())) {
                Email::setConfig($configName, $config);
            }
        }
    }

    /**
     * @param string $name setting ID, from OrganizationSetting constants
     * @param bool $return_default_value whether to return default value or null
     * @return bool|float|int|string|null
     */
    public function getSettingValue(string $name, bool $return_default_value = false)
    {
        $setting = $this->getSetting($name);

        return $setting === null ? ($return_default_value ? OrganizationSetting::getDefaultValue($name) : null) : $setting->deserialize();
    }

    /**
     * @param string $name setting ID, from OrganizationSetting constants
     * @return OrganizationSetting|null
     */
    public function getSetting(string $name): ?OrganizationSetting
    {
        foreach ($this->organization_settings as $setting) {
            if ($setting->name === $name) {
                return $setting;
            }
        }

        return null;
    }
}
