<?php

namespace App\Model\Entity;

use App\Model\Table\BudgetItemsTable;
use Cake\I18n\FrozenTime;
use Cake\ORM\Entity;

/**
 * RequestBudget Entity
 *
 * @property int $id
 * @property int $request_id
 * @property float $total_income
 * @property float $total_costs
 * @property float $total_own_sources
 * @property float $total_other_subsidy
 * @property float $requested_amount
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 *
 * @property Request $request
 * @property BudgetItem[] $incomes
 * @property BudgetItem[] $costs
 * @property BudgetItem[] $own_sources
 * @property BudgetItem[] $other_subsidies
 */
class RequestBudget extends Entity
{
    public const BUDGET_OK = 1,
        BUDGET_MISSING = 2,
        BUDGET_MISSING_BUDGET = 3,
        BUDGET_MISSING_BALANCE_SHEET = 4,
        BUDGET_MISSING_REQUESTED_AMOUNT = 5,
        BUDGET_REQUESTED_HIGHER_THAN_ALLOWED = 6;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'request_id' => true,
        'total_income' => true,
        'total_costs' => true,
        'total_own_sources' => true,
        'total_other_subsidy' => true,
        'requested_amount' => true,
        'modified' => true,
        'created' => true,
        'request' => true,
        'incomes' => true,
        'costs' => true,
        'own_sources' => true,
        'other_subsidies' => true,
    ];

    private array $_itemsToDelete = [];

    public function deleteRemovedItems(BudgetItemsTable $table)
    {
        foreach ($this->_itemsToDelete as $index => $item) {
            if (!empty($item['id'])) {
                //Log::debug(json_encode($item));
                $raw = $table->get(intval($item['id']), [
                    'conditions' => [
                        'request_budget_id' => $this->id,
                    ],
                ]);
                if (!empty($raw)) {
                    $table->delete($raw);
                }
            }
            unset($this->_itemsToDelete[$index]);
        }
    }

    /**
     * @return void
     */
    public function recountItems(): void
    {
        $this->total_costs = array_reduce(
            $this->costs,
            function ($carry, $item) {
                return $carry + floatval($item->amount);
            }
        ) ?? 0;
        $this->total_income = array_reduce(
            $this->incomes,
            function ($carry, $item) {
                return $carry + floatval($item->amount);
            }
        ) ?? 0;
        $this->total_other_subsidy = array_reduce(
            $this->other_subsidies,
            function ($carry, $item) {
                return $carry + floatval($item->amount);
            }
        ) ?? 0;
        $this->total_own_sources = array_reduce(
            $this->own_sources,
            function ($carry, $item) {
                return $carry + floatval($item->amount);
            }
        ) ?? 0;
    }

    /**
     * @param array $data POST/Request data to be cleaned
     * @return array cleaned data
     */
    public function cleanPostData(array $data): array
    {
        foreach (['incomes', 'costs', 'own_sources', 'other_subsidies'] as $group) {
            if (!isset($data[$group]) || !is_array($data[$group])) {
                continue;
            }
            foreach ($data[$group] as $index => $budget_item) {
                if (empty($budget_item['description']) || empty($budget_item['amount'])) {
                    if (!empty($budget_item['id'])) {
                        $this->_itemsToDelete[] = $budget_item;
                    }
                    unset($data[$group][$index]);
                }
            }
        }

        return $data;
    }

    /**
     * @return bool
     */
    public function hasNoBudgetItems(): bool
    {
        return empty($this->incomes) && empty($this->costs) && empty($this->own_sources) && empty($this->total_other_subsidy);
    }

    public function validateBudgetRequirements(Appeal $appeal, Program $program): int
    {
        if (empty($this->requested_amount)) {
            return self::BUDGET_MISSING_REQUESTED_AMOUNT;
        }
        if ($program->requires_budget && empty($this->own_sources) && empty($this->other_subsidies)) {
            // this shall be replaced by additional program setting, that organization requires the own-sources and other-subsidies provided
            //return self::BUDGET_MISSING_BUDGET;
        }
        if ($program->requires_balance_sheet && empty($this->costs) && empty($this->incomes)) {
            return self::BUDGET_MISSING_BALANCE_SHEET;
        }
        if (!$appeal->checkRequestLimit($this->requested_amount, $program->id)) {
            return self::BUDGET_REQUESTED_HIGHER_THAN_ALLOWED;
        }

        return self::BUDGET_OK;
    }
}
